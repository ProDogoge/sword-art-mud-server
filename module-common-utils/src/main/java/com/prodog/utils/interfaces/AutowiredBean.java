package com.prodog.utils.interfaces;

import com.prodog.utils.bean.SpringBeanUtils;
import org.springframework.beans.factory.annotation.Autowired;

import java.lang.reflect.Field;

public interface AutowiredBean {
    default void autowired() {
        Field[] fields = this.getClass().getDeclaredFields();
        for (Field field : fields) {
            Autowired autowired = field.getAnnotation(Autowired.class);
            if (autowired != null) {
                try {
                    field.setAccessible(true);
                    field.set(this, SpringBeanUtils.getBean(field.getType()));
                } catch (IllegalAccessException e) {
                    e.printStackTrace();
                }
            }
        }
    }
}
