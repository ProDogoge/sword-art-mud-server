package com.prodog.utils.interfaces;

public interface VOConvertor<T,R> {
    R convert(T obj);
}
