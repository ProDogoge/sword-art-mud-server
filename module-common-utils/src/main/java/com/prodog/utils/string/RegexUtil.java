package com.prodog.utils.string;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class RegexUtil {
    public static Matcher getMatcher(String regex, String str) {
        Pattern pattern = Pattern.compile(regex);
        Matcher matcher = pattern.matcher(str);
        matcher.find();
        return matcher;
    }
}
