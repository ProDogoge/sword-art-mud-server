package com.prodog.utils;

import cn.hutool.json.JSONArray;
import com.prodog.utils.bean.BeanUtil;

import java.util.LinkedHashMap;
import java.util.Map;

public class RandomUtil {
    public static <T> T randomByMap(Map<String, JSONArray> randomMap, Class<T> clz) {
        Map entityMap = new LinkedHashMap();
        randomMap.forEach((k, jsonArr) -> {
            Object[] arr = jsonArr.stream().toArray();
            if (arr.length == 1) {
                entityMap.put(k, arr[0]);
            } else {
                entityMap.put(k, cn.hutool.core.util.RandomUtil.randomLong(Long.parseLong(arr[0] + ""), Long.parseLong(Long.parseLong(arr[1] + "") + 1 + "")));
            }
        });
        return BeanUtil.mapToBean(entityMap, clz);
    }

    public static boolean percentTest(int percent) {
        return cn.hutool.core.util.RandomUtil.randomInt(1, 101) <= percent;
    }

    public static boolean percentTest(double percent) {
        return cn.hutool.core.util.RandomUtil.randomDouble(1, 101) <= percent;
    }
}
