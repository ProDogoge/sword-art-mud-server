package com.prodog.utils.bean;

import org.aopalliance.intercept.MethodInterceptor;
import org.springframework.aop.framework.ProxyFactory;
import org.springframework.aop.support.DefaultPointcutAdvisor;
import org.springframework.beans.BeansException;
import org.springframework.beans.factory.BeanFactory;
import org.springframework.beans.factory.NoSuchBeanDefinitionException;
import org.springframework.beans.factory.ObjectProvider;
import org.springframework.beans.factory.config.AutowireCapableBeanFactory;
import org.springframework.beans.factory.config.BeanDefinition;
import org.springframework.beans.factory.support.BeanDefinitionBuilder;
import org.springframework.beans.factory.support.BeanDefinitionRegistry;
import org.springframework.beans.factory.support.DefaultListableBeanFactory;
import org.springframework.context.*;
import org.springframework.core.ResolvableType;
import org.springframework.core.env.Environment;
import org.springframework.core.io.Resource;
import org.springframework.stereotype.Component;

import java.io.IOException;
import java.lang.annotation.Annotation;
import java.util.ArrayList;
import java.util.List;
import java.util.Locale;
import java.util.Map;

@Component
public class SpringBeanUtils implements ApplicationContextAware {
    private static ApplicationContext context;

    @Override
    public void setApplicationContext(ApplicationContext applicationContext) throws BeansException {
        context = applicationContext;
    }

    public static Object getBean(String s) throws BeansException {
        return context.getBean(s);
    }

    public static <T> T getBean(String s, Class<T> aClass) throws BeansException {
        return context.getBean(s, aClass);
    }

    public static Object getBean(String s, Object... objects) throws BeansException {
        return context.getBean(s, objects);
    }

    public static <T> T getBean(Class<T> clazz) {
        return context.getBean(clazz);
    }

    public static <T> ObjectProvider<T> getBeanProvider(Class<T> aClass) {
        return context.getBeanProvider(aClass);
    }

    public static <T> ObjectProvider<T> getBeanProvider(ResolvableType resolvableType) {
        return getBeanProvider(resolvableType);
    }

    public static boolean containsBean(String s) {
        return containsBean(s);
    }

    public static boolean isSingleton(String s) throws NoSuchBeanDefinitionException {
        return context.isSingleton(s);
    }

    public static boolean isPrototype(String s) throws NoSuchBeanDefinitionException {
        return context.isPrototype(s);
    }

    public static boolean isTypeMatch(String s, ResolvableType resolvableType) throws NoSuchBeanDefinitionException {
        return context.isTypeMatch(s, resolvableType);
    }

    public static boolean isTypeMatch(String s, Class<?> aClass) throws NoSuchBeanDefinitionException {
        return context.isTypeMatch(s, aClass);
    }

    public static Class<?> getType(String s) throws NoSuchBeanDefinitionException {
        return context.getType(s);
    }

    public static Class<?> getType(String s, boolean b) throws NoSuchBeanDefinitionException {
        return context.getType(s, b);
    }

    public static String[] getAliases(String s) {
        return context.getAliases(s);
    }

    public static List getBeansByAnnotation(Class<? extends Annotation> annoClaz) {
        return new ArrayList(context.getBeansWithAnnotation(annoClaz).values());
    }

    public static String getId() {
        return context.getId();
    }

    public static String getApplicationName() {
        return context.getApplicationName();
    }

    public static String getDisplayName() {
        return context.getDisplayName();
    }

    public static long getStartupDate() {
        return context.getStartupDate();
    }

    public static ApplicationContext getParent() {
        return context.getParent();
    }

    public static AutowireCapableBeanFactory getAutowireCapableBeanFactory() throws IllegalStateException {
        return context.getAutowireCapableBeanFactory();
    }

    public static BeanFactory getParentBeanFactory() {
        return context.getParentBeanFactory();
    }

    public static boolean containsLocalBean(String s) {
        return context.containsLocalBean(s);
    }

    public static boolean containsBeanDefinition(String s) {
        return context.containsBeanDefinition(s);
    }

    public static int getBeanDefinitionCount() {
        return context.getBeanDefinitionCount();
    }

    public static String[] getBeanDefinitionNames() {
        return context.getBeanDefinitionNames();
    }

    public static <T> ObjectProvider<T> getBeanProvider(Class<T> aClass, boolean b) {
        return context.getBeanProvider(aClass, b);
    }

    public static <T> ObjectProvider<T> getBeanProvider(ResolvableType resolvableType, boolean b) {
        return context.getBeanProvider(resolvableType, b);
    }

    public static String[] getBeanNamesForType(ResolvableType resolvableType) {
        return context.getBeanNamesForType(resolvableType);
    }

    public static String[] getBeanNamesForType(ResolvableType resolvableType, boolean b, boolean b1) {
        return context.getBeanNamesForType(resolvableType, b, b1);
    }

    public static String[] getBeanNamesForType(Class<?> aClass) {
        return context.getBeanNamesForType(aClass);
    }

    public static String[] getBeanNamesForType(Class<?> aClass, boolean b, boolean b1) {
        return context.getBeanNamesForType(aClass, b, b1);
    }

    public static <T> Map<String, T> getBeansOfType(Class<T> aClass) throws BeansException {
        return context.getBeansOfType(aClass);
    }

    public static <T> Map<String, T> getBeansOfType(Class<T> aClass, boolean b, boolean b1) throws BeansException {
        return context.getBeansOfType(aClass, b, b1);
    }

    public static String[] getBeanNamesForAnnotation(Class<? extends Annotation> aClass) {
        return context.getBeanNamesForAnnotation(aClass);
    }

    public static Map<String, Object> getBeansWithAnnotation(Class<? extends Annotation> aClass) throws BeansException {
        return context.getBeansWithAnnotation(aClass);
    }

    public static <A extends Annotation> A findAnnotationOnBean(String s, Class<A> aClass) throws NoSuchBeanDefinitionException {
        return context.findAnnotationOnBean(s, aClass);
    }

    public static <A extends Annotation> A findAnnotationOnBean(String s, Class<A> aClass, boolean b) throws NoSuchBeanDefinitionException {
        return context.findAnnotationOnBean(s, aClass, b);
    }

    public static void publishEvent(Object event) {
        context.publishEvent(event);
    }

    public static String getMessage(String code, Object[] args, String defaultMessage, Locale locale) {
        return context.getMessage(code, args, defaultMessage, locale);
    }

    public static String getMessage(String code, Object[] args, Locale locale) throws NoSuchMessageException {
        return context.getMessage(code, args, locale);
    }

    public static String getMessage(MessageSourceResolvable resolvable, Locale locale) throws NoSuchMessageException {
        return context.getMessage(resolvable, locale);
    }

    public static Environment getEnvironment() {
        return context.getEnvironment();
    }

    public static Resource[] getResources(String locationPattern) throws IOException {
        return context.getResources(locationPattern);
    }

    public static Resource getResource(String location) {
        return context.getResource(location);
    }

    public static ClassLoader getClassLoader() {
        return context.getClassLoader();
    }

    public static void addBean(String beanName, Class<?> beanClass) {
        BeanDefinitionRegistry beanDefinitionRegistry = (BeanDefinitionRegistry) context.getAutowireCapableBeanFactory();
        BeanDefinitionBuilder beanDefinitionBuilder = BeanDefinitionBuilder.genericBeanDefinition(beanClass);
        BeanDefinition beanDefinition = beanDefinitionBuilder.getBeanDefinition();
        if (!beanDefinitionRegistry.containsBeanDefinition(beanName)) {
            beanDefinitionRegistry.registerBeanDefinition(beanName, beanDefinition);
        }
    }

    public static void removeBean(String beanName) {
        BeanDefinitionRegistry beanDefinitionRegistry = (BeanDefinitionRegistry) context.getAutowireCapableBeanFactory();
        if (beanDefinitionRegistry.containsBeanDefinition(beanName)) {
            beanDefinitionRegistry.removeBeanDefinition(beanName);
        }
    }

    public static void removeBean(Class clz) {
        String beanName = Character.toLowerCase(clz.getName().charAt(0)) + clz.getName().substring(1);
        removeBean(beanName);
    }

    public static Object toAopBean(Object obj, MethodInterceptor proxy) {
        DefaultPointcutAdvisor advisor = new DefaultPointcutAdvisor();
        advisor.setAdvice(proxy);

        ProxyFactory factory = new ProxyFactory();
        factory.setTarget(obj);
        factory.addAdvisor(advisor);
        return factory.getProxy();
    }

    public static Object addBean(String beanName, Object singletonObject) {
        //将applicationContext转换为ConfigurableApplicationContext
        ConfigurableApplicationContext configurableApplicationContext = (ConfigurableApplicationContext) context;
        //获取BeanFactory
        DefaultListableBeanFactory defaultListableBeanFactory = (DefaultListableBeanFactory) configurableApplicationContext.getAutowireCapableBeanFactory();
        //动态注册bean.
        defaultListableBeanFactory.registerSingleton(beanName, singletonObject);
        //获取动态注册的bean.
        return configurableApplicationContext.getBean(beanName);
    }


}
