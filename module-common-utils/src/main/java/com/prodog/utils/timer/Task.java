package com.prodog.utils.timer;

@FunctionalInterface
public interface Task {
    void execute();
}
