package com.prodog.utils.timer;

import java.util.*;

public class TimerUtil {
    private static final Map<String, Timer> timerMap = new HashMap<>();

    public static void schedule(String id, Task task, long delay) {
        Timer timer = new Timer();
        timer.schedule(new TimerTask() {
            @Override
            public void run() {
                try {
                    task.execute();
                } catch (Exception e) {
                    e.printStackTrace();
                } finally {
                    timerMap.remove(id);
                }
            }
        }, delay);
        timerMap.put(id, timer);
    }

    public static void schedule(String id, Task task, Date date) {
        Timer timer = new Timer();
        timer.schedule(new TimerTask() {
            @Override
            public void run() {
                try {
                    task.execute();
                } catch (Exception e) {
                    e.printStackTrace();
                } finally {
                    timerMap.remove(id);
                }
            }
        }, date);
        timerMap.put(id, timer);
    }

    public static void schedule(String id, Task task, long delay, long period) {
        Timer timer = new Timer();
        timer.schedule(new TimerTask() {
            @Override
            public void run() {
                try {
                    task.execute();
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        }, delay, period);
        timerMap.put(id, timer);
    }

    public static void schedule(String id, Task task, Date firstTime, long period) {
        Timer timer = new Timer();
        timer.schedule(new TimerTask() {
            @Override
            public void run() {
                try {
                    task.execute();
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        }, firstTime, period);
        timerMap.put(id, timer);
    }

    public static void scheduleAtFixedRate(String id, Task task, Date firstTime, long period) {
        Timer timer = new Timer();
        timer.scheduleAtFixedRate(new TimerTask() {
            @Override
            public void run() {
                try {
                    task.execute();
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        }, firstTime, period);
        timerMap.put(id, timer);
    }

    public static void scheduleAtFixedRate(String id, Task task, long delay, long period) {
        Timer timer = new Timer();
        timer.scheduleAtFixedRate(new TimerTask() {
            @Override
            public void run() {
                try {
                    task.execute();
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        }, delay, period);
        timerMap.put(id, timer);
    }

    public static void cancel(String id) {
        timerMap.get(id).cancel();
        timerMap.remove(id);
    }
}
