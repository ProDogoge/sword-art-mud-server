package com.prodog.utils.exception;

import java.util.function.Function;
import java.util.function.Supplier;

public class CommonException extends Exception {
    public CommonException() {
        super();
    }

    public CommonException(String message) {
        super(message);
    }

    public static void bySupplier(Supplier<Boolean> p, String message) throws CommonException {
        if (p.get()) throw new CommonException(message);
    }

    public static <T> T byFunc(T t, Function<T, Boolean> c, String message) throws CommonException {
        if (c.apply(t)) throw new CommonException(message);
        return t;
    }

    public static void byNull(Object obj, String message) throws CommonException {
        if (obj == null) throw new CommonException(message);
    }
}
