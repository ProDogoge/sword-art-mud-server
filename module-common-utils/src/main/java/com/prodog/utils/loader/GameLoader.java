package com.prodog.utils.loader;

public interface GameLoader {
    void load();
}
