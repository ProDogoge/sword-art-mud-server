package com.prodog.utils.quartz;

import org.quartz.Job;
import org.quartz.JobExecutionContext;
import org.quartz.JobExecutionException;

public class QzJob implements Job {
    @Override
    public void execute(JobExecutionContext context) throws JobExecutionException {
        QzJobDetail qzJobDetail = (QzJobDetail) context.getJobDetail().getJobDataMap().get("qzJobDetail");
        qzJobDetail.execute(context);
    }
}
