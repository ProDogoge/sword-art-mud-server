package com.prodog.utils.quartz;

import org.quartz.JobExecutionContext;

@FunctionalInterface
public interface QzJobDetail {
    void execute(JobExecutionContext context);
}
