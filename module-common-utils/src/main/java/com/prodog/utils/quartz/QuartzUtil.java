package com.prodog.utils.quartz;

import org.quartz.*;
import org.quartz.impl.StdSchedulerFactory;
import org.quartz.impl.matchers.GroupMatcher;

import java.util.*;

import static org.quartz.CronScheduleBuilder.cronSchedule;
import static org.quartz.JobBuilder.newJob;
import static org.quartz.SimpleScheduleBuilder.simpleSchedule;
import static org.quartz.TriggerBuilder.newTrigger;

public class QuartzUtil {
    private static final SchedulerFactory schedulerFactory = new StdSchedulerFactory();
    private static final String DEFAULT_GROUP = "DEFAULT_GROUP";

    public static void schedule(String key, String group, QzJobDetail qzJobDetail, long delay) {
        try {
            Scheduler sched = schedulerFactory.getScheduler();
            sched.start();
            long fTime = System.currentTimeMillis() + delay;
            JobDataMap map = new JobDataMap();
            map.put("qzJobDetail", qzJobDetail);
            JobDetail job = newJob(QzJob.class).withIdentity(key, group).setJobData(map).build();
            Trigger trigger = newTrigger().withIdentity(key, group).startAt(new Date(fTime))
                    .withSchedule(simpleSchedule().withRepeatCount(0)).build();
            sched.scheduleJob(job, trigger);
        } catch (SchedulerException e) {
            e.printStackTrace();
        }
    }

    public static void schedule(String key, QzJobDetail qzJobDetail, long delay) {
        schedule(key, DEFAULT_GROUP, qzJobDetail, delay);
    }

    public static void schedule(String key, String group, QzJobDetail qzJobDetail, Date startTime) {
        try {
            Scheduler sched = schedulerFactory.getScheduler();
            sched.start();
            JobDataMap map = new JobDataMap();
            map.put("qzJobDetail", qzJobDetail);
            JobDetail job = newJob(QzJob.class).withIdentity(key, group).setJobData(map).build();
            Trigger trigger = newTrigger().withIdentity(key, group).startAt(startTime)
                    .withSchedule(simpleSchedule().withRepeatCount(0)).build();
            sched.scheduleJob(job, trigger);

        } catch (SchedulerException e) {
            e.printStackTrace();
        }
    }

    public static void schedule(String key, QzJobDetail qzJobDetail, Date startTime) {
        schedule(key, DEFAULT_GROUP, qzJobDetail, startTime);
    }

    public static void schedule(String key, String group, QzJobDetail qzJobDetail, long delay, int interval, int count) {
        try {
            Scheduler sched = schedulerFactory.getScheduler();
            sched.start();
            long fTime = System.currentTimeMillis() + delay;
            JobDataMap map = new JobDataMap();
            map.put("qzJobDetail", qzJobDetail);
            JobDetail job = newJob(QzJob.class).setJobData(map).withIdentity(key, group).build();
            Trigger trigger = newTrigger().withIdentity(key, group).startAt(new Date(fTime))
                    .withSchedule(simpleSchedule().withIntervalInSeconds(interval).withRepeatCount(count)).build();
            sched.scheduleJob(job, trigger);

        } catch (SchedulerException e) {
            e.printStackTrace();
        }
    }

    public static void schedule(String key, QzJobDetail qzJobDetail, long delay, int interval, int count) {
        schedule(key, DEFAULT_GROUP, qzJobDetail, delay, interval, count);
    }

    public static void schedule(String key, String group, QzJobDetail qzJobDetail, long delay, int interval) {
        try {
            Scheduler sched = schedulerFactory.getScheduler();
            sched.start();
            long fTime = System.currentTimeMillis() + delay;
            JobDataMap map = new JobDataMap();
            map.put("qzJobDetail", qzJobDetail);
            JobDetail job = newJob(QzJob.class).setJobData(map).withIdentity(key, group).build();
            Trigger trigger = newTrigger().withIdentity(key, group).startAt(new Date(fTime))
                    .withSchedule(simpleSchedule().withIntervalInSeconds(interval).repeatForever()).build();
            sched.scheduleJob(job, trigger);

        } catch (SchedulerException e) {
            e.printStackTrace();
        }
    }

    public static void schedule(String key, QzJobDetail qzJobDetail, long delay, int interval) {
        schedule(key, DEFAULT_GROUP, qzJobDetail, delay, interval);
    }

    public static void schedule(String key, String group, QzJobDetail qzJobDetail, String cron) {
        try {
            Scheduler sched = schedulerFactory.getScheduler();
            sched.start();
            JobDataMap map = new JobDataMap();
            map.put("qzJobDetail", qzJobDetail);
            JobDetail job = newJob(QzJob.class).setJobData(map).withIdentity(key, group).build();
            Trigger trigger = newTrigger().withIdentity(key, group).startNow().withSchedule(cronSchedule(cron)).build();
            sched.scheduleJob(job, trigger);

        } catch (SchedulerException e) {
            e.printStackTrace();
        }
    }

    public static void schedule(String key, QzJobDetail qzJobDetail, String cron) {
        schedule(key, DEFAULT_GROUP, qzJobDetail, cron);
    }

    public static void cancel(String key, String group) {
        JobKey jk = new JobKey(key, group);
        Collection<Scheduler> collection;
        try {
            collection = schedulerFactory.getAllSchedulers();
            Iterator<Scheduler> iter = collection.iterator();
            while (iter.hasNext()) {
                Scheduler sched = iter.next();
                for (String groupName : sched.getJobGroupNames()) {
                    for (JobKey jobKey : sched.getJobKeys(GroupMatcher.jobGroupEquals(groupName))) {
                        if (jobKey.equals(jk)) {
                            sched.deleteJob(jk);
                        }
                    }
                }
            }
        } catch (SchedulerException e) {
            e.printStackTrace();
        }
    }

    public static void cancel(String key) {
        cancel(key, DEFAULT_GROUP);
    }

    public static void printJob() {
        Collection<Scheduler> collection;
        try {
            collection = schedulerFactory.getAllSchedulers();
            System.out.println("[Print] Current Scheduler Size : " + collection.size());
            Iterator<Scheduler> iter = collection.iterator();
            while (iter.hasNext()) {
                Scheduler sched = iter.next();
                List<String> groupList = sched.getJobGroupNames();
                System.out.println("[Print] Current Group Size : " + groupList.size());
                for (String groupName : groupList) {
                    Set<JobKey> jobKeySet = sched.getJobKeys(GroupMatcher.jobGroupEquals(groupName));
                    System.out.println("[Print] Current JOB Size : " + jobKeySet.size());
                    for (JobKey jobKey : jobKeySet) {
                        System.out.println("[Print] Current JOB : " + jobKey);
                        // System.out.println(sched.getTriggersOfJob(jobKey));
                    }
                }
            }
        } catch (SchedulerException e) {
            e.printStackTrace();
        }
    }

}
