package com.prodog.utils.data;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

@Component
public class DatabaseContext {
    @Value("game.rootPath")
    private String gameRootPath;

}
