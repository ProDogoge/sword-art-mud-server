package com.prodog.utils;

public class TimeUtil {
    public static String formatMs(long millsecond) {
        StringBuilder res = new StringBuilder();
        long ms = millsecond;
        long s = 0;
        long m = 0;
        long h = 0;
        long d = 0;
        if (ms >= 1000) {
            s = millsecond / 1000;
            ms = ms % 1000;
        }
        if (s >= 60) {
            m = s / 60;
            s = s % 60;
        }
        if (m >= 60) {
            h = m / 60;
            m = m % 60;
        }
        if (h >= 24) {
            d = h / 24;
            h = h % 60;
        }
        if (d != 0) res.append(d).append("天");
        if (h != 0) res.append(h).append("小时");
        if (m != 0) res.append(m).append("分钟");
        if (s != 0) res.append(s).append("秒");
        if (ms != 0) res.append(ms).append("毫秒");
        return res.toString();
    }
}
