package com.prodog.message.sender;

import java.util.Collection;

public interface MessageSender {
    void sendToGroup(String groupId, String msg);

    void sendToSelf(String selfId, String msg);

    void sendToGroups(String msg, Collection<String> groupIds);

    void sendToSelfs(String msg, Collection<String> selfIds);
}
