package com.prodog.usermodule.role.exp;

import com.prodog.gamemodule.gameworld.monster.entity.Monster;
import com.prodog.usermodule.caculate.CalVal;
import com.prodog.usermodule.role.entity.RoleInfo;

import java.util.ArrayList;
import java.util.List;

public class ExpGetter {
    private final List<ExpHandler> handlers = new ArrayList<>();

    public ExpGetter() {
        handlers.add(new AttenHandler());
    }

    public long get(RoleInfo roleInfo, Monster monster, long exp) {
        CalVal<Long> calVal = new CalVal<>(exp, exp);
        for (ExpHandler handler : handlers) {
            handler.handle(roleInfo, monster, calVal);
        }
        return calVal.getResult();
    }
}
