package com.prodog.usermodule.role.entity;

import lombok.Data;
import org.springframework.data.mongodb.core.mapping.Document;

/***
 * 角色穿戴装备
 */
@Data
@Document("g_role_equip")
public class RoleEquip {
    private String id;  //账户ID
    private String title; //称号
    private String weapon;  //武器
    private String secondWeapon;    //副手
    private String shoulder;    //肩部
    private String clothes;   //上衣
    private String pants;   //裤子
    private String belt;    //腰带
    private String shoes;   //鞋子
    private String ring;    //戒指
    private String bracelet;    //手镯
    private String neck;    //项链
    private String aux; //辅助装备
}
