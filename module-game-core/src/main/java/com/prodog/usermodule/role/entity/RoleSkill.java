package com.prodog.usermodule.role.entity;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.data.mongodb.core.mapping.Document;

import java.util.Map;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Document("g_role_skill")
public class RoleSkill {
    private String id;
    private Map<String, Integer> learnedSkill;
}
