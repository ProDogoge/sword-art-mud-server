package com.prodog.usermodule.role.util;

import cn.hutool.script.ScriptUtil;
import com.prodog.gamemodule.config.ExpItemConfig;
import com.prodog.gamemodule.config.UpdateExpConfig;
import com.prodog.util.MsgTemplateUtil;
import com.prodog.utils.bean.SpringBeanUtils;

import java.util.HashMap;
import java.util.Map;

public class ExpUtil {
    public static long getLevelExp(int level) {
        UpdateExpConfig config = SpringBeanUtils.getBean(UpdateExpConfig.class);
        ExpItemConfig itemConfig = config.getItemConfig(level);
        Map<String, Object> params = new HashMap<>();
        params.putAll(itemConfig.getParams());
        params.put("level", level);
        String expression = MsgTemplateUtil.formatStr(itemConfig.getExpression(), params);
        double doubleVal = Double.parseDouble(String.valueOf(ScriptUtil.eval(expression)));
        return (long) doubleVal;
    }

    public static long getRangeLevelExp(int start, int end) {
        long exp = 0;
        for (int i = start + 1; i <= end; i++) {
            exp += getLevelExp(i);
        }
        return exp;
    }
}
