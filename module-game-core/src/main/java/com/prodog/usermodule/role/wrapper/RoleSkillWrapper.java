package com.prodog.usermodule.role.wrapper;

import com.prodog.database.annotation.Wrapper;
import com.prodog.database.wrapper.MongoDataWrapper;
import com.prodog.usermodule.role.entity.RoleSkill;

@Wrapper(path = "玩家数据/角色/角色技能", module = "角色技能")
public class RoleSkillWrapper extends MongoDataWrapper<RoleSkill, String> {
}
