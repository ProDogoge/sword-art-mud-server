package com.prodog.usermodule.role.service;

import com.prodog.command.entity.CommandResult;
import com.prodog.command.entity.RoleGameCommand;

public interface RoleBagService {
    CommandResult bagItems(RoleGameCommand command, int type, String label, String key, Integer pno);
}
