package com.prodog.usermodule.role.handler;

import com.prodog.command.annonations.Command;
import com.prodog.command.annonations.CommandBean;
import com.prodog.command.annonations.Iptor;
import com.prodog.command.annonations.Iptors;
import com.prodog.command.entity.CommandResult;
import com.prodog.command.entity.RoleGameCommand;
import com.prodog.gamemodule.menu.MenuHandler;
import com.prodog.usermodule.role.interceptor.StateInterceptor;
import com.prodog.usermodule.role.service.EquipService;
import com.prodog.usermodule.user.interceptor.NotRegistInterceptor;
import com.prodog.utils.exception.CommonException;
import lombok.RequiredArgsConstructor;

@CommandBean(setName = "装备操作")
@RequiredArgsConstructor
@Iptors(vals = @Iptor(value = NotRegistInterceptor.class))
public class EquipHandler {
    private final EquipService equipService;

    @Command(value = "(装备|查看装备|ckzb|zb)")
    @Iptors(vals = @Iptor(value = StateInterceptor.class, params = "{'states':[3],'required':false}", msg = "[{{state}}]状态下无法进行此操作。"))
    public CommandResult equipInfos(RoleGameCommand command) {
        try {
            return equipService.equipInfos(command);
        } catch (Exception e) {
            e.printStackTrace();
            return CommandResult.failure("服务器错误，装备信息获取失败。");
        }
    }

    @Command(value = "(查询装备|cxzb|czb)\\s*(.+)", props = "null,equipName")
    @Iptors(vals = @Iptor(value = StateInterceptor.class, params = "{'states':[2,3],'required':false}", msg = "[{{state}}]状态下无法进行此操作。"))
    public CommandResult searchEquip(RoleGameCommand command, String equipName) {
        try {
            return equipService.searchEquip(command, equipName);
        } catch (CommonException e) {
            return CommandResult.failure(e.getMessage());
        } catch (Exception e) {
            e.printStackTrace();
            return CommandResult.failure("服务器错误，装备查询失败。");
        }
    }

    @Command(value = "(查询实例|cxsl|csl)\\s*(.+)", props = "null,activeEquName")
    @Iptors(vals = @Iptor(value = StateInterceptor.class, params = "{'states':[2,3],'required':false}", msg = "[{{state}}]状态下无法进行此操作。"))
    public CommandResult searchActiveEquip(RoleGameCommand command, String activeEquName) {
        try {
            return equipService.searchActiveEquip(command, activeEquName);
        } catch (CommonException e) {
            return CommandResult.failure(e.getMessage());
        } catch (Exception e) {
            e.printStackTrace();
            return CommandResult.failure("服务器错误，实例查询失败。");
        }
    }

    @Command(value = "激活(\\s*.+)+", props = "equipNames")
    @Iptors(vals = @Iptor(value = StateInterceptor.class, params = "{'states':[1]}", msg = "[{{state}}]状态下无法进行此操作。"))
    public CommandResult activeEquip(RoleGameCommand command, String equipNames) {
        try {
            return equipService.activeEquip(command, equipNames);
        } catch (CommonException e) {
            return CommandResult.failure(e.getMessage());
        } catch (Exception e) {
            e.printStackTrace();
            return CommandResult.failure("服务器错误，装备激活失败。");
        }
    }

    @Command(value = "装备(\\s*.+)+", props = "equipNames")
    @Iptors(vals = @Iptor(value = StateInterceptor.class, params = "{'states':[1]}", msg = "{{state}}状态下无法进行此操作"))
    public CommandResult wearEquip(RoleGameCommand command, String equipNames) {
        try {
            if (equipNames.equals("系统") || equipNames.equals("功能")) return new MenuHandler().equipFuncs();
            return equipService.wearEquip(command, equipNames);
        } catch (CommonException e) {
            return CommandResult.failure(e.getMessage());
        } catch (Exception e) {
            e.printStackTrace();
            return CommandResult.failure("服务器错误，装备失败。");
        }
    }

    @Command(value = "卸下(\\s*.+)+", props = "parts")
    @Iptors(vals = @Iptor(value = StateInterceptor.class, params = "{'states':[1]}", msg = "{{state}}状态下无法进行此操作"))
    public CommandResult removeEquip(RoleGameCommand command, String parts) {
        try {
            return equipService.removeEquip(command, parts);
        } catch (CommonException e) {
            return CommandResult.failure(e.getMessage());
        } catch (Exception e) {
            e.printStackTrace();
            return CommandResult.failure("服务器错误，卸下失败。");
        }
    }
}
