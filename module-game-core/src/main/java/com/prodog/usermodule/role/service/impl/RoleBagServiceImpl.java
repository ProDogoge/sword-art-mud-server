package com.prodog.usermodule.role.service.impl;

import cn.hutool.core.util.StrUtil;
import com.prodog.command.annonations.Service;
import com.prodog.command.entity.CommandResult;
import com.prodog.command.entity.RoleGameCommand;
import com.prodog.database.entity.Page;
import com.prodog.gamemodule.config.dict.MapDictConfig;
import com.prodog.gamemodule.item.entity.GameItem;
import com.prodog.usermodule.role.entity.RoleBag;
import com.prodog.usermodule.role.entity.RoleBagItem;
import com.prodog.usermodule.role.service.RoleBagService;
import com.prodog.usermodule.role.util.GameItemUtil;
import com.prodog.usermodule.role.util.RoleInfoUtil;
import com.prodog.usermodule.role.vo.RoleBagItemVo;
import com.prodog.usermodule.role.wrapper.RoleBagWrapper;
import com.prodog.util.MsgTemplateUtil;
import lombok.RequiredArgsConstructor;

import java.util.HashMap;
import java.util.List;
import java.util.stream.Collectors;

@Service
@RequiredArgsConstructor
public class RoleBagServiceImpl implements RoleBagService {
    private final RoleBagWrapper bagWrapper;
    private final MapDictConfig mapDictConfig;

    @SuppressWarnings(value = "all")
    public Page<RoleBagItemVo> typeItems(String roleId, Integer type, String key, int pno) {
        RoleBag bag = bagWrapper.getById(roleId);
        List<RoleBagItem> bagItems = bag.getItems();

        List<RoleBagItemVo> vos = (List<RoleBagItemVo>) bagItems.stream().map(i -> {
            RoleBagItem bItem = (RoleBagItem) i;
            GameItem item = GameItemUtil.getById(bItem.getItemId());
            RoleBagItemVo vo = new RoleBagItemVo();
            if (item == null) return null;
            vo.setItemName(item.getName());
            vo.setType(item.getType());
            vo.setTypeName(mapDictConfig.get("gItem", String.valueOf(item.getType())));
            vo.setRarityName(mapDictConfig.get("rarity", StrUtil.toString(item.getRarity())));
            vo.setNums(bItem.getNums());
            return vo;
        }).filter(v -> v != null).collect(Collectors.toList());

        //过滤类型
        if (type != -1) vos = vos.stream().filter(i -> i.getType() == type).collect(Collectors.toList());

        //背包搜索
        if (StrUtil.isNotBlank(key))
            vos = vos.stream().filter(vo -> StrUtil.containsIgnoreCase(vo.getItemName(), key)).collect(Collectors.toList());
        Page page = Page.page(vos, RoleBagItemVo.class, pno, 15);
        return page;
    }

    @Override
    public CommandResult bagItems(RoleGameCommand command, int type, String label, String key, Integer pno) {
        Page<RoleBagItemVo> page = typeItems(command.getRoleId(), type, key, pno);
        return CommandResult.success(MsgTemplateUtil.formatMsg("/游戏配置/消息模板/背包/背包查看.txt", new HashMap<String, Object>() {{
            put("roleInfo", command.getRoleInfo());
            put("prefix", RoleInfoUtil.getPrefix(command.getRoleInfo()));
            put("page", page);
            put("label", label);
            put("content", label.equals("none") ? "" : label + "");
            if (StrUtil.isNotBlank(key)) put("content", (label.equals("none") ? "" : label) + "[" + key + "]搜索");
        }}));
    }

}
