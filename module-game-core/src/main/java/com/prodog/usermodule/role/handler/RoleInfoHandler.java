package com.prodog.usermodule.role.handler;

import com.prodog.command.annonations.*;
import com.prodog.command.entity.CommandResult;
import com.prodog.command.entity.GameCommand;
import com.prodog.command.entity.RoleGameCommand;
import com.prodog.usermodule.role.interceptor.StateInterceptor;
import com.prodog.usermodule.role.service.RoleInfoService;
import com.prodog.usermodule.user.dtos.CreateRoleDTO;
import com.prodog.usermodule.user.interceptor.NotRegistInterceptor;
import com.prodog.utils.exception.CommonException;
import lombok.NoArgsConstructor;
import org.springframework.beans.factory.annotation.Autowired;

@CommandBean(setName = "角色操作")
@NoArgsConstructor
public class RoleInfoHandler {
    @Autowired
    private RoleInfoService roleInfoService;

    @Command(value = {
            "(创建角色|角色创建|新建角色|角色新建|创建|角创|cjjs)\\s+(.+?)\\s+([男女])",
            "(创建角色|角色创建|新建角色|角色新建|创建|角创|cjjs)\\s+(.+?)",
            "(创建角色|角色创建|新建角色|角色新建|创建|角创|cjjs)"
    }, props = {"null,roleName,sex", "null,roleName", "null"})
    public CommandResult createRole(GameCommand command, CreateRoleDTO dto) {
        try {
            return roleInfoService.createRole(command, dto);
        } catch (CommonException e) {
            return CommandResult.failure(e.getMessage());
        } catch (Exception e) {
            e.printStackTrace();
            return CommandResult.failure("服务器错误，角色创建失败");
        }
    }

    @Command(value = "(切换角色|角色切换|切角)\\s*(.+)", props = "null,roleName")
    @Iptors(vals = {@Iptor(value = NotRegistInterceptor.class)})
    public CommandResult changeRole(GameCommand command, String roleName) {
        try {
            return roleInfoService.changeRole(command, roleName);
        } catch (Exception e) {
            e.printStackTrace();
            return CommandResult.failure("服务器错误，角色切换失败");
        }
    }

    @Command(value = "(角色列表|列表角色|所有角色|角色们|jslb|lbjs)")
    @Iptors(vals = {@Iptor(value = NotRegistInterceptor.class)})
    public CommandResult roleList(GameCommand command) {
        try {
            return roleInfoService.roleList(command);
        } catch (Exception e) {
            e.printStackTrace();
            return CommandResult.failure("服务器错误，角色列表获取失败");
        }
    }

    @Command(value = "角色信息")
    @Iptors(vals = {
            @Iptor(value = NotRegistInterceptor.class),
            @Iptor(value = StateInterceptor.class, params = "{'states':[3],'required':false}", msg = "[{{state}}]状态下无法进行此操作。")
    })
    public CommandResult roleInfo(GameCommand command) {
        try {
            return roleInfoService.roleInfo(command);
        } catch (Exception e) {
            e.printStackTrace();
            return CommandResult.failure("服务器错误，角色信息获取失败");
        }
    }

    @Command(value = "(删除角色|角色删除|删角色|删角|scjs)\\s+(.+)", props = "null,roleName")
    @Iptors(vals = {
            @Iptor(value = NotRegistInterceptor.class),
            @Iptor(value = StateInterceptor.class, params = "{'states':[2,3],'required':false}", msg = "[{{state}}]状态下无法进行此操作。")
    })
    public CommandResult deleteRole(GameCommand command, String roleName) {
        try {
            return roleInfoService.deleteRole(command, roleName);
        } catch (Exception e) {
            e.printStackTrace();
            return CommandResult.failure("服务器错误，角色删除失败");
        }
    }

    @Command(value = "确认删除")
    @Iptors(vals = {
            @Iptor(value = NotRegistInterceptor.class),
            @Iptor(value = StateInterceptor.class, params = "{'states':[2,3],'required':false}", msg = "[{{state}}]状态下无法进行此操作。")
    })
    public CommandResult confirmDelete(GameCommand command) {
        try {
            return roleInfoService.confirmDelete(command);
        } catch (Exception e) {
            e.printStackTrace();
            return CommandResult.failure("服务器错误，角色删除失败");
        }
    }

    @Command(value = "取消删除")
    @Iptors(vals = {
            @Iptor(value = NotRegistInterceptor.class),
            @Iptor(value = StateInterceptor.class, params = "{'states':[2,3],'required':false}", msg = "[{{state}}]状态下无法进行此操作。")
    })
    public CommandResult cancelDelete(GameCommand command) {
        try {
            return roleInfoService.cancelDelete(command);
        } catch (Exception e) {
            e.printStackTrace();
            return CommandResult.failure("服务器错误，角色删除失败");
        }
    }

    @Command(value = {"选择职业\\s*(.+)"}, props = {"professionName"})
    @Iptors(vals = {
            @Iptor(value = NotRegistInterceptor.class),
            @Iptor(value = StateInterceptor.class, params = "{'states':[2,3],'required':false}", msg = "[{{state}}]状态下无法进行此操作。")
    })
    public CommandResult selectProfession(RoleGameCommand cmd, String professionName) {
        try {
            return roleInfoService.selectProfession(cmd, professionName);
        } catch (Exception e) {
            e.printStackTrace();
            return CommandResult.failure("服务器错误，选择职业失败");
        }
    }

    @Command(value = "(查看属性|角色属性|属性|jssx)")
    @Iptors(vals = {
            @Iptor(value = NotRegistInterceptor.class),
            @Iptor(value = StateInterceptor.class, params = "{'states':[3],'required':false}", msg = "[{{state}}]状态下无法进行此操作。")
    })
    public CommandResult roleProp(RoleGameCommand cmd) {
        try {
            return roleInfoService.roleProp(cmd);
        } catch (Exception e) {
            e.printStackTrace();
            return CommandResult.failure("服务器错误，获取失败");
        }
    }

    @Command(value = "(详细属性|所有属性|属性2|xxsx)")
    @Iptors(vals = {
            @Iptor(value = NotRegistInterceptor.class),
            @Iptor(value = StateInterceptor.class, params = "{'states':[3],'required':false}", msg = "[{{state}}]状态下无法进行此操作。")
    })
    public CommandResult rolePropDetail(RoleGameCommand cmd) {
        try {
            return roleInfoService.rolePropDetail(cmd);
        } catch (Exception e) {
            e.printStackTrace();
            return CommandResult.failure("服务器错误，获取失败");
        }
    }

    @Command(value = {"升级", "升级\\s*(\\d+)"}, props = {"lev", "lev"})
    @Iptors(vals = {
            @Iptor(value = NotRegistInterceptor.class),
            @Iptor(value = StateInterceptor.class, params = "{'states':[1]}", msg = "[{{state}}]状态下无法进行此操作。")
    })
    public CommandResult update(RoleGameCommand cmd, @Default("1") int lev) {
        try {
            return roleInfoService.update(cmd, lev);
        } catch (Exception e) {
            e.printStackTrace();
            return CommandResult.failure("服务器错误，升级失败");
        }
    }

    @Command(value = "复活")
    @Iptors(vals = {
            @Iptor(value = NotRegistInterceptor.class),
            @Iptor(value = StateInterceptor.class, params = "{'states':[3]}", msg = "您没有死亡，不需要复活。")
    })
    public CommandResult revive(RoleGameCommand cmd) {
        try {
            return roleInfoService.revive(cmd);
        } catch (Exception e) {
            e.printStackTrace();
            return CommandResult.failure("服务器错误，复活失败");
        }
    }

    @Command(value = "(账户|查看账户|看账户|查账|查帐户)")
    @Iptors(vals = {
            @Iptor(value = NotRegistInterceptor.class),
            @Iptor(value = StateInterceptor.class, params = "{'states':[3],'required':false}", msg = "[{{state}}]状态下无法进行此操作。")
    })
    public CommandResult userInfo(RoleGameCommand cmd) {
        try {
            return roleInfoService.userInfo(cmd);
        } catch (Exception e) {
            e.printStackTrace();
            return CommandResult.failure("服务器错误，账户信息获取失败。");
        }
    }
}
