package com.prodog.usermodule.role.util;

import com.prodog.gamemodule.item.entity.GameItem;
import com.prodog.usermodule.role.entity.RoleBag;
import com.prodog.usermodule.role.entity.RoleBagItem;
import com.prodog.usermodule.role.wrapper.RoleBagWrapper;
import com.prodog.utils.bean.SpringBeanUtils;
import com.prodog.utils.exception.CommonException;

public class BagUtil {
    /***
     * 检查背包是否存在某物品
     * @param bag   背包对象
     * @param itemId    物品ID
     * @param num   数量
     * @return
     */
    public static boolean checkIsHad(RoleBag bag, String itemId, long num) {
        RoleBagItem item = bag.getItems().stream()
                .filter(i -> i.getItemId().equals(itemId))
                .findAny().orElse(new RoleBagItem(itemId, 0L));
        return item.getNums() >= num;
    }

    /***
     * 消耗背包中的物品
     * @param bag   背包对象
     * @param itemId    物品ID
     * @param num   数量
     * @return
     */
    public static boolean consumeItem(RoleBag bag, String itemId, long num) {
        RoleBagItem item = bag.getItems().stream()
                .filter(i -> i.getItemId().equals(itemId))
                .findAny().orElse(new RoleBagItem(itemId, 0L));
        if (item.getNums() < num) return false;
        item.setNums(item.getNums() - num);
        if (item.getNums() <= 0) bag.getItems().remove(item);
        SpringBeanUtils.getBean(RoleBagWrapper.class).save(bag);
        return true;
    }

    /***
     * 消耗背包中的物品并保存
     * @param bag   背包对象
     * @param itemId    物品ID
     * @param num   数量
     * @return
     */
    public static boolean consumeItemAndSave(RoleBag bag, String itemId, long num) {
        boolean res = consumeItem(bag, itemId, num);
        if (res) SpringBeanUtils.getBean(RoleBagWrapper.class).save(bag);
        return res;
    }

    /***
     * 检查上限
     */
    public static boolean checkAddLimit(RoleBag bag, String itemId, long num) {
        GameItem item = GameItemUtil.getById(itemId);
        return checkAddLimit(bag, item, num);
    }

    /***
     * 检查上限
     */
    public static boolean checkAddLimit(RoleBag bag, GameItem item, long num) {
        if (item.getLimit() == null) return true;
        RoleBagItem bagItem = bag.getItems().stream()
                .filter(i -> i.getItemId().equals(item.getId()))
                .findAny().orElseGet(() -> {
                    RoleBagItem i = new RoleBagItem(item.getId(), 0L);
                    bag.getItems().add(i);
                    return i;
                });
        if (bagItem.getNums() + num > item.getLimit()) return false;
        return true;
    }

    /***
     * 给背包添加物品
     * @param bag   背包对象
     * @param itemId    物品ID
     * @param num   数量
     */
    public static void addItem(RoleBag bag, String itemId, long num) {
        RoleBagItem item = bag.getItems().stream()
                .filter(i -> i.getItemId().equals(itemId))
                .findAny().orElseGet(() -> {
                    RoleBagItem i = new RoleBagItem(itemId, 0L);
                    bag.getItems().add(i);
                    return i;
                });
        item.setNums(item.getNums() + num);
        SpringBeanUtils.getBean(RoleBagWrapper.class).save(bag);
    }

    /***
     * 带检查的给背包添加物品
     * @param bag   背包对象
     * @param itemId    物品ID
     * @param num   数量
     * @param msg 错误消息
     */
    public static void addItemWithCheck(RoleBag bag, String itemId, long num, String msg) throws CommonException {
        GameItem gItem = GameItemUtil.getById(itemId);
        CommonException.bySupplier(() -> !checkAddLimit(bag, gItem, num), msg.replace("{{itemName}}", gItem.getName()));
        addItem(bag, itemId, num);
    }

    /***
     * 带检查的给背包添加物品并保存
     * @param bag   背包对象
     * @param itemId    物品ID
     * @param num   数量
     * @param msg 错误消息
     */
    public static void addItemAndSaveWithCheck(RoleBag bag, String itemId, long num, String msg) throws CommonException {
        GameItem gItem = GameItemUtil.getById(itemId);
        CommonException.bySupplier(() -> !checkAddLimit(bag, gItem, num), msg.replace("{{itemName}}", gItem.getName()));
        addItemAndSave(bag, itemId, num);
    }

    /***
     * 给背包添加物品并保存
     * @param bag   背包对象
     * @param itemId    物品ID
     * @param num   数量
     */
    public static void addItemAndSave(RoleBag bag, String itemId, long num) {
        addItem(bag, itemId, num);
        SpringBeanUtils.getBean(RoleBagWrapper.class).save(bag);
    }

    public static boolean checkIsHad(String roleId, String itemId, long num) {
        RoleBag roleBag = SpringBeanUtils.getBean(RoleBagWrapper.class).getById(roleId);
        return checkIsHad(roleBag, itemId, num);
    }

    public static boolean consumeItem(String roleId, String itemId, long num) {
        RoleBag roleBag = SpringBeanUtils.getBean(RoleBagWrapper.class).getById(roleId);
        return consumeItem(roleBag, itemId, num);
    }

    public static boolean checkAddLimit(String roleId, String itemId, long num) {
        RoleBag roleBag = SpringBeanUtils.getBean(RoleBagWrapper.class).getById(roleId);
        return checkAddLimit(roleBag, itemId, num);
    }

    public static void addItem(String roleId, String itemId, long num) {
        RoleBag roleBag = SpringBeanUtils.getBean(RoleBagWrapper.class).getById(roleId);
        addItem(roleBag, itemId, num);
    }

    public static boolean consumeItemAndSave(String roleId, String itemId, long num) {
        RoleBag roleBag = SpringBeanUtils.getBean(RoleBagWrapper.class).getById(roleId);
        return consumeItemAndSave(roleBag, itemId, num);
    }

    public static void addItemAndSave(String roleId, String itemId, long num) {
        RoleBag roleBag = SpringBeanUtils.getBean(RoleBagWrapper.class).getById(roleId);
        addItemAndSave(roleBag, itemId, num);
    }
}
