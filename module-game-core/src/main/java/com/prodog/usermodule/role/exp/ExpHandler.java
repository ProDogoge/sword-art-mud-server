package com.prodog.usermodule.role.exp;

import com.prodog.gamemodule.gameworld.monster.entity.Monster;
import com.prodog.usermodule.caculate.CalVal;
import com.prodog.usermodule.role.entity.RoleInfo;

public interface ExpHandler {
    void handle(RoleInfo roleInfo, Monster monster, CalVal<Long> calVal);
}
