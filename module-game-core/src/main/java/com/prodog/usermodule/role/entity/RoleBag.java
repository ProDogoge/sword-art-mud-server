package com.prodog.usermodule.role.entity;

import lombok.Data;
import org.springframework.data.mongodb.core.mapping.Document;

import java.util.List;

@Data
@Document("g_role_bag")
public class RoleBag {
    private String id;
    private int itemNums;   //背包格子数(容量)
    private List<RoleBagItem> items;    //背包物品
}
