package com.prodog.usermodule.role.vo.convetor;

import com.prodog.gamemodule.config.dict.MapDictConfig;
import com.prodog.gamemodule.gameworld.map.entity.GameMap;
import com.prodog.gamemodule.gameworld.map.wrapper.GameMapWrapper;
import com.prodog.gamemodule.profession.entity.Profession;
import com.prodog.gamemodule.profession.wrapper.ProfessionWrapper;
import com.prodog.usermodule.role.entity.RoleInfo;
import com.prodog.usermodule.role.vo.RoleInfoVo;
import com.prodog.utils.bean.BeanUtil;
import com.prodog.utils.interfaces.AutowiredBean;
import com.prodog.utils.interfaces.VOConvertor;
import org.springframework.beans.factory.annotation.Autowired;

public class RoleInfoVoConvetor implements AutowiredBean, VOConvertor<RoleInfo, RoleInfoVo> {
    @Autowired
    private GameMapWrapper gameMapWrapper;
    @Autowired
    private ProfessionWrapper professionWrapper;
    @Autowired
    private MapDictConfig mapDictConfig;

    public RoleInfoVoConvetor() {
        autowired();
    }

    @Override
    public RoleInfoVo convert(RoleInfo roleInfo) {
        GameMap map = gameMapWrapper.getById(roleInfo.getMapId());
        Profession profession = professionWrapper.getById(roleInfo.getProfessionId());

        //拼成VO
        RoleInfoVo vo = BeanUtil.beanToBean(roleInfo, RoleInfoVo.class);
        vo.setProfessionName(profession != null ? profession.getProfessionName() : "无");
        vo.setMap(map.getName());
        vo.setState(mapDictConfig.get("roleState", roleInfo.getState()));
        return vo;
    }
}
