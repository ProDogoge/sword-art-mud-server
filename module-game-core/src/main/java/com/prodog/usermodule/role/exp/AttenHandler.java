package com.prodog.usermodule.role.exp;

import com.prodog.gamemodule.config.BaseConfig;
import com.prodog.gamemodule.gameworld.monster.entity.Monster;
import com.prodog.usermodule.caculate.CalVal;
import com.prodog.usermodule.role.entity.RoleInfo;
import com.prodog.utils.interfaces.AutowiredBean;
import org.springframework.beans.factory.annotation.Autowired;

public class AttenHandler implements ExpHandler, AutowiredBean {
    @Autowired
    private BaseConfig baseConfig;

    public AttenHandler() {
        this.autowired();
    }

    @Override
    public void handle(RoleInfo roleInfo, Monster monster, CalVal<Long> calVal) {
        int myLevel = roleInfo.getLevel();
        int monsterLevel = monster.getProp().getLevel();
        int attenTimes = Math.abs((myLevel - monsterLevel) / baseConfig.getExpAttenLevel());
        int attenRate = Math.min(baseConfig.getAttenRate() * attenTimes, 100);
        long attenExp = calVal.getBase() - (calVal.getBase() * (100 - attenRate) / 100);
        calVal.setResult(calVal.getBase() - attenExp);
    }
}
