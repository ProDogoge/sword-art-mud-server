package com.prodog.usermodule.role.service.impl;

import com.prodog.command.annonations.Service;
import com.prodog.command.entity.CommandResult;
import com.prodog.command.entity.GameCommand;
import com.prodog.command.entity.RoleGameCommand;
import com.prodog.commonmodule.prop.caculate.getter.RolePropGetter;
import com.prodog.commonmodule.prop.caculate.util.PropUtil;
import com.prodog.commonmodule.prop.entity.ObjProp;
import com.prodog.database.wrapper.DataWrapperCentral;
import com.prodog.gamemodule.config.BaseConfig;
import com.prodog.gamemodule.config.BirthConfig;
import com.prodog.gamemodule.config.dict.DictConfig;
import com.prodog.gamemodule.config.ServerConfig;
import com.prodog.gamemodule.gameworld.map.entity.GameMap;
import com.prodog.gamemodule.gameworld.map.util.MapUtil;
import com.prodog.gamemodule.gameworld.map.wrapper.GameMapWrapper;
import com.prodog.gamemodule.item.entity.ActiveEquipItem;
import com.prodog.gamemodule.item.wrapper.ActiveEquipWrapper;
import com.prodog.gamemodule.profession.entity.Profession;
import com.prodog.gamemodule.profession.service.ProfessionService;
import com.prodog.gamemodule.profession.wrapper.ProfessionWrapper;
import com.prodog.usermodule.role.constant.RoleState;
import com.prodog.usermodule.role.entity.RoleBag;
import com.prodog.usermodule.role.entity.RoleEquip;
import com.prodog.usermodule.role.entity.RoleInfo;
import com.prodog.usermodule.role.entity.RoleSkill;
import com.prodog.usermodule.role.service.RoleInfoService;
import com.prodog.usermodule.role.util.ExpUtil;
import com.prodog.usermodule.role.util.RoleInfoUtil;
import com.prodog.usermodule.role.vo.RoleInfoVo;
import com.prodog.usermodule.role.vo.RoleListVo;
import com.prodog.usermodule.role.vo.convetor.RoleInfoVoConvetor;
import com.prodog.usermodule.role.wrapper.RoleBagWrapper;
import com.prodog.usermodule.role.wrapper.RoleEquipWrapper;
import com.prodog.usermodule.role.wrapper.RoleInfoWrapper;
import com.prodog.usermodule.role.wrapper.RoleSkillWrapper;
import com.prodog.usermodule.user.dtos.CreateRoleDTO;
import com.prodog.usermodule.user.entity.UserInfo;
import com.prodog.usermodule.user.wrapper.UserInfoWrapper;
import com.prodog.util.MsgTemplateUtil;
import com.prodog.utils.bean.SpringBeanUtils;
import com.prodog.utils.exception.CommonException;
import lombok.RequiredArgsConstructor;
import org.springframework.transaction.annotation.Transactional;

import java.util.*;
import java.util.concurrent.locks.ReentrantLock;

@Service
@RequiredArgsConstructor
public class RoleInfoServiceImpl implements RoleInfoService {
    private final UserInfoWrapper userInfoWrapper;
    private final RoleInfoWrapper roleInfoWrapper;
    private final RoleBagWrapper roleBagWrapper;
    private final GameMapWrapper gameMapWrapper;
    private final ProfessionWrapper professionWrapper;
    private final RoleEquipWrapper roleEquipWrapper;
    private final RoleSkillWrapper roleSkillWrapper;

    private final BirthConfig birthConfig;
    private final ServerConfig serverConfig;
    private final BaseConfig baseConfig;
    private final DictConfig dictConfig;

    private final ProfessionService professionService;

    private final ReentrantLock createRoleLock = new ReentrantLock();


    @Override
    @Transactional(rollbackFor = Exception.class)
    public CommandResult createRole(GameCommand command, CreateRoleDTO dto) throws CommonException {
        CommonException.bySupplier(() -> dto.getRoleName() == null || dto.getSex() == null, "格式错误，正确创建格式为:创建角色 角色名 男或女");
        //查询是否存在账户
        UserInfo userInfo = userInfoWrapper.getById(command.getFromQQ());
        if (userInfo == null) {
            userInfo = new UserInfo() {{
                setId(command.getFromQQ());
                setRegistTime(new Date());
                setCopoun(birthConfig.getCopoun());
            }};
        }
        //查询账户存在的角色数量
        long roleCount = roleInfoWrapper.countByColumn("userId", userInfo.getId());
        if (roleCount >= serverConfig.getRoleLimit()) {
            return CommandResult.failure("创建失败，一个账户最多可创建" + serverConfig.getRoleLimit() + "个角色！");
        } else if (roleInfoWrapper.countByColumn("roleName", dto.getRoleName()) > 0) {
            return CommandResult.failure("创建失败，已存在相同角色名的玩家角色");
        } else {
            createRoleLock.lock();
            RoleInfo roleInfo;
            try {
                //创建角色信息 角色属性 角色背包
                long maxSerialNum = roleInfoWrapper.max("serialNum");
                String roleId = command.getFromQQ() + "-" + (maxSerialNum + 1);
                roleInfo = new RoleInfo() {{
                    setId(roleId);
                    setUserId(command.getFromQQ());
                    setSerialNum(maxSerialNum + 1);
                    setRoleName(dto.getRoleName());
                    setSex(dto.getSex());
                    setMapId(birthConfig.getBirthMap());
                    setMoney(birthConfig.getMoney());
                    setTitle(birthConfig.getBirthTitle());

                    setLevel(1);
                    setExp(0);
                    setCurrHp(professionWrapper.getById(birthConfig.getBirthProfession()).getInitialProp().getCurHp());
                    setCurrMp(professionWrapper.getById(birthConfig.getBirthProfession()).getInitialProp().getCurMp());
                    setEnergy(baseConfig.getBaseEnergy());
                }};

                RoleBag roleBag = new RoleBag() {{
                    setId(roleId);
                    setItemNums(birthConfig.getBagItemNums());
                    setItems(new ArrayList<>());
                }};

                RoleEquip roleEquip = new RoleEquip();
                roleEquip.setId(roleInfo.getId());

                roleInfoWrapper.save(roleInfo);
                roleBagWrapper.save(roleBag);
                roleEquipWrapper.save(roleEquip);
                roleSkillWrapper.save(new RoleSkill(roleInfo.getId(), new HashMap<>()));
                professionService.changeProfession(roleInfo, birthConfig.getBirthProfession());

                //切换角色
                userInfo.setRoleId(roleId);
                userInfoWrapper.save(userInfo);
            } finally {
                createRoleLock.unlock();
            }
            ObjProp prop = SpringBeanUtils.getBean(RolePropGetter.class).get(roleInfo);

            Map<String, Object> fmtData = new HashMap<>();
            fmtData.put("professionName", professionWrapper.getById(roleInfo.getProfessionId()).getProfessionName());
            fmtData.put("roleProp", prop);
            fmtData.put("roleInfo", roleInfo);
            fmtData.put("mapName", gameMapWrapper.getById(roleInfo.getMapId()).getName());
            return CommandResult.success(MsgTemplateUtil.formatMsg("游戏配置/消息模板/创建角色成功.txt", fmtData));
        }
    }

    @Override
    @Transactional(rollbackFor = Exception.class)
    public CommandResult changeRole(GameCommand command, String roleName) {
        //获取当前角色对应要切换的角色名
        UserInfo info = userInfoWrapper.getById(command.getFromQQ());
        RoleInfo curRole = roleInfoWrapper.getById(info.getRoleId());
        if (curRole.getRoleName().equals(roleName)) {
            return CommandResult.failure("您正在使用该角色，无需切换。");
        }
        //获取当前用户id和切换角色名的角色
        RoleInfo changeRoleInfo = roleInfoWrapper.getByColumns("userId", info.getId(), "roleName", roleName);
        if (changeRoleInfo == null) {
            return CommandResult.failure("切换失败，您的账户里没有该角色。");
        }
        //切换角色
        info.setRoleId(changeRoleInfo.getId());
        //切换到要删除的角色则取消删除
        info.setDelRoleId(changeRoleInfo.getId().equals(info.getDelRoleId()) ? null : info.getDelRoleId());
        userInfoWrapper.save(info);
        return CommandResult.success("切换成功！当前角色：[" + changeRoleInfo.getLevel() + "级]" + changeRoleInfo.getRoleName());
    }

    @Override
    public CommandResult roleList(GameCommand command) {
        //获取当前用户的角色信息 拼成角色列表VO
        List<RoleInfo> roleInfoList = roleInfoWrapper.listByColumn("userId", command.getFromQQ());
        List<RoleListVo> listVos = new ArrayList<>();
        for (RoleInfo roleInfo : roleInfoList) {
            RoleListVo vo = new RoleListVo() {{
                setRoleName(roleInfo.getRoleName());
                setLevel(roleInfo.getLevel());
                setProfessionName(professionWrapper.getById(roleInfo.getProfessionId()).getProfessionName());
                if (roleInfo.getId().equals(userInfoWrapper.getById(command.getFromQQ()).getRoleId())) {
                    setUsing(true);
                }
            }};
            listVos.add(vo);
        }
        return CommandResult.success(MsgTemplateUtil.formatMsg("游戏配置/消息模板/角色列表.txt", new HashMap<String, Object>() {{
            put("roleList", listVos);
        }}));
    }

    @Override
    public CommandResult roleInfo(GameCommand command) {
        UserInfo userInfo = userInfoWrapper.getById(command.getFromQQ());
        RoleInfo roleInfo = roleInfoWrapper.getById(userInfo.getRoleId());
        RoleInfoVo vo = new RoleInfoVoConvetor().convert(roleInfo);
        String title;
        RoleEquip roleEquip = SpringBeanUtils.getBean(RoleEquipWrapper.class).getById(roleInfo.getId());
        if (roleEquip.getTitle() != null) {
            ActiveEquipItem equ = SpringBeanUtils.getBean(ActiveEquipWrapper.class).getById(roleEquip.getTitle());
            title = equ.getSuperName();
        } else {
            title = "无";
        }
        //返回模板消息
        String finalTitle = title;
        return CommandResult.success(MsgTemplateUtil.formatMsg("游戏配置/消息模板/角色信息.txt", new HashMap<String, Object>() {{
            put("roleInfo", vo);
            put("title", finalTitle);
        }}));
    }

    @Override
    @Transactional(rollbackFor = Exception.class)
    public CommandResult deleteRole(GameCommand command, String roleName) {
        //获取当前操作角色
        UserInfo info = userInfoWrapper.getById(command.getFromQQ());
        RoleInfo curRole = roleInfoWrapper.getById(info.getRoleId());
        if (curRole.getRoleName().equals(roleName)) {
            return CommandResult.failure("您正在使用该角色，无法删除。");
        }
        //获取当前用户id和切换角色名的角色
        RoleInfo deleteRoleInfo = roleInfoWrapper.getByColumns("userId", info.getId(), "roleName", roleName);
        if (deleteRoleInfo == null) {
            return CommandResult.failure("您的账户里没有该角色，无法删除");
        }
        info.setDelRoleId(deleteRoleInfo.getId());
        userInfoWrapper.save(info);
        return CommandResult.success(MsgTemplateUtil.formatMsg("游戏配置/消息模板/删除角色.txt", new HashMap<String, Object>() {{
            put("level", deleteRoleInfo.getLevel());
            put("roleName", deleteRoleInfo.getRoleName());
        }}));
    }

    @Override
    @Transactional(rollbackFor = Exception.class)
    public CommandResult confirmDelete(GameCommand command) {
        //获取当前要删除的角色
        UserInfo userInfo = userInfoWrapper.getById(command.getFromQQ());
        if (userInfo.getDelRoleId() == null) {
            return CommandResult.failure("您没有需要删除的角色");
        }
        RoleInfo delRoleInfo = roleInfoWrapper.getById(userInfo.getDelRoleId());
        DataWrapperCentral.removeById(userInfo.getDelRoleId());
        //删除角色
        return CommandResult.success(MsgTemplateUtil.formatMsg("游戏配置/消息模板/删除角色成功.txt", new HashMap<String, Object>() {{
            put("level", delRoleInfo.getLevel());
            put("roleName", delRoleInfo.getRoleName());
        }}));
    }

    @Override
    @Transactional(rollbackFor = Exception.class)
    public CommandResult cancelDelete(GameCommand command) {
        //获取当前要删除的角色
        UserInfo userInfo = userInfoWrapper.getById(command.getFromQQ());
        if (userInfo.getDelRoleId() == null) {
            return CommandResult.failure("您没有需要删除的角色。");
        }
        userInfo.setDelRoleId(null);
        userInfoWrapper.save(userInfo);
        return CommandResult.success("取消删除成功！");
    }

    @Override
    @Transactional(rollbackFor = Exception.class)
    public CommandResult selectProfession(RoleGameCommand cmd, String professionName) {
        //判断是否已有职业 是否初始职业
        if (!cmd.getRoleInfo().getProfessionId().equals(birthConfig.getBirthProfession())) {
            return CommandResult.failure(MsgTemplateUtil.formatMsg("游戏配置/消息模板/选择职业_已有职业.txt", new HashMap<String, Object>() {{
                put("roleInfo", cmd.getRoleInfo());
            }}));
        }
        Profession profession = professionWrapper.getByColumn("professionName", professionName);
        if (profession == null) {
            return CommandResult.failure(MsgTemplateUtil.formatMsg("游戏配置/消息模板/选择职业_职业不存在.txt", new HashMap<String, Object>() {{
                put("roleInfo", cmd.getRoleInfo());
            }}));
        }
        if (!profession.isInitial()) {
            return CommandResult.failure(MsgTemplateUtil.formatMsg("游戏配置/消息模板/选择职业_非初始职业.txt", new HashMap<String, Object>() {{
                put("roleInfo", cmd.getRoleInfo());
            }}));
        }
        ObjProp oldProp = SpringBeanUtils.getBean(RolePropGetter.class).get(cmd.getRoleInfo());
        professionService.changeProfession(cmd.getRoleInfo(), profession.getId());
        ObjProp newProp = SpringBeanUtils.getBean(RolePropGetter.class).get(cmd.getRoleInfo());
        //选择职业保存
        return CommandResult.success(MsgTemplateUtil.formatMsg("游戏配置/消息模板/选择职业_选择成功.txt", new HashMap<String, Object>() {{
            put("roleInfo", cmd.getRoleInfo());
            put("profession", profession);
            put("propChangeInfo", PropUtil.propChangeInfo(oldProp, newProp));
        }}));
    }

    @Override
    public CommandResult roleProp(RoleGameCommand cmd) {
        //获取角色属性 计算
        RoleInfo info = roleInfoWrapper.getById(cmd.getRoleId());
        ObjProp prop = SpringBeanUtils.getBean(RolePropGetter.class).get(info);
        Profession profession = professionWrapper.getById(cmd.getRoleInfo().getProfessionId());

        long reqExp = ExpUtil.getLevelExp(cmd.getRoleInfo().getLevel() + 1);
        return CommandResult.success(MsgTemplateUtil.formatMsg("游戏配置/消息模板/角色属性.txt", new HashMap<String, Object>() {{
            put("professionName", profession == null ? "无" : profession.getProfessionName());
            put("prefix", RoleInfoUtil.getPrefix(cmd.getRoleInfo()));
            put("roleInfo", cmd.getRoleInfo());
            put("roleProp", prop);
            put("energy", RoleInfoUtil.getEnergy(info));
            put("maxEnergy", baseConfig.getBaseEnergy());
            put("reqExp", reqExp);
            put("percentExp", (int) (info.getExp() * 100 / reqExp));
        }}));
    }

    @Override
    @Transactional(rollbackFor = Exception.class)
    public CommandResult update(RoleGameCommand cmd, int lev) {
        String prefix = RoleInfoUtil.getPrefix(cmd.getRoleInfo());
        long reqExp = ExpUtil.getRangeLevelExp(cmd.getRoleInfo().getLevel(), cmd.getRoleInfo().getLevel() + lev);
        if (cmd.getRoleInfo().getLevel() + lev > baseConfig.getMaxLevel())
            return CommandResult.failure(prefix + "所升等级超过当前版本最高等级Lv." + baseConfig.getMaxLevel() + "。");
        if (cmd.getRoleInfo().getExp() < reqExp) {
            long rangeExp = reqExp - cmd.getRoleInfo().getExp();
            return CommandResult.success(MsgTemplateUtil.formatMsg("游戏配置/消息模板/升级_失败.txt", new HashMap<String, Object>() {{
                put("roleInfo", cmd.getRoleInfo());
                put("target", cmd.getRoleInfo().getLevel() + lev);
                put("reqExp", reqExp);
                put("rangeExp", rangeExp);
                put("prefix", RoleInfoUtil.getPrefix(cmd.getRoleInfo()));
            }}));
        } else {
            ObjProp oldProp = SpringBeanUtils.getBean(RolePropGetter.class).get(cmd.getRoleInfo());
            cmd.getRoleInfo().setExp(cmd.getRoleInfo().getExp() - reqExp);
            cmd.getRoleInfo().setLevel(cmd.getRoleInfo().getLevel() + lev);

            ObjProp newProp = SpringBeanUtils.getBean(RolePropGetter.class).get(cmd.getRoleInfo());
            cmd.getRoleInfo().setCurrHp(newProp.getHp());
            cmd.getRoleInfo().setCurrMp(newProp.getMp());
            roleInfoWrapper.save(cmd.getRoleInfo());
            return CommandResult.success(MsgTemplateUtil.formatMsg("游戏配置/消息模板/升级_成功.txt", new HashMap<String, Object>() {{
                put("roleInfo", cmd.getRoleInfo());
                put("propChangeInfo", PropUtil.propChangeInfo(oldProp, newProp));
                put("prefix", RoleInfoUtil.getPrefix(cmd.getRoleInfo()));
            }}));
        }
    }

    @Override
    @Transactional(rollbackFor = Exception.class)
    public CommandResult revive(RoleGameCommand cmd) {
        StringBuilder res = new StringBuilder(RoleInfoUtil.getPrefix(cmd.getRoleInfo())).append("复活成功！");
        if (cmd.getRoleInfo().getLevel() > baseConfig.getDeadProtectLevel()) {
            long currExp = cmd.getRoleInfo().getExp();
            long currMoney = cmd.getRoleInfo().getMoney();
            long loseExp = ExpUtil.getLevelExp(cmd.getRoleInfo().getLevel() + 1) * baseConfig.getDeadLoseExpRate() / 100;
            long loseMoney = currMoney * baseConfig.getDeadLoseMoneyRate() / 100;
            loseExp = Math.min(currExp, loseExp);
            cmd.getRoleInfo().setExp(currExp - loseExp);
            cmd.getRoleInfo().setMoney(currMoney - loseMoney);
            res.append("丢失").append(dictConfig.get("exp")).append(":").append(loseExp).append("  ");
            res.append("丢失").append(dictConfig.get("money")).append(":").append(loseMoney).append("  ");
        }
        cmd.getRoleInfo().setState(RoleState.NORMAL);
        cmd.getRoleInfo().setCurrHp(1);
        GameMap reviveMap = MapUtil.getMainCityByMapId(cmd.getRoleInfo().getMapId());
        cmd.getRoleInfo().setMapId(reviveMap.getId());
        res.append("当前位置: [").append(reviveMap.getName()).append("]");
        roleInfoWrapper.save(cmd.getRoleInfo());
        return CommandResult.success(res.toString().trim());
    }

    @Override
    public CommandResult userInfo(RoleGameCommand cmd) {
        String prefix = RoleInfoUtil.getPrefix(cmd.getRoleInfo());
        StringBuilder res = new StringBuilder().append(prefix).append("您的账户信息如下:\n");
        res.append("当前角色: " + cmd.getRoleInfo().getRoleName()).append("\n");
        res.append("账户余额: ").append(cmd.getRoleInfo().getMoney()).append(dictConfig.get("money")).append("\n");
        res.append(dictConfig.get("copoun")).append("余额: ").append(cmd.getUserInfo().getCopoun()).append(dictConfig.get("copoun")).append("\n");
        return CommandResult.success(res.toString().trim());
    }

    @Override
    public CommandResult rolePropDetail(RoleGameCommand cmd) {
        //获取角色属性 计算
        RoleInfo info = roleInfoWrapper.getById(cmd.getRoleId());
        ObjProp prop = SpringBeanUtils.getBean(RolePropGetter.class).get(info);
        Profession profession = professionWrapper.getById(cmd.getRoleInfo().getProfessionId());

        long reqExp = ExpUtil.getLevelExp(cmd.getRoleInfo().getLevel() + 1);
        return CommandResult.success(MsgTemplateUtil.formatMsg("游戏配置/消息模板/角色详细属性.txt", new HashMap<String, Object>() {{
            put("professionName", profession == null ? "无" : profession.getProfessionName());
            put("prefix", RoleInfoUtil.getPrefix(cmd.getRoleInfo()));
            put("roleInfo", cmd.getRoleInfo());
            put("roleProp", prop);
            put("energy", RoleInfoUtil.getEnergy(info));
            put("maxEnergy", baseConfig.getBaseEnergy());
            put("reqExp", reqExp);
            put("percentExp", (int) (info.getExp() * 100 / reqExp));
        }}));
    }
}
