package com.prodog.usermodule.role.entity;

import com.prodog.commonmodule.prop.entity.ObjProp;
import lombok.Data;
import org.springframework.data.mongodb.core.mapping.Document;

@Data
@Document("g_role_prop")
public class RoleProp extends ObjProp {
    private String id;
    private long exp;   //经验值
}
