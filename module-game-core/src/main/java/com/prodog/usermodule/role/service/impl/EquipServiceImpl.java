package com.prodog.usermodule.role.service.impl;

import cn.hutool.core.util.ReflectUtil;
import cn.hutool.core.util.StrUtil;
import com.prodog.command.annonations.Service;
import com.prodog.command.entity.CommandResult;
import com.prodog.command.entity.RoleGameCommand;
import com.prodog.commonmodule.prop.caculate.getter.RolePropGetter;
import com.prodog.commonmodule.prop.caculate.util.PropUtil;
import com.prodog.commonmodule.prop.entity.ObjProp;
import com.prodog.gamemodule.config.dict.DictConfig;
import com.prodog.gamemodule.item.entity.ActiveEquipItem;
import com.prodog.gamemodule.item.entity.EquipItem;
import com.prodog.gamemodule.item.wrapper.ActiveEquipWrapper;
import com.prodog.gamemodule.item.wrapper.EquipSerialNumWrapper;
import com.prodog.gamemodule.item.wrapper.EquipWrapper;
import com.prodog.gamemodule.profession.entity.Profession;
import com.prodog.gamemodule.profession.wrapper.ProfessionWrapper;
import com.prodog.gamemodule.suit.domain.model.EquipSuit;
import com.prodog.gamemodule.suit.util.EquipSuitUtil;
import com.prodog.usermodule.role.entity.RoleBag;
import com.prodog.usermodule.role.entity.RoleEquip;
import com.prodog.usermodule.role.service.EquipService;
import com.prodog.usermodule.role.util.BagUtil;
import com.prodog.usermodule.role.util.EquipUtil;
import com.prodog.usermodule.role.util.RoleInfoUtil;
import com.prodog.usermodule.role.wrapper.RoleBagWrapper;
import com.prodog.usermodule.role.wrapper.RoleEquipWrapper;
import com.prodog.utils.bean.SpringBeanUtils;
import com.prodog.utils.exception.CommonException;
import lombok.RequiredArgsConstructor;
import org.springframework.beans.BeanUtils;
import org.springframework.transaction.annotation.Transactional;

import java.lang.reflect.Field;
import java.util.*;
import java.util.stream.Collectors;

@Service
@RequiredArgsConstructor
public class EquipServiceImpl implements EquipService {
    private final RoleEquipWrapper roleEquipWrapper;
    private final ActiveEquipWrapper activeEquipWrapper;
    private final EquipWrapper equipWrapper;
    private final RoleBagWrapper bagWrapper;
    private final EquipSerialNumWrapper serialNumWrapper;
    private final ProfessionWrapper professionWrapper;

    private final DictConfig dictConfig;


    @Override
    public CommandResult equipInfos(RoleGameCommand command) {
        List<String> canIgnore = Arrays.asList("secondWeapon");
        RoleEquip equip = Optional.ofNullable(roleEquipWrapper.getById(command.getRoleId()))
                .orElseGet(() -> {
                    RoleEquip roleEquip = new RoleEquip();
                    roleEquip.setId(command.getRoleId());
                    roleEquipWrapper.save(roleEquip);
                    return roleEquip;
                });
        StringBuilder respStr = new StringBuilder(RoleInfoUtil.getPrefix(command.getRoleInfo()) + "您的装备信息如下:\n");
        for (Field field : EquipUtil.roleEquipFields) {
            if (Objects.equals(field.getName(), "id")) continue;

            try {
                String equipIdx = (String) field.get(equip);
                if (equipIdx == null) {
                    if (canIgnore.contains(field.getName())) continue;
                    respStr.append(dictConfig.get(field.getName())).append("：");
                    respStr.append("无\n");
                    continue;
                }
                respStr.append(dictConfig.get(field.getName())).append("：");
                ActiveEquipItem activeEquipItem = activeEquipWrapper.getById(equipIdx);
                if (activeEquipItem.getEnhanceLevel() > 0)
                    respStr.append("+").append(activeEquipItem.getEnhanceLevel()).append(" ");
                respStr.append(activeEquipItem.getName()).append("\n");
            } catch (Exception e) {
                e.printStackTrace();
            }
        }

        //套装
        Map<EquipSuit, Integer> suits = EquipSuitUtil.getSuits(equip);
        if (suits.size() > 0) respStr.append("【套装】\n");
        suits.forEach((suit, nums) -> respStr.append(suit.getName()).append("(").append(nums).append("): ").append(suit.getDesc()).append("\n\n"));
        return CommandResult.success(respStr.toString().trim());
    }

    @Override
    public CommandResult searchEquip(RoleGameCommand command, String equipName) throws CommonException {
        EquipItem equip = equipWrapper.getByColumn("name", equipName);
        CommonException.byNull(equip, RoleInfoUtil.getPrefix(command.getRoleInfo()) + "查询失败，名称为[" + equipName + "]的装备不存在。");
        StringBuilder res = new StringBuilder(RoleInfoUtil.getPrefix(command.getRoleInfo()) + "该装备信息如下:\n");
        //装备信息
        res.append(EquipUtil.equipInfo(equip));
        return CommandResult.success(res.toString().trim());
    }

    @Override
    @Transactional(rollbackFor = Exception.class)
    public CommandResult activeEquip(RoleGameCommand command, String equipStr) throws CommonException {
        List<String> equipNames = Arrays.asList(equipStr.trim().split("\\s"));
        List<String> successNames = new ArrayList<>();
        List<String> resIds = new ArrayList<>();
        StringBuilder res = new StringBuilder(RoleInfoUtil.getPrefix(command.getRoleInfo()) + "装备");
        RoleBag bag = bagWrapper.getById(command.getRoleId());
        for (String equipName : equipNames) {
            EquipItem equipItem = equipWrapper.getByColumn("name", equipName);
            CommonException.byNull(equipItem, RoleInfoUtil.getPrefix(command.getRoleInfo()) + "激活失败，名称为[" + equipName + "]的装备不存在。");
            CommonException.bySupplier(() -> !BagUtil.checkIsHad(bag, equipItem.getId(), 1), RoleInfoUtil.getPrefix(command.getRoleInfo()) + "激活失败，您背包中的装备[" + equipName + "]数量不足。");

            //创建并保存装备实例对象
            ActiveEquipItem activeEquip = new ActiveEquipItem();
            BeanUtils.copyProperties(equipItem, activeEquip);
            long serialNum = serialNumWrapper.getNextNumAndSave(equipItem.getId());
            activeEquip.setId(activeEquipWrapper.max("id") == 0 ? "400001" : activeEquipWrapper.max("id") + 1 + "");
            activeEquip.setEquipId(equipItem.getId());
            activeEquip.setSerialNum(serialNum);
            activeEquip.setType(4);
            activeEquipWrapper.save(activeEquip);
            BagUtil.consumeItem(bag, equipItem.getId(), 1);
            BagUtil.addItem(bag, activeEquip.getId(), 1);

            successNames.add(activeEquip.getName());
            resIds.add(activeEquip.getId());
        }
        bagWrapper.save(bag);
        String equs = successNames.stream().map(successName -> "[" + successName + "]").collect(Collectors.joining(" "));
        res.append(equs).append("激活成功！");
        if (successNames.size() == 1) {
            res.append("\n");
            ActiveEquipItem activeEquip = activeEquipWrapper.getByName(successNames.get(0));
            res.append(EquipUtil.equipInfo(activeEquip, command.getRoleId()));
        }
        return CommandResult.success(res.toString().trim(), resIds);
    }

    @Override
    public CommandResult wearEquip(RoleGameCommand command, String equipNames) throws CommonException {
        List<String> equNames = Arrays.asList(equipNames.split("\\s+"));
        List<ActiveEquipItem> activeEqus = new ArrayList<>();
        for (String equName : equNames) {
            ActiveEquipItem equ = activeEquipWrapper.getByName(equName);
            if (equ == null)
                return CommandResult.failure(RoleInfoUtil.getPrefix(command.getRoleInfo()) + "装备失败，名称为[" + equName + "]的实例不存在。");
            activeEqus.add(equ);
        }
        return wearEquip(command, activeEqus);
    }

    @Override
    @Transactional(rollbackFor = Exception.class)
    public CommandResult wearEquip(RoleGameCommand command, List<ActiveEquipItem> activeEqus) throws CommonException {
        RoleBag bag = bagWrapper.getById(command.getRoleInfo().getId());
        RoleEquip roleEquip = roleEquipWrapper.getById(command.getRoleInfo().getId());
        List<String> successNames = new ArrayList<>();
        ObjProp oldp = SpringBeanUtils.getBean(RolePropGetter.class).get(command.getRoleInfo());
        for (ActiveEquipItem equ : activeEqus) {
            //检查背包是否有此装备
            CommonException.bySupplier(() -> !BagUtil.checkIsHad(bag, equ.getId(), 1), RoleInfoUtil.getPrefix(command.getRoleInfo()) + "装备失败，您的背包找不到装备[" + equ.getName() + "]。");
            //检查当前角色等级是否足够
            CommonException.bySupplier(() -> equ.getRequiredLevel() > command.getRoleInfo().getLevel(), RoleInfoUtil.getPrefix(command.getRoleInfo()) + "装备失败，您的等级不足Lv" + equ.getRequiredLevel() + "。");
            //检查当前职业是否可以装备该武器
            if (equ.getEquipType().equalsIgnoreCase("weapon")) {
                Profession profession = professionWrapper.getById(command.getRoleInfo().getProfessionId());
                CommonException.bySupplier(() -> !profession.getEnableWeapons().contains(equ.getSecondType()), RoleInfoUtil.getPrefix(command.getRoleInfo()) + "装备失败，您的职业[" + profession.getProfessionName() + "]不可穿戴" + dictConfig.get(equ.getSecondType()) + "武器，可穿戴武器:" + profession.getCanWearWeapons());
            }
            //检查是否穿上了同部位装备 卸下它
            String hadWearId = (String) ReflectUtil.getFieldValue(roleEquip, equ.getEquipType());
            if (hadWearId != null) BagUtil.addItem(bag, hadWearId, 1);
            ReflectUtil.setFieldValue(roleEquip, equ.getEquipType(), equ.getId());
            BagUtil.consumeItem(bag, equ.getId(), 1);
            successNames.add(equ.getName());
        }
        roleEquipWrapper.save(roleEquip);
        bagWrapper.save(bag);

        StringBuilder res = new StringBuilder(RoleInfoUtil.getPrefix(command.getRoleInfo())).append("装备成功！");
        //res.append(successNames.stream().map(n -> "[" + n + "]").collect(Collectors.joining(" ")));
        //res.append("\n");

        ObjProp newp = SpringBeanUtils.getBean(RolePropGetter.class).get(command.getRoleInfo());
        String propChangeInfo = PropUtil.propChangeInfo(oldp, newp);
        if (StrUtil.isNotBlank(propChangeInfo)) {
            res.append("您的属性变更如下:\n").append(propChangeInfo);
        }
        return CommandResult.success(res.toString());
    }

    @Override
    public CommandResult searchActiveEquip(RoleGameCommand command, String activeEquName) throws CommonException {
        ActiveEquipItem equip = activeEquipWrapper.getByName(activeEquName);
        CommonException.byNull(equip, RoleInfoUtil.getPrefix(command.getRoleInfo()) + "查询失败，该实例不存在。");
        StringBuilder res = new StringBuilder(RoleInfoUtil.getPrefix(command.getRoleInfo()) + "该实例信息如下:\n");
        //实例信息
        res.append(EquipUtil.equipInfo(equip, command.getRoleId()));
        return CommandResult.success(res.toString().trim());
    }

    @Override
    @Transactional(rollbackFor = Exception.class)
    public CommandResult removeEquip(RoleGameCommand command, List<String> parts) throws CommonException {
        RoleEquip roleEquip = roleEquipWrapper.getById(command.getRoleId());
        RoleBag roleBag = bagWrapper.getById(command.getRoleId());
        ObjProp oldp = SpringBeanUtils.getBean(RolePropGetter.class).get(command.getRoleInfo());
        String prefix = RoleInfoUtil.getPrefix(command.getRoleInfo());
        int removeNums = 0;
        for (String part : parts) {
            String curr = (String) ReflectUtil.getFieldValue(roleEquip, part);
            if (curr != null) {
                removeNums++;
                BagUtil.addItem(roleBag, curr, 1);
            }
            ReflectUtil.setFieldValue(roleEquip, part, null);
        }
        int finalRemoveNums = removeNums;
        CommonException.bySupplier(() -> finalRemoveNums == 0, prefix + "卸下失败，您没有装备可卸下。");

        bagWrapper.save(roleBag);
        roleEquipWrapper.save(roleEquip);

        ObjProp newp = SpringBeanUtils.getBean(RolePropGetter.class).get(command.getRoleInfo());
        StringBuilder res = new StringBuilder(prefix).append("卸下成功！");
        String propChangeInfo = PropUtil.propChangeInfo(oldp, newp);
        if (StrUtil.isNotBlank(propChangeInfo)) {
            res.append("您的属性变更如下:\n").append(propChangeInfo);
        }
        return CommandResult.success(res.toString().trim());
    }

    @Override
    @Transactional(rollbackFor = Exception.class)
    public CommandResult removeEquip(RoleGameCommand command, String parts) throws CommonException {
        Map<String, String> partMap = new HashMap<String, String>() {{
            put("称号", "title");
            put("武器", "weapon");
            put("副手", "secondWeapon");
            put("肩部", "shoulder");
            put("上衣", "clothes");
            put("裤子", "pants");
            put("腰带", "belt");
            put("鞋子", "shoes");
            put("戒指", "ring");
            put("手镯", "bracelet");
            put("项链", "neck");
            put("辅助装备", "aux");
        }};
        List<String> partNames = Arrays.asList(parts.split("\\s+"));
        if (parts.equals("全部") || parts.equals("所有"))
            partNames = new ArrayList<>(partMap.values());
        else
            partNames = partNames.stream().map(partMap::get).filter(Objects::nonNull).collect(Collectors.toList());
        String prefix = RoleInfoUtil.getPrefix(command.getRoleInfo());
        List<String> finalPartNames = partNames;
        CommonException.bySupplier(() -> finalPartNames.size() == 0, prefix + "卸下失败，请填写正确的部位。");
        return removeEquip(command, partNames);
    }
}