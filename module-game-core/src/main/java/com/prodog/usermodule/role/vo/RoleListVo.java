package com.prodog.usermodule.role.vo;

import lombok.Data;

@Data
public class RoleListVo {
    private int level;  //等级
    private String roleName;    //角色名
    private String professionName;  //职业名
    private boolean using;   //正在使用
}
