package com.prodog.usermodule.role.wrapper;

import com.prodog.database.annotation.Wrapper;
import com.prodog.database.wrapper.MongoDataWrapper;
import com.prodog.usermodule.role.entity.RoleBag;

@Wrapper(path = "玩家数据/角色/角色背包", module = "角色背包")
public class RoleBagWrapper extends MongoDataWrapper<RoleBag, String> {
}
