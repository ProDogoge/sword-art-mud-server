package com.prodog.usermodule.role.vo;

import lombok.Data;

@Data
public class RoleInfoVo {
    private String userId;  //账号
    private String roleName;    //角色名
    private long serialNum;   //识别码
    private String title;   //称号
    private String professionName;
    private String sex; //性别
    private long money; //灵石
    private String state;   //状态
    private String map; //位置
}
