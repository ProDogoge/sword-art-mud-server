package com.prodog.usermodule.role.util;

import com.prodog.gamemodule.config.BaseConfig;
import com.prodog.gamemodule.config.BirthConfig;
import com.prodog.gamemodule.item.entity.ActiveEquipItem;
import com.prodog.gamemodule.item.wrapper.ActiveEquipWrapper;
import com.prodog.gamemodule.profession.entity.Profession;
import com.prodog.gamemodule.profession.wrapper.ProfessionWrapper;
import com.prodog.gamemodule.skill.entity.Skill;
import com.prodog.gamemodule.skill.wrapper.SkillWrapper;
import com.prodog.usermodule.role.entity.RoleEquip;
import com.prodog.usermodule.role.entity.RoleInfo;
import com.prodog.usermodule.role.entity.RoleSkill;
import com.prodog.usermodule.role.wrapper.RoleEquipWrapper;
import com.prodog.usermodule.role.wrapper.RoleSkillWrapper;
import com.prodog.utils.bean.SpringBeanUtils;

import java.util.concurrent.atomic.AtomicLong;

public class RoleInfoUtil {
    public static String getPrefix(RoleInfo info) {
        StringBuilder res = new StringBuilder(info.getRoleName());
        RoleEquip roleEquip = SpringBeanUtils.getBean(RoleEquipWrapper.class).getById(info.getId());
        if (roleEquip.getTitle() != null) {
            ActiveEquipItem equ = SpringBeanUtils.getBean(ActiveEquipWrapper.class).getById(roleEquip.getTitle());
            if (equ != null) res.append("[").append(equ.getSuperName()).append("]");
        }
        return res.append("：").toString();
    }

    public static String getLevelProPrefix(RoleInfo info) {
        Profession profession = SpringBeanUtils.getBean(ProfessionWrapper.class).getById(info.getProfessionId());
        return "[" + info.getLevel() + "级]" + info.getRoleName() + " (" + profession.getProfessionName() + ")";
    }

    public static long getSp(RoleInfo info) {
        BaseConfig baseConfig = SpringBeanUtils.getBean(BaseConfig.class);
        BirthConfig birthConfig = SpringBeanUtils.getBean(BirthConfig.class);
        AtomicLong sp = new AtomicLong(birthConfig.getSp() + (info.getLevel() - 1) * baseConfig.getUpLevelSp());

        RoleSkillWrapper roleSkillWrapper = SpringBeanUtils.getBean(RoleSkillWrapper.class);
        SkillWrapper skillWrapper = SpringBeanUtils.getBean(SkillWrapper.class);
        RoleSkill roleSkill = roleSkillWrapper.getById(info.getId());
        roleSkill.getLearnedSkill().forEach((skillId, level) -> {
            Skill skill = skillWrapper.getById(skillId);
            sp.addAndGet(-skill.getConsumeSp() * level);
        });
        return sp.get();
    }

    public static long getEnergy(RoleInfo info) {
        return Math.min(info.getEnergy(), SpringBeanUtils.getBean(BaseConfig.class).getBaseEnergy());
    }
}
