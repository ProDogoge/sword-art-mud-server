package com.prodog.usermodule.role.service;

import com.prodog.command.entity.CommandResult;
import com.prodog.command.entity.RoleGameCommand;
import com.prodog.gamemodule.item.entity.ActiveEquipItem;
import com.prodog.utils.exception.CommonException;

import java.util.List;

public interface EquipService {
    /***
     * 查看装备
     * @param command
     * @return
     */
    CommandResult equipInfos(RoleGameCommand command);

    CommandResult searchEquip(RoleGameCommand command, String equipName) throws CommonException;

    CommandResult activeEquip(RoleGameCommand command, String equipNames) throws CommonException;

    CommandResult wearEquip(RoleGameCommand command, String equipNames) throws CommonException;

    CommandResult wearEquip(RoleGameCommand command, List<ActiveEquipItem> activeEquIds) throws CommonException;

    CommandResult searchActiveEquip(RoleGameCommand command, String activeEquName) throws CommonException;

    CommandResult removeEquip(RoleGameCommand command, List<String> parts) throws CommonException;

    CommandResult removeEquip(RoleGameCommand command, String parts) throws CommonException;
}
