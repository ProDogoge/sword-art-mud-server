package com.prodog.usermodule.role.util;

import cn.hutool.core.util.StrUtil;
import com.prodog.gamemodule.config.dict.DictConfig;
import com.prodog.gamemodule.config.dict.MapDictConfig;
import com.prodog.gamemodule.item.entity.ActiveEquipItem;
import com.prodog.gamemodule.item.entity.PropItem;
import com.prodog.utils.bean.SpringBeanUtils;

public class PropItemUtil {
    public static String baseInfo(PropItem prop) {
        DictConfig dictConfig = SpringBeanUtils.getBean(DictConfig.class);
        MapDictConfig mapDictConfig = SpringBeanUtils.getBean(MapDictConfig.class);
        StringBuilder baseInfo = new StringBuilder();
        //基础信息
        baseInfo.append("名称：").append(prop.getName()).append("\n");
        baseInfo.append("类型：").append(mapDictConfig.get("propType", prop.getPropType())).append("\n");
        baseInfo.append("品质：").append(mapDictConfig.get("rarity", String.valueOf(prop.getRarity()))).append("\n");
        baseInfo.append("交易类型：").append(mapDictConfig.get("tradeType", String.valueOf(prop.getRarity()))).append("\n");
        baseInfo.append("要求等级：").append(prop.getRequiredLevel()).append("\n");
        baseInfo.append("出售价格：").append(prop.getSellPrice()).append(dictConfig.get("money")).append("\n");
        return baseInfo.toString().trim();
    }

    public static String baseInfo(ActiveEquipItem equip) {
        DictConfig dictConfig = SpringBeanUtils.getBean(DictConfig.class);
        MapDictConfig mapDictConfig = SpringBeanUtils.getBean(MapDictConfig.class);
        StringBuilder baseInfo = new StringBuilder();
        //基础信息
        baseInfo.append("名称：");
        if (equip.getEnhanceLevel() > 0) baseInfo.append("+").append(equip.getEnhanceLevel()).append(" ");
        baseInfo.append(equip.getName()).append("\n");
        baseInfo.append("类型：").append(dictConfig.get(equip.getEquipType())).append("\n");
        if (equip.getSecondType() != null)
            baseInfo.append("分类：").append(dictConfig.get(equip.getSecondType())).append("\n");
        baseInfo.append("品质：").append(mapDictConfig.get("rarity", String.valueOf(equip.getRarity()))).append("\n");
        baseInfo.append("交易类型：").append(mapDictConfig.get("tradeType", String.valueOf(equip.getRarity()))).append("\n");
        baseInfo.append("要求等级：").append(equip.getRequiredLevel()).append("\n");
        baseInfo.append("出售价格：").append(equip.getSellPrice()).append(dictConfig.get("money")).append("\n");
        return baseInfo.toString().trim();
    }

    public static String propInfo(PropItem prop) {
        StringBuilder res = new StringBuilder();
        res.append(PropItemUtil.baseInfo(prop)).append("\n");
        if (StrUtil.isNotBlank(prop.getDesc())) {
            res.append("[描述]\n").append(prop.getDesc()).append("\n");
        }
        return res.toString().trim();
    }
}
