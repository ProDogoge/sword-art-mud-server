package com.prodog.usermodule.role.util;

import com.prodog.database.wrapper.DataWrapper;
import com.prodog.gamemodule.item.entity.GameItem;
import com.prodog.gamemodule.item.wrapper.inf.GameItemWrapper;
import com.prodog.utils.bean.SpringBeanUtils;
import org.springframework.stereotype.Component;

import javax.annotation.PostConstruct;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

@Component
public class GameItemUtil {
    private static final List<GameItemWrapper<? extends GameItem, String>> wrapperList = new ArrayList<>();

    @PostConstruct
    private void initWrapperList() {
        SpringBeanUtils.getBeansOfType(GameItemWrapper.class).values().forEach(wrapper -> wrapperList.add(wrapper));
    }

    public static GameItem getById(String itemId) {
        for (DataWrapper<? extends GameItem, String> wrapper : wrapperList) {
            GameItem item = wrapper.getById(itemId);
            if (item != null) return item;
        }
        return null;
    }

    public static <T> T getById(String itemId, Class<T> clz) {
        return (T) getById(itemId);
    }

    public static GameItem getByName(String name) {
        for (DataWrapper<? extends GameItem, String> wrapper : wrapperList) {
            GameItem item = wrapper.getByColumn("name", name);
            if (item != null) return item;
        }
        return null;
    }

    public static <T> T getByName(String name, Class<T> clz) {
        return (T) getByName(name);
    }

    public static String itemNumsStr(Map<String, Long> itemNums, String format, String separator) {
        List<String> strs = new ArrayList<>();
        itemNums.forEach((k, v) -> {
            String itemName = getById(k).getName();
            strs.add(format.replace("{{itemName}}", itemName).replace("{{nums}}", v + ""));
        });
        return String.join(separator, strs);
    }
}
