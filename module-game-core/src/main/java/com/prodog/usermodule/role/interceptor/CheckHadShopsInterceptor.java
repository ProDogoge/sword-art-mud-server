package com.prodog.usermodule.role.interceptor;

import com.prodog.command.entity.GameCommand;
import com.prodog.command.entity.RoleGameCommand;
import com.prodog.command.interceptor.Interceptor;
import com.prodog.command.interceptor.InterceptorRes;
import com.prodog.gamemodule.gameworld.map.entity.GameMap;
import com.prodog.gamemodule.gameworld.map.wrapper.GameMapWrapper;
import com.prodog.gamemodule.gameworld.npc.entity.GameNpc;
import com.prodog.gamemodule.gameworld.npc.wrapper.GameNpcWrapper;
import com.prodog.usermodule.role.entity.RoleInfo;
import com.prodog.usermodule.role.util.RoleInfoUtil;
import com.prodog.usermodule.role.wrapper.RoleInfoWrapper;
import com.prodog.usermodule.user.entity.UserInfo;
import com.prodog.usermodule.user.wrapper.UserInfoWrapper;
import com.prodog.utils.exception.CommonException;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Component;

import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.stream.Collectors;

@Component
@RequiredArgsConstructor
public class CheckHadShopsInterceptor implements Interceptor {
    private final RoleInfoWrapper roleInfoWrapper;
    private final UserInfoWrapper userInfoWrapper;
    private final GameMapWrapper mapWrapper;
    private final GameNpcWrapper npcWrapper;

    @Override
    public InterceptorRes intercept(GameCommand command, Map<String, Object> params, String formatMsg) throws CommonException {
        RoleInfo roleInfo = null;
        if (command instanceof RoleGameCommand) {
            roleInfo = ((RoleGameCommand) command).getRoleInfo();
        }
        if (roleInfo == null) {
            UserInfo userInfo = userInfoWrapper.getById(command.getFromQQ());
            roleInfo = roleInfoWrapper.getById(userInfo.getRoleId());
        }

        //检查是否存在商店
        GameMap map = mapWrapper.getById(roleInfo.getMapId());
        String prefix = RoleInfoUtil.getPrefix(roleInfo);
        CommonException.bySupplier(() -> map.getNpcs() == null || map.getNpcs().size() == 0, prefix + "当前位置没有商店。");

        List<GameNpc> npcs = map.getNpcs().stream().map(npcWrapper::getById).collect(Collectors.toList());
        List<String> shopIds = npcs.stream().map(GameNpc::getItemShop).filter(Objects::nonNull).collect(Collectors.toList());
        CommonException.bySupplier(() -> shopIds.size() == 0, prefix + "当前位置没有商店。");

        return InterceptorRes.pass();
    }
}
