package com.prodog.usermodule.role.service;

import com.prodog.command.entity.CommandResult;
import com.prodog.command.entity.GameCommand;
import com.prodog.command.entity.RoleGameCommand;
import com.prodog.usermodule.user.dtos.CreateRoleDTO;
import com.prodog.utils.exception.CommonException;

public interface RoleInfoService {
    /***
     * 创建角色
     * @param command 游戏命令对象
     * @param dto 角色创建DTO
     * @return
     */
    CommandResult createRole(GameCommand command, CreateRoleDTO dto) throws CommonException;

    /***
     * 切换角色
     * @param command 游戏命令对象
     * @param roleName  角色名
     * @return
     */
    CommandResult changeRole(GameCommand command, String roleName);

    /***
     * 角色列表
     * @param command 游戏命令对象
     * @return
     */
    CommandResult roleList(GameCommand command);

    /***
     * 删除角色
     * @param command 游戏命令对象
     * @param roleName 角色名
     * @return
     */
    CommandResult deleteRole(GameCommand command, String roleName);

    /***
     * 确认删除
     * @param command 游戏命令对象
     * @return
     */
    CommandResult confirmDelete(GameCommand command);

    /***
     * 取消删除
     * @param command 游戏命令对象
     * @return
     */
    CommandResult cancelDelete(GameCommand command);

    /***
     * 角色信息
     * @param command 游戏命令对象
     * @return
     */
    CommandResult roleInfo(GameCommand command);

    /***
     * 选择职业
     * @param cmd 游戏命令对象
     * @param profesionName 职业名称
     * @return
     */
    CommandResult selectProfession(RoleGameCommand cmd, String profesionName);

    /***
     * 角色属性
     * @param cmd 游戏命令对象
     * @return
     */
    CommandResult roleProp(RoleGameCommand cmd);

    CommandResult update(RoleGameCommand cmd, int lev);

    CommandResult revive(RoleGameCommand cmd);

    CommandResult userInfo(RoleGameCommand cmd);

    CommandResult rolePropDetail(RoleGameCommand cmd);
}
