package com.prodog.usermodule.role.entity;

import lombok.AllArgsConstructor;
import lombok.Data;

@Data
@AllArgsConstructor
public class RoleBagItem {
    private String itemId;  //物品ID
    private long nums;   //数量
}
