package com.prodog.usermodule.role.vo;

import lombok.Data;

@Data
public class RoleBagItemVo {
    private String itemName;
    private int type;
    private String typeName;
    private String rarityName;
    private long nums;

}
