package com.prodog.usermodule.role.handler;

import com.prodog.command.annonations.*;
import com.prodog.command.entity.CommandResult;
import com.prodog.command.entity.RoleGameCommand;
import com.prodog.usermodule.role.service.RoleBagService;
import com.prodog.usermodule.user.interceptor.NotRegistInterceptor;
import lombok.RequiredArgsConstructor;

import java.util.HashMap;

@CommandBean(setName = "背包操作")
@RequiredArgsConstructor
@Iptors(vals = @Iptor(value = NotRegistInterceptor.class))
public class RoleBagHandler {
    private final RoleBagService roleBagService;

    private static final HashMap<String, Integer> typeMap = new HashMap<String, Integer>() {{
        put("材料", 1);
        put("道具", 2);
        put("装备", 3);
        put("实例", 4);
        put("徽章", 5);
        put("none", -1);
    }};

    @Command(value = {
            "背包(材料|道具|装备|实例|徽章)*\\s*(\\d*)",
            "背包(材料|道具|装备|实例|徽章)*搜索\\s+(.+?)\\s+(\\d*)"
    }, props = {"label,pno", "label,key,pno"})
    public CommandResult bagItems(RoleGameCommand command, @Default("none") String label, String key, @Default("1") Integer pno) {
        try {
            return roleBagService.bagItems(command, typeMap.get(label), label, key, pno);
        } catch (Exception e) {
            e.printStackTrace();
            return CommandResult.failure("服务器错误，背包信息获取失败。");
        }
    }

    @Command(value = {"背包(材料|道具|装备|实例|徽章)*搜索\\s+(.+)"}, props = {"label,key,pno"})
    public CommandResult bagSearch(RoleGameCommand command, @Default("none") String label, String key, @Default("1") Integer pno) {
        try {
            return roleBagService.bagItems(command, typeMap.get(label), label, key, pno);
        } catch (Exception e) {
            e.printStackTrace();
            return CommandResult.failure("服务器错误，背包信息获取失败。");
        }
    }

}
