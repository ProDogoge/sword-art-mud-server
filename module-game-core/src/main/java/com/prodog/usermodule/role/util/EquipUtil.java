package com.prodog.usermodule.role.util;

import cn.hutool.core.util.ReflectUtil;
import cn.hutool.core.util.StrUtil;
import com.prodog.commonmodule.prop.entity.ObjProp;
import com.prodog.gamemodule.config.dict.DictConfig;
import com.prodog.gamemodule.config.dict.MapDictConfig;
import com.prodog.gamemodule.config.enhance.EnhanceConfig;
import com.prodog.gamemodule.item.entity.ActiveEquipItem;
import com.prodog.gamemodule.item.entity.BadgeItem;
import com.prodog.gamemodule.item.entity.EquipItem;
import com.prodog.gamemodule.item.wrapper.BadgeWrapper;
import com.prodog.gamemodule.secondProfession.getter.EnhanceSuccessRateGetter;
import com.prodog.usermodule.caculate.CalVal;
import com.prodog.usermodule.role.entity.RoleEquip;
import com.prodog.utils.bean.SpringBeanUtils;

import java.lang.reflect.Field;
import java.util.Arrays;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

public class EquipUtil {
    public static final Field[] roleEquipFields = ReflectUtil.getFields(RoleEquip.class);

    static {
        for (Field roleEquipField : roleEquipFields) roleEquipField.setAccessible(true);
    }

    public static String baseInfo(EquipItem equip) {
        DictConfig dictConfig = SpringBeanUtils.getBean(DictConfig.class);
        MapDictConfig mapDictConfig = SpringBeanUtils.getBean(MapDictConfig.class);
        StringBuilder baseInfo = new StringBuilder();
        //基础信息
        baseInfo.append("名称：").append(equip.getName()).append("\n");
        baseInfo.append("类型：").append(dictConfig.get(equip.getEquipType())).append("\n");
        if (equip.getSecondType() != null)
            baseInfo.append("分类：").append(dictConfig.get(equip.getSecondType())).append("\n");
        baseInfo.append("品质：").append(mapDictConfig.get("rarity", String.valueOf(equip.getRarity()))).append("\n");
        baseInfo.append("交易类型：").append(mapDictConfig.get("tradeType", String.valueOf(equip.getTradeType()))).append("\n");
        baseInfo.append("要求等级：").append(equip.getRequiredLevel()).append("\n");
        baseInfo.append("徽章栏：").append(equip.getMaxBadgeNums()).append("\n");
        baseInfo.append("出售价格：").append(equip.getSellPrice()).append(dictConfig.get("money")).append("\n");
        return baseInfo.toString().trim();
    }

    public static String baseInfo(ActiveEquipItem equip, String roleId) {
        DictConfig dictConfig = SpringBeanUtils.getBean(DictConfig.class);
        MapDictConfig mapDictConfig = SpringBeanUtils.getBean(MapDictConfig.class);
        StringBuilder baseInfo = new StringBuilder();
        //基础信息
        baseInfo.append("名称：");
        if (equip.getEnhanceLevel() > 0) baseInfo.append("+").append(equip.getEnhanceLevel()).append(" ");
        baseInfo.append(equip.getName()).append("\n");
        baseInfo.append("类型：").append(dictConfig.get(equip.getEquipType())).append("\n");
        if (equip.getSecondType() != null)
            baseInfo.append("分类：").append(dictConfig.get(equip.getSecondType())).append("\n");
        baseInfo.append("品质：").append(mapDictConfig.get("rarity", String.valueOf(equip.getRarity()))).append("\n");
        baseInfo.append("交易类型：").append(mapDictConfig.get("tradeType", String.valueOf(equip.getTradeType()))).append("\n");
        baseInfo.append("要求等级：").append(equip.getRequiredLevel()).append("\n");
        baseInfo.append("徽章栏：").append(equip.getMaxBadgeNums()).append("\n");
        baseInfo.append("出售价格：").append(equip.getSellPrice()).append(dictConfig.get("money")).append("\n");

        //强化信息
        EnhanceConfig enhanceConfig = SpringBeanUtils.getBean(EnhanceConfig.class);
        if (equip.getEnhanceLevel() < enhanceConfig.getMaxUpLevel()) {
            baseInfo.append("强化所需：");
            long money = equip.getEnhanceMoney();
            long copoun = equip.getEnhanceCopoun();
            Map<String, Long> reqItems = equip.getEnhanceReqItems();
            if (money > 0) baseInfo.append(money).append(dictConfig.get("money")).append("  ");
            if (copoun > 0) baseInfo.append(money).append(dictConfig.get("copoun")).append("  ");
            baseInfo.append("材料:").append(GameItemUtil.itemNumsStr(reqItems, "{{itemName}}x{{nums}}", "、")).append("  ");
            Double baseSuccessRate = enhanceConfig.getSuccessRate().get(equip.getEnhanceLevel());
            baseInfo.append("成功率:").append(new EnhanceSuccessRateGetter().get(new CalVal<>(baseSuccessRate, baseSuccessRate), roleId)).append("%\n");
        }
        if (equip.getBadges().size() > 0) {
            baseInfo.append("[徽章]\n");
            BadgeWrapper badgeWrapper = SpringBeanUtils.getBean(BadgeWrapper.class);
            List<BadgeItem> badges = badgeWrapper.getByIds(equip.getBadges());
            for (int i = 0; i < badges.size(); i++) {
                baseInfo.append(badges.get(i).getName()).append(" (").append(i + 1).append(")").append("\n");
            }
        }
        return baseInfo.toString().trim();
    }

    public static String propInfo(ObjProp prop) {
        DictConfig dictConfig = SpringBeanUtils.getBean(DictConfig.class);
        List<String> ignoreField = Arrays.asList("id", "level", "race", "curHp", "curMp");
        List<String> percentField = Arrays.asList("hpReplyRate", "mpReplyRate", "physicalCrit",
                "magicalCrit", "criticalDamage", "physicalThroughRate",
                "magicalThroughRate", "physicalHpAbsorb", "magicalHpAbsorb",
                "physicalMpAbsorb", "magicalMpAbsorb", "addDamageRate",
                "reduceDamageRate", "evedge");
        StringBuilder equipPropStr = new StringBuilder();
        List<Field> fields = Arrays.stream(ReflectUtil.getFields(ObjProp.class))
                .filter(f -> !ignoreField.contains(f.getName())).collect(Collectors.toList());
        for (Field field : fields) {
            try {
                field.setAccessible(true);
                Object val = field.get(prop);
                if (val == null) continue;
                if ((val instanceof Integer && (int) val > 0) || (val instanceof Long && (long) val > 0)
                        || (val instanceof Double && (double) val > 0)) {
                    equipPropStr.append(dictConfig.get(field.getName())).append(" +").append(val);
                    if (percentField.contains(field.getName())) equipPropStr.append("%");
                    equipPropStr.append("\n");
                } else if ((val instanceof Integer && (int) val < 0) || (val instanceof Long && (long) val < 0)
                        || (val instanceof Double && (double) val < 0)) {
                    equipPropStr.append(dictConfig.get(field.getName())).append(" ").append(val);
                    if (percentField.contains(field.getName())) equipPropStr.append("%");
                    equipPropStr.append("\n");
                } else if (field.getName().equals("elemental") && prop.getElemental().size() > 0) {
                    equipPropStr.append(prop.getElementalStr()).append("属性攻击\n");
                }
            } catch (IllegalAccessException e) {
                e.printStackTrace();
            }
        }
        return equipPropStr.toString().trim();
    }

    public static String equipInfo(EquipItem equ) {
        StringBuilder res = new StringBuilder();
        res.append(EquipUtil.baseInfo(equ)).append("\n");

        //属性
        res.append("[属性]\n");
        res.append(EquipUtil.propInfo(equ.getProp())).append("\n");

        //附加效果
        if (StrUtil.isNotBlank(equ.getEffectDesc())) {
            res.append("[附加效果]\n").append(equ.getEffectDesc()).append("\n");
        }
        if (StrUtil.isNotBlank(equ.getDesc())) {
            res.append("[描述]\n").append(equ.getDesc()).append("\n");
        }
        return res.toString().trim();
    }

    public static String equipInfo(ActiveEquipItem equ, String roleId) {
        StringBuilder res = new StringBuilder();
        res.append(EquipUtil.baseInfo(equ, roleId)).append("\n");

        //属性
        res.append("[属性]\n");
        res.append(EquipUtil.propInfo(equ.getProp())).append("\n");

        //附加效果
        if (StrUtil.isNotBlank(equ.getEffectDesc())) {
            res.append("[附加效果]\n").append(equ.getEffectDesc()).append("\n");
        }
        if (StrUtil.isNotBlank(equ.getDesc())) {
            res.append("[描述]\n").append(equ.getDesc()).append("\n");
        }
        return res.toString().trim();
    }
}
