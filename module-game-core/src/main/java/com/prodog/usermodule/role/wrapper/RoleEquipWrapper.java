package com.prodog.usermodule.role.wrapper;

import com.prodog.database.annotation.Wrapper;
import com.prodog.database.wrapper.MongoDataWrapper;
import com.prodog.usermodule.role.entity.RoleEquip;

@Wrapper(path = "玩家数据/角色/角色装备", module = "角色装备")
public class RoleEquipWrapper extends MongoDataWrapper<RoleEquip, String> {
}
