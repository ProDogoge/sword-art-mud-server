package com.prodog.usermodule.role.constant;

public class RoleState {
    public static final int NORMAL = 1; //正常
    public static final int FIGHT = 2;   //战斗
    public static final int DEAD = 3;   //死亡
    public static final int SLEEP = 4;    //睡觉
    public static final int FORGE = 5;  //锻造
}
