package com.prodog.usermodule.user.interceptor;

import com.prodog.command.entity.GameCommand;
import com.prodog.command.interceptor.Interceptor;
import com.prodog.command.interceptor.InterceptorRes;
import com.prodog.usermodule.user.entity.UserInfo;
import com.prodog.usermodule.user.wrapper.UserInfoWrapper;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Component;

import java.util.Map;

@Component
@RequiredArgsConstructor
public class NotRegistInterceptor implements Interceptor {
    private final UserInfoWrapper userInfoWrapper;

    @Override
    public InterceptorRes intercept(GameCommand command, Map<String, Object> params, String formatMsg) {
        UserInfo userInfo = userInfoWrapper.getById(command.getFromQQ());
        if (userInfo != null) {
            return InterceptorRes.pass();
        }
        return InterceptorRes.intercept("您还没有创建角色，请先创建角色。 创建格式：创建角色 角色名 性别");
    }
}
