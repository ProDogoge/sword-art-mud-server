package com.prodog.usermodule.user.dtos;

import lombok.Data;

@Data
public class CreateRoleDTO {
    private String roleName;
    private String sex;
}
