package com.prodog.gamemodule.config;

import com.prodog.database.annotation.Config;
import lombok.Data;

import java.util.List;

@Config(path = "游戏配置/配置/服务器配置.json", module = "服务器配置")
@Data
public class ServerConfig {
    private List<String> gameMasters;   //超级管理员
    private String version; //版本号
    private String moneyName;    //金币名称
    private String copounName; //点券名称
    private Integer roleLimit;  //限制注册角色个数
    private Integer maxLevel;   //最大等级

    public Integer getMaxLevel() {
        return maxLevel == null ? Integer.MAX_VALUE : maxLevel;
    }
}
