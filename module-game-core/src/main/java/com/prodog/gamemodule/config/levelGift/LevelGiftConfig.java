package com.prodog.gamemodule.config.levelGift;

import com.prodog.database.annotation.Config;
import lombok.Data;

import java.util.List;

@Config(path = "游戏配置/配置/成长礼包配置.json", module = "成长礼包配置")
@Data
public class LevelGiftConfig {
    private List<LevelGift> gifts;
}
