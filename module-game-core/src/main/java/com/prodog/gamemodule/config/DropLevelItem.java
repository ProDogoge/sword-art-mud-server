package com.prodog.gamemodule.config;

import lombok.Data;

@Data
public class DropLevelItem {
    private int dropLevel;
    private int weight;
}
