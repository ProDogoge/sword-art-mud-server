package com.prodog.gamemodule.config;

import com.prodog.database.annotation.Config;
import lombok.Data;

@Config(path = "游戏配置/配置/基础配置.json", module = "基础配置")
@Data
public class BaseConfig {
    /******经验******/
    private int maxLevel;
    private long upLevelSp; //升级获得的技能点
    private int expAttenLevel;  //经验衰减等级
    private int attenRate;  //衰减率

    /******死亡惩罚******/
    private int deadProtectLevel;   //死亡保护等级
    private int deadLoseExpRate;    //死亡丢失经验率
    private int deadLoseMoneyRate;  //失望丢失货币率

    /******精力******/
    private long baseEnergy;    //基础精力
    private long normalFightEnergy; //普通战斗消耗的精力
    private long energyMinute = 1;  //每几分钟恢复精力
    private long replyEnergy = 1;   //恢复精力值

    /******队伍******/
    private int maxTeamRoleNums = 4;    //队伍最大人数
    private int maxTeamNameLength = 12; //最大队伍名称长度
    private int maxTeamDescLength = 50; //最大队伍名称长度
    private long applyTtl = 1000 * 30;  //申请过期时间
    private long inviteTtl = 1000 * 30; //邀请过期时间
}
