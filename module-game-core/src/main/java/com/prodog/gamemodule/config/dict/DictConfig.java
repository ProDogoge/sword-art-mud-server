package com.prodog.gamemodule.config.dict;

import com.prodog.database.annotation.Config;
import lombok.Data;

import java.util.Map;

@Config(path = "游戏配置/配置/字典配置.json", module = "字典配置")
@Data
public class DictConfig {
    private Map<String, String> dicts;

    public String get(String dictName) {
        return dicts.get(dictName);
    }
}
