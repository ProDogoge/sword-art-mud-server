package com.prodog.gamemodule.config.sign;

import com.prodog.database.annotation.Config;
import lombok.Data;

import java.util.ArrayList;
import java.util.List;

@Config(path = "游戏配置/配置/签到配置.json", module = "签到配置")
@Data
public class SignInConfig {
    private List<SignInLevelConfig> signInLevelConfigs = new ArrayList<>(); //各等级范围的签到配置
}
