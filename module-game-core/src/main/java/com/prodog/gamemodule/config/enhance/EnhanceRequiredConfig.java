package com.prodog.gamemodule.config.enhance;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.LinkedHashMap;
import java.util.Map;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class EnhanceRequiredConfig {
    private int startLevel;
    private int endLevel;
    private Map<String, String> requiredItems = new LinkedHashMap<>();
    private String money;
    private String copoun;
}
