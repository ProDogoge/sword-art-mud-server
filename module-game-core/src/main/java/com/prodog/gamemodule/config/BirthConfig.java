package com.prodog.gamemodule.config;

import com.prodog.database.annotation.Config;
import lombok.Data;

@Config(path = "游戏配置/配置/出生配置.json", module = "出生配置")
@Data
public class BirthConfig {
    private long money; //出生珂尔量
    private long copoun;    //出生点券量
    private String birthProfession; //出生职业
    private String birthTitle;  //出生称号
    private String birthMap;    //出生地图
    private int bagItemNums;    //出生背包容量
    private long sp;    //出生SP
}
