package com.prodog.gamemodule.config.sign;

import lombok.Data;

import java.util.ArrayList;
import java.util.List;

@Data
public class SignInItem {
    private List<String> items = new ArrayList<>(); //签到可得的物品
    private long money; //珂尔
    private long copoun; //点券
}
