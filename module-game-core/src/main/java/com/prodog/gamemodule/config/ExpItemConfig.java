package com.prodog.gamemodule.config;

import lombok.Data;

import java.util.HashMap;
import java.util.Map;

@Data
public class ExpItemConfig {
    private int startLevel;
    private int endLevel;
    private String expression;
    private Map<String, Object> params = new HashMap<>();
}
