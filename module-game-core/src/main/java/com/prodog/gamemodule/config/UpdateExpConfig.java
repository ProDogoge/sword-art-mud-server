package com.prodog.gamemodule.config;

import com.prodog.database.annotation.Config;
import lombok.Data;

import java.util.ArrayList;
import java.util.List;

@Config(path = "游戏配置/配置/升级经验公式配置.json", module = "升级经验公式配置")
@Data
public class UpdateExpConfig {
    private List<ExpItemConfig> settings = new ArrayList<>();

    public ExpItemConfig getItemConfig(int level) {
        return settings.stream()
                .filter(item -> level >= item.getStartLevel() && level <= item.getEndLevel())
                .findAny().get();
    }
}
