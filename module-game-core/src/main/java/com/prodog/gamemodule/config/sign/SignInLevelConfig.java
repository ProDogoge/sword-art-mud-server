package com.prodog.gamemodule.config.sign;

import lombok.Data;

import java.util.ArrayList;
import java.util.List;

@Data
public class SignInLevelConfig {
    private int startLevel; //开始等级
    private int endLevel;   //结束等级
    private List<SignInItem> signInItems = new ArrayList<>(); //周一~周日签到可得配置
}
