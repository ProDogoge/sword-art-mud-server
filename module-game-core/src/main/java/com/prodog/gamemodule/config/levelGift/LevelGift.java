package com.prodog.gamemodule.config.levelGift;

import lombok.Data;

import java.util.List;

@Data
public class LevelGift {
    private String name;
    private int requiredLevel = 1;
    private List<String> content;
    private long money;
    private long copoun;
}
