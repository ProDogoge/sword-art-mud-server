package com.prodog.gamemodule.config.enhance;

import com.alibaba.fastjson2.annotation.JSONField;
import com.prodog.database.annotation.Config;
import com.prodog.gamemodule.config.DropLevelItem;
import lombok.Data;

import java.util.List;
import java.util.Map;

@Config(path = "游戏配置/配置/装备强化配置.json", module = "装备强化配置")
@Data
public class EnhanceConfig {
    private int maxUpLevel;
    private String upPropExpression;
    private Map<String, List<String>> enhanceProp;
    private List<Double> successRate;
    private List<List<DropLevelItem>> dropLevel;
    private List<EnhanceRequiredConfig> requiredConfigs;

    @JSONField(serialize = false)
    public EnhanceRequiredConfig getRequiredConfig(int level) {
        return requiredConfigs.stream().filter(r -> r.getStartLevel() <= level && level <= r.getEndLevel()).findFirst().get();
    }
}
