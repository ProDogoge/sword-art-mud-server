package com.prodog.gamemodule.kill.handler;

import com.prodog.command.annonations.*;
import com.prodog.command.entity.CommandResult;
import com.prodog.command.entity.RoleGameCommand;
import com.prodog.gamemodule.kill.service.KillRecordService;
import com.prodog.usermodule.role.interceptor.StateInterceptor;
import com.prodog.usermodule.user.interceptor.NotRegistInterceptor;
import com.prodog.utils.exception.CommonException;
import lombok.RequiredArgsConstructor;

@CommandBean(setName = "击杀记录操作")
@RequiredArgsConstructor
@Iptors(vals = @Iptor(value = NotRegistInterceptor.class))
public class KillRecordHandler {
    private final KillRecordService killRecordService;

    @Command(value = {
            "击杀(.*)记录\\s*(\\d+)\\s+(\\d+)",
            "击杀(.*)记录\\s*(\\d*)"
    }, props = {"label,pno,size", "label,pno"})
    @Iptors(vals = @Iptor(value = StateInterceptor.class, params = "{'states':[2,3],'required':false}", msg = "[{{state}}]状态下无法进行此操作。"))
    public CommandResult getRecords(RoleGameCommand command, @Default("怪物") String label, @Default("1") Integer pno, @Default("15") Integer size) {
        try {
            return killRecordService.getRecords(command, label, pno, size);
        } catch (CommonException e) {
            return CommandResult.failure(e.getMessage());
        } catch (Exception e) {
            e.printStackTrace();
            return CommandResult.failure("服务器发生错误，击杀记录获取失败。");
        }
    }
}
