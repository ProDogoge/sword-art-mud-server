package com.prodog.gamemodule.kill.entity;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.Date;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class KillRecordItem {
    private String objId;   //单位ID
    private int objType = 2;    //单位类型 同FightObj 1.玩家 2.怪物 3.召唤物 4.精英怪物 5.领主怪物
    private long nums;
    private Date lastKillTime;
}
