package com.prodog.gamemodule.kill.entity;

import com.prodog.gamemodule.kill.wrapper.KillRecordWrapper;
import com.prodog.utils.bean.SpringBeanUtils;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.data.mongodb.core.mapping.Document;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.function.Predicate;

@Document("g_kill_record")
@Data
@AllArgsConstructor
@NoArgsConstructor
public class KillRecord {
    private String id;
    private List<KillRecordItem> records = new ArrayList<>();

    public KillRecord(String id) {
        this.id = id;
    }

    public void addRecord(String objId, int objType, long nums) {
        Predicate<KillRecordItem> p = r -> r.getObjId().equals(objId) && r.getObjType() == objType;
        KillRecordItem record = records.stream().filter(p).findAny().orElseGet(() -> {
            KillRecordItem item = new KillRecordItem(objId, objType, 0, new Date());
            records.add(item);
            return item;
        });
        record.setNums(record.getNums() + nums);
    }

    public void addRecord(String objId, int objType) {
        addRecord(objId, objType, 1);
    }

    public void addRecordAndSave(String objId, int objType, long nums) {
        addRecord(objId, objType, nums);
        SpringBeanUtils.getBean(KillRecordWrapper.class).save(this);
    }

    public void addRecordAndSave(String objId, int objType) {
        addRecordAndSave(objId, objType, 1);
    }
}
