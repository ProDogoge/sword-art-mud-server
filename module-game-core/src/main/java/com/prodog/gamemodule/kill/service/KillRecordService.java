package com.prodog.gamemodule.kill.service;

import com.prodog.command.entity.CommandResult;
import com.prodog.command.entity.RoleGameCommand;
import com.prodog.utils.exception.CommonException;

public interface KillRecordService {
    CommandResult getRecords(RoleGameCommand command, String label, Integer pno, Integer size) throws CommonException;
}
