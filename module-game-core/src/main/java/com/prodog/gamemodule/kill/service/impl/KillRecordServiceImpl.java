package com.prodog.gamemodule.kill.service.impl;

import com.prodog.command.annonations.Service;
import com.prodog.command.entity.CommandResult;
import com.prodog.command.entity.RoleGameCommand;
import com.prodog.database.entity.Page;
import com.prodog.gamemodule.gameworld.monster.wrapper.MonsterWrapper;
import com.prodog.gamemodule.kill.entity.KillRecord;
import com.prodog.gamemodule.kill.entity.KillRecordItem;
import com.prodog.gamemodule.kill.service.KillRecordService;
import com.prodog.gamemodule.kill.wrapper.KillRecordWrapper;
import com.prodog.usermodule.role.util.RoleInfoUtil;
import com.prodog.usermodule.role.wrapper.RoleInfoWrapper;
import com.prodog.utils.exception.CommonException;
import lombok.RequiredArgsConstructor;

import java.util.Arrays;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.stream.Collectors;

@Service
@RequiredArgsConstructor
public class KillRecordServiceImpl implements KillRecordService {
    private final KillRecordWrapper recordWrapper;
    private final RoleInfoWrapper roleInfoWrapper;
    private final MonsterWrapper monsterWrapper;

    private final HashMap<String, List<Integer>> labelMap = new HashMap<String, List<Integer>>() {{
        put("玩家", Collections.singletonList(1));
        put("怪物", Arrays.asList(2, 4, 5));
        put("精英", Collections.singletonList(4));
        put("领主", Collections.singletonList(5));
    }};

    @Override
    public CommandResult getRecords(RoleGameCommand command, String label, Integer pno, Integer size) throws CommonException {
        String prefix = RoleInfoUtil.getPrefix(command.getRoleInfo());

        CommonException.byNull(labelMap.get(label), prefix + "没有该类型的击杀记录。");

        KillRecord killRecord = recordWrapper.getById(command.getRoleId());
        List<Integer> types = labelMap.get(label);
        List<KillRecordItem> records = killRecord.getRecords().stream().filter(r -> types.contains(r.getObjType())).collect(Collectors.toList());
        Page<KillRecordItem> page = Page.page(records, KillRecordItem.class, pno, size);

        StringBuilder res = new StringBuilder(prefix).append("您的").append(label).append("击杀记录如下:\n");
        if (page.getDatas().size() == 0) res.append("您目前没有").append(label).append("击杀记录。\n");
        for (KillRecordItem record : page.getDatas()) {
            String name;
            if (record.getObjType() == 1) name = roleInfoWrapper.getById(record.getObjId()).getRoleName();
            else name = monsterWrapper.getById(record.getObjId()).getName();
            res.append(name).append(": ").append(record.getNums()).append("\n");
        }
        res.append(page.getBottom());
        return CommandResult.success(res.toString().trim());
    }
}
