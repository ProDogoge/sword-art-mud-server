package com.prodog.gamemodule.kill.wrapper;

import com.prodog.database.annotation.Wrapper;
import com.prodog.database.wrapper.MongoDataWrapper;
import com.prodog.gamemodule.kill.entity.KillRecord;

import java.util.Optional;

@Wrapper(path = "玩家数据/角色/击杀记录", module = "击杀记录数据")
public class KillRecordWrapper extends MongoDataWrapper<KillRecord, String> {
    @Override
    public KillRecord getById(String id) {
        KillRecord killRecord = Optional.ofNullable(super.getById(id)).orElse(new KillRecord(id));
        save(killRecord);
        return killRecord;
    }
}
