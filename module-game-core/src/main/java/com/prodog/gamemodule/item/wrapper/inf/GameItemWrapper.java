package com.prodog.gamemodule.item.wrapper.inf;

import com.prodog.database.wrapper.DataWrapper;
import com.prodog.gamemodule.item.entity.GameItem;

public interface GameItemWrapper<T extends GameItem, P> extends DataWrapper<T, P> {
}
