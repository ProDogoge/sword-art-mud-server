package com.prodog.gamemodule.item.entity;

import lombok.AllArgsConstructor;
import lombok.Data;
import org.springframework.data.mongodb.core.mapping.Document;

/***
 * 装备编号
 */
@Document("g_equip_serial_num")
@Data
@AllArgsConstructor
public class EquipSerialNum {
    private String id;
    private long serialNum;
}
