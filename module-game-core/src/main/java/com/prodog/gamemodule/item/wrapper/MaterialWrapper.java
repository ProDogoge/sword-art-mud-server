package com.prodog.gamemodule.item.wrapper;

import com.prodog.database.annotation.Wrapper;
import com.prodog.gamemodule.item.entity.MaterialItem;
import com.prodog.gamemodule.item.wrapper.inf.LocalGameItemWrapper;

@Wrapper(path = "游戏数据/物品/材料", module = "材料数据")
public class MaterialWrapper extends LocalGameItemWrapper<MaterialItem, String> {
}
