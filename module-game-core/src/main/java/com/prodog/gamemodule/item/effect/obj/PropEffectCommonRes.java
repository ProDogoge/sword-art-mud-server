package com.prodog.gamemodule.item.effect.obj;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class PropEffectCommonRes {
    private String res;
    private long actualNums;
    private boolean success;
}
