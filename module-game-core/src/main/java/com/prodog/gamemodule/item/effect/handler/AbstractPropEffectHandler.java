package com.prodog.gamemodule.item.effect.handler;

import com.prodog.gamemodule.item.effect.obj.PropEffect;
import lombok.AllArgsConstructor;

import java.util.Map;

@AllArgsConstructor
public abstract class AbstractPropEffectHandler implements PropEffectHandler {
    private PropEffect effect;

    public Map<String, Object> getParams() {
        return effect.getParams();
    }

    public Object get(String prop) {
        return effect.getParams().get(prop);
    }

    public Object contains(String prop) {
        return effect.getParams().containsKey(prop);
    }
}
