package com.prodog.gamemodule.item.filter.handler;

import com.prodog.gamemodule.config.BaseConfig;
import com.prodog.gamemodule.item.entity.PropItem;
import com.prodog.gamemodule.item.filter.obj.PropFilter;
import com.prodog.gamemodule.item.filter.obj.PropFilterRes;
import com.prodog.usermodule.role.entity.RoleInfo;
import com.prodog.usermodule.role.util.RoleInfoUtil;
import com.prodog.utils.bean.SpringBeanUtils;

import java.util.Map;

public class CheckEnergyHandler extends AbstractPropFilterHandler {
    public CheckEnergyHandler(PropItem propItem, PropFilter filter) {
        super(propItem, filter);
    }

    @Override
    public PropFilterRes commonFilter(RoleInfo info, PropItem propItem, Map<String, Object> params, long nums) {
        long loseEnergy = SpringBeanUtils.getBean(BaseConfig.class).getBaseEnergy() - RoleInfoUtil.getEnergy(info);
        if (loseEnergy == 0)
            return new PropFilterRes((String) getParams().get("failMsg"), false);
        return new PropFilterRes(null, true);
    }
}
