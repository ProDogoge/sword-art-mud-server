package com.prodog.gamemodule.item.service.impl;

import com.prodog.command.annonations.Service;
import com.prodog.command.entity.CommandResult;
import com.prodog.command.entity.RoleGameCommand;
import com.prodog.gamemodule.config.dict.MapDictConfig;
import com.prodog.gamemodule.item.effect.handler.PropEffectManager;
import com.prodog.gamemodule.item.entity.PropItem;
import com.prodog.gamemodule.item.filter.handler.PropFilterManager;
import com.prodog.gamemodule.item.filter.obj.PropFilterRes;
import com.prodog.gamemodule.item.service.PropItemService;
import com.prodog.gamemodule.item.wrapper.PropWrapper;
import com.prodog.usermodule.role.util.BagUtil;
import com.prodog.usermodule.role.util.PropItemUtil;
import com.prodog.usermodule.role.util.RoleInfoUtil;
import com.prodog.utils.exception.CommonException;
import lombok.RequiredArgsConstructor;

import java.util.HashMap;
import java.util.Map;

@Service
@RequiredArgsConstructor
public class PropItemServiceImpl implements PropItemService {
    private final PropWrapper propWrapper;
    private final MapDictConfig mapDictConfig;

    public CommandResult useProp(RoleGameCommand cmd, PropItem prop, Map<String, Object> params, Long nums) throws CommonException {
        String prefix = RoleInfoUtil.getPrefix(cmd.getRoleInfo());
        CommonException.bySupplier(() -> !BagUtil.checkIsHad(cmd.getRoleId(), prop.getId(), nums), prefix + "使用失败，您背包中的[" + prop.getName() + "]数量不足。");
        CommonException.bySupplier(() -> prop.getRequiredLevel() > cmd.getRoleInfo().getLevel(), prefix + "使用失败，您的当前等级无法使用[" + prop.getName() + "]，要求等级:" + prop.getRequiredLevel() + "。");
        CommonException.bySupplier(() -> !prop.getAllowState().contains(cmd.getRoleInfo().getState()), prefix + "使用失败，[" + mapDictConfig.get("roleState", cmd.getRoleInfo().getState() + "") + "]状态下无法使用该物品。");
        PropFilterRes filterRes = PropFilterManager.commonFilter(cmd.getRoleInfo(), prop, null, nums);
        CommonException.bySupplier(() -> !filterRes.isSuccess(), prefix + filterRes.getRes());
        String res = PropEffectManager.commonHandle(cmd.getRoleInfo(), prop, params, nums);
        return CommandResult.success(res);
    }

    public CommandResult useProp(RoleGameCommand cmd, String propName, Map<String, Object> params, Long nums) throws CommonException {
        PropItem prop = propWrapper.getByColumn("name", propName);
        String prefix = RoleInfoUtil.getPrefix(cmd.getRoleInfo());
        CommonException.byNull(prop, prefix + "使用失败，名称为[" + propName + "]的道具不存在。");
        return useProp(cmd, prop, params, nums);
    }

    @Override
    public CommandResult useProp(RoleGameCommand cmd, String propName, Long nums) throws CommonException {
        return useProp(cmd, propName, null, nums);
    }

    @Override
    public CommandResult openGift(RoleGameCommand cmd, String itemName, Integer index, Long nums) throws CommonException {
        PropItem prop = propWrapper.getByColumn("name", itemName);
        String prefix = RoleInfoUtil.getPrefix(cmd.getRoleInfo());
        CommonException.byNull(prop, prefix + "使用失败，名称为[" + itemName + "]的道具不存在。");
        CommonException.bySupplier(() -> !prop.getPropType().equals("selectOneGift"), prefix + "打开失败，该道具类型不是单选礼包。");

        if (index == null)
            return CommandResult.failure(RoleInfoUtil.getPrefix(cmd.getRoleInfo()) + "请发送正确的命令，打开道具名称 索引。");
        Map<String, Object> params = new HashMap<String, Object>() {{
            put("index", index - 1);
        }};
        return useProp(cmd, itemName, params, nums);
    }

    @Override
    public CommandResult searchProp(RoleGameCommand cmd, String itemName) throws CommonException {
        PropItem prop = propWrapper.getByColumn("name", itemName);
        String prefix = RoleInfoUtil.getPrefix(cmd.getRoleInfo());
        CommonException.byNull(prop, prefix + "查询失败，名称为[" + itemName + "]的道具不存在。");
        StringBuilder res = new StringBuilder(prefix).append("该道具信息如下:\n");
        res.append(PropItemUtil.propInfo(prop));
        return CommandResult.success(res.toString().trim());
    }
}
