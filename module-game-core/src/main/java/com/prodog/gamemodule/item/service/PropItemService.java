package com.prodog.gamemodule.item.service;

import com.prodog.command.entity.CommandResult;
import com.prodog.command.entity.RoleGameCommand;
import com.prodog.utils.exception.CommonException;

public interface PropItemService {
    CommandResult useProp(RoleGameCommand cmd, String propName, Long nums) throws CommonException;

    CommandResult openGift(RoleGameCommand cmd, String itemName, Integer index, Long nums) throws CommonException;

    CommandResult searchProp(RoleGameCommand cmd, String itemName) throws CommonException;
}
