package com.prodog.gamemodule.item.wrapper;

import com.prodog.database.annotation.Wrapper;
import com.prodog.gamemodule.item.entity.EquipItem;
import com.prodog.gamemodule.item.wrapper.inf.LocalGameItemWrapper;

@Wrapper(path = "游戏数据/物品/装备", module = "装备数据")
public class EquipWrapper extends LocalGameItemWrapper<EquipItem, String> {
}
