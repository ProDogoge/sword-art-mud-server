package com.prodog.gamemodule.item.entity;

import lombok.Data;

import java.util.Optional;

/***
 * 游戏物品类
 */
@Data
public class GameItem {
    private String id;
    private int type;   //1.材料 2.消耗品 3.装备
    private String name;    //物品名称
    private Long price; //购买价格 出售价格正常为购买价格的1/5
    private Long sellPrice; //指定出售价格
    private Long limit; //上限持有数
    private int tradeType = 1;  //交易类型(1.可交易 2.账号绑定 3.无法交易 4.无法删除)
    private int rarity = 1; //稀有度(1.普通 2.优秀 3.精良 4.稀有 5.神器 6.传奇 7.史诗 8.神话)
    private String desc;    //描述

    public Long getPrice() {
        return Optional.ofNullable(price).orElse(0L);
    }

    public Long getSellPrice() {
        if (sellPrice == null) {
            if (price != null) {
                return price / 5;
            }
            return 1L;
        }
        return sellPrice;
    }
}
