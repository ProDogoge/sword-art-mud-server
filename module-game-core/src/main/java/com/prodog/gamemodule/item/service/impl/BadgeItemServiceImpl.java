package com.prodog.gamemodule.item.service.impl;

import cn.hutool.core.util.ReflectUtil;
import com.prodog.command.annonations.Service;
import com.prodog.command.entity.CommandResult;
import com.prodog.command.entity.RoleGameCommand;
import com.prodog.commonmodule.prop.caculate.util.PropUtil;
import com.prodog.commonmodule.prop.entity.ObjProp;
import com.prodog.gamemodule.config.dict.DictConfig;
import com.prodog.gamemodule.item.entity.ActiveEquipItem;
import com.prodog.gamemodule.item.entity.BadgeItem;
import com.prodog.gamemodule.item.service.BadgeItemService;
import com.prodog.gamemodule.item.wrapper.ActiveEquipWrapper;
import com.prodog.gamemodule.item.wrapper.BadgeWrapper;
import com.prodog.gamemodule.secondProfession.domain.dos.SecondProfessionItem;
import com.prodog.gamemodule.secondProfession.domain.po.Forger;
import com.prodog.gamemodule.secondProfession.domain.po.RoleSecondProfession;
import com.prodog.gamemodule.secondProfession.wrapper.RoleSecondProfessionWrapper;
import com.prodog.usermodule.role.entity.RoleBag;
import com.prodog.usermodule.role.entity.RoleEquip;
import com.prodog.usermodule.role.util.BagUtil;
import com.prodog.usermodule.role.util.RoleInfoUtil;
import com.prodog.usermodule.role.wrapper.RoleBagWrapper;
import com.prodog.usermodule.role.wrapper.RoleEquipWrapper;
import com.prodog.utils.exception.CommonException;
import lombok.RequiredArgsConstructor;
import org.springframework.transaction.annotation.Transactional;

@Service
@RequiredArgsConstructor
public class BadgeItemServiceImpl implements BadgeItemService {
    private final RoleSecondProfessionWrapper roleSecondProfessionWrapper;
    private final Forger forger;
    private final BadgeWrapper badgeWrapper;
    private final ActiveEquipWrapper activeEquipWrapper;
    private final RoleBagWrapper roleBagWrapper;
    private final RoleEquipWrapper roleEquipWrapper;
    private final DictConfig dictConfig;

    @Override
    @Transactional(rollbackFor = Exception.class)
    public CommandResult inlay(RoleGameCommand cmd, String equipName, String badgeName) throws CommonException {
        String prefix = RoleInfoUtil.getPrefix(cmd.getRoleInfo());
        StringBuilder res = new StringBuilder(prefix).append("镶嵌成功！[").append(equipName).append("]的属性变化如下:\n");

        ActiveEquipItem equip = activeEquipWrapper.getByName(equipName);
        CommonException.byNull(equip, prefix + "名称为[" + equipName + "]的实例装备不存在。");

        RoleBag bag = roleBagWrapper.getById(cmd.getRoleId());
        if (!BagUtil.checkIsHad(bag, equip.getId(), 1)) {
            RoleEquip roleEqu = roleEquipWrapper.getById(cmd.getRoleId());
            String wear = (String) ReflectUtil.getFieldValue(roleEqu, equip.getEquipType());
            CommonException.bySupplier(() -> !equip.getId().equals(wear), prefix + "您的背包或穿戴中没有名称为[" + equipName + "]的实例装备。");
        }

        //判断装备镶嵌栏数量
        CommonException.bySupplier(() -> equip.getBadges().size() >= equip.getMaxBadgeNums(), prefix + "该装备已没有空余的镶嵌栏。");

        BadgeItem badge = badgeWrapper.getByColumn("name", badgeName);
        CommonException.byNull(badge, prefix + "名称为[" + badgeName + "]的徽章不存在。");
        CommonException.bySupplier(() -> !BagUtil.checkIsHad(bag, badge.getId(), 1), prefix + "您的背包中没有名称为[" + badgeName + "]的徽章。");
        CommonException.bySupplier(() -> !badge.getEquipType().contains(equip.getEquipType()),
                prefix + "[" + badgeName + "]不可镶嵌在" + dictConfig.get(equip.getEquipType()) + "上。");

        //判断锻造师等级
        RoleSecondProfession roleSecondProfession = roleSecondProfessionWrapper.getById(cmd.getRoleId());
        SecondProfessionItem professionItem = roleSecondProfession.getItemById(forger.getId());
        CommonException.bySupplier(() -> badge.getRequiredForgerLevel() > professionItem.getLevel(),
                prefix + "[" + badgeName + "]要求锻造师等级为Lv" + badge.getRequiredForgerLevel()
                        + "，当前锻造师等级为Lv" + professionItem.getLevel() + "。");

        ObjProp oldProp = equip.getProp();

        equip.getBadges().add(badge.getId());
        BagUtil.consumeItem(bag, badge.getId(), 1);

        ObjProp newProp = equip.getProp();

        roleBagWrapper.save(bag);
        activeEquipWrapper.save(equip);
        res.append(PropUtil.propChangeInfo(oldProp, newProp));
        return CommandResult.success(res.toString().trim());
    }

    @Override
    @Transactional(rollbackFor = Exception.class)
    public CommandResult removal(RoleGameCommand cmd, String equipName, Integer idx) throws CommonException {
        String prefix = RoleInfoUtil.getPrefix(cmd.getRoleInfo());
        StringBuilder res = new StringBuilder(prefix).append("摘除成功！[").append(equipName).append("]的属性变化如下:\n");

        ActiveEquipItem equip = activeEquipWrapper.getByName(equipName);
        CommonException.byNull(equip, prefix + "名称为[" + equipName + "]的实例装备不存在。");

        RoleBag bag = roleBagWrapper.getById(cmd.getRoleId());
        if (!BagUtil.checkIsHad(bag, equip.getId(), 1)) {
            RoleEquip roleEqu = roleEquipWrapper.getById(cmd.getRoleId());
            String wear = (String) ReflectUtil.getFieldValue(roleEqu, equip.getEquipType());
            CommonException.bySupplier(() -> !equip.getId().equals(wear), prefix + "您的背包或穿戴中没有名称为[" + equipName + "]的实例装备。");
        }

        CommonException.bySupplier(() -> equip.getBadges().size() < idx, prefix + "该镶嵌栏没有镶嵌过徽章。");

        ObjProp oldProp = equip.getProp();
        equip.getBadges().remove(idx - 1);
        ObjProp newProp = equip.getProp();

        activeEquipWrapper.save(equip);
        res.append(PropUtil.propChangeInfo(oldProp, newProp));
        return CommandResult.success(res.toString().trim());
    }
}
