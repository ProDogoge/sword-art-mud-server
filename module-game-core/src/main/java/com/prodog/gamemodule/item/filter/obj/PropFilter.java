package com.prodog.gamemodule.item.filter.obj;

import lombok.Data;

import java.util.Map;

@Data
public class PropFilter {
    private String name;
    private Map<String, Object> params;
}
