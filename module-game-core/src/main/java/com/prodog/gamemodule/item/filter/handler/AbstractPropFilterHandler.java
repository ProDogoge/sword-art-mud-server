package com.prodog.gamemodule.item.filter.handler;

import com.prodog.gamemodule.item.effect.obj.PropEffect;
import com.prodog.gamemodule.item.entity.PropItem;
import com.prodog.gamemodule.item.filter.obj.PropFilter;
import lombok.AllArgsConstructor;

import java.util.List;
import java.util.Map;
import java.util.Optional;

@AllArgsConstructor
public abstract class AbstractPropFilterHandler implements PropFilterHandler {
    private PropItem propItem;
    private PropFilter filter;

    public Map<String, Object> getParams() {
        return filter.getParams();
    }

    public Object get(String prop) {
        return filter.getParams().get(prop);
    }

    public Object contains(String prop) {
        return filter.getParams().containsKey(prop);
    }

    public List<PropEffect> getEffects() {
        return propItem.getEffects();
    }

    public PropEffect getEffect(String name) {
        return getEffects().stream().filter(e -> e.getName().equals(name)).findAny().orElse(null);
    }

    public Object getEffectVal(String name, String key) {
        return Optional.ofNullable(getEffect(name))
                .map(e -> e.getParams().get(key))
                .orElse(null);
    }
}
