package com.prodog.gamemodule.item.effect.handler;

import com.prodog.gamemodule.config.dict.DictConfig;
import com.prodog.gamemodule.fight.fightobj.entity.FightObj;
import com.prodog.gamemodule.fight.session.FightSession;
import com.prodog.gamemodule.item.effect.obj.PropEffect;
import com.prodog.gamemodule.item.effect.obj.PropEffectCommonRes;
import com.prodog.gamemodule.item.entity.PropItem;
import com.prodog.usermodule.role.constant.RoleState;
import com.prodog.usermodule.role.entity.RoleInfo;
import com.prodog.usermodule.role.wrapper.RoleInfoWrapper;
import com.prodog.utils.bean.SpringBeanUtils;

import java.util.List;
import java.util.Map;

/***
 * 恢复HPMP效果
 */
public class ReviveHandler extends AbstractPropEffectHandler {
    public ReviveHandler(PropEffect effect) {
        super(effect);
    }

    @Override
    public PropEffectCommonRes commonHandle(RoleInfo info, PropItem propItem, Map<String, Object> params, long nums) {
        if (info.getState() != RoleState.DEAD) return new PropEffectCommonRes("您当前没有死亡，无需使用该道具", 0, false);
        info.setState(RoleState.NORMAL);
        SpringBeanUtils.getBean(RoleInfoWrapper.class).save(info);
        StringBuilder res = new StringBuilder();
        res.append("身上发出了金色光芒，死者得到复活，");
        PropEffectCommonRes replyRes = new ReplyHpMpHandler(new PropEffect("replyHpMp", getParams())).commonHandle(info, propItem, params, 1);
        res.append(replyRes.getRes());
        return new PropEffectCommonRes(res.toString().trim(), 1, true);
    }

    @Override
    public void fightHandle(FightSession session, PropItem propItem, FightObj actObj, FightObj beActedObj, List<FightObj> beActedObjs) {
        StringBuilder res = new StringBuilder();
        if (!actObj.equals(beActedObj)) res.append(beActedObj.getName());
        if (actObj.equals(beActedObj) && beActedObjs.size() > 1) res.append("自己");
        if (beActedObj.getState() == 2) res.append("复活并");
        beActedObj.setState(1);
        long replyHp = Long.parseLong(String.valueOf(getParams().getOrDefault("hp", 0L)));
        long replyMp = Long.parseLong(String.valueOf(getParams().getOrDefault("mp", 0L)));
        long actualReplyHp = Math.min(beActedObj.getFightProp().getHp() - beActedObj.getFightProp().getCurHp(), replyHp);
        long actualReplyMp = Math.min(beActedObj.getFightProp().getMp() - beActedObj.getFightProp().getCurMp(), replyMp);
        res.append("恢复了");
        DictConfig dict = SpringBeanUtils.getBean(DictConfig.class);
        if (actualReplyHp != 0) res.append(dict.get("hp")).append(":").append(actualReplyHp).append("  ");
        if (actualReplyMp != 0) res.append(dict.get("mp")).append(":").append(actualReplyMp).append("  ");
        if (actualReplyHp == 0 && actualReplyMp == 0) {
            if (replyHp != 0 && replyMp != 0)
                res.append(dict.get("hp")).append(":0  ").append(dict.get("mp")).append(":0");
            else if (replyHp != 0) res.append(dict.get("hp")).append(":0");
            else res.append(dict.get("mp")).append(":0");
        }
        new ReplyHpMpHandler(new PropEffect("replyHpMp", getParams())).fightHandle(session, propItem, actObj, beActedObj, beActedObjs);
        session.getObjInfo().addBeActInfo(actObj.getUuid(), beActedObj.getUuid(), res.toString().trim());
    }
}
