package com.prodog.gamemodule.item.filter.handler;

import com.prodog.gamemodule.item.entity.PropItem;
import com.prodog.gamemodule.item.filter.obj.PropFilterRes;
import com.prodog.usermodule.role.entity.RoleInfo;

import java.util.Map;

public interface PropFilterHandler {
    /***
     * 平常使用的过滤
     * @param info 角色信息
     * @param nums 使用数量
     * @return
     */
    PropFilterRes commonFilter(RoleInfo info, PropItem propItem, Map<String, Object> params, long nums);
}
