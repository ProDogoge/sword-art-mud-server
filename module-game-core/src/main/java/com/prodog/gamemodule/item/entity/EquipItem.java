package com.prodog.gamemodule.item.entity;

import com.alibaba.fastjson2.annotation.JSONField;
import com.prodog.commonmodule.prop.entity.ObjProp;
import com.prodog.gamemodule.config.enhance.EnhanceConfig;
import com.prodog.gamemodule.config.enhance.EnhanceRequiredConfig;
import com.prodog.gamemodule.fight.effect.entity.FightEffect;
import com.prodog.util.ExpressionUtil;
import com.prodog.utils.bean.SpringBeanUtils;
import lombok.Data;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/***
 * 装备
 */
@Data
public class EquipItem extends GameItem {
    private String equipType;   //装备类型
    private String secondType;  //分类 如长剑 巨剑等
    private int requiredLevel;  //要求最低装备等级
    private String suitId;  //套装ID
    private ObjProp prop;   //属性
    private List<FightEffect> effects = new ArrayList<>();   //效果
    private String effectDesc;  //附加效果
    private int maxBadgeNums = 1;

    @JSONField(serialize = false)
    public List<Map<String, Long>> getEnhanceReqItemsList() {
        EnhanceConfig enhanceConfig = SpringBeanUtils.getBean(EnhanceConfig.class);
        List<Map<String, Long>> res = new ArrayList<>();
        for (int i = 0; i < enhanceConfig.getMaxUpLevel(); i++) {
            Map<String, Long> reqItems = new HashMap<>();
            EnhanceRequiredConfig reqCfg = enhanceConfig.getRequiredConfig(i);
            for (Map.Entry<String, String> e : reqCfg.getRequiredItems().entrySet()) {
                String itemId = e.getKey();
                int finalI = i;
                double doubleNums = ExpressionUtil.parse(e.getValue(), new HashMap<String, Object>() {{
                    put("equipLevel", getRequiredLevel());
                    put("enhanceLevel", finalI);
                }}, Double.class);
                long nums = (long) doubleNums;
                reqItems.putIfAbsent(itemId, 0L);
                reqItems.put(itemId, reqItems.get(itemId) + nums);
            }
            res.add(reqItems);
        }
        return res;
    }

    @JSONField(serialize = false)
    public List<Long> getEnhanceMoneyList() {
        EnhanceConfig enhanceConfig = SpringBeanUtils.getBean(EnhanceConfig.class);
        List<Long> res = new ArrayList<>();
        for (int i = 0; i < enhanceConfig.getMaxUpLevel(); i++) {
            EnhanceRequiredConfig reqCfg = enhanceConfig.getRequiredConfig(i);
            int finalI = i;
            double doubleCMoney = ExpressionUtil.parse(reqCfg.getMoney(), new HashMap<String, Object>() {{
                put("equipLevel", getRequiredLevel());
                put("enhanceLevel", finalI);
            }}, Double.class);
            res.add((long) doubleCMoney);
        }
        return res;
    }

    @JSONField(serialize = false)
    public List<Long> getEnhanceCopounList() {
        EnhanceConfig enhanceConfig = SpringBeanUtils.getBean(EnhanceConfig.class);
        List<Long> res = new ArrayList<>();
        for (int i = 0; i < enhanceConfig.getMaxUpLevel(); i++) {
            EnhanceRequiredConfig reqCfg = enhanceConfig.getRequiredConfig(i);
            int finalI = i;
            double doubleCCopoun = ExpressionUtil.parse(reqCfg.getCopoun(), new HashMap<String, Object>() {{
                put("equipLevel", getRequiredLevel());
                put("enhanceLevel", finalI);
            }}, Double.class);
            res.add((long) doubleCCopoun);
        }
        return res;
    }
}
