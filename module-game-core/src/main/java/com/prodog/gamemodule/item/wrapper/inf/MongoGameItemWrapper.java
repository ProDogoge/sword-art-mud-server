package com.prodog.gamemodule.item.wrapper.inf;

import com.prodog.database.wrapper.MongoDataWrapper;
import com.prodog.gamemodule.item.entity.GameItem;

public class MongoGameItemWrapper<T extends GameItem, P> extends MongoDataWrapper<T, P> implements GameItemWrapper<T, P> {
}
