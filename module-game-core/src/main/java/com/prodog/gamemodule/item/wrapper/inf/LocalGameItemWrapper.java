package com.prodog.gamemodule.item.wrapper.inf;

import com.prodog.database.wrapper.LocalDataWrapper;
import com.prodog.gamemodule.item.entity.GameItem;

public class LocalGameItemWrapper<T extends GameItem, P> extends LocalDataWrapper<T, P> implements GameItemWrapper<T, P> {
}
