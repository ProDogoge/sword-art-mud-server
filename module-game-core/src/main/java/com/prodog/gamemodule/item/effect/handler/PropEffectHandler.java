package com.prodog.gamemodule.item.effect.handler;

import com.prodog.gamemodule.fight.fightobj.entity.FightObj;
import com.prodog.gamemodule.fight.session.FightSession;
import com.prodog.gamemodule.item.effect.obj.PropEffectCommonRes;
import com.prodog.gamemodule.item.entity.PropItem;
import com.prodog.usermodule.role.entity.RoleInfo;

import java.util.List;
import java.util.Map;

public interface PropEffectHandler {
    /***
     * 平常使用的效果
     * @param info 角色信息
     * @param nums 使用数量
     * @return
     */
    PropEffectCommonRes commonHandle(RoleInfo info, PropItem propItem, Map<String, Object> params, long nums);

    /***
     *
     * @param session 战斗会话
     * @param actObj 使用该物品的对象
     * @param beActedObj 被作用的当前对象
     * @param beActedObjs 被作用的所有对象
     */
    void fightHandle(FightSession session, PropItem propItem, FightObj actObj, FightObj beActedObj, List<FightObj> beActedObjs);
}
