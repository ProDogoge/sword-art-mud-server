package com.prodog.gamemodule.item.effect.handler;

import com.prodog.gamemodule.fight.fightobj.entity.FightObj;
import com.prodog.gamemodule.fight.session.FightSession;
import com.prodog.gamemodule.item.effect.obj.PropEffect;
import com.prodog.gamemodule.item.effect.obj.PropEffectCommonRes;
import com.prodog.gamemodule.item.entity.GameItem;
import com.prodog.gamemodule.item.entity.PropItem;
import com.prodog.usermodule.role.entity.RoleBag;
import com.prodog.usermodule.role.entity.RoleInfo;
import com.prodog.usermodule.role.util.BagUtil;
import com.prodog.usermodule.role.util.GameItemUtil;
import com.prodog.usermodule.role.wrapper.RoleBagWrapper;
import com.prodog.utils.bean.BeanUtil;
import com.prodog.utils.bean.SpringBeanUtils;

import java.util.*;

/***
 * 单选礼包效果
 */
public class SelectOneGiftHandler extends AbstractPropEffectHandler {
    public SelectOneGiftHandler(PropEffect effect) {
        super(effect);
    }

    @Override
    public PropEffectCommonRes commonHandle(RoleInfo info, PropItem propItem, Map<String, Object> params, long nums) {
        if (params == null || params.get("index") == null)
            return new PropEffectCommonRes("该道具为单选礼包，请先使用命令:[查询道具 道具名]查看礼包内容，再使用命令:[打开 道具名 索引]选择您想要的内容。", nums, false);
        StringBuilder res = new StringBuilder().append("获得物品");

        List<String> giftContents = BeanUtil.bean2Bean(get("content"), ArrayList.class);
        Integer index = (Integer) params.get("index");
        if (index < 0 || index >= giftContents.size())
            return new PropEffectCommonRes("该礼包没有该索引的内容，请发送:查询道具道具名 来查看礼包自选内容。", nums, false);
        String content = giftContents.get(index);
        String itemId = content.split("\\s+")[0];
        Long num = Long.parseLong(content.split("\\s+")[1]) * nums;

        RoleBag bag = SpringBeanUtils.getBean(RoleBagWrapper.class).getById(info.getId());
        GameItem item = GameItemUtil.getById(itemId);
        if (!BagUtil.checkAddLimit(bag, itemId, num))
            return new PropEffectCommonRes("打开失败，您背包中的[" + item.getName() + "]持有数已达上限。", nums, false);

        BagUtil.addItem(bag, itemId, num);
        SpringBeanUtils.getBean(RoleBagWrapper.class).save(bag);

        res.append("[").append(item.getName()).append("]x").append(num);
        return new PropEffectCommonRes(res.toString().trim(), nums, true);
    }

    @Override
    public void fightHandle(FightSession session, PropItem propItem, FightObj actObj, FightObj beActedObj, List<FightObj> beActedObjs) {
    }
}
