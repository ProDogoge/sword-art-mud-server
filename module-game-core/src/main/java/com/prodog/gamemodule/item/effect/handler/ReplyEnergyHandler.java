package com.prodog.gamemodule.item.effect.handler;

import com.prodog.gamemodule.config.BaseConfig;
import com.prodog.gamemodule.config.dict.DictConfig;
import com.prodog.gamemodule.fight.fightobj.entity.FightObj;
import com.prodog.gamemodule.fight.session.FightSession;
import com.prodog.gamemodule.item.effect.obj.PropEffect;
import com.prodog.gamemodule.item.effect.obj.PropEffectCommonRes;
import com.prodog.gamemodule.item.entity.PropItem;
import com.prodog.usermodule.role.entity.RoleInfo;
import com.prodog.usermodule.role.util.RoleInfoUtil;
import com.prodog.usermodule.role.wrapper.RoleInfoWrapper;
import com.prodog.utils.bean.SpringBeanUtils;

import java.util.List;
import java.util.Map;

/***
 * 恢复HPMP效果
 */
public class ReplyEnergyHandler extends AbstractPropEffectHandler {
    public ReplyEnergyHandler(PropEffect effect) {
        super(effect);
    }

    @Override
    public PropEffectCommonRes commonHandle(RoleInfo info, PropItem propItem, Map<String, Object> params, long nums) {
        StringBuilder res = new StringBuilder();
        long replyEnergy = Long.parseLong(String.valueOf(getParams().getOrDefault("energy", 0L)));
        long loseEnergy = SpringBeanUtils.getBean(BaseConfig.class).getBaseEnergy() - RoleInfoUtil.getEnergy(info);
        long actualReplyEnergy = Math.min(loseEnergy, replyEnergy * nums);
        if (replyEnergy * nums > loseEnergy) {
            nums = replyEnergy == 0 ? 0 : (actualReplyEnergy % replyEnergy == 0 ? actualReplyEnergy / replyEnergy : actualReplyEnergy / replyEnergy + 1);
        }
        info.setEnergy(info.getEnergy() + actualReplyEnergy);
        SpringBeanUtils.getBean(RoleInfoWrapper.class).save(info);
        DictConfig dict = SpringBeanUtils.getBean(DictConfig.class);
        res.append("恢复了").append(actualReplyEnergy).append("点").append(dict.get("energy"));
        return new PropEffectCommonRes(res.toString().trim(), nums, true);
    }

    @Override
    public void fightHandle(FightSession session, PropItem propItem, FightObj actObj, FightObj beActedObj, List<FightObj> beActedObjs) {
    }
}
