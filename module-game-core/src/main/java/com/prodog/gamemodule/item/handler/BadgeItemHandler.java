package com.prodog.gamemodule.item.handler;

import com.prodog.command.annonations.*;
import com.prodog.command.entity.CommandResult;
import com.prodog.command.entity.RoleGameCommand;
import com.prodog.gamemodule.item.service.BadgeItemService;
import com.prodog.usermodule.role.interceptor.StateInterceptor;
import com.prodog.usermodule.user.interceptor.NotRegistInterceptor;
import com.prodog.utils.exception.CommonException;
import lombok.RequiredArgsConstructor;

@CommandBean(setName = "徽章操作")
@RequiredArgsConstructor
@Iptors(vals = @Iptor(value = NotRegistInterceptor.class))
public class BadgeItemHandler {
    private final BadgeItemService badgeItemService;

    @Command(value = "镶嵌\\s*(.+?)\\s+(.+)", props = "equipName,badgeName")
    @Iptors(vals = {@Iptor(value = StateInterceptor.class, params = "{'states':[1]}", msg = "[{{state}}]状态下无法进行此操作。")})
    public CommandResult inlayvt(RoleGameCommand cmd, String equipName, String badgeName) {
        try {
            return badgeItemService.inlay(cmd, equipName, badgeName);
        } catch (CommonException e) {
            return CommandResult.failure(e.getMessage());
        } catch (Exception e) {
            e.printStackTrace();
            return CommandResult.failure("服务器错误，徽章镶嵌失败。");
        }
    }

    @Command(value = "摘除\\s*(.+?)\\s+(\\d+)", props = "equipName,idx")
    @Iptors(vals = {@Iptor(value = StateInterceptor.class, params = "{'states':[1]}", msg = "[{{state}}]状态下无法进行此操作。")})
    public CommandResult removal(RoleGameCommand cmd, String equipName, Integer idx) {
        try {
            return badgeItemService.removal(cmd, equipName, idx);
        } catch (CommonException e) {
            return CommandResult.failure(e.getMessage());
        } catch (Exception e) {
            e.printStackTrace();
            return CommandResult.failure("服务器错误，徽章摘除失败。");
        }
    }
}
