package com.prodog.gamemodule.item.effect.handler;

import com.prodog.gamemodule.config.dict.DictConfig;
import com.prodog.gamemodule.fight.fightobj.entity.FightObj;
import com.prodog.gamemodule.fight.session.FightSession;
import com.prodog.gamemodule.item.effect.obj.PropEffect;
import com.prodog.gamemodule.item.effect.obj.PropEffectCommonRes;
import com.prodog.gamemodule.item.entity.GameItem;
import com.prodog.gamemodule.item.entity.PropItem;
import com.prodog.usermodule.role.entity.RoleBag;
import com.prodog.usermodule.role.entity.RoleInfo;
import com.prodog.usermodule.role.util.BagUtil;
import com.prodog.usermodule.role.util.GameItemUtil;
import com.prodog.usermodule.role.wrapper.RoleBagWrapper;
import com.prodog.usermodule.role.wrapper.RoleInfoWrapper;
import com.prodog.usermodule.user.entity.UserInfo;
import com.prodog.usermodule.user.wrapper.UserInfoWrapper;
import com.prodog.utils.bean.BeanUtil;
import com.prodog.utils.interfaces.AutowiredBean;
import org.springframework.beans.factory.annotation.Autowired;

import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

/***
 * 礼包效果
 */
public class GiftHandler extends AbstractPropEffectHandler implements AutowiredBean {
    @Autowired
    private UserInfoWrapper userWrapper;
    @Autowired
    private RoleInfoWrapper roleInfoWrapper;
    @Autowired
    private DictConfig dictConfig;
    @Autowired
    private RoleBagWrapper bagWrapper;

    public GiftHandler(PropEffect effect) {
        super(effect);
        this.autowired();
    }

    @Override
    public PropEffectCommonRes commonHandle(RoleInfo info, PropItem propItem, Map<String, Object> params, long nums) {
        StringBuilder res = new StringBuilder();

        Map<String, Long> itemMap = new LinkedHashMap<>();
        List<String> giftContents = BeanUtil.bean2Bean(get("content"), ArrayList.class);
        giftContents.forEach((content) -> {
            String itemId = content.split("\\s+")[0];
            Long num = Long.parseLong(content.split("\\s+")[1]);
            itemMap.put(itemId, num * nums);
        });
        if (giftContents.size() > 0) res.append("获得以下物品:\n");
        RoleBag bag = bagWrapper.getById(info.getId());
        for (Map.Entry<String, Long> e : itemMap.entrySet()) {
            String itemId = e.getKey();
            Long num = e.getValue();
            GameItem item = GameItemUtil.getById(itemId);
            if (!BagUtil.checkAddLimit(bag, itemId, num))
                return new PropEffectCommonRes("打开失败，您背包中的[" + item.getName() + "]持有数已达上限。", nums, false);
            BagUtil.addItem(bag, itemId, num);
            res.append("[").append(item.getName()).append("]").append("x").append(num).append("\n");
        }

        UserInfo user = userWrapper.getById(info.getUserId());
        long money = Long.parseLong(String.valueOf(getParams().getOrDefault("money", 0))) * nums;
        long copoun = Long.parseLong(String.valueOf(getParams().getOrDefault("copoun", 0))) * nums;
        info.setMoney(info.getMoney() + money);
        user.setCopoun(user.getCopoun() + copoun);
        bagWrapper.save(bag);
        roleInfoWrapper.save(info);
        userWrapper.save(user);
        if (money > 0 || copoun > 0) {
            res.append("获得");
            if (money > 0) res.append(dictConfig.get("money")).append(":").append(money).append("  ");
            if (copoun > 0) res.append(dictConfig.get("copoun")).append(":").append(copoun);
            res.append("\n");
        }

        return new PropEffectCommonRes(res.toString().trim(), nums, true);
    }

    @Override
    public void fightHandle(FightSession session, PropItem propItem, FightObj actObj, FightObj beActedObj, List<FightObj> beActedObjs) {
    }
}
