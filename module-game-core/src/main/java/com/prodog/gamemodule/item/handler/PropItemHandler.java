package com.prodog.gamemodule.item.handler;

import com.prodog.command.annonations.*;
import com.prodog.command.entity.CommandResult;
import com.prodog.command.entity.RoleGameCommand;
import com.prodog.gamemodule.fight.service.impl.FightServiceImpl;
import com.prodog.gamemodule.item.service.PropItemService;
import com.prodog.usermodule.role.constant.RoleState;
import com.prodog.usermodule.role.interceptor.StateInterceptor;
import com.prodog.usermodule.user.interceptor.NotRegistInterceptor;
import com.prodog.utils.exception.CommonException;
import lombok.RequiredArgsConstructor;

@CommandBean(setName = "道具操作")
@RequiredArgsConstructor
@Iptors(vals = @Iptor(value = NotRegistInterceptor.class))
public class PropItemHandler {
    private final PropItemService propItemService;
    private final FightServiceImpl fightService;

    @Command(value = {"使用\\s*(.+?)\\s+(\\d+)", "使用\\s*(.+)"}, props = {"propName,nums", "propName"})
    public CommandResult useProp(RoleGameCommand cmd, String propName, Long nums) {
        try {
            if (cmd.getRoleInfo().getState() == RoleState.FIGHT) {
                return fightService.useItem(cmd, propName, nums == null ? null : Integer.valueOf(String.valueOf(nums)));
            } else {
                return propItemService.useProp(cmd, propName, nums == null ? 1 : nums);
            }
        } catch (CommonException e) {
            return CommandResult.failure(e.getMessage());
        } catch (Exception e) {
            e.printStackTrace();
            return CommandResult.failure("服务器错误，使用道具失败");
        }
    }

    @Command(value = {"打开\\s*(.+?)\\s+(\\d+)\\s+(\\d+)", "打开\\s*(.+)\\s+(\\d+)", "打开(.+)"}, props = {"itemName,index,nums", "itemName,index", "itemName"})
    @Iptors(vals = {@Iptor(value = StateInterceptor.class, params = "{'states':[1]}", msg = "[{{state}}]状态下无法进行此操作。")})
    public CommandResult openGift(RoleGameCommand cmd, String itemName, Integer index, @Default("1") Long nums) {
        try {
            return propItemService.openGift(cmd, itemName, index, nums);
        } catch (CommonException e) {
            return CommandResult.failure(e.getMessage());
        } catch (Exception e) {
            e.printStackTrace();
            return CommandResult.failure("服务器错误，使用道具失败");
        }
    }

    @Command(value = {"查询道具\\s*(.+)"}, props = {"itemName"})
    @Iptors(vals = @Iptor(value = StateInterceptor.class, params = "{'states':[2,3],'required':false}", msg = "[{{state}}]状态下无法进行此操作。"))
    public CommandResult searchProp(RoleGameCommand cmd, String itemName) {
        try {
            return propItemService.searchProp(cmd, itemName);
        } catch (CommonException e) {
            return CommandResult.failure(e.getMessage());
        } catch (Exception e) {
            e.printStackTrace();
            return CommandResult.failure("服务器错误，使用道具失败");
        }
    }
}
