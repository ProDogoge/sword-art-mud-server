package com.prodog.gamemodule.item.wrapper;

import com.prodog.database.annotation.Wrapper;
import com.prodog.database.wrapper.MongoDataWrapper;
import com.prodog.gamemodule.item.entity.EquipSerialNum;

import java.util.Optional;

@Wrapper(path = "游戏数据/物品/装备编号", module = "装备编号数据")
public class EquipSerialNumWrapper extends MongoDataWrapper<EquipSerialNum, String> {
    public long getCurrNum(String equipId) {
        return Optional.ofNullable(getById(equipId))
                .map(EquipSerialNum::getSerialNum)
                .orElse(0L);
    }

    public long getNextNum(String equipId) {
        return Optional.ofNullable(getById(equipId))
                .map(EquipSerialNum::getSerialNum)
                .orElse(1L);
    }

    public long getNextNumAndSave(String equipId) {
        long nextNum = Optional.ofNullable(getById(equipId))
                .map(n -> n.getSerialNum() + 1)
                .orElse(1L);
        save(new EquipSerialNum(equipId, nextNum));
        return nextNum;
    }
}
