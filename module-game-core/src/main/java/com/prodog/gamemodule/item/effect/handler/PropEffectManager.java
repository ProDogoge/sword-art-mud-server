package com.prodog.gamemodule.item.effect.handler;

import cn.hutool.core.util.ReflectUtil;
import com.prodog.gamemodule.fight.fightobj.entity.FightObj;
import com.prodog.gamemodule.fight.session.FightSession;
import com.prodog.gamemodule.item.effect.obj.PropEffect;
import com.prodog.gamemodule.item.effect.obj.PropEffectCommonRes;
import com.prodog.gamemodule.item.entity.PropItem;
import com.prodog.usermodule.role.entity.RoleInfo;
import com.prodog.usermodule.role.util.BagUtil;
import com.prodog.usermodule.role.util.RoleInfoUtil;
import com.prodog.utils.exception.CommonException;

import java.lang.reflect.Constructor;
import java.lang.reflect.InvocationTargetException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

public class PropEffectManager {
    public static Map<String, Constructor<? extends PropEffectHandler>> handlerMap;

    static {
        handlerMap = new HashMap<String, Constructor<? extends PropEffectHandler>>() {{
            put("replyHpMp", ReflectUtil.getConstructor(ReplyHpMpHandler.class, PropEffect.class));
            put("gift", ReflectUtil.getConstructor(GiftHandler.class, PropEffect.class));
            put("selectOne", ReflectUtil.getConstructor(SelectOneGiftHandler.class, PropEffect.class));
            put("replyEnergy", ReflectUtil.getConstructor(ReplyEnergyHandler.class, PropEffect.class));
            put("revive", ReflectUtil.getConstructor(ReviveHandler.class, PropEffect.class));
        }};
    }

    public static String commonHandle(RoleInfo roleInfo, PropItem item, Map<String, Object> params, long nums) throws CommonException {
        StringBuilder res = new StringBuilder(RoleInfoUtil.getPrefix(roleInfo));
        List<PropEffectCommonRes> effectReses = new ArrayList<>();
        for (PropEffect effect : item.getEffects()) {
            try {
                PropEffectHandler handler = handlerMap.get(effect.getName()).newInstance(effect);
                PropEffectCommonRes effectRes = handler.commonHandle(roleInfo, item, params, nums);
                CommonException.bySupplier(() -> !effectRes.isSuccess(), RoleInfoUtil.getPrefix(roleInfo) + effectRes.getRes());
                effectReses.add(effectRes);
            } catch (InstantiationException | InvocationTargetException | IllegalAccessException e) {
                e.printStackTrace();
            }
        }
        nums = effectReses.stream().mapToLong(PropEffectCommonRes::getActualNums).max().getAsLong();
        BagUtil.consumeItemAndSave(roleInfo.getId(), item.getId(), nums);
        String content = effectReses.stream().map(PropEffectCommonRes::getRes).collect(Collectors.joining(","));
        res.append("使用了").append(nums).append("个").append("[").append(item.getName()).append("]: ");
        res.append(content);
        return res.toString().trim();
    }

    public static void fightHandle(FightSession session, PropItem propItem, FightObj actObj, FightObj beActedObj, List<FightObj> beActedObjs) {
        for (PropEffect effect : propItem.getEffects()) {
            try {
                PropEffectHandler handler = handlerMap.get(effect.getName()).newInstance(effect);
                handler.fightHandle(session, propItem, actObj, beActedObj, beActedObjs);
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }
}
