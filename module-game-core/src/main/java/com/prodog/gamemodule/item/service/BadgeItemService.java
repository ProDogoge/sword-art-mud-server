package com.prodog.gamemodule.item.service;

import com.prodog.command.entity.CommandResult;
import com.prodog.command.entity.RoleGameCommand;
import com.prodog.utils.exception.CommonException;

public interface BadgeItemService {
    CommandResult inlay(RoleGameCommand cmd, String equipName, String badgeName) throws CommonException;

    CommandResult removal(RoleGameCommand cmd, String equipName, Integer idx) throws CommonException;
}
