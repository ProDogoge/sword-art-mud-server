package com.prodog.gamemodule.item.wrapper;

import com.prodog.database.annotation.Wrapper;
import com.prodog.gamemodule.item.entity.PropItem;
import com.prodog.gamemodule.item.wrapper.inf.LocalGameItemWrapper;

@Wrapper(path = "游戏数据/物品/道具", module = "道具数据")
public class PropWrapper extends LocalGameItemWrapper<PropItem, String> {
}
