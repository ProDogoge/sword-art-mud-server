package com.prodog.gamemodule.item.effect.handler;

import com.prodog.commonmodule.prop.caculate.getter.RolePropGetter;
import com.prodog.commonmodule.prop.entity.ObjProp;
import com.prodog.gamemodule.config.dict.DictConfig;
import com.prodog.gamemodule.fight.damage.util.DamageUtil;
import com.prodog.gamemodule.fight.fightobj.entity.FightObj;
import com.prodog.gamemodule.fight.session.FightSession;
import com.prodog.gamemodule.item.effect.obj.PropEffect;
import com.prodog.gamemodule.item.effect.obj.PropEffectCommonRes;
import com.prodog.gamemodule.item.entity.PropItem;
import com.prodog.usermodule.role.entity.RoleInfo;
import com.prodog.usermodule.role.wrapper.RoleInfoWrapper;
import com.prodog.utils.bean.SpringBeanUtils;

import java.util.List;
import java.util.Map;

/***
 * 恢复HPMP效果
 */
public class ReplyHpMpHandler extends AbstractPropEffectHandler {
    public ReplyHpMpHandler(PropEffect effect) {
        super(effect);
    }

    @Override
    public PropEffectCommonRes commonHandle(RoleInfo info, PropItem propItem, Map<String, Object> params, long nums) {
        StringBuilder res = new StringBuilder();
        long replyHp = Long.parseLong(String.valueOf(getParams().getOrDefault("hp", 0L)));
        long replyMp = Long.parseLong(String.valueOf(getParams().getOrDefault("mp", 0L)));
        ObjProp roleProp = SpringBeanUtils.getBean(RolePropGetter.class).get(info);
        long loseHp = roleProp.getHp() - roleProp.getCurHp();
        long loseMp = roleProp.getMp() - roleProp.getCurMp();
        long actualReplyHp = Math.min(loseHp, replyHp * nums);
        long actualReplyMp = Math.min(loseMp, replyMp * nums);
        if (replyHp * nums > loseHp || replyMp * nums > loseMp) {
            long replyHpNums = replyHp == 0 ? 0 : (actualReplyHp % replyHp == 0 ? actualReplyHp / replyHp : actualReplyHp / replyHp + 1);
            long replyMpNums = replyMp == 0 ? 0 : (actualReplyMp % replyMp == 0 ? actualReplyMp / replyMp : actualReplyMp / replyMp + 1);
            nums = Math.max(replyHpNums, replyMpNums);
        }
        info.setCurrHp(roleProp.getCurHp() + actualReplyHp);
        info.setCurrMp(roleProp.getCurMp() + actualReplyMp);
        SpringBeanUtils.getBean(RoleInfoWrapper.class).save(info);
        DictConfig dict = SpringBeanUtils.getBean(DictConfig.class);
        res.append("恢复了");
        if (actualReplyHp != 0) res.append(dict.get("hp")).append(":").append(actualReplyHp).append("  ");
        if (actualReplyMp != 0) res.append(dict.get("mp")).append(":").append(actualReplyMp).append("  ");
        if (actualReplyHp == 0 && actualReplyMp == 0) {
            if (replyHp != 0 && replyMp != 0)
                res.append(dict.get("hp")).append(":0  ").append(dict.get("mp")).append(":0");
            else if (replyHp != 0) res.append(dict.get("hp")).append(":0");
            else res.append(dict.get("mp")).append(":0");
        }
        return new PropEffectCommonRes(res.toString().trim(), nums, true);
    }

    @Override
    public void fightHandle(FightSession session, PropItem propItem, FightObj actObj, FightObj beActedObj, List<FightObj> beActedObjs) {
        long replyHp = Long.parseLong(String.valueOf(getParams().getOrDefault("hp", 0L)));
        long replyMp = Long.parseLong(String.valueOf(getParams().getOrDefault("mp", 0L)));
        long actualReplyHp = Math.min(beActedObj.getFightProp().getHp() - beActedObj.getFightProp().getCurHp(), replyHp);
        long actualReplyMp = Math.min(beActedObj.getFightProp().getMp() - beActedObj.getFightProp().getCurMp(), replyMp);
        DamageUtil.replyHp(beActedObj, actualReplyHp);
        DamageUtil.replyMp(beActedObj, actualReplyMp);
        StringBuilder res = new StringBuilder();
        if (!actObj.equals(beActedObj)) res.append(beActedObj.getName());
        if (actObj.equals(beActedObj) && beActedObjs.size() > 1) res.append("自己");
        res.append("恢复了");
        DictConfig dict = SpringBeanUtils.getBean(DictConfig.class);
        if (actualReplyHp != 0) res.append(dict.get("hp")).append(":").append(actualReplyHp).append("  ");
        if (actualReplyMp != 0) res.append(dict.get("mp")).append(":").append(actualReplyMp).append("  ");
        if (actualReplyHp == 0 && actualReplyMp == 0) {
            if (replyHp != 0 && replyMp != 0)
                res.append(dict.get("hp")).append(":0  ").append(dict.get("mp")).append(":0");
            else if (replyHp != 0) res.append(dict.get("hp")).append(":0");
            else res.append(dict.get("mp")).append(":0");
        }
        session.getObjInfo().appendBeActInfo(actObj.getUuid(), beActedObj.getUuid(), res.toString().trim());
    }
}
