package com.prodog.gamemodule.item.filter.handler;

import cn.hutool.core.util.ReflectUtil;
import com.prodog.gamemodule.item.entity.PropItem;
import com.prodog.gamemodule.item.filter.obj.PropFilter;
import com.prodog.gamemodule.item.filter.obj.PropFilterRes;
import com.prodog.usermodule.role.entity.RoleInfo;
import com.prodog.utils.exception.CommonException;

import java.lang.reflect.Constructor;
import java.util.HashMap;
import java.util.Map;

public class PropFilterManager {
    public static Map<String, Constructor<? extends PropFilterHandler>> handlerMap;

    static {
        handlerMap = new HashMap<String, Constructor<? extends PropFilterHandler>>() {{
            put("checkHpMp", ReflectUtil.getConstructor(CheckHpMpHandler.class, PropItem.class, PropFilter.class));
            put("checkEnergy", ReflectUtil.getConstructor(CheckEnergyHandler.class, PropItem.class, PropFilter.class));
        }};
    }

    public static PropFilterRes commonFilter(RoleInfo roleInfo, PropItem item, Map<String, Object> params, long nums) throws CommonException {
        PropFilterRes res = new PropFilterRes(null, true);
        for (PropFilter filter : item.getFilters()) {
            try {
                PropFilterHandler handler = handlerMap.get(filter.getName()).newInstance(item, filter);
                res = handler.commonFilter(roleInfo, item, params, nums);
                if (!res.isSuccess()) return res;
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
        return res;
    }
}
