package com.prodog.gamemodule.item.filter.obj;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class PropFilterRes {
    private String res;
    private boolean success;
}
