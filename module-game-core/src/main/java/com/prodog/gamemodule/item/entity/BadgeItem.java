package com.prodog.gamemodule.item.entity;

import com.prodog.commonmodule.prop.entity.ObjProp;
import com.prodog.gamemodule.fight.effect.entity.FightEffect;
import lombok.Data;

import java.util.ArrayList;
import java.util.List;

/***
 * 徽章
 */
@Data
public class BadgeItem extends GameItem {
    private int requiredForgerLevel = 1;    //要求锻造等级
    private List<String> equipType = new ArrayList<>(); //允许镶嵌的装备类型
    private String icon;    //图标
    private ObjProp prop = new ObjProp();   //增加的属性
    private List<FightEffect> equipEffects = new ArrayList<>(); //对武器的效果(主要用于百分比增加属性)
    private List<FightEffect> roleEffects = new ArrayList<>();  //对角色的效果
}
