package com.prodog.gamemodule.item.entity;


import com.alibaba.fastjson2.annotation.JSONField;
import com.prodog.commonmodule.prop.caculate.getter.ActiveEquipPropGetter;
import com.prodog.commonmodule.prop.entity.ObjProp;
import com.prodog.gamemodule.config.enhance.EnhanceConfig;
import com.prodog.gamemodule.config.enhance.EnhanceRequiredConfig;
import com.prodog.gamemodule.fight.effect.entity.FightEffect;
import com.prodog.gamemodule.item.wrapper.EquipWrapper;
import com.prodog.util.ExpressionUtil;
import com.prodog.utils.bean.SpringBeanUtils;
import lombok.Data;
import org.springframework.data.mongodb.core.mapping.Document;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/***
 * 已激活装备
 */
@Document("g_active_equip_item")
@Data
public class ActiveEquipItem extends EquipItem {
    private String equipId; //装备ID
    private long serialNum; //编号
    private List<String> gems = new ArrayList<>();  //镶嵌宝石
    private List<String> badges = new ArrayList<>();  //镶嵌徽章
    private int enhanceLevel;   //强化等级

    @Override
    public String getName() {
        EquipItem equip = SpringBeanUtils.getBean(EquipWrapper.class).getById(equipId);
        return equip.getName() + serialNum;
    }

    public Long getPrice() {
        EquipItem equip = SpringBeanUtils.getBean(EquipWrapper.class).getById(equipId);
        return equip.getPrice();
    }

    public Long getSellPrice() {
        EquipItem equip = SpringBeanUtils.getBean(EquipWrapper.class).getById(equipId);
        return equip.getSellPrice();
    }

    public Long getLimit() {
        EquipItem equip = SpringBeanUtils.getBean(EquipWrapper.class).getById(equipId);
        return equip.getLimit();
    }

    public int getTradeType() {
        EquipItem equip = SpringBeanUtils.getBean(EquipWrapper.class).getById(equipId);
        return equip.getTradeType();
    }

    public int getRarity() {
        EquipItem equip = SpringBeanUtils.getBean(EquipWrapper.class).getById(equipId);
        return equip.getRarity();
    }

    public String getDesc() {
        EquipItem equip = SpringBeanUtils.getBean(EquipWrapper.class).getById(equipId);
        return equip.getDesc();
    }

    public String getEquipType() {
        EquipItem equip = SpringBeanUtils.getBean(EquipWrapper.class).getById(equipId);
        return equip.getEquipType();
    }

    public String getSecondType() {
        EquipItem equip = SpringBeanUtils.getBean(EquipWrapper.class).getById(equipId);
        return equip.getSecondType();
    }

    public int getRequiredLevel() {
        EquipItem equip = SpringBeanUtils.getBean(EquipWrapper.class).getById(equipId);
        return equip.getRequiredLevel();
    }

    public String getSuitId() {
        EquipItem equip = SpringBeanUtils.getBean(EquipWrapper.class).getById(equipId);
        return equip.getSuitId();
    }

    public ObjProp getProp() {
        return SpringBeanUtils.getBean(ActiveEquipPropGetter.class).get(this);
    }

    public List<FightEffect> getEffects() {
        EquipItem equip = SpringBeanUtils.getBean(EquipWrapper.class).getById(equipId);
        return equip.getEffects();
    }

    public String getEffectDesc() {
        EquipItem equip = SpringBeanUtils.getBean(EquipWrapper.class).getById(equipId);
        return equip.getEffectDesc();
    }

    public String getSuperName() {
        EquipItem equip = SpringBeanUtils.getBean(EquipWrapper.class).getById(equipId);
        return equip.getName();
    }

    public int getMaxBadgeNums() {
        EquipItem equip = SpringBeanUtils.getBean(EquipWrapper.class).getById(equipId);
        return equip.getMaxBadgeNums();
    }

    @JSONField(serialize = false)
    public List<Map<String, Long>> getEnhanceReqItemsList() {
        EquipItem equip = SpringBeanUtils.getBean(EquipWrapper.class).getById(equipId);
        return equip.getEnhanceReqItemsList();
    }

    @JSONField(serialize = false)
    public List<Long> getEnhanceMoneyList() {
        EquipItem equip = SpringBeanUtils.getBean(EquipWrapper.class).getById(equipId);
        return equip.getEnhanceMoneyList();
    }

    @JSONField(serialize = false)
    public List<Long> getEnhanceCopounList() {
        EquipItem equip = SpringBeanUtils.getBean(EquipWrapper.class).getById(equipId);
        return equip.getEnhanceCopounList();
    }

    @JSONField(serialize = false)
    public Map<String, Long> getEnhanceReqItems() {
        Map<String, Long> reqItems = new HashMap<>();
        EnhanceRequiredConfig reqCfg = SpringBeanUtils.getBean(EnhanceConfig.class).getRequiredConfig(getEnhanceLevel());
        for (Map.Entry<String, String> e : reqCfg.getRequiredItems().entrySet()) {
            String itemId = e.getKey();
            double doubleNums = ExpressionUtil.parse(e.getValue(), new HashMap<String, Object>() {{
                put("equipLevel", getRequiredLevel());
                put("enhanceLevel", getEnhanceLevel());
            }}, Double.class);
            long nums = (long) doubleNums;
            reqItems.putIfAbsent(itemId, 0L);
            reqItems.put(itemId, reqItems.get(itemId) + nums);
        }
        return reqItems;
    }

    @JSONField(serialize = false)
    public long getEnhanceMoney() {
        EnhanceRequiredConfig reqCfg = SpringBeanUtils.getBean(EnhanceConfig.class).getRequiredConfig(getEnhanceLevel());
        double doubleCMoney = ExpressionUtil.parse(reqCfg.getMoney(), new HashMap<String, Object>() {{
            put("equipLevel", getRequiredLevel());
            put("enhanceLevel", getEnhanceLevel());
        }}, Double.class);
        return (long) doubleCMoney;
    }

    @JSONField(serialize = false)
    public long getEnhanceCopoun() {
        EnhanceRequiredConfig reqCfg = SpringBeanUtils.getBean(EnhanceConfig.class).getRequiredConfig(getEnhanceLevel());
        double doubleCCopoun = ExpressionUtil.parse(reqCfg.getCopoun(), new HashMap<String, Object>() {{
            put("equipLevel", getRequiredLevel());
            put("enhanceLevel", getEnhanceLevel());
        }}, Long.class);
        return (long) doubleCCopoun;
    }
}
