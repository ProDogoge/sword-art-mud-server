package com.prodog.gamemodule.item.filter.handler;

import com.prodog.commonmodule.prop.caculate.getter.RolePropGetter;
import com.prodog.commonmodule.prop.entity.ObjProp;
import com.prodog.gamemodule.item.entity.PropItem;
import com.prodog.gamemodule.item.filter.obj.PropFilter;
import com.prodog.gamemodule.item.filter.obj.PropFilterRes;
import com.prodog.usermodule.role.entity.RoleInfo;
import com.prodog.utils.bean.SpringBeanUtils;

import java.util.Map;

public class CheckHpMpHandler extends AbstractPropFilterHandler {
    public CheckHpMpHandler(PropItem propItem, PropFilter filter) {
        super(propItem, filter);
    }

    @Override
    public PropFilterRes commonFilter(RoleInfo info, PropItem propItem, Map<String, Object> params, long nums) {
        long replyHp = Long.parseLong(String.valueOf(getEffect("replyHpMp").getParams().getOrDefault("hp", 0L)));
        long replyMp = Long.parseLong(String.valueOf(getEffect("replyHpMp").getParams().getOrDefault("mp", 0L)));

        ObjProp roleProp = SpringBeanUtils.getBean(RolePropGetter.class).get(info);
        long loseHp = roleProp.getHp() - roleProp.getCurHp();
        long loseMp = roleProp.getMp() - roleProp.getCurMp();

        if (loseHp == 0 && replyHp >= 0 && replyMp == 0)
            return new PropFilterRes((String) getParams().get("failMsg"), false);
        if (loseMp == 0 && replyMp >= 0 && replyHp == 0)
            return new PropFilterRes((String) getParams().get("failMsg"), false);
        if (loseHp == 0 && replyHp >= 0 && loseMp == 0 && replyMp >= 0)
            return new PropFilterRes((String) getParams().get("failMsg"), false);
        return new PropFilterRes(null, true);
    }
}
