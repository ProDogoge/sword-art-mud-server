package com.prodog.gamemodule.item.effect.obj;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.Map;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class PropEffect {
    private String name;
    private Map<String, Object> params;
}
