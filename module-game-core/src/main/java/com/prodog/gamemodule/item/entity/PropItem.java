package com.prodog.gamemodule.item.entity;

import com.prodog.gamemodule.item.effect.obj.PropEffect;
import com.prodog.gamemodule.item.filter.obj.PropFilter;
import lombok.Data;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

/***
 * 道具
 */
@Data
public class PropItem extends GameItem {
    private String propType = "medicament";    //道具类型
    private List<Integer> allowState = Arrays.asList(1, 2);
    private List<PropFilter> filters = new ArrayList<>();   //使用前的过滤(检测一些条件用)
    private List<PropEffect> effects = new ArrayList<>();   //道具效果
    private int requiredLevel = 1;
    private Integer coolTime;   //冷却时间
    private int targetNums = 1; //目标数量
    private boolean affectTeam = true; //作用于己方
    private boolean ignoreDead;   //忽略单位死亡(除自身)
    private boolean ignoreMyDead;    //自身死亡时可使用
}
