package com.prodog.gamemodule.item.wrapper;

import com.prodog.database.annotation.Wrapper;
import com.prodog.gamemodule.item.entity.BadgeItem;
import com.prodog.gamemodule.item.wrapper.inf.LocalGameItemWrapper;

@Wrapper(path = "游戏数据/物品/徽章", module = "徽章数据")
public class BadgeWrapper extends LocalGameItemWrapper<BadgeItem, String> {
}
