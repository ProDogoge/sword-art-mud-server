package com.prodog.gamemodule.item.wrapper;

import com.prodog.database.annotation.Wrapper;
import com.prodog.gamemodule.item.entity.ActiveEquipItem;
import com.prodog.gamemodule.item.entity.EquipItem;
import com.prodog.gamemodule.item.wrapper.inf.MongoGameItemWrapper;
import com.prodog.utils.bean.SpringBeanUtils;
import com.prodog.utils.string.RegexUtil;

import java.util.regex.Matcher;

@Wrapper(path = "游戏数据/物品/装备实例", module = "装备实例数据")
public class ActiveEquipWrapper extends MongoGameItemWrapper<ActiveEquipItem, String> {
    public ActiveEquipItem getByName(String equipName) {
        try {
            Matcher matcher = RegexUtil.getMatcher("(.*?)(\\d+)", equipName);
            EquipItem equip = SpringBeanUtils.getBean(EquipWrapper.class).getByColumn("name", matcher.group(1));
            if (equip == null) return null;
            return getByColumns("equipId", equip.getId(), "serialNum", Long.parseLong(matcher.group(2)));
        } catch (Exception e) {
            //e.printStackTrace();
            return null;
        }
    }

    @Override
    public ActiveEquipItem getByColumn(String column, Object val) {
        if (column.equals("name")) return getByName((String) val);
        return super.getByColumn(column, val);
    }
}
