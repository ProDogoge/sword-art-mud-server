package com.prodog.gamemodule.menu;

import com.prodog.command.annonations.Command;
import com.prodog.command.annonations.CommandBean;
import com.prodog.command.annonations.Weight;
import com.prodog.command.entity.CommandResult;
import com.prodog.util.MsgTemplateUtil;
import lombok.RequiredArgsConstructor;

import java.util.HashMap;

@CommandBean(setName = "菜单操作")
@RequiredArgsConstructor
public class MenuHandler {

    @Command(value = "(菜单|功能|功能列表|菜单列表)")
    public CommandResult funcs() {
        return CommandResult.success(MsgTemplateUtil.formatMsg("游戏配置/消息模板/菜单/功能列表.txt", new HashMap<>()));
    }

    @Command(value = "(角色系统|角色|角色功能)")
    public CommandResult roleFuncs() {
        return CommandResult.success(MsgTemplateUtil.formatMsg("游戏配置/消息模板/菜单/角色系统.txt", new HashMap<>()));
    }

    @Command(value = "(装备系统|装备功能)")
    public CommandResult equipFuncs() {
        return CommandResult.success(MsgTemplateUtil.formatMsg("游戏配置/消息模板/菜单/装备系统.txt", new HashMap<>()));
    }

    @Command(value = "(背包系统|背包功能)")
    @Weight(2)
    public CommandResult bagFuncs() {
        return CommandResult.success(MsgTemplateUtil.formatMsg("游戏配置/消息模板/菜单/背包系统.txt", new HashMap<>()));
    }

    @Command(value = "(地图系统|地图功能)")
    public CommandResult mapFuncs() {
        return CommandResult.success(MsgTemplateUtil.formatMsg("游戏配置/消息模板/菜单/地图系统.txt", new HashMap<>()));
    }

    @Command(value = "(商店系统|商店功能)")
    public CommandResult shopFuncs() {
        return CommandResult.success(MsgTemplateUtil.formatMsg("游戏配置/消息模板/菜单/商店系统.txt", new HashMap<>()));
    }

    @Command(value = "(道具系统|道具功能|道具)")
    public CommandResult propItemFuncs() {
        return CommandResult.success(MsgTemplateUtil.formatMsg("游戏配置/消息模板/菜单/道具系统.txt", new HashMap<>()));
    }

    @Command(value = "(战斗系统|战斗功能)")
    public CommandResult fightFuncs() {
        return CommandResult.success(MsgTemplateUtil.formatMsg("游戏配置/消息模板/菜单/战斗系统.txt", new HashMap<>()));
    }

    @Command(value = "(职业系统|职业功能|职业)")
    public CommandResult professionFuncs() {
        return CommandResult.success(MsgTemplateUtil.formatMsg("游戏配置/消息模板/菜单/职业系统.txt", new HashMap<>()));
    }

    @Command(value = "(技能系统|技能功能|技能)")
    public CommandResult skillFuncs() {
        return CommandResult.success(MsgTemplateUtil.formatMsg("游戏配置/消息模板/菜单/技能系统.txt", new HashMap<>()));
    }

    @Command(value = "(击杀系统|击杀功能|击杀)")
    public CommandResult killFuncs() {
        return CommandResult.success(MsgTemplateUtil.formatMsg("游戏配置/消息模板/菜单/击杀系统.txt", new HashMap<>()));
    }

    @Command(value = "(礼包系统|礼包功能)")
    public CommandResult giftFuncs() {
        return CommandResult.success(MsgTemplateUtil.formatMsg("游戏配置/消息模板/菜单/礼包系统.txt", new HashMap<>()));
    }

    @Command(value = "(队伍|队伍系统|队伍功能)")
    public CommandResult teamFuncs() {
        return CommandResult.success(MsgTemplateUtil.formatMsg("游戏配置/消息模板/菜单/队伍系统.txt", new HashMap<>()));
    }
}
