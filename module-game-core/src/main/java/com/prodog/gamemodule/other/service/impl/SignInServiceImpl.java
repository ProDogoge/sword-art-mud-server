package com.prodog.gamemodule.other.service.impl;

import cn.hutool.core.date.DateUtil;
import cn.hutool.core.util.IdUtil;
import com.prodog.command.entity.CommandResult;
import com.prodog.command.entity.RoleGameCommand;
import com.prodog.gamemodule.config.dict.DictConfig;
import com.prodog.gamemodule.config.sign.SignInConfig;
import com.prodog.gamemodule.config.sign.SignInItem;
import com.prodog.gamemodule.config.sign.SignInLevelConfig;
import com.prodog.gamemodule.item.entity.GameItem;
import com.prodog.gamemodule.other.domain.model.SignInRecord;
import com.prodog.gamemodule.other.service.SignInService;
import com.prodog.gamemodule.other.wrapper.SignInRecordWrapper;
import com.prodog.usermodule.role.entity.RoleBag;
import com.prodog.usermodule.role.util.BagUtil;
import com.prodog.usermodule.role.util.GameItemUtil;
import com.prodog.usermodule.role.util.RoleInfoUtil;
import com.prodog.usermodule.role.wrapper.RoleBagWrapper;
import com.prodog.usermodule.role.wrapper.RoleInfoWrapper;
import com.prodog.usermodule.user.wrapper.UserInfoWrapper;
import com.prodog.utils.exception.CommonException;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.Date;

@Service
@RequiredArgsConstructor
public class SignInServiceImpl implements SignInService {
    private final SignInRecordWrapper signInRecordWrapper;
    private final RoleBagWrapper roleBagWrapper;
    private final SignInConfig signInConfig;
    private final RoleInfoWrapper roleInfoWrapper;
    private final UserInfoWrapper userInfoWrapper;
    private final DictConfig dictConfig;

    @Override
    @Transactional(rollbackFor = Exception.class)
    public CommandResult signIn(RoleGameCommand cmd) throws CommonException {
        String prefix = RoleInfoUtil.getPrefix(cmd.getRoleInfo());
        //检查是否已经签到过了
        Date now = new Date();
        int year = DateUtil.year(now);
        int month = DateUtil.month(now);
        int day = DateUtil.dayOfMonth(now);
        Date date = DateUtil.parse(year + "-" + month + "-" + day, "yyyy-MM-dd");
        SignInRecord record = signInRecordWrapper.getByColumns("roleId", cmd.getRoleId(), "signDate", date);
        if (record != null) return CommandResult.failure(prefix + "您今天已经签到过了。");

        //获取当天签到的奖励 检查背包物品是否超出上限
        SignInLevelConfig levelConfig = signInConfig.getSignInLevelConfigs().stream()
                .filter(config -> config.getStartLevel() <= cmd.getRoleInfo().getLevel() && config.getEndLevel() >= cmd.getRoleInfo().getLevel())
                .findAny().orElse(null);
        if (levelConfig == null) return CommandResult.failure(prefix + "没有找到适合等级段的签到配置。");

        StringBuilder res = new StringBuilder(prefix).append("签到成功！获得以下物品:\n");
        RoleBag roleBag = roleBagWrapper.getById(cmd.getRoleId());
        SignInItem signInItem = levelConfig.getSignInItems().get(DateUtil.dayOfWeek(now) - 1);
        for (String itemStr : signInItem.getItems()) {
            String[] split = itemStr.split("\\s+");
            String itemId = split[0];
            long nums = Long.parseLong(split[1]);
            BagUtil.addItemWithCheck(roleBag, itemId, nums, prefix + "签到失败，您背包中的[{{itemName}}]持有数已达上限。");

            GameItem gameItem = GameItemUtil.getById(itemId);
            res.append(gameItem.getName()).append("x").append(nums).append("\n");
        }

        cmd.getRoleInfo().addMoney(signInItem.getMoney());
        cmd.getUserInfo().addCopoun(signInItem.getCopoun());
        if (signInItem.getMoney() > 0)
            res.append(dictConfig.get("money")).append(":").append(signInItem.getMoney()).append("  ");
        if (signInItem.getCopoun() > 0)
            res.append(dictConfig.get("copoun")).append(":").append(signInItem.getCopoun());

        //保存背包 角色 账户
        roleBagWrapper.save(roleBag);
        roleInfoWrapper.save(cmd.getRoleInfo());
        userInfoWrapper.save(cmd.getUserInfo());
        //签到记录
        signInRecordWrapper.save(new SignInRecord(IdUtil.simpleUUID(), cmd.getRoleId(), now, date));
        return CommandResult.success(res.toString().trim());
    }
}
