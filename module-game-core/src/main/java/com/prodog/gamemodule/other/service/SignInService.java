package com.prodog.gamemodule.other.service;

import com.prodog.command.entity.CommandResult;
import com.prodog.command.entity.RoleGameCommand;
import com.prodog.utils.exception.CommonException;

public interface SignInService {
    /***
     * 签到
     * @param cmd
     * @return
     */
    CommandResult signIn(RoleGameCommand cmd) throws CommonException;
}
