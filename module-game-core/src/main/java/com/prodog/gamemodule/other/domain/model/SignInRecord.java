package com.prodog.gamemodule.other.domain.model;

import org.springframework.data.mongodb.core.mapping.Document;

import java.util.Date;

@Document("g_sign_in_record")
public class SignInRecord {
    private String id;
    private String roleId;  //角色ID
    private Date signTime;  //签到时间
    private Date signDate;  //签到日期

    public SignInRecord() {
    }

    public SignInRecord(String id, String roleId, Date signTime, Date signDate) {
        this.id = id;
        this.roleId = roleId;
        this.signTime = signTime;
        this.signDate = signDate;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getRoleId() {
        return roleId;
    }

    public void setRoleId(String roleId) {
        this.roleId = roleId;
    }

    public Date getSignTime() {
        return signTime;
    }

    public void setSignTime(Date signTime) {
        this.signTime = signTime;
    }

    public Date getSignDate() {
        return signDate;
    }

    public void setSignDate(Date signDate) {
        this.signDate = signDate;
    }
}
