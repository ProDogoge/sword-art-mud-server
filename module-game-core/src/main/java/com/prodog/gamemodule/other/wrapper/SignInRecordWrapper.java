package com.prodog.gamemodule.other.wrapper;

import com.prodog.database.annotation.Wrapper;
import com.prodog.database.wrapper.MongoDataWrapper;
import com.prodog.gamemodule.other.domain.model.SignInRecord;

@Wrapper(path = "玩家数据/角色/签到记录", module = "签到记录数据")
public class SignInRecordWrapper extends MongoDataWrapper<SignInRecord, String> {
}
