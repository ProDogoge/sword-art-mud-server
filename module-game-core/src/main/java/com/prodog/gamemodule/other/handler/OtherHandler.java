package com.prodog.gamemodule.other.handler;

import com.prodog.command.annonations.Command;
import com.prodog.command.annonations.CommandBean;
import com.prodog.command.annonations.Iptor;
import com.prodog.command.annonations.Iptors;
import com.prodog.command.entity.CommandResult;
import com.prodog.command.entity.RoleGameCommand;
import com.prodog.gamemodule.other.service.SignInService;
import com.prodog.usermodule.role.interceptor.StateInterceptor;
import com.prodog.usermodule.user.interceptor.NotRegistInterceptor;
import com.prodog.utils.exception.CommonException;
import lombok.RequiredArgsConstructor;

@CommandBean(setName = "其它操作")
@RequiredArgsConstructor
@Iptors(vals = @Iptor(value = NotRegistInterceptor.class))
public class OtherHandler {
    private final SignInService signInService;

    @Command(value = "签到")
    @Iptors(vals = @Iptor(value = StateInterceptor.class, params = "{'states':[2,3],'required':false}", msg = "[{{state}}]状态下无法签到。"))
    public CommandResult signIn(RoleGameCommand cmd) {
        try {
            return signInService.signIn(cmd);
        } catch (CommonException e) {
            return CommandResult.failure(e.getMessage());
        } catch (Exception e) {
            e.printStackTrace();
            return CommandResult.failure("服务器发生错误，签到失败。");
        }
    }
}
