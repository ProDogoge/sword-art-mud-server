package com.prodog.gamemodule.runner;

import com.prodog.gamemodule.config.BaseConfig;
import com.prodog.usermodule.role.entity.RoleInfo;
import com.prodog.usermodule.role.wrapper.RoleInfoWrapper;
import com.prodog.utils.quartz.QuartzUtil;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.boot.ApplicationArguments;
import org.springframework.boot.ApplicationRunner;
import org.springframework.core.annotation.Order;
import org.springframework.stereotype.Component;

import java.util.List;

@Slf4j(topic = "精力恢复启动器")
@Component
@Order(10000)
@RequiredArgsConstructor
public class ReplyEnergyRunner implements ApplicationRunner {
    private final RoleInfoWrapper roleInfoWrapper;

    private final BaseConfig baseConfig;

    @Override
    public void run(ApplicationArguments args) {
        String cron = "0 0/" + baseConfig.getEnergyMinute() + " * * * ?";
        QuartzUtil.schedule("REPLY_ENERGY", (context) -> {
            List<RoleInfo> roles = roleInfoWrapper.list();
            for (RoleInfo role : roles) {
                RoleInfo r = roleInfoWrapper.getById(role.getId());
                r.setEnergy(Math.min(r.getEnergy() + baseConfig.getReplyEnergy(), baseConfig.getBaseEnergy()));
                roleInfoWrapper.save(r);
            }
        }, cron);
        log.info("定时精力恢复已开启...");
    }
}
