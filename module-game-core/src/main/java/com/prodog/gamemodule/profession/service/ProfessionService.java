package com.prodog.gamemodule.profession.service;

import com.prodog.usermodule.role.entity.RoleInfo;
import org.springframework.transaction.annotation.Transactional;

public interface ProfessionService {
    @Transactional(rollbackFor = Exception.class)
    void changeProfession(RoleInfo info, String professionId);
}
