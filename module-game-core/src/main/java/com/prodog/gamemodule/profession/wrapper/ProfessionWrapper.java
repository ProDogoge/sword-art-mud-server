package com.prodog.gamemodule.profession.wrapper;

import com.prodog.database.annotation.Wrapper;
import com.prodog.database.wrapper.LocalDataWrapper;
import com.prodog.gamemodule.profession.entity.Profession;

@Wrapper(path = "游戏数据/职业/职业信息", module = "职业信息")
public class ProfessionWrapper extends LocalDataWrapper<Profession, String> {
}
