package com.prodog.gamemodule.profession.entity;

import com.prodog.commonmodule.prop.entity.ObjProp;
import com.prodog.gamemodule.config.dict.DictConfig;
import com.prodog.utils.bean.SpringBeanUtils;
import lombok.Data;

import java.util.List;
import java.util.stream.Collectors;

@Data
public class Profession {
    private String id;
    private String professionName; //职业名称
    private List<String> enableWeapons; //允许使用的武器类型
    private boolean initial;  //是否初始职业
    private ObjProp initialProp;   //职业初始属性
    private List<ProfessionGrowth> growths; //职业成长
    private List<String> initialSkills; //初始化技能
    private List<String> canBeLearned; //可学习的技能
    private List<ProfessionTransfer> transfers; //可转职职业


    public ProfessionGrowth getGrowth(int level) {
        for (ProfessionGrowth growth : growths) {
            if (level >= growth.getStartLevel() && level <= growth.getEndLevel())
                return growth;
        }
        return null;
    }

    public String getCanWearWeapons() {
        if (enableWeapons.size() == 0) return "无";
        DictConfig dict = SpringBeanUtils.getBean(DictConfig.class);
        return enableWeapons.stream().map(dict::get).collect(Collectors.joining("、"));
    }
}
