package com.prodog.gamemodule.profession.entity;

import lombok.Data;

@Data
public class ProfessionTransfer {
    private String professionId; //职业ID
    private int level; //转职等级
}
