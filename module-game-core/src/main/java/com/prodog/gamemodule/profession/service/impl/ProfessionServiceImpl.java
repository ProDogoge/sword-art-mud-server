package com.prodog.gamemodule.profession.service.impl;

import com.prodog.command.annonations.Service;
import com.prodog.gamemodule.profession.entity.Profession;
import com.prodog.gamemodule.profession.service.ProfessionService;
import com.prodog.gamemodule.profession.wrapper.ProfessionWrapper;
import com.prodog.usermodule.role.entity.RoleInfo;
import com.prodog.usermodule.role.entity.RoleSkill;
import com.prodog.usermodule.role.wrapper.RoleInfoWrapper;
import com.prodog.usermodule.role.wrapper.RoleSkillWrapper;
import lombok.RequiredArgsConstructor;
import org.springframework.transaction.annotation.Transactional;

import java.util.HashMap;

@Service
@RequiredArgsConstructor
public class ProfessionServiceImpl implements ProfessionService {
    private final RoleSkillWrapper roleSkillWrapper;
    private final ProfessionWrapper professionWrapper;
    private final RoleInfoWrapper roleInfoWrapper;

    @Override
    @Transactional(rollbackFor = Exception.class)
    public void changeProfession(RoleInfo info, String professionId) {
        RoleSkill roleSkill = roleSkillWrapper.getById(info.getId());
        roleSkill.setLearnedSkill(new HashMap<>());
        Profession profession = professionWrapper.getById(professionId);
        profession.getInitialSkills().forEach(skillId -> roleSkill.getLearnedSkill().put(skillId, 1));
        roleSkillWrapper.save(roleSkill);
        info.setProfessionId(professionId);
        roleInfoWrapper.save(info);
    }
}
