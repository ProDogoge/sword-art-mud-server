package com.prodog.gamemodule.profession.entity;

import com.prodog.commonmodule.prop.entity.ObjProp;
import lombok.Data;

@Data
public class ProfessionGrowth {
    private int startLevel; //开始等级
    private int endLevel; //结束等级
    private ObjProp prop; //属性成长
}
