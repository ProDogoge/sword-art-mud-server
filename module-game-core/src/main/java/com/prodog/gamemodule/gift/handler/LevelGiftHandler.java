package com.prodog.gamemodule.gift.handler;

import com.prodog.command.annonations.Command;
import com.prodog.command.annonations.CommandBean;
import com.prodog.command.annonations.Iptor;
import com.prodog.command.annonations.Iptors;
import com.prodog.command.entity.CommandResult;
import com.prodog.command.entity.RoleGameCommand;
import com.prodog.gamemodule.gift.service.GiftService;
import com.prodog.usermodule.role.interceptor.StateInterceptor;
import com.prodog.usermodule.user.interceptor.NotRegistInterceptor;
import com.prodog.utils.exception.CommonException;
import lombok.RequiredArgsConstructor;

@CommandBean(setName = "成长礼包操作")
@RequiredArgsConstructor
@Iptors(vals = @Iptor(value = NotRegistInterceptor.class))
public class LevelGiftHandler {
    private final GiftService giftService;

    @Command("礼包")
    @Iptors(vals = @Iptor(value = StateInterceptor.class, params = "{'states':[2,3],'required':false}", msg = "[{{state}}]状态下无法进行此操作。"))
    public CommandResult currGift(RoleGameCommand command) {
        try {
            return giftService.currGift(command);
        } catch (CommonException e) {
            return CommandResult.failure(e.getMessage());
        } catch (Exception e) {
            e.printStackTrace();
            return CommandResult.failure("服务器发生错误，礼包信息获取失败。");
        }
    }

    @Command("开礼包")
    @Iptors(vals = @Iptor(value = StateInterceptor.class, params = "{'states':[1]}", msg = "[{{state}}]状态下无法进行此操作。"))
    public CommandResult openGift(RoleGameCommand command) {
        try {
            return giftService.openGift(command);
        } catch (CommonException e) {
            return CommandResult.failure(e.getMessage());
        } catch (Exception e) {
            e.printStackTrace();
            return CommandResult.failure("服务器发生错误，礼包开启失败。");
        }
    }

    @Command(value = "查询礼包\\s*(.+)", props = "giftName")
    @Iptors(vals = @Iptor(value = StateInterceptor.class, params = "{'states':[2,3],'required':false}", msg = "[{{state}}]状态下无法进行此操作。"))
    public CommandResult searchGift(RoleGameCommand cmd, String giftName) {
        try {
            return giftService.searchGift(cmd, giftName);
        } catch (CommonException e) {
            return CommandResult.failure(e.getMessage());
        } catch (Exception e) {
            e.printStackTrace();
            return CommandResult.failure("服务器发生错误，礼包信息获取失败。");
        }
    }

    @Command("礼包列表")
    @Iptors(vals = @Iptor(value = StateInterceptor.class, params = "{'states':[2,3],'required':false}", msg = "[{{state}}]状态下无法进行此操作。"))
    public CommandResult giftList(RoleGameCommand cmd) {
        try {
            return giftService.giftList(cmd);
        } catch (Exception e) {
            e.printStackTrace();
            return CommandResult.failure("服务器发生错误，礼包信息获取失败。");
        }
    }
}
