package com.prodog.gamemodule.gift.service.impl;

import com.prodog.command.annonations.Service;
import com.prodog.command.entity.CommandResult;
import com.prodog.command.entity.RoleGameCommand;
import com.prodog.gamemodule.config.dict.DictConfig;
import com.prodog.gamemodule.config.levelGift.LevelGift;
import com.prodog.gamemodule.config.levelGift.LevelGiftConfig;
import com.prodog.gamemodule.gift.service.GiftService;
import com.prodog.gamemodule.item.entity.GameItem;
import com.prodog.usermodule.role.entity.RoleBag;
import com.prodog.usermodule.role.entity.RoleEtc;
import com.prodog.usermodule.role.util.BagUtil;
import com.prodog.usermodule.role.util.GameItemUtil;
import com.prodog.usermodule.role.util.RoleInfoUtil;
import com.prodog.usermodule.role.wrapper.RoleBagWrapper;
import com.prodog.usermodule.role.wrapper.RoleEtcWrapper;
import com.prodog.usermodule.role.wrapper.RoleInfoWrapper;
import com.prodog.usermodule.user.wrapper.UserInfoWrapper;
import com.prodog.utils.exception.CommonException;
import lombok.RequiredArgsConstructor;
import org.springframework.transaction.annotation.Transactional;

@Service
@RequiredArgsConstructor
public class GiftServiceImpl implements GiftService {
    private final RoleEtcWrapper roleEtcWrapper;
    private final RoleBagWrapper roleBagWrapper;
    private final UserInfoWrapper userInfoWrapper;
    private final RoleInfoWrapper roleInfoWrapper;
    private final DictConfig dictConfig;
    private final LevelGiftConfig giftConfig;

    private String giftInfo(LevelGift gift) {
        int currIdx = giftConfig.getGifts().indexOf(gift);
        StringBuilder res = new StringBuilder();
        res.append("名称：").append(gift.getName()).append("\n");
        res.append("领取等级：").append(gift.getRequiredLevel()).append("\n");
        res.append("礼包内容：\n");
        for (String content : gift.getContent()) {
            String itemId = content.split("\\s+")[0];
            long nums = Long.parseLong(content.split("\\s+")[1]);
            GameItem item = GameItemUtil.getById(itemId);
            res.append("[").append(item.getName()).append("]x").append(nums).append("\n");
        }
        if (gift.getMoney() > 0)
            res.append(dictConfig.get("money")).append(":").append(gift.getMoney()).append("  ");
        if (gift.getCopoun() > 0)
            res.append(dictConfig.get("copoun")).append(":").append(gift.getCopoun()).append("  ");
        res.deleteCharAt(res.length() - 1);
        if (giftConfig.getGifts().size() > currIdx + 1) {
            LevelGift next = giftConfig.getGifts().get(currIdx + 1);
            res.append("\n下阶段礼包：").append(next.getName());
        }
        return res.toString().trim();
    }

    @Override
    public CommandResult currGift(RoleGameCommand command) throws CommonException {
        RoleEtc etc = roleEtcWrapper.getById(command.getRoleId());
        String prefix = RoleInfoUtil.getPrefix(command.getRoleInfo());
        CommonException.bySupplier(() -> giftConfig.getGifts().size() <= etc.getNextGift(), prefix + "您已领取了所有成长礼包。");
        LevelGift gift = giftConfig.getGifts().get(etc.getNextGift());
        StringBuilder res = new StringBuilder(prefix).append("当前成长阶段礼包:\n");
        res.append(giftInfo(gift));
        return CommandResult.success(res.toString().trim());
    }

    @Override
    @Transactional(rollbackFor = Exception.class)
    public CommandResult openGift(RoleGameCommand cmd) throws CommonException {
        RoleEtc etc = roleEtcWrapper.getById(cmd.getRoleId());
        String prefix = RoleInfoUtil.getPrefix(cmd.getRoleInfo());
        CommonException.bySupplier(() -> giftConfig.getGifts().size() <= etc.getNextGift(), prefix + "您已领取了所有成长礼包。");
        LevelGift gift = giftConfig.getGifts().get(etc.getNextGift());
        CommonException.bySupplier(() -> gift.getRequiredLevel() > cmd.getRoleInfo().getLevel(), prefix + "开启失败，您的当前等级不足，要求等级:" + gift.getRequiredLevel() + "。");
        StringBuilder res = new StringBuilder(prefix).append("[").append(gift.getName()).append("]开启成功！获得以下物品:\n");
        RoleBag bag = roleBagWrapper.getById(cmd.getRoleId());
        for (String content : gift.getContent()) {
            String itemId = content.split("\\s+")[0];
            long nums = Long.parseLong(content.split("\\s+")[1]);
            GameItem item = GameItemUtil.getById(itemId);
            BagUtil.addItemWithCheck(bag, itemId, nums, prefix + "礼包开启失败，您背包中的{{itemName}}持有数已达上限。");
            res.append("[").append(item.getName()).append("]x").append(nums).append("\n");
        }
        cmd.getRoleInfo().setMoney(cmd.getRoleInfo().getMoney() + gift.getMoney());
        cmd.getUserInfo().setCopoun(cmd.getUserInfo().getCopoun() + gift.getCopoun());
        if (gift.getMoney() > 0 || gift.getCopoun() > 0) {
            res.append("获得");
            if (gift.getMoney() > 0)
                res.append(dictConfig.get("money")).append(":").append(gift.getMoney()).append("  ");
            if (gift.getCopoun() > 0) res.append(dictConfig.get("copoun")).append(":").append(gift.getCopoun());
            res.append("\n");
        }
        if (giftConfig.getGifts().size() > etc.getNextGift() + 1) {
            LevelGift next = giftConfig.getGifts().get(etc.getNextGift() + 1);
            res.append("下阶段礼包：").append(next.getName());
        }
        etc.setNextGift(etc.getNextGift() + 1);
        roleEtcWrapper.save(etc);
        roleBagWrapper.save(bag);
        roleInfoWrapper.save(cmd.getRoleInfo());
        userInfoWrapper.save(cmd.getUserInfo());
        return CommandResult.success(res.toString().trim());
    }

    @Override
    public CommandResult searchGift(RoleGameCommand cmd, String giftName) throws CommonException {
        LevelGift gift = giftConfig.getGifts().stream().filter(g -> g.getName().equals(giftName)).findAny().orElse(null);
        String prefix = RoleInfoUtil.getPrefix(cmd.getRoleInfo());
        CommonException.byNull(gift, prefix + "名称为[" + giftName + "]的礼包不存在。");
        StringBuilder res = new StringBuilder(prefix).append("该礼包信息如下:\n");
        res.append(giftInfo(gift));
        return CommandResult.success(res.toString().trim());
    }

    @Override
    public CommandResult giftList(RoleGameCommand cmd) {
        String prefix = RoleInfoUtil.getPrefix(cmd.getRoleInfo());
        RoleEtc etc = roleEtcWrapper.getById(cmd.getRoleId());
        StringBuilder res = new StringBuilder(prefix).append("所有成长礼包如下:\n");
        for (int i = 0; i < giftConfig.getGifts().size(); i++) {
            LevelGift gift = giftConfig.getGifts().get(i);
            res.append("[").append(gift.getName()).append("]").append("  等级:").append(gift.getRequiredLevel());
            if (etc.getNextGift() - 1 >= i) {
                res.append("  (已开启)");
            }
            res.append("\n");
        }
        return CommandResult.success(res.toString().trim());
    }
}
