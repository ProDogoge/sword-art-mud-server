package com.prodog.gamemodule.gift.service;

import com.prodog.command.entity.CommandResult;
import com.prodog.command.entity.RoleGameCommand;
import com.prodog.utils.exception.CommonException;

public interface GiftService {

    CommandResult currGift(RoleGameCommand command) throws CommonException;

    CommandResult openGift(RoleGameCommand command) throws CommonException;

    CommandResult searchGift(RoleGameCommand cmd, String giftName) throws CommonException;

    CommandResult giftList(RoleGameCommand cmd);
}
