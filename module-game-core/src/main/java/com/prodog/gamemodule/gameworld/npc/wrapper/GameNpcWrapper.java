package com.prodog.gamemodule.gameworld.npc.wrapper;

import com.prodog.database.annotation.Wrapper;
import com.prodog.database.wrapper.LocalDataWrapper;
import com.prodog.gamemodule.gameworld.npc.entity.GameNpc;

@Wrapper(path = "游戏数据/世界观/NPC", module = "NPC数据")
public class GameNpcWrapper extends LocalDataWrapper<GameNpc, String> {
}
