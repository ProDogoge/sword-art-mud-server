package com.prodog.gamemodule.gameworld.shop.service.impl;

import com.prodog.command.annonations.Iptor;
import com.prodog.command.annonations.Iptors;
import com.prodog.command.annonations.Service;
import com.prodog.command.entity.CommandResult;
import com.prodog.command.entity.RoleGameCommand;
import com.prodog.database.entity.Page;
import com.prodog.gamemodule.config.dict.DictConfig;
import com.prodog.gamemodule.gameworld.map.entity.GameMap;
import com.prodog.gamemodule.gameworld.map.wrapper.GameMapWrapper;
import com.prodog.gamemodule.gameworld.npc.entity.GameNpc;
import com.prodog.gamemodule.gameworld.npc.wrapper.GameNpcWrapper;
import com.prodog.gamemodule.gameworld.shop.entity.ItemShop;
import com.prodog.gamemodule.gameworld.shop.entity.ShopItem;
import com.prodog.gamemodule.gameworld.shop.service.ShopService;
import com.prodog.gamemodule.gameworld.shop.wrapper.ItemShopWrapper;
import com.prodog.gamemodule.item.entity.GameItem;
import com.prodog.usermodule.role.entity.RoleBag;
import com.prodog.usermodule.role.interceptor.CheckHadShopsInterceptor;
import com.prodog.usermodule.role.util.BagUtil;
import com.prodog.usermodule.role.util.GameItemUtil;
import com.prodog.usermodule.role.util.RoleInfoUtil;
import com.prodog.usermodule.role.wrapper.RoleBagWrapper;
import com.prodog.usermodule.role.wrapper.RoleInfoWrapper;
import com.prodog.usermodule.user.wrapper.UserInfoWrapper;
import com.prodog.utils.exception.CommonException;
import lombok.RequiredArgsConstructor;
import org.springframework.transaction.annotation.Transactional;

import java.util.ArrayList;
import java.util.List;
import java.util.Objects;
import java.util.concurrent.atomic.AtomicLong;
import java.util.stream.Collectors;

@Service
@RequiredArgsConstructor
public class ShopServiceImpl implements ShopService {
    private final GameMapWrapper mapWrapper;
    private final GameNpcWrapper npcWrapper;
    private final ItemShopWrapper shopWrapper;
    private final RoleInfoWrapper roleInfoWrapper;
    private final UserInfoWrapper userInfoWrapper;
    private final DictConfig dict;
    private final RoleBagWrapper roleBagWrapper;

    @Override
    public CommandResult shopItemPage(RoleGameCommand command, Integer shopIdx, Integer pno) throws CommonException {
        GameMap map = mapWrapper.getById(command.getRoleInfo().getMapId());
        String prefix = RoleInfoUtil.getPrefix(command.getRoleInfo());
        CommonException.bySupplier(() -> map.getNpcs() == null || map.getNpcs().size() == 0, prefix + "当前位置没有商店。");

        List<GameNpc> npcs = map.getNpcs().stream().map(npcWrapper::getById).collect(Collectors.toList());
        List<String> shopIds = npcs.stream().map(GameNpc::getItemShop).filter(Objects::nonNull).collect(Collectors.toList());
        CommonException.bySupplier(() -> shopIds.size() == 0, prefix + "当前位置没有商店。");
        CommonException.bySupplier(() -> shopIds.size() < shopIdx, prefix + "找不到指定索引的商店。");

        ItemShop shop = shopWrapper.getById(shopIds.get(shopIdx - 1));
        StringBuilder res = new StringBuilder();
        res.append(prefix).append("商店[").append(shop.getName()).append("]出售商品如下:\n");
        Page<ShopItem> page = Page.page(shop.getItems(), ShopItem.class, pno, 15);
        for (ShopItem shopItem : page.getDatas()) {
            GameItem item = GameItemUtil.getById(shopItem.getItemId());
            if (item == null) continue;
            res.append(item.getName()).append("      ");
            res.append(shopItem.getVal()).append(shopItem.getSellType() == 1 ? dict.get("money") : dict.get("copoun"));
            if (shopItem.getNums() > 1) {
                res.append("/").append(shopItem.getNums()).append("个");
            }
            res.append("      ");
            if (shopItem.getInventory() != null) {
                res.append("库存:").append(shopItem.getInventory());
            }
            res.append("\n");
        }
        res.append(page.getBottom());
        return CommandResult.success(res.toString().trim());
    }

    @Override
    @Transactional(rollbackFor = Exception.class)
    public CommandResult buy(RoleGameCommand command, String itemName, Integer nums) throws CommonException {
        //检查是否存在商店
        GameMap map = mapWrapper.getById(command.getRoleInfo().getMapId());
        String prefix = RoleInfoUtil.getPrefix(command.getRoleInfo());
        CommonException.bySupplier(() -> map.getNpcs() == null || map.getNpcs().size() == 0, prefix + "当前位置没有商店。");

        List<GameNpc> npcs = map.getNpcs().stream().map(npcWrapper::getById).collect(Collectors.toList());
        List<String> shopIds = npcs.stream().map(GameNpc::getItemShop).filter(Objects::nonNull).collect(Collectors.toList());
        CommonException.bySupplier(() -> shopIds.size() == 0, prefix + "当前位置没有商店。");

        AtomicLong consumeMoney = new AtomicLong(0);
        AtomicLong consumeCopoun = new AtomicLong(0);

        //检查物品
        GameItem item = GameItemUtil.getByName(itemName);
        CommonException.byNull(item, prefix + "购买失败，名称为[" + itemName + "]的物品不存在。");
        List<ItemShop> shops = shopWrapper.getByIds(shopIds);
        List<ShopItem> allItems = new ArrayList<>();
        shops.forEach((shop) -> allItems.addAll(shop.getItems()));
        ShopItem buyItem = allItems.stream().filter(i -> i.getItemId().equals(item.getId())).findAny().orElse(null);
        CommonException.byNull(buyItem, prefix + "当前位置的商店没有出售物品[" + item.getName() + "]。");

        //检查库存
        if (buyItem.getInventory() != null) {
            CommonException.bySupplier(() -> nums > buyItem.getInventory(), prefix + "购买失败，商品[" + itemName + "]库存不足。");
        }

        //检查持有上限
        boolean inLimit = BagUtil.checkAddLimit(command.getRoleId(), item.getId(), nums * buyItem.getNums());
        CommonException.bySupplier(() -> !inLimit, prefix + "购买失败，物品[" + item.getName() + "]持有数达到上限。");

        //检查货币
        if (buyItem.getSellType() == 1) {
            consumeMoney.addAndGet(buyItem.getVal() * nums);
        } else {
            consumeCopoun.addAndGet(buyItem.getVal() * nums);
        }
        long currMoney = command.getRoleInfo().getMoney();
        long currCopoun = command.getUserInfo().getCopoun();
        CommonException.bySupplier(() -> currMoney < consumeMoney.get(), prefix + "购买失败，您的" + dict.get("money") + "不足" + consumeMoney.get() + "，还差" + (consumeMoney.get() - currMoney) + "。");
        CommonException.bySupplier(() -> currCopoun < consumeCopoun.get(), prefix + "购买失败，您的" + dict.get("copoun") + "不足" + consumeCopoun.get() + "，还差" + (consumeCopoun.get() - currCopoun) + "。");

        //扣钱
        command.getRoleInfo().setMoney(currMoney - consumeMoney.get());
        command.getUserInfo().setCopoun(currCopoun - consumeCopoun.get());
        roleInfoWrapper.save(command.getRoleInfo());
        userInfoWrapper.save(command.getUserInfo());

        //减库存
        if (buyItem.getInventory() != null) {
            buyItem.setInventory(buyItem.getInventory() - nums);
        }
        shops.forEach(shopWrapper::save);

        BagUtil.addItem(command.getRoleId(), item.getId(), nums * buyItem.getNums());
        StringBuilder res = new StringBuilder(prefix + "购买成功，获得[" + itemName + "]x" + buyItem.getNums() * nums + "，本次花费");
        if (consumeMoney.get() > 0) {
            res.append(dict.get("money")).append(":").append(consumeMoney).append("  ");
        }
        if (consumeCopoun.get() > 0) {
            res.append(dict.get("copoun")).append(":").append(consumeCopoun).append("  ");
        }
        res.delete(res.length() - 2, res.length());
        res.append("。");
        return CommandResult.success(res.toString().trim());
    }

    @Override
    @Iptors(vals = @Iptor(CheckHadShopsInterceptor.class))
    @Transactional(rollbackFor = Exception.class)
    public CommandResult sell(RoleGameCommand command, String itemName, Integer nums) throws CommonException {
        String prefix = RoleInfoUtil.getPrefix(command.getRoleInfo());

        //检物
        GameItem item = GameItemUtil.getByName(itemName);
        CommonException.byNull(item, prefix + "出售失败，名称为[" + itemName + "]的物品不存在。");
        CommonException.bySupplier(() -> item.getTradeType() == 4, prefix + "出售失败，[" + itemName + "]交易类型为无法删除。");

        //检包
        RoleBag roleBag = roleBagWrapper.getById(command.getRoleId());
        CommonException.bySupplier(() -> !BagUtil.checkIsHad(roleBag, item.getId(), nums), prefix + "出售失败，您背包中的[" + itemName + "]不足" + nums + "个。");

        long sellMoney = item.getSellPrice() * nums;
        command.getRoleInfo().addMoney(sellMoney);
        roleInfoWrapper.save(command.getRoleInfo());

        BagUtil.consumeItemAndSave(roleBag, item.getId(), nums);
        StringBuilder res = new StringBuilder().append(prefix);
        res.append("成功出售[").append(itemName).append("]x").append(nums).append("，");
        res.append("获得").append(dict.get("money")).append(": ").append(sellMoney).append("。");
        return CommandResult.success(res.toString().trim());
    }
}
