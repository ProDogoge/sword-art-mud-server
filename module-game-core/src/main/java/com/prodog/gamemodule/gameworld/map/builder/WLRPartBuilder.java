package com.prodog.gamemodule.gameworld.map.builder;

import com.prodog.command.entity.RoleGameCommand;
import com.prodog.gamemodule.config.dict.DictConfig;
import com.prodog.gamemodule.gameworld.local.entiy.GameLocal;
import com.prodog.gamemodule.gameworld.local.wrapper.GameLocalWrapper;
import com.prodog.gamemodule.gameworld.map.entity.GameMap;
import com.prodog.gamemodule.gameworld.map.wrapper.GameMapWrapper;
import com.prodog.gamemodule.gameworld.region.entity.GameRegion;
import com.prodog.gamemodule.gameworld.region.wrapper.GameRegionWrapper;
import com.prodog.gamemodule.gameworld.world.entity.GameWorld;
import com.prodog.gamemodule.gameworld.world.wrapper.GameWorldWrapper;
import com.prodog.utils.interfaces.AutowiredBean;
import org.springframework.beans.factory.annotation.Autowired;

public class WLRPartBuilder implements MapInfoPartBuilder, AutowiredBean {
    private RoleGameCommand command;
    private GameMap curMap;
    private GameMap moveMap;

    @Autowired
    private GameMapWrapper mapWrapper;
    @Autowired
    private GameRegionWrapper regionWrapper;
    @Autowired
    private GameLocalWrapper localWrapper;
    @Autowired
    private GameWorldWrapper worldWrapper;

    @Autowired
    private DictConfig dictConfig;

    @Autowired

    public WLRPartBuilder(RoleGameCommand command, GameMap curMap, GameMap moveMap) {
        this.command = command;
        this.curMap = curMap;
        this.moveMap = moveMap;
        this.autowired();
    }

    @Override
    public String build() {
        StringBuilder sb = new StringBuilder();
        //对比世界 地域 区域是否不同
        GameRegion curRegion = regionWrapper.getById(curMap.getRegion());
        GameLocal curLocal = localWrapper.getById(curRegion.getLocalId());
        GameWorld curWorld = worldWrapper.getById(curLocal.getWorldId());
        GameRegion movRegion = regionWrapper.getById(moveMap.getRegion());
        GameLocal movLocal = localWrapper.getById(movRegion.getLocalId());
        GameWorld movWord = worldWrapper.getById(movLocal.getWorldId());

        if (!curMap.getId().equals(moveMap.getId())) sb.append("进入了地点：[" + moveMap.getName() + "]，");
        sb.append("您当前的位置信息如下：\n");
        if (!curWorld.getId().equals(movWord.getId()))
            sb.append("当前世界：").append("【").append(movWord.getName()).append("】\n");
        sb.append("地域：").append("【").append(movLocal.getName()).append("】\n");
        sb.append("区域：").append("<").append(movRegion.getName()).append("> ");

        if (curRegion.getRecommendMinLevel() != null) {
            //判断等级
            Integer minLevel = curRegion.getRecommendMinLevel() == null ? 0 : curRegion.getRecommendMinLevel();
            if (command.getRoleInfo().getLevel() < minLevel) {
                sb.append("推荐" + dictConfig.get("level") + "：Lv" + minLevel + " ");
                if (curRegion.getRecommendMaxLevel() != null) {
                    sb.append("~ Lv").append(curRegion.getRecommendMaxLevel()).append("，");
                } else {
                    sb.append("以上，");
                }
                sb.append(" 您当前的等级不建议停留在此区域，请尽快离开！！");
            }
        }
        sb.append("\n");
        sb.append("当前位置：").append("[").append(moveMap.getName()).append("]\n\n");
        return sb.toString();
    }
}
