package com.prodog.gamemodule.gameworld.map.builder;

import cn.hutool.core.collection.CollectionUtil;
import com.prodog.command.entity.RoleGameCommand;
import com.prodog.gamemodule.config.dict.MapDictConfig;
import com.prodog.gamemodule.gameworld.map.entity.GameMap;
import com.prodog.gamemodule.gameworld.map.wrapper.GameMapWrapper;
import com.prodog.gamemodule.gameworld.monster.entity.Monster;
import com.prodog.gamemodule.gameworld.monster.wrapper.MonsterWrapper;
import com.prodog.utils.interfaces.AutowiredBean;
import org.springframework.beans.factory.annotation.Autowired;

import java.util.List;
import java.util.stream.Collectors;

public class MonsterPartBuilder implements MapInfoPartBuilder, AutowiredBean {
    private RoleGameCommand command;
    private GameMap curMap;
    private GameMap moveMap;

    @Autowired
    private GameMapWrapper mapWrapper;
    @Autowired
    private MonsterWrapper monsterWrapper;
    @Autowired
    private MapDictConfig mapDictConfig;

    public MonsterPartBuilder(RoleGameCommand command, GameMap curMap, GameMap moveMap) {
        this.command = command;
        this.curMap = curMap;
        this.moveMap = moveMap;
        this.autowired();
    }

    @Override
    public String build() {
        StringBuilder sb = new StringBuilder();
        if (CollectionUtil.isNotEmpty(moveMap.getMonsters())) {
            List<Monster> monsters = monsterWrapper.getByIds(moveMap.getMonsters()).stream().collect(Collectors.toList());
            String monstersStr = monsters.stream().map(m -> "Lv." + m.getProp().getLevel() + " " + m.getName()).collect(Collectors.joining(","));
            sb.append("怪物：[").append(monstersStr).append("]").append("\n\n");
        }
        return sb.toString();
    }
}
