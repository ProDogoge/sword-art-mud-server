package com.prodog.gamemodule.gameworld.local.wrapper;

import com.prodog.database.annotation.Wrapper;
import com.prodog.database.wrapper.LocalDataWrapper;
import com.prodog.gamemodule.gameworld.local.entiy.GameLocal;

@Wrapper(path = "游戏数据/世界观/地域", module = "地域数据")
public class GameLocalWrapper extends LocalDataWrapper<GameLocal,String> {
}
