package com.prodog.gamemodule.gameworld.monster.entity;

import com.prodog.commonmodule.prop.entity.ObjProp;
import lombok.Data;

@Data
public class MonsterProp extends ObjProp {
    private String id;
}
