package com.prodog.gamemodule.gameworld.local.entiy;

import lombok.Data;

@Data
public class GameLocal {
    private String id;  //id
    private String worldId; //世界id
    private String name;    //地域名称
}
