package com.prodog.gamemodule.gameworld.map.checker;

public interface MoveMapChecker {
    MoveMapCheckRes check();
}
