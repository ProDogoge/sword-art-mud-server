package com.prodog.gamemodule.gameworld.map.util;

import com.prodog.gamemodule.gameworld.local.wrapper.GameLocalWrapper;
import com.prodog.gamemodule.gameworld.map.entity.GameMap;
import com.prodog.gamemodule.gameworld.map.wrapper.GameMapWrapper;
import com.prodog.gamemodule.gameworld.region.entity.GameRegion;
import com.prodog.gamemodule.gameworld.region.wrapper.GameRegionWrapper;
import com.prodog.utils.bean.SpringBeanUtils;
import org.springframework.stereotype.Component;

import javax.annotation.PostConstruct;
import java.util.List;

@Component
public class MapUtil {
    private static GameMapWrapper mapWrapper;
    private static GameRegionWrapper regionWrapper;
    private static GameLocalWrapper localWrapper;

    @PostConstruct
    public void init() {
        mapWrapper = SpringBeanUtils.getBean(GameMapWrapper.class);
        regionWrapper = SpringBeanUtils.getBean(GameRegionWrapper.class);
        localWrapper = SpringBeanUtils.getBean(GameLocalWrapper.class);
    }

    public static GameMap getMainCityByMapId(String mapId) {
        GameMap map = mapWrapper.getById(mapId);
        GameRegion region = regionWrapper.getById(map.getRegion());
        List<GameRegion> regions = regionWrapper.listByColumn("localId", region.getLocalId());
        for (GameRegion r : regions) {
            for (GameMap m : mapWrapper.listByColumn("region", r.getId())) {
                if (m.isMainCity()) return m;
            }
        }
        return null;
    }
}
