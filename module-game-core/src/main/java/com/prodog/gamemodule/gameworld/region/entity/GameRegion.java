package com.prodog.gamemodule.gameworld.region.entity;

import lombok.Data;

@Data
public class GameRegion {
    private String id;  //id
    private String localId; //地域ID
    private String name;    //区域名称
    private Integer recommendMinLevel;  //推荐最低等级
    private Integer recommendMaxLevel;  //推荐最高等级
    private Integer requiredMinLevel;   //要求最低等级
    private Integer requiredMaxLevel;   //要求最高等级
}
