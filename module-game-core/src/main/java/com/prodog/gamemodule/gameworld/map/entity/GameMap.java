package com.prodog.gamemodule.gameworld.map.entity;

import lombok.Data;

import java.util.List;

@Data
public class GameMap {
    private String id;
    private String region;  //所属区域
    private String name;    //名称
    private List<String> npcs;  //npc
    private List<String> monsters;  //怪物
    private Boolean playerInvisible;    //玩家不可视
    private Boolean regionMark; //区域标记点
    private Boolean localMark;  //地域标记点
    private Boolean worldMark;  //世界标记点
    private String portal;  //传送门ID
    private Integer requiredMinLevel;   //要求最低等级
    private Integer requiredMaxLevel;   //要求最高等级
    private String up;  //上
    private String down;    //下
    private String left;    //左
    private String right;   //右
    private boolean mainCity;   //主城(复活点)

    public String getNameStr() {
        return "name:" + name;
    }

    public Boolean getRegionMark() {
        return regionMark == null ? false : regionMark;
    }

    public Boolean getLocalMark() {
        return localMark == null ? false : localMark;
    }

    public Boolean getWorldMark() {
        return worldMark == null ? false : worldMark;
    }
}
