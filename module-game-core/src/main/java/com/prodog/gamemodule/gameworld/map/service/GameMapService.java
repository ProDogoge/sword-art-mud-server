package com.prodog.gamemodule.gameworld.map.service;

import com.prodog.command.entity.CommandResult;
import com.prodog.command.entity.RoleGameCommand;
import com.prodog.gamemodule.gameworld.map.entity.GameMap;

public interface GameMapService {
    CommandResult move(RoleGameCommand command, GameMap curMap, GameMap map);

    CommandResult move(RoleGameCommand command, String direction) throws Exception;

    CommandResult currentMap(RoleGameCommand command);
}
