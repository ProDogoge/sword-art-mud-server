package com.prodog.gamemodule.gameworld.map.checker;

import com.prodog.command.entity.RoleGameCommand;
import com.prodog.gamemodule.gameworld.map.entity.GameMap;

import java.util.ArrayList;
import java.util.List;

public class MoveMapCheckerStack {
    private RoleGameCommand command;
    private GameMap curMap;
    private GameMap moveMap;
    private List<MoveMapChecker> checkers;

    public MoveMapCheckerStack(RoleGameCommand command, GameMap curMap, GameMap moveMap) {
        this.command = command;
        this.curMap = curMap;
        this.moveMap = moveMap;
        this.initCheckers();
    }

    private void initCheckers() {
        checkers = new ArrayList<MoveMapChecker>() {{
            add(new DirectChecker(moveMap));
            add(new RegionLevelChecker(command, moveMap));
            add(new MapLevelChecker(command, moveMap));
        }};
    }

    public MoveMapCheckRes check() {
        MoveMapCheckRes res = MoveMapCheckRes.pass();
        for (MoveMapChecker checker : checkers) {
            res = checker.check();
            if (!res.isPass()) {
                return res;
            }
        }
        return res;
    }
}
