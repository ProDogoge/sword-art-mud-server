package com.prodog.gamemodule.gameworld.region.wrapper;

import com.prodog.database.annotation.Wrapper;
import com.prodog.database.wrapper.LocalDataWrapper;
import com.prodog.gamemodule.gameworld.region.entity.GameRegion;

@Wrapper(path = "游戏数据/世界观/区域", module = "区域数据")
public class GameRegionWrapper extends LocalDataWrapper<GameRegion, String> {
}
