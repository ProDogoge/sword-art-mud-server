package com.prodog.gamemodule.gameworld.map.checker;

import com.prodog.gamemodule.gameworld.map.entity.GameMap;
import com.prodog.util.MsgTemplateUtil;

import java.util.HashMap;

public class DirectChecker implements MoveMapChecker {
    private GameMap moveMap;

    public DirectChecker(GameMap moveMap) {
        this.moveMap = moveMap;
    }

    @Override
    public MoveMapCheckRes check() {
        if (moveMap == null) {
            return MoveMapCheckRes.intercept(MsgTemplateUtil.formatMsg("游戏配置/消息模板/移动_此路不通.txt", new HashMap()));
        }
        return MoveMapCheckRes.pass();
    }
}
