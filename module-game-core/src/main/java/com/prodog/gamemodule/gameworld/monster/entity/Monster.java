package com.prodog.gamemodule.gameworld.monster.entity;

import com.prodog.gamemodule.fight.effect.entity.FightEffect;
import lombok.Data;

import java.util.ArrayList;
import java.util.List;


@Data
public class Monster {
    private String id;
    private String name;    //怪物名称
    private String title;   //怪物的称号
    private MonsterProp prop;   //怪物属性
    private int objType = 2;    //单位类型 1.玩家 2.怪物 3.召唤物 4.精英怪物 5.领主怪物
    private String fightAi; //战斗AI类名
    private List<FightEffect> effects = new ArrayList<>();  //被动效果
    private List<MonsterSkill> skills = new ArrayList<>();  //怪物技能

    private long exp;
    private long money;
    private List<MonsterDrop> drops = new ArrayList<>();

    public MonsterProp getProp() {
        prop.setId(id);
        prop.setCurHp(prop.getHp());
        prop.setCurMp(prop.getMp());
        return prop;
    }
}
