package com.prodog.gamemodule.gameworld.map.builder;

import com.prodog.command.entity.RoleGameCommand;
import com.prodog.gamemodule.gameworld.map.entity.GameMap;

import java.util.ArrayList;
import java.util.List;

/***
 * 位置面板生成器
 */
public class MapInfoBuilder {
    private RoleGameCommand command;
    private GameMap curMap;
    private GameMap moveMap;
    private List<MapInfoPartBuilder> partBuilders;

    public MapInfoBuilder(RoleGameCommand command, GameMap curMap, GameMap moveMap) {
        this.command = command;
        this.curMap = curMap;
        this.moveMap = moveMap;
        this.initBuilder();
    }

    private void initBuilder() {
        this.partBuilders = new ArrayList<MapInfoPartBuilder>() {{
            add(new WLRPartBuilder(command, curMap, moveMap)); //生成区域信息
            add(new MonsterPartBuilder(command, curMap, moveMap));    //生成怪物信息
            add(new NpcPartBuilder(command, curMap, moveMap));    //生成NPC信息
            add(new MoveMapPartBuilder(command, curMap, moveMap));  //生成位置信息
        }};
    }

    public String build() {
        StringBuilder mapInfo = new StringBuilder();
        partBuilders.forEach(builder -> mapInfo.append(builder.build()));
        return mapInfo.toString();
    }
}
