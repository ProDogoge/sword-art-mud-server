package com.prodog.gamemodule.gameworld.map.wrapper;

import com.prodog.database.annotation.Wrapper;
import com.prodog.database.wrapper.LocalDataWrapper;
import com.prodog.gamemodule.gameworld.map.entity.GameMap;

@Wrapper(path = "游戏数据/世界观/地图", module = "地图数据")
public class GameMapWrapper extends LocalDataWrapper<GameMap, String> {
    public GameMap getByStr(String str) {
        GameMap gameMap;
        if (str == null) {
            return null;
        }
        if (str.startsWith("name:")) {
            String mapName = str.split(":")[1];
            gameMap = getByColumn("name", mapName);
        } else {
            gameMap = getById(str);
        }
        return gameMap;
    }
}
