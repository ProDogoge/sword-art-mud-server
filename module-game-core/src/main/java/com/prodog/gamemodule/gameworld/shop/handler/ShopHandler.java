package com.prodog.gamemodule.gameworld.shop.handler;

import com.prodog.command.annonations.*;
import com.prodog.command.entity.CommandResult;
import com.prodog.command.entity.RoleGameCommand;
import com.prodog.gamemodule.gameworld.shop.service.ShopService;
import com.prodog.usermodule.role.interceptor.StateInterceptor;
import com.prodog.usermodule.user.interceptor.NotRegistInterceptor;
import com.prodog.utils.exception.CommonException;
import lombok.RequiredArgsConstructor;

@CommandBean(setName = "商店操作")
@RequiredArgsConstructor
@Iptors(vals = {
        @Iptor(value = NotRegistInterceptor.class),
        @Iptor(value = StateInterceptor.class, params = "{'states':[1]}", msg = "[{{state}}]状态下无法进行此操作。")
})
public class ShopHandler {
    private final ShopService shopService;

    @Command(value = {"商店\\s*(\\d*)", "商店\\s*(\\d+)\\s+(\\d+)"}, props = {"pno", "shopIdx,pno"})
    public CommandResult shopItemPage(RoleGameCommand command, @Default("1") Integer shopIdx, @Default("1") Integer pno) {
        try {
            return shopService.shopItemPage(command, shopIdx, pno);
        } catch (CommonException e) {
            return CommandResult.failure(e.getMessage());
        } catch (Exception e) {
            e.printStackTrace();
            return CommandResult.failure("服务器发生错误，商店内容获取失败。");
        }
    }

    @Command(value = {
            "购买\\s*(.+?)\\s+(\\d+)",
            "购买\\s*(.+)",
    }, props = {"itemName,nums", "itemName"})
    public CommandResult buy(RoleGameCommand command, String itemName, @Default("1") Integer nums) {
        try {
            return shopService.buy(command, itemName, nums);
        } catch (CommonException e) {
            return CommandResult.failure(e.getMessage());
        } catch (Exception e) {
            e.printStackTrace();
            return CommandResult.failure("服务器发生错误，商店内容获取失败。");
        }
    }

    @Command(value = {
            "出售\\s*(.+?)\\s+(\\d+)",
            "出售\\s*(.+)",
    }, props = {"itemName,nums", "itemName"})
    public CommandResult sell(RoleGameCommand command, String itemName, @Default("1") Integer nums) {
        try {
            return shopService.sell(command, itemName, nums);
        } catch (CommonException e) {
            return CommandResult.failure(e.getMessage());
        } catch (Exception e) {
            e.printStackTrace();
            return CommandResult.failure("服务器发生错误，物品出售失败。");
        }
    }
}
