package com.prodog.gamemodule.gameworld.shop.entity;

import lombok.Data;

import java.util.List;

@Data
public class ItemShop {
    private String id;
    private String name;    //商店名称
    private String describe;    //商店描述
    private List<ShopItem> items;  //出售的物品
}
