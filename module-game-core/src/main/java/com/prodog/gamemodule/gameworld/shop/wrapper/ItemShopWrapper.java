package com.prodog.gamemodule.gameworld.shop.wrapper;

import com.prodog.database.annotation.Wrapper;
import com.prodog.database.wrapper.LocalDataWrapper;
import com.prodog.gamemodule.gameworld.shop.entity.ItemShop;

@Wrapper(path = "游戏数据/世界观/商店", module = "商店数据")
public class ItemShopWrapper extends LocalDataWrapper<ItemShop, String> {
}
