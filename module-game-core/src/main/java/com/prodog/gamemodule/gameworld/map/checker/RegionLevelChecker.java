package com.prodog.gamemodule.gameworld.map.checker;

import com.prodog.command.entity.RoleGameCommand;
import com.prodog.gamemodule.config.ServerConfig;
import com.prodog.gamemodule.gameworld.map.entity.GameMap;
import com.prodog.gamemodule.gameworld.region.entity.GameRegion;
import com.prodog.gamemodule.gameworld.region.wrapper.GameRegionWrapper;
import com.prodog.util.MsgTemplateUtil;
import com.prodog.utils.interfaces.AutowiredBean;
import org.springframework.beans.factory.annotation.Autowired;

import java.util.HashMap;

public class RegionLevelChecker implements MoveMapChecker, AutowiredBean {
    private RoleGameCommand command;
    private GameMap moveMap;

    @Autowired
    private GameRegionWrapper regionWrapper;
    @Autowired
    private ServerConfig serverConfig;

    public RegionLevelChecker(RoleGameCommand command, GameMap moveMap) {
        this.command = command;
        this.moveMap = moveMap;
        this.autowired();
    }

    @Override
    public MoveMapCheckRes check() {
        //获取区域判断等级区间
        GameRegion region = regionWrapper.getById(moveMap.getRegion());
        if (region.getRequiredMinLevel() != null || region.getRequiredMaxLevel() != null) {
            Integer min = region.getRequiredMinLevel() == null ? 0 : region.getRequiredMinLevel();
            Integer max = region.getRequiredMaxLevel() == null ? serverConfig.getMaxLevel() : region.getRequiredMaxLevel();
            region.setRequiredMinLevel(min);
            region.setRequiredMaxLevel(max);
            if (command.getRoleInfo().getLevel() < min || command.getRoleInfo().getLevel() > max) {
                return MoveMapCheckRes.intercept(MsgTemplateUtil.formatMsg("游戏配置/消息模板/移动_区域等级未达.txt", new HashMap<String, Object>() {{
                    put("region", region);
                    put("maxLevel", serverConfig.getMaxLevel());
                }}));
            }
        }
        return MoveMapCheckRes.pass();
    }
}
