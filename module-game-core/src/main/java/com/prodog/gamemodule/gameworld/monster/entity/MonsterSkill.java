package com.prodog.gamemodule.gameworld.monster.entity;

import lombok.Data;


@Data
public class MonsterSkill {
    private String skillId;
    private int level;
}
