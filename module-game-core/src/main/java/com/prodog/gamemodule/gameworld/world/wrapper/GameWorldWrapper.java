package com.prodog.gamemodule.gameworld.world.wrapper;

import com.prodog.database.annotation.Wrapper;
import com.prodog.database.wrapper.LocalDataWrapper;
import com.prodog.gamemodule.gameworld.world.entity.GameWorld;

@Wrapper(path = "游戏数据/世界观/世界",module = "世界数据")
public class GameWorldWrapper extends LocalDataWrapper<GameWorld,String> {
}
