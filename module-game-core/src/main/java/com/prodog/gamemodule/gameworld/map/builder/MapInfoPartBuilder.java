package com.prodog.gamemodule.gameworld.map.builder;

public interface MapInfoPartBuilder {
    String build();
}
