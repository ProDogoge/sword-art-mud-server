package com.prodog.gamemodule.gameworld.map.checker;

import lombok.Data;

@Data
public class MoveMapCheckRes {
    private boolean pass;
    private String msg;

    private MoveMapCheckRes(boolean pass, String msg) {
        this.pass = pass;
        this.msg = msg;
    }

    public static MoveMapCheckRes pass() {
        return new MoveMapCheckRes(true, null);
    }

    public static MoveMapCheckRes intercept(String msg) {
        return new MoveMapCheckRes(false, msg);
    }
}
