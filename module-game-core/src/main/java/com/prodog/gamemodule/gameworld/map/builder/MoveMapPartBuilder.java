package com.prodog.gamemodule.gameworld.map.builder;

import com.prodog.command.entity.RoleGameCommand;
import com.prodog.gamemodule.gameworld.map.entity.GameMap;
import com.prodog.gamemodule.gameworld.map.wrapper.GameMapWrapper;
import com.prodog.utils.interfaces.AutowiredBean;
import org.springframework.beans.factory.annotation.Autowired;

public class MoveMapPartBuilder implements MapInfoPartBuilder, AutowiredBean {
    private RoleGameCommand command;
    private GameMap curMap;
    private GameMap moveMap;

    @Autowired
    private GameMapWrapper mapWrapper;

    public MoveMapPartBuilder(RoleGameCommand command, GameMap curMap, GameMap moveMap) {
        this.command = command;
        this.curMap = curMap;
        this.moveMap = moveMap;
        this.autowired();
    }

    @Override
    public String build() {
        StringBuilder sb = new StringBuilder();
        GameMap up = mapWrapper.getByStr(moveMap.getUp());
        GameMap down = mapWrapper.getByStr(moveMap.getDown());
        GameMap left = mapWrapper.getByStr(moveMap.getLeft());
        GameMap right = mapWrapper.getByStr(moveMap.getRight());
        if (up != null) sb.append("上：[").append(up.getName()).append("]\n");
        if (down != null) sb.append("下：[").append(down.getName()).append("]\n");
        if (left != null) sb.append("左：[").append(left.getName()).append("]\n");
        if (right != null) sb.append("右：[").append(right.getName()).append("]\n");
        return sb.toString();
    }
}
