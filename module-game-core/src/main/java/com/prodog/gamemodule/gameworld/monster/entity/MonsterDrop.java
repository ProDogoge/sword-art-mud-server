package com.prodog.gamemodule.gameworld.monster.entity;

import lombok.Data;

import java.util.HashMap;
import java.util.Map;

@Data
public class MonsterDrop {
    private int dropRate;
    private int minDropNums = 1;
    private int maxDropNums = 1;
    private Map<String, Integer> dropItems = new HashMap<>();
}
