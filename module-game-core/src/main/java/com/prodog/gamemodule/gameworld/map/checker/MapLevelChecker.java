package com.prodog.gamemodule.gameworld.map.checker;

import com.prodog.command.entity.RoleGameCommand;
import com.prodog.gamemodule.config.ServerConfig;
import com.prodog.gamemodule.gameworld.map.entity.GameMap;
import com.prodog.util.MsgTemplateUtil;
import com.prodog.utils.interfaces.AutowiredBean;
import org.springframework.beans.factory.annotation.Autowired;

import java.util.HashMap;

public class MapLevelChecker implements MoveMapChecker, AutowiredBean {
    private RoleGameCommand command;
    private GameMap moveMap;

    @Autowired
    private ServerConfig serverConfig;

    public MapLevelChecker(RoleGameCommand command, GameMap moveMap) {
        this.command = command;
        this.moveMap = moveMap;
        this.autowired();
    }

    @Override
    public MoveMapCheckRes check() {
        //判断等级区间
        if (moveMap.getRequiredMinLevel() != null || moveMap.getRequiredMaxLevel() != null) {
            Integer min = moveMap.getRequiredMinLevel() == null ? 0 : moveMap.getRequiredMinLevel();
            Integer max = moveMap.getRequiredMaxLevel() == null ? serverConfig.getMaxLevel() : moveMap.getRequiredMaxLevel();
            moveMap.setRequiredMinLevel(min);
            moveMap.setRequiredMaxLevel(max);
            if (command.getRoleInfo().getLevel() < min || command.getRoleInfo().getLevel() > max) {
                return MoveMapCheckRes.intercept(MsgTemplateUtil.formatMsg("游戏配置/消息模板/移动_地图等级未达.txt", new HashMap<String, Object>() {{
                    put("moveMap", moveMap);
                    put("maxLevel", serverConfig.getMaxLevel());
                }}));
            }
        }
        return MoveMapCheckRes.pass();
    }
}
