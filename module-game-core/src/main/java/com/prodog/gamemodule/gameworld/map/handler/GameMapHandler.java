package com.prodog.gamemodule.gameworld.map.handler;

import com.prodog.command.annonations.Command;
import com.prodog.command.annonations.CommandBean;
import com.prodog.command.annonations.Iptor;
import com.prodog.command.annonations.Iptors;
import com.prodog.command.entity.CommandResult;
import com.prodog.command.entity.RoleGameCommand;
import com.prodog.gamemodule.gameworld.map.service.GameMapService;
import com.prodog.usermodule.role.interceptor.StateInterceptor;
import com.prodog.usermodule.user.interceptor.NotRegistInterceptor;
import lombok.RequiredArgsConstructor;

@CommandBean(setName = "地图操作")
@RequiredArgsConstructor
@Iptors(vals = @Iptor(value = NotRegistInterceptor.class))
public class GameMapHandler {
    private final GameMapService mapService;

    @Command(value = "([上下左右])", props = "direction")
    @Iptors(vals = @Iptor(value = StateInterceptor.class, params = "{'states':[1]}", msg = "[{{state}}]状态下无法进行此操作。"))
    public CommandResult move(RoleGameCommand command, String direction) {
        try {
            return mapService.move(command, direction);
        } catch (Exception e) {
            e.printStackTrace();
            return CommandResult.failure("服务器发生错误，移动失败");
        }
    }

    @Command(value = "位置")
    @Iptors(vals = @Iptor(value = StateInterceptor.class, params = "{'states':[3],'required':false}", msg = "[{{state}}]状态下无法进行此操作。"))
    public CommandResult currentMap(RoleGameCommand command) {
        try {
            return mapService.currentMap(command);
        } catch (Exception e) {
            e.printStackTrace();
            return CommandResult.failure("服务器发生错误，获取失败");
        }
    }
}
