package com.prodog.gamemodule.gameworld.npc.entity;

import lombok.Data;

import java.util.List;

@Data
public class GameNpc {
    private String id;
    private String name;    //NPC名称
    private List<String> dialog;    //NPC对话
    private String itemShop;    //商店ID
    private List<String> funcs; //功能
}
