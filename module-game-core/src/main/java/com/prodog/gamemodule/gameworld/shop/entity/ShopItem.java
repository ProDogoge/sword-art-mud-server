package com.prodog.gamemodule.gameworld.shop.entity;

import com.prodog.usermodule.role.util.GameItemUtil;
import lombok.Data;

@Data
public class ShopItem {
    private String itemId;  //道具ID
    private int sellType = 1;   //出售方式 1.货币 2.点券
    private Long val;    //出售价格
    private long nums = 1;
    private Long inventory;  //库存 不设即为无限

    public Long getVal() {
        if (val != null) return val;
        return GameItemUtil.getById(itemId).getPrice();
    }
}
