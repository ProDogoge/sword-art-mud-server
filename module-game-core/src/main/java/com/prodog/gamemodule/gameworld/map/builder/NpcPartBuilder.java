package com.prodog.gamemodule.gameworld.map.builder;

import cn.hutool.core.collection.CollectionUtil;
import com.prodog.command.entity.RoleGameCommand;
import com.prodog.gamemodule.config.dict.MapDictConfig;
import com.prodog.gamemodule.gameworld.map.entity.GameMap;
import com.prodog.gamemodule.gameworld.map.wrapper.GameMapWrapper;
import com.prodog.gamemodule.gameworld.npc.entity.GameNpc;
import com.prodog.gamemodule.gameworld.npc.wrapper.GameNpcWrapper;
import com.prodog.gamemodule.gameworld.shop.entity.ItemShop;
import com.prodog.gamemodule.gameworld.shop.wrapper.ItemShopWrapper;
import com.prodog.utils.interfaces.AutowiredBean;
import org.springframework.beans.factory.annotation.Autowired;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

public class NpcPartBuilder implements MapInfoPartBuilder, AutowiredBean {
    private RoleGameCommand command;
    private GameMap curMap;
    private GameMap moveMap;

    @Autowired
    private GameMapWrapper mapWrapper;
    @Autowired
    private GameNpcWrapper npcWrapper;
    @Autowired
    private ItemShopWrapper shopWrapper;

    @Autowired
    private MapDictConfig mapDictConfig;

    public NpcPartBuilder(RoleGameCommand command, GameMap curMap, GameMap moveMap) {
        this.command = command;
        this.curMap = curMap;
        this.moveMap = moveMap;
        this.autowired();
    }

    @Override
    public String build() {
        StringBuilder sb = new StringBuilder();
        if (CollectionUtil.isNotEmpty(moveMap.getNpcs())) {
            sb.append("【NPC】\n");
            //获取当前地图所有NPC
            List<GameNpc> npcs = moveMap.getNpcs().stream().map(npc -> npcWrapper.getById(npc)).collect(Collectors.toList());
            //拼NPC名称 功能 随机对话
            for (GameNpc npc : npcs) {
                sb.append(npc.getName());
                if (CollectionUtil.isNotEmpty(npc.getFuncs())) {
                    List<String> funcs = npc.getFuncs().stream().map(func -> mapDictConfig.get("npcFunc", func)).collect(Collectors.toList());
                    sb.append("(功能：").append(funcs.stream().collect(Collectors.joining(","))).append(")");
                }
                if (CollectionUtil.isNotEmpty(npc.getDialog())) {
                    Collections.shuffle(npc.getDialog());
                    sb.append("：").append(npc.getDialog().get(0));
                }
                sb.append("\n");
            }
            sb.append("\n");
            //拼商店
            List<String> shopIds = new ArrayList<>();
            npcs.forEach(npc -> Optional.ofNullable(npc.getItemShop()).ifPresent(shopId -> shopIds.add(shopId)));
            List<ItemShop> shops = shopWrapper.getByIds(shopIds);
            if (CollectionUtil.isNotEmpty(shops)) {
                sb.append("【商店】\n");
                for (ItemShop shop : shops) {
                    sb.append("[").append(shop.getName()).append("]");
                    if (shop.getDescribe() != null) {
                        sb.append("：").append(shop.getDescribe());
                    }
                    sb.append("\n");
                }
                sb.append("\n");
            }
        }
        return sb.toString();
    }
}
