package com.prodog.gamemodule.gameworld.monster.interceptor;

import cn.hutool.core.collection.CollectionUtil;
import com.prodog.command.entity.GameCommand;
import com.prodog.command.entity.RoleGameCommand;
import com.prodog.command.interceptor.Interceptor;
import com.prodog.command.interceptor.InterceptorRes;
import com.prodog.gamemodule.gameworld.map.entity.GameMap;
import com.prodog.gamemodule.gameworld.map.wrapper.GameMapWrapper;
import com.prodog.usermodule.role.util.RoleInfoUtil;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Component;

import java.util.Map;

@Component
@RequiredArgsConstructor
public class NoMonsterInterceptor implements Interceptor {
    private final GameMapWrapper mapWrapper;

    @Override
    public InterceptorRes intercept(GameCommand command, Map<String, Object> params, String formatMsg) {
        RoleGameCommand cmd = (RoleGameCommand) command;
        GameMap map = mapWrapper.getById(cmd.getRoleInfo().getMapId());
        return CollectionUtil.isNotEmpty(map.getMonsters()) ? InterceptorRes.pass() : InterceptorRes.intercept(RoleInfoUtil.getPrefix(cmd.getRoleInfo()) + formatMsg);
    }
}
