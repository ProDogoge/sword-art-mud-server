package com.prodog.gamemodule.gameworld.world.entity;

import lombok.Data;

@Data
public class GameWorld {
    private String id;  //id
    private String name;    //世界名称
}
