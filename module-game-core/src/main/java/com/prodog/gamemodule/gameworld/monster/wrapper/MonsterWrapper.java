package com.prodog.gamemodule.gameworld.monster.wrapper;

import com.prodog.database.annotation.Wrapper;
import com.prodog.database.wrapper.LocalDataWrapper;
import com.prodog.gamemodule.gameworld.monster.entity.Monster;

@Wrapper(path = "游戏数据/世界观/怪物", module = "怪物数据")
public class MonsterWrapper extends LocalDataWrapper<Monster, String> {
}
