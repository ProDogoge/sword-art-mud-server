package com.prodog.gamemodule.gameworld.shop.service;

import com.prodog.command.entity.CommandResult;
import com.prodog.command.entity.RoleGameCommand;
import com.prodog.utils.exception.CommonException;

public interface ShopService {
    CommandResult shopItemPage(RoleGameCommand command, Integer shopIdx, Integer pno) throws CommonException;

    CommandResult buy(RoleGameCommand command, String itemName, Integer nums) throws CommonException;

    CommandResult sell(RoleGameCommand command, String itemName, Integer nums) throws CommonException;
}
