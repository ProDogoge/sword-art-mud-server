package com.prodog.gamemodule.secondProfession.job;

import com.prodog.gamemodule.item.entity.GameItem;
import com.prodog.gamemodule.secondProfession.constant.ForgeState;
import com.prodog.gamemodule.secondProfession.domain.dos.WorkingForgerItem;
import com.prodog.gamemodule.secondProfession.domain.po.Forger;
import com.prodog.gamemodule.secondProfession.domain.po.WorkingForger;
import com.prodog.gamemodule.secondProfession.service.ForgerService;
import com.prodog.gamemodule.secondProfession.util.SecondProfessionUtil;
import com.prodog.gamemodule.secondProfession.wrapper.SecondProfessionWrapper;
import com.prodog.gamemodule.secondProfession.wrapper.WorkingForgerWrapper;
import com.prodog.message.sender.MessageSender;
import com.prodog.usermodule.role.constant.RoleState;
import com.prodog.usermodule.role.entity.RoleBag;
import com.prodog.usermodule.role.entity.RoleInfo;
import com.prodog.usermodule.role.util.BagUtil;
import com.prodog.usermodule.role.util.GameItemUtil;
import com.prodog.usermodule.role.util.RoleInfoUtil;
import com.prodog.usermodule.role.wrapper.RoleBagWrapper;
import com.prodog.usermodule.role.wrapper.RoleInfoWrapper;
import com.prodog.utils.bean.SpringBeanUtils;
import com.prodog.utils.exception.CommonException;
import com.prodog.utils.interfaces.AutowiredBean;
import com.prodog.utils.quartz.QuartzUtil;
import com.prodog.utils.quartz.QzJobDetail;
import org.quartz.JobExecutionContext;
import org.springframework.beans.factory.annotation.Autowired;

import java.util.List;

import static com.prodog.gamemodule.secondProfession.service.ForgerService.WORKING_KEY;

public class ForgeJob implements QzJobDetail, AutowiredBean {
    @Autowired
    private RoleInfoWrapper roleInfoWrapper;
    @Autowired
    private WorkingForgerWrapper workingForgerWrapper;
    @Autowired
    private RoleBagWrapper roleBagWrapper;
    @Autowired
    private MessageSender messageSender;
    @Autowired
    private ForgerService forgerService;

    private Forger forger = (Forger) SpringBeanUtils.getBean(SecondProfessionWrapper.class).getById("1");

    private String roleId;

    public ForgeJob(String roleId) {
        this.roleId = roleId;
        this.autowired();
    }

    @Override
    public void execute(JobExecutionContext context) {
        RoleInfo roleInfo = roleInfoWrapper.getById(roleId);
        String prefix = RoleInfoUtil.getPrefix(roleInfo);
        WorkingForger workingForger = workingForgerWrapper.getById(roleId);
        RoleBag roleBag = roleBagWrapper.getById(roleId);
        StringBuilder msg = new StringBuilder(prefix).append("[");

        List<WorkingForgerItem> notCompleteItems = workingForger.getNotCompleteItems();
        try {
            WorkingForgerItem workingItem = notCompleteItems.get(0);
            GameItem item = GameItemUtil.getById(workingItem.getItemId());
            BagUtil.addItemWithCheck(roleBag, workingItem.getItemId(), 1, prefix + "您背包中的[" + item.getName() + "]持有数已达上限，无法自动完成锻造，请检查背包后手动完成锻造。");
            msg.append(item.getName()).append("]锻造完成！");
            workingForger.getItems().remove(workingItem);

            //副职经验
            long exp = forger.getForgeByItemId(item.getId()).getExp();
            msg.append("获得经验:").append(exp).append("，");
            SecondProfessionUtil.addExp(roleId, "1", exp);

            //保存
            workingForgerWrapper.save(workingForger);
            roleBagWrapper.save(roleBag);

            notCompleteItems = workingForger.getNotCompleteItems();
            if (notCompleteItems.size() > 0)
                msg.append("剩余锻造任务如下:\n").append(forgerService.workingStr(workingForger));
            else
                msg.append("所有锻造任务已完成！");
            messageSender.sendToSelf(roleInfo.getUserId(), msg.toString().trim());
        } catch (CommonException e) {
            notCompleteItems.get(0).setComplete(true);
            workingForgerWrapper.save(workingForger);
            messageSender.sendToSelf(roleInfo.getUserId(), e.getMessage());
        }

        //下一个任务进来
        notCompleteItems = workingForger.getNotCompleteItems();
        if (notCompleteItems.size() > 0) {
            WorkingForgerItem next = notCompleteItems.get(0);
            QuartzUtil.cancel(WORKING_KEY + roleId);
            QuartzUtil.schedule(WORKING_KEY + roleId, new ForgeJob(roleId), next.getEndTime());
        } else {
            roleInfo.setState(RoleState.NORMAL);
            workingForger.setState(ForgeState.STOP);
            roleInfoWrapper.save(roleInfo);
            workingForgerWrapper.save(workingForger);
        }
    }
}
