package com.prodog.gamemodule.secondProfession.domain.dos;

import lombok.Data;

import java.util.ArrayList;
import java.util.List;

/***
 * 副职技术
 */
@Data
public class SecondProfession {
    private String id;  //id
    private String name;    //副职业名称
    private String enName;  //英文名
    private String desc;    //副职业描述
    private String expression;  //经验公式
    private int maxLevel = 10;  //副职最高级别
    private int order;  //排序
    private List<String> levelTitles = new ArrayList<>();
}
