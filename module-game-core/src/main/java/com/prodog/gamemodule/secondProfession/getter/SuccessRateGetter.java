package com.prodog.gamemodule.secondProfession.getter;

import com.prodog.usermodule.caculate.CalVal;

public interface SuccessRateGetter {
    double get(CalVal<Double> res, String roleId);
}
