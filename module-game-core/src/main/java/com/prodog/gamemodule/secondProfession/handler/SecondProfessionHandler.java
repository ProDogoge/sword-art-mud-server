package com.prodog.gamemodule.secondProfession.handler;

import com.prodog.command.annonations.Command;
import com.prodog.command.annonations.CommandBean;
import com.prodog.command.annonations.Iptor;
import com.prodog.command.annonations.Iptors;
import com.prodog.command.entity.CommandResult;
import com.prodog.command.entity.RoleGameCommand;
import com.prodog.gamemodule.secondProfession.service.RoleSecondProfessionService;
import com.prodog.gamemodule.secondProfession.service.SecondProfessionService;
import com.prodog.usermodule.role.interceptor.StateInterceptor;
import com.prodog.usermodule.user.interceptor.NotRegistInterceptor;
import lombok.RequiredArgsConstructor;

@CommandBean(setName = "副职操作")
@RequiredArgsConstructor
@Iptors(vals = @Iptor(value = NotRegistInterceptor.class))
public class SecondProfessionHandler {
    private final RoleSecondProfessionService roleSecondProfessionService;
    private final SecondProfessionService secondProfessionService;

    @Command(value = "(副职业|副职|查看副职业|查看副职|副职业信息|副职信息)")
    @Iptors(vals = @Iptor(value = StateInterceptor.class, params = "{'states':[3],'required':false}", msg = "[{{state}}]状态下无法进行此操作。"))
    public CommandResult professionList(RoleGameCommand cmd) {
        try {
            return roleSecondProfessionService.professionList(cmd);
        } catch (Exception e) {
            e.printStackTrace();
            return CommandResult.failure("服务器发生错误,副职信息获取失败。");
        }
    }

    @Command(value = "(副职列表|副职业列表|副职说明|副职业说明|副职介绍|副职业介绍)")
    @Iptors(vals = @Iptor(value = StateInterceptor.class, params = "{'states':[3],'required':false}", msg = "[{{state}}]状态下无法进行此操作。"))
    public CommandResult myProfession(RoleGameCommand cmd) {
        try {
            return secondProfessionService.professionList(cmd);
        } catch (Exception e) {
            e.printStackTrace();
            return CommandResult.failure("服务器发生错误,副职列表获取失败。");
        }
    }
}
