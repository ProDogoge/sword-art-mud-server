package com.prodog.gamemodule.secondProfession.domain.po;

import com.alibaba.fastjson2.annotation.JSONField;
import com.prodog.gamemodule.secondProfession.domain.dos.Forge;
import com.prodog.gamemodule.secondProfession.domain.dos.WorkingForgerItem;
import com.prodog.gamemodule.secondProfession.wrapper.SecondProfessionWrapper;
import com.prodog.utils.bean.SpringBeanUtils;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.data.mongodb.core.mapping.Document;

import java.util.Date;
import java.util.List;
import java.util.stream.Collectors;

@Document("g_working_forger")
@Data
@NoArgsConstructor
@AllArgsConstructor
public class WorkingForger {
    private String id;  //角色ID
    private List<WorkingForgerItem> items;    //锻造中的物品
    private int state = 1;  //任务状态 1.未运行 2.运行中 3.暂停

    @JSONField(serialize = false)
    public List<WorkingForgerItem> getNotCompleteItems() {
        return items.stream().filter(f -> !f.isComplete()).collect(Collectors.toList());
    }

    @JSONField(serialize = false)
    public List<WorkingForgerItem> getCompleteItems() {
        return items.stream().filter(WorkingForgerItem::isComplete).collect(Collectors.toList());
    }

    public void resetEndTimeFrom(int index) {
        List<WorkingForgerItem> notCompletes = getNotCompleteItems();
        Date prevEndTime = index == 0 ? new Date() : notCompletes.get(index - 1).getEndTime();
        for (int i = index; i < notCompletes.size(); i++) {
            WorkingForgerItem item = notCompletes.get(i);
            Forge forge = ((Forger) SpringBeanUtils.getBean(SecondProfessionWrapper.class).getById("1")).getForgeByItemId(item.getItemId());
            item.setEndTime(new Date(prevEndTime.getTime() + forge.getConsumeTime()));
            prevEndTime = new Date(item.getEndTime().getTime());
        }
    }
}
