package com.prodog.gamemodule.secondProfession.domain.po;

import com.alibaba.fastjson2.annotation.JSONField;
import com.prodog.gamemodule.secondProfession.domain.dos.SecondProfessionItem;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.data.mongodb.core.mapping.Document;

import java.util.List;

@Document("g_role_second_profession")
@Data
@NoArgsConstructor
@AllArgsConstructor
public class RoleSecondProfession {
    private String id;  //角色Id
    private List<SecondProfessionItem> professions; //副职业

    @JSONField(serialize = false)
    public SecondProfessionItem getItemById(String id) {
        return professions.stream().filter(i -> i.getId().equals(id)).findAny().orElse(null);
    }
}
