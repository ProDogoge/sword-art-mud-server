package com.prodog.gamemodule.secondProfession.service.impl;

import com.prodog.command.annonations.Service;
import com.prodog.command.entity.CommandResult;
import com.prodog.command.entity.RoleGameCommand;
import com.prodog.gamemodule.config.dict.MapDictConfig;
import com.prodog.gamemodule.secondProfession.domain.dos.SecondProfession;
import com.prodog.gamemodule.secondProfession.service.SecondProfessionService;
import com.prodog.gamemodule.secondProfession.wrapper.SecondProfessionWrapper;
import com.prodog.usermodule.role.util.RoleInfoUtil;
import lombok.RequiredArgsConstructor;

import java.util.List;

@Service
@RequiredArgsConstructor
public class SecondProfessionServiceImpl implements SecondProfessionService {
    private final SecondProfessionWrapper secondProfessionWrapper;
    private final MapDictConfig mapDictConfig;

    @Override
    public CommandResult professionList(RoleGameCommand cmd) {
        String prefix = RoleInfoUtil.getPrefix(cmd.getRoleInfo());
        StringBuilder res = new StringBuilder(prefix).append("当前版本所有副职技术如下:\n");
        List<SecondProfession> professions = secondProfessionWrapper.list();

        for (SecondProfession profession : professions) {
            res.append("[").append(mapDictConfig.get("secondSkill", profession.getEnName())).append("]  ").append("最高等级:")
                    .append(profession.getMaxLevel()).append("  介绍: ").append(profession.getDesc())
                    .append("\n\n");
        }
        return CommandResult.success(res.toString().trim());
    }
}
