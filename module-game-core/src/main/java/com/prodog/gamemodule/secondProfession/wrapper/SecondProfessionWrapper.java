package com.prodog.gamemodule.secondProfession.wrapper;

import com.prodog.database.wrapper.QueryDataWrapper;
import com.prodog.gamemodule.secondProfession.domain.dos.SecondProfession;
import com.prodog.utils.bean.SpringBeanUtils;
import org.springframework.boot.ApplicationArguments;
import org.springframework.boot.ApplicationRunner;
import org.springframework.core.annotation.Order;
import org.springframework.stereotype.Component;

import java.util.HashMap;

@Component("SecondProfessionWrapper")
@Order(4)
public class SecondProfessionWrapper extends QueryDataWrapper<SecondProfession, String> implements ApplicationRunner {

    @Override
    public void run(ApplicationArguments args) throws Exception {
        setDataMap(new HashMap<>());
        setIdColumn("id");
        SpringBeanUtils.getBeansOfType(SecondProfession.class).values()
                .forEach(sp -> this.getDataMap().put(sp.getId(), sp));
    }
}
