package com.prodog.gamemodule.secondProfession.service;

import com.prodog.command.entity.CommandResult;
import com.prodog.command.entity.RoleGameCommand;

public interface RoleSecondProfessionService {
    CommandResult professionList(RoleGameCommand cmd);
}
