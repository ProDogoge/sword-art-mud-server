package com.prodog.gamemodule.secondProfession.domain.po;

import com.alibaba.fastjson2.annotation.JSONField;
import com.prodog.database.annotation.Config;
import com.prodog.gamemodule.secondProfession.domain.dos.Forge;
import com.prodog.gamemodule.secondProfession.domain.dos.SecondProfession;
import lombok.Data;

import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

/***
 * 锻造师
 */
@Config(path = "游戏数据/副职业/副职列表/锻造师.json", module = "锻造师配置")
@Data
public class Forger extends SecondProfession {
    private List<Forge> forges = new ArrayList<>(); //锻造列表
    private List<Integer> enhanceSuccessRateUp = new ArrayList<>();
    private int maxWorkings = 12;   //锻造任务上限

    @JSONField(serialize = false)
    public Forge getForgeByItemId(String itemId) {
        return forges.stream().filter(f -> f.getItemId().equals(itemId)).findAny().orElse(null);
    }

    @JSONField(serialize = false)
    public Map<String, Long> getRequiredItemsMap(Map<String, Long> itemNumsMap) {
        Map<String, Long> requiredItemsNums = new LinkedHashMap<>();
        for (Map.Entry<String, Long> e : itemNumsMap.entrySet()) {
            Forge forge = getForgeByItemId(e.getKey());
            for (Map.Entry<String, Long> ee : forge.getRequiredItems().entrySet()) {
                String itemId = ee.getKey();
                long nums = ee.getValue() * e.getValue();
                requiredItemsNums.putIfAbsent(itemId, 0L);
                requiredItemsNums.put(itemId, requiredItemsNums.get(itemId) + nums);
            }
        }
        return requiredItemsNums;
    }
}
