package com.prodog.gamemodule.secondProfession.service.impl;

import com.prodog.command.annonations.Service;
import com.prodog.command.entity.CommandResult;
import com.prodog.command.entity.RoleGameCommand;
import com.prodog.gamemodule.config.dict.MapDictConfig;
import com.prodog.gamemodule.secondProfession.domain.dos.SecondProfession;
import com.prodog.gamemodule.secondProfession.domain.dos.SecondProfessionItem;
import com.prodog.gamemodule.secondProfession.domain.po.RoleSecondProfession;
import com.prodog.gamemodule.secondProfession.service.RoleSecondProfessionService;
import com.prodog.gamemodule.secondProfession.wrapper.RoleSecondProfessionWrapper;
import com.prodog.gamemodule.secondProfession.wrapper.SecondProfessionWrapper;
import com.prodog.usermodule.role.util.RoleInfoUtil;
import com.prodog.util.ExpressionUtil;
import lombok.RequiredArgsConstructor;

import java.util.HashMap;

@Service
@RequiredArgsConstructor
public class RoleSecondProfessionServiceImpl implements RoleSecondProfessionService {
    private final RoleSecondProfessionWrapper roleSecondProfessionWrapper;
    private final SecondProfessionWrapper secondProfessionWrapper;
    private final MapDictConfig mapDictConfig;

    @Override
    public CommandResult professionList(RoleGameCommand cmd) {
        RoleSecondProfession roleSecondProfession = roleSecondProfessionWrapper.getById(cmd.getRoleId());
        String prefix = RoleInfoUtil.getPrefix(cmd.getRoleInfo());
        StringBuilder res = new StringBuilder(prefix).append("您的副职技能信息如下:\n");
        for (SecondProfessionItem profession : roleSecondProfession.getProfessions()) {
            SecondProfession p = secondProfessionWrapper.getById(profession.getId());
            Long requiredExp = ExpressionUtil.parse(p.getExpression(), new HashMap<String, Object>() {{
                put("level", profession.getLevel() + 1);
            }}, Long.class);
            int percent = (int) (profession.getExp() * 100 / requiredExp);
            res.append("[").append(mapDictConfig.get("secondSkill", p.getEnName())).append("]")
                    .append("  等级:").append(profession.getLevel())
                    .append("  称号:").append(p.getLevelTitles().get(profession.getLevel() - 1))
                    .append("  经验值:").append(profession.getExp()).append("/").append(requiredExp).append("『").append(percent).append("%』");
            res.append("\n\n");
        }
        return CommandResult.success(res.toString().trim());
    }
}
