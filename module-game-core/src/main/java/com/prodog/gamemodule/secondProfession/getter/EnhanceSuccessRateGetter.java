package com.prodog.gamemodule.secondProfession.getter;

import com.prodog.usermodule.caculate.CalVal;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.util.ArrayList;
import java.util.List;

public class EnhanceSuccessRateGetter implements SuccessRateGetter {
    private final static List<SuccessRateGetter> getters = new ArrayList<SuccessRateGetter>() {{
        add(new SecondProfessionEnhanceSuccessGetter());
    }};

    @Override
    public double get(CalVal<Double> res, String roleId) {
        for (SuccessRateGetter getter : getters) {
            getter.get(res, roleId);
        }
        res.setResult(Math.min(100, res.getResult()));
        BigDecimal bigDecimal = BigDecimal.valueOf(res.getResult()).setScale(2, RoundingMode.HALF_UP);
        return bigDecimal.doubleValue();
    }
}
