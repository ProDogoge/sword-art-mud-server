package com.prodog.gamemodule.secondProfession.domain.po;

import com.prodog.database.annotation.Config;
import com.prodog.gamemodule.secondProfession.domain.dos.SecondProfession;
import lombok.Data;

/***
 * 烹饪师
 */
@Config(path = "游戏数据/副职业/副职列表/烹饪师.json", module = "烹饪师配置")
@Data
public class Cooker extends SecondProfession {
}

