package com.prodog.gamemodule.secondProfession.service;

import com.prodog.command.entity.CommandResult;
import com.prodog.command.entity.RoleGameCommand;
import com.prodog.gamemodule.secondProfession.domain.po.WorkingForger;
import com.prodog.utils.exception.CommonException;

import java.util.Set;

public interface ForgerService {
    String WORKING_KEY = "WORKING_FORGER_";

    CommandResult forgeList(RoleGameCommand cmd, Integer pno, Integer size);

    String workingStr(WorkingForger working);

    CommandResult forge(RoleGameCommand cmd, String itemName, Long nums) throws CommonException;

    CommandResult currForgers(RoleGameCommand cmd);

    CommandResult cancelForges(RoleGameCommand cmd, Set<Integer> indexs) throws CommonException;

    CommandResult cancelForges(RoleGameCommand cmd, String indexStr) throws CommonException;

    CommandResult cancelForges(RoleGameCommand cmd, Integer from, Integer to) throws CommonException;

    CommandResult cancelForges(RoleGameCommand cmd) throws CommonException;

    CommandResult switchForge(RoleGameCommand cmd, int left, int right) throws CommonException;

    CommandResult moveForge(RoleGameCommand cmd, int from, int to) throws CommonException;

    CommandResult pauseForge(RoleGameCommand cmd);

    CommandResult continueForge(RoleGameCommand cmd) throws CommonException;

    CommandResult enhance(RoleGameCommand cmd, String equipName, Integer times, Integer maxLevel) throws CommonException;
}
