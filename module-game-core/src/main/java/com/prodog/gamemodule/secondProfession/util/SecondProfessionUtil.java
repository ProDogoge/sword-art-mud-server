package com.prodog.gamemodule.secondProfession.util;

import com.prodog.gamemodule.secondProfession.domain.dos.SecondProfession;
import com.prodog.gamemodule.secondProfession.domain.dos.SecondProfessionItem;
import com.prodog.gamemodule.secondProfession.domain.po.RoleSecondProfession;
import com.prodog.gamemodule.secondProfession.wrapper.RoleSecondProfessionWrapper;
import com.prodog.gamemodule.secondProfession.wrapper.SecondProfessionWrapper;
import com.prodog.util.ExpressionUtil;
import com.prodog.utils.bean.SpringBeanUtils;

import java.util.HashMap;

public class SecondProfessionUtil {

    /***
     * 副职增加经验
     * @param roleId    角色ID
     * @param secondId  副职ID
     * @param exp   经验值
     */
    public static void addExp(String roleId, String secondId, long exp) {
        SecondProfessionWrapper secondProfessionWrapper = SpringBeanUtils.getBean(SecondProfessionWrapper.class);
        RoleSecondProfessionWrapper roleSecondProfessionWrapper = SpringBeanUtils.getBean(RoleSecondProfessionWrapper.class);

        SecondProfession profession = secondProfessionWrapper.getById(secondId);
        RoleSecondProfession roleProfession = roleSecondProfessionWrapper.getById(roleId);
        SecondProfessionItem roleProfessionItem = roleProfession.getProfessions().stream().filter(p -> p.getId().equals(secondId)).findAny().get();

        roleProfessionItem.setExp(roleProfessionItem.getExp() + exp);
        while (true) {
            Long requiredExp = ExpressionUtil.parse(profession.getExpression(), new HashMap<String, Object>() {{
                put("level", roleProfessionItem.getLevel() + 1);
            }}, Long.class);
            if (requiredExp <= roleProfessionItem.getExp()) {
                if (profession.getMaxLevel() > roleProfessionItem.getLevel()) {
                    roleProfessionItem.setExp(roleProfessionItem.getExp() - requiredExp);
                    roleProfessionItem.setLevel(roleProfessionItem.getLevel() + 1);
                } else {
                    roleProfessionItem.setExp(requiredExp);
                    break;
                }
            } else {
                break;
            }
        }
        roleSecondProfessionWrapper.save(roleProfession);
    }
}
