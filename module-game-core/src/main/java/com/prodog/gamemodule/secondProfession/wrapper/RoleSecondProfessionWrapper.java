package com.prodog.gamemodule.secondProfession.wrapper;

import com.prodog.database.annotation.Wrapper;
import com.prodog.database.wrapper.MongoDataWrapper;
import com.prodog.gamemodule.secondProfession.domain.dos.SecondProfession;
import com.prodog.gamemodule.secondProfession.domain.dos.SecondProfessionItem;
import com.prodog.gamemodule.secondProfession.domain.po.RoleSecondProfession;
import com.prodog.utils.bean.SpringBeanUtils;

import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

@Wrapper(path = "玩家数据/角色/副职/副职信息", module = "副职信息数据")
public class RoleSecondProfessionWrapper extends MongoDataWrapper<RoleSecondProfession, String> {

    @Override
    public RoleSecondProfession getById(String id) {
        RoleSecondProfession roleSecondProfession = Optional.ofNullable(super.getById(id)).orElseGet(() -> new RoleSecondProfession(id, new ArrayList<>()));
        List<SecondProfession> secondProfessions = SpringBeanUtils.getBean(SecondProfessionWrapper.class).list();
        secondProfessions = secondProfessions.stream().sorted(Comparator.comparing(SecondProfession::getOrder)).collect(Collectors.toList());
        ArrayList<SecondProfessionItem> professions = new ArrayList<>();
        for (SecondProfession secondProfession : secondProfessions) {
            SecondProfessionItem item = roleSecondProfession.getProfessions().stream().filter(p -> p.getId().equals(secondProfession.getId()))
                    .findAny().orElse(new SecondProfessionItem(secondProfession.getId(), 1, 0));
            professions.add(item);
        }
        roleSecondProfession.setProfessions(professions);
        save(roleSecondProfession);
        return roleSecondProfession;
    }
}
