package com.prodog.gamemodule.secondProfession.constant;

public class ForgeState {
    public static final int STOP = 1; //停止
    public static final int RUN = 2;  //进行
    public static final int PAUSE = 3;    //暂停
}
