package com.prodog.gamemodule.secondProfession.domain.dos;

import lombok.Data;

import java.util.Map;

/***
 * 锻造物
 */
@Data
public class Forge {
    private String itemId;  //物品ID
    private int requiredLevel = 1;  //所需等级
    private Map<String, Long> requiredItems; //所需物品
    private long exp;   //可得经验
    private long consumeStrength;   //消耗体力
    private long consumeTime;   //消耗时间
}
