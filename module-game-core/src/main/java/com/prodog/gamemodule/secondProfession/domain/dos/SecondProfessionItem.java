package com.prodog.gamemodule.secondProfession.domain.dos;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class SecondProfessionItem {
    private String id;  //副职业ID
    private int level;  //等级
    private long exp;   //当前副职业经验
}
