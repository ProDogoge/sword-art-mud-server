package com.prodog.gamemodule.secondProfession.wrapper;

import com.prodog.database.annotation.Wrapper;
import com.prodog.database.wrapper.MongoDataWrapper;
import com.prodog.gamemodule.secondProfession.constant.ForgeState;
import com.prodog.gamemodule.secondProfession.domain.po.WorkingForger;

import java.util.ArrayList;
import java.util.Optional;

@Wrapper(path = "玩家数据/角色/副职任务", module = "副职任务数据")
public class WorkingForgerWrapper extends MongoDataWrapper<WorkingForger, String> {
    @Override
    public WorkingForger getById(String id) {
        return Optional.ofNullable(super.getById(id)).orElseGet(() -> {
            WorkingForger workingForger = new WorkingForger(id, new ArrayList<>(), ForgeState.STOP);
            save(workingForger);
            return workingForger;
        });
    }
}
