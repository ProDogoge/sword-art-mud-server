package com.prodog.gamemodule.secondProfession.domain.dos;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.Date;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class WorkingForgerItem {
    private String itemId;  //物品ID
    private boolean complete;   //是否完成
    private Date endTime;   //结束时间

    public WorkingForgerItem(String itemId) {
        this.itemId = itemId;
    }
}
