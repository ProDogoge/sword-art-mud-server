package com.prodog.gamemodule.secondProfession.service.impl;

import cn.hutool.core.date.DateUtil;
import cn.hutool.core.lang.WeightRandom;
import cn.hutool.core.util.ReflectUtil;
import cn.hutool.core.util.StrUtil;
import com.prodog.command.annonations.Service;
import com.prodog.command.entity.CommandResult;
import com.prodog.command.entity.RoleGameCommand;
import com.prodog.commonmodule.prop.caculate.util.PropUtil;
import com.prodog.commonmodule.prop.entity.ObjProp;
import com.prodog.database.entity.Page;
import com.prodog.gamemodule.config.dict.DictConfig;
import com.prodog.gamemodule.config.DropLevelItem;
import com.prodog.gamemodule.config.enhance.EnhanceConfig;
import com.prodog.gamemodule.item.entity.ActiveEquipItem;
import com.prodog.gamemodule.item.entity.GameItem;
import com.prodog.gamemodule.item.wrapper.ActiveEquipWrapper;
import com.prodog.gamemodule.secondProfession.constant.ForgeState;
import com.prodog.gamemodule.secondProfession.domain.dos.Forge;
import com.prodog.gamemodule.secondProfession.domain.dos.SecondProfessionItem;
import com.prodog.gamemodule.secondProfession.domain.dos.WorkingForgerItem;
import com.prodog.gamemodule.secondProfession.domain.po.Forger;
import com.prodog.gamemodule.secondProfession.domain.po.RoleSecondProfession;
import com.prodog.gamemodule.secondProfession.domain.po.WorkingForger;
import com.prodog.gamemodule.secondProfession.getter.EnhanceSuccessRateGetter;
import com.prodog.gamemodule.secondProfession.job.ForgeJob;
import com.prodog.gamemodule.secondProfession.service.ForgerService;
import com.prodog.gamemodule.secondProfession.wrapper.RoleSecondProfessionWrapper;
import com.prodog.gamemodule.secondProfession.wrapper.SecondProfessionWrapper;
import com.prodog.gamemodule.secondProfession.wrapper.WorkingForgerWrapper;
import com.prodog.usermodule.caculate.CalVal;
import com.prodog.usermodule.role.constant.RoleState;
import com.prodog.usermodule.role.entity.RoleBag;
import com.prodog.usermodule.role.entity.RoleEquip;
import com.prodog.usermodule.role.entity.RoleInfo;
import com.prodog.usermodule.role.util.BagUtil;
import com.prodog.usermodule.role.util.GameItemUtil;
import com.prodog.usermodule.role.util.RoleInfoUtil;
import com.prodog.usermodule.role.wrapper.RoleBagWrapper;
import com.prodog.usermodule.role.wrapper.RoleEquipWrapper;
import com.prodog.usermodule.role.wrapper.RoleInfoWrapper;
import com.prodog.usermodule.user.wrapper.UserInfoWrapper;
import com.prodog.utils.RandomUtil;
import com.prodog.utils.TimeUtil;
import com.prodog.utils.exception.CommonException;
import com.prodog.utils.quartz.QuartzUtil;
import lombok.RequiredArgsConstructor;
import org.springframework.boot.ApplicationArguments;
import org.springframework.boot.ApplicationRunner;
import org.springframework.core.annotation.Order;
import org.springframework.transaction.annotation.Transactional;

import java.util.*;
import java.util.stream.Collectors;

@Service
@Order(5)
@RequiredArgsConstructor
public class ForgerServiceImpl implements ForgerService, ApplicationRunner {
    private final SecondProfessionWrapper secondProfessionWrapper;
    private final RoleSecondProfessionWrapper roleSecondProfessionWrapper;
    private final WorkingForgerWrapper workingForgerWrapper;
    private final RoleBagWrapper roleBagWrapper;
    private final RoleInfoWrapper roleInfoWrapper;
    private final RoleEquipWrapper roleEquipWrapper;
    private final ActiveEquipWrapper activeEquipWrapper;
    private final UserInfoWrapper userInfoWrapper;

    private final EnhanceConfig enhanceConfig;
    private final DictConfig dictConfig;

    /***
     * 重启锻造任务
     * @param args
     * @throws Exception
     */
    @Override
    public void run(ApplicationArguments args) throws Exception {
        List<RoleInfo> roles = roleInfoWrapper.listByColumn("state", 5);
        for (RoleInfo role : roles) {
            WorkingForger workingForger = workingForgerWrapper.getById(role.getId());
            List<WorkingForgerItem> workingItems = workingForger.getNotCompleteItems();
            if (workingItems.size() > 0) {
                QuartzUtil.schedule(WORKING_KEY + role.getId(), new ForgeJob(role.getId()), workingItems.get(0).getEndTime());
            }
        }
    }

    @Override
    public CommandResult forgeList(RoleGameCommand cmd, Integer pno, Integer size) {
        String prefix = RoleInfoUtil.getPrefix(cmd.getRoleInfo());
        StringBuilder res = new StringBuilder(prefix).append("装备锻造大全如下:\n");
        Forger forger = (Forger) secondProfessionWrapper.getByColumn("enName", "forger");
        Page<Forge> page = Page.page(forger.getForges(), Forge.class, pno, size);
        if (page.getTotal() <= 0) res.append("暂无锻造数据。\n\n");
        for (Forge forge : page.getDatas()) {
            GameItem item = GameItemUtil.getById(forge.getItemId());
            res.append("[").append(item.getName()).append("]  要求等级:").append(forge.getRequiredLevel())
                    .append("  可得经验:").append(forge.getExp())
                    .append("  所需时间:").append(TimeUtil.formatMs(forge.getConsumeTime()));
            List<String> requiredItems = new ArrayList<>();
            forge.getRequiredItems().forEach((itemId, nums) -> requiredItems.add(GameItemUtil.getById(itemId).getName() + "x" + nums));
            res.append("  所需物品:").append(String.join("、", requiredItems)).append("\n\n");
        }
        res.deleteCharAt(res.length() - 1);
        res.append(page.getBottom());
        return CommandResult.success(res.toString().trim());
    }

    @Override
    public String workingStr(WorkingForger working) {
        StringBuilder res = new StringBuilder();
        int no = 0;
        Map<String, GameItem> itemMap = new HashMap<>();
        List<WorkingForgerItem> workingItems = working.getItems().stream().filter(w -> !w.isComplete()).collect(Collectors.toList());
        if (workingItems.size() <= 0) return "目前暂无锻造任务。";
        for (WorkingForgerItem workingItem : workingItems) {
            GameItem gItem = itemMap.computeIfAbsent(workingItem.getItemId(), (key) -> GameItemUtil.getById(workingItem.getItemId()));
            res.append(no + 1).append(".[").append(gItem.getName()).append("]").append(" ");
            no++;
            if (no % 3 == 0) res.append("\n");
        }
        if (!res.toString().endsWith("\n")) res.append("\n");
        if (working.getState() == ForgeState.RUN)
            res.append("预计完成时间: ").append(DateUtil.format(workingItems.get(workingItems.size() - 1).getEndTime(), "YYYY-MM-dd HH:mm:ss"));
        else
            res.append("当前锻造任务已暂停。");
        return res.toString().trim();
    }

    @Transactional(rollbackFor = Exception.class)
    public CommandResult forge(RoleGameCommand cmd, List<WorkingForgerItem> items) throws CommonException {
        String prefix = RoleInfoUtil.getPrefix(cmd.getRoleInfo());
        StringBuilder res = new StringBuilder(prefix).append("锻造任务添加成功，消耗材料: ");
        Map<String, Long> itemNumsMap = new LinkedHashMap<>();
        Map<String, GameItem> itemMap = new HashMap<>();

        //角色副职信息
        Forger forger = (Forger) secondProfessionWrapper.getByColumn("enName", "forger");
        RoleSecondProfession profession = roleSecondProfessionWrapper.getById(cmd.getRoleId());
        SecondProfessionItem forgerItem = profession.getProfessions().stream().filter(p -> p.getId().equals("1")).findAny().get();

        RoleBag bag = roleBagWrapper.getById(cmd.getRoleId());

        WorkingForger working = workingForgerWrapper.getById(cmd.getRoleId());
        for (WorkingForgerItem item : items) {
            CommonException.bySupplier(() -> (working.getNotCompleteItems().size() + items.size()) > forger.getMaxWorkings(), prefix + "锻造任务添加失败，最多可添加" + forger.getMaxWorkings() + "个锻造任务。");

            //判断锻造列表是否有此物
            GameItem gItem = itemMap.computeIfAbsent(item.getItemId(), GameItemUtil::getById);
            Forge forge = forger.getForgeByItemId(item.getItemId());
            CommonException.byNull(forge, prefix + "锻造任务添加失败，列表里没有找到[" + gItem.getName() + "]的锻造方法。");

            //判断锻造等级是否足够
            CommonException.bySupplier(() -> forge.getRequiredLevel() > forgerItem.getLevel(), prefix + "锻造任务添加失败，[" + gItem.getName() + "]要求锻造等级为" + forge.getRequiredLevel() + "，当前锻造等级:" + forgerItem.getLevel() + "。");

            //加入数量map
            itemNumsMap.putIfAbsent(item.getItemId(), 0L);
            itemNumsMap.put(item.getItemId(), itemNumsMap.get(item.getItemId()) + 1);
        }
        for (Map.Entry<String, Long> e : itemNumsMap.entrySet()) {
            //判断持有物上限
            CommonException.bySupplier(() -> !BagUtil.checkAddLimit(bag, e.getKey(), e.getValue()), prefix + "锻造任务添加失败，您背包中的[" + itemMap.get(e.getKey()).getName() + "]持有数已达上限。");
        }

        //判断材料是否足够
        Map<String, Long> requiredItemsNums = forger.getRequiredItemsMap(itemNumsMap);
        for (Map.Entry<String, Long> e : requiredItemsNums.entrySet()) {
            GameItem item = GameItemUtil.getById(e.getKey());
            CommonException.bySupplier(() -> !BagUtil.checkIsHad(bag, e.getKey(), e.getValue()), prefix + "锻造任务添加失败，您背包中的[" + item.getName() + "]数量不足" + e.getValue() + "个。");
            BagUtil.consumeItem(bag, e.getKey(), e.getValue());
            res.append(item.getName()).append("x").append(e.getValue()).append("、");
        }
        if (res.toString().endsWith("、")) res.deleteCharAt(res.length() - 1);
        roleBagWrapper.save(bag);

        //添加到锻造任务
        if (working.getState() == ForgeState.PAUSE) working.resetEndTimeFrom(0);
        Date startTime = new Date();
        List<WorkingForgerItem> notComplete = working.getNotCompleteItems();
        if (notComplete.size() > 0)
            startTime = notComplete.get(notComplete.size() - 1).getEndTime();
        for (WorkingForgerItem item : items) {
            item.setEndTime(new Date(startTime.getTime() + forger.getForgeByItemId(item.getItemId()).getConsumeTime()));
            working.getItems().add(item);
            startTime = new Date(item.getEndTime().getTime());
        }

        notComplete = working.getNotCompleteItems();
        QuartzUtil.cancel(WORKING_KEY + cmd.getRoleId());
        QuartzUtil.schedule(WORKING_KEY + cmd.getRoleId(), new ForgeJob(cmd.getRoleId()), notComplete.get(0).getEndTime());
        working.setState(ForgeState.RUN);
        workingForgerWrapper.save(working);
        cmd.getRoleInfo().setState(RoleState.FORGE);
        roleInfoWrapper.save(cmd.getRoleInfo());

        res.append("，当前锻造任务如下:\n").append(workingStr(working));
        return CommandResult.success(res.toString().trim());
    }

    private long getConsumeTime(List<WorkingForgerItem> items) {
        long consumeTime = 0;
        Forger forger = (Forger) secondProfessionWrapper.getByColumn("enName", "forger");
        Map<String, Forge> forgeMap = new LinkedHashMap<>();
        for (WorkingForgerItem item : items.stream().filter(i -> !i.isComplete()).collect(Collectors.toList())) {
            Forge forge = forgeMap.computeIfAbsent(item.getItemId(), (key) -> forger.getForges().stream().filter(f -> f.getItemId().equals(key)).findAny().get());
            consumeTime += forge.getConsumeTime();
        }
        return consumeTime;
    }

    @Override
    @Transactional(rollbackFor = Exception.class)
    public CommandResult forge(RoleGameCommand cmd, String itemName, Long nums) throws CommonException {
        String prefix = RoleInfoUtil.getPrefix(cmd.getRoleInfo());
        GameItem item = GameItemUtil.getByName(itemName);
        CommonException.byNull(item, prefix + "锻造任务添加失败，名称为[" + itemName + "]的物品不存在。");
        List<WorkingForgerItem> workings = new ArrayList<>();
        for (long i = 0; i < nums; i++) {
            workings.add(new WorkingForgerItem(item.getId()));
        }
        return forge(cmd, workings);
    }

    @Override
    public CommandResult currForgers(RoleGameCommand cmd) {
        String prefix = RoleInfoUtil.getPrefix(cmd.getRoleInfo());
        WorkingForger working = workingForgerWrapper.getById(cmd.getRoleId());
        return CommandResult.success(prefix + "您当前的锻造任务如下:\n" + workingStr(working));
    }

    @Override
    @Transactional(rollbackFor = Exception.class)
    public CommandResult cancelForges(RoleGameCommand cmd, Set<Integer> indexs) throws CommonException {
        String prefix = RoleInfoUtil.getPrefix(cmd.getRoleInfo());
        StringBuilder res = new StringBuilder(prefix).append("成功取消所选锻造任务，返还材料如下:\n");
        Forger forger = (Forger) secondProfessionWrapper.getByColumn("enName", "forger");

        //判断任务是否存在或是否进行中
        WorkingForger workingForger = workingForgerWrapper.getById(cmd.getRoleId());
        List<WorkingForgerItem> notCompletes = workingForger.getNotCompleteItems();
        CommonException.bySupplier(() -> notCompletes.size() == 0, prefix + "您当前暂无锻造任务，无法取消。");
        CommonException.bySupplier(() -> indexs.stream().anyMatch(i -> i < 0 || i >= notCompletes.size()), prefix + "锻造任务取消失败，存在没有指定索引的锻造任务");
        CommonException.bySupplier(() -> indexs.contains(0) && workingForger.getState() == ForgeState.RUN, prefix + "指定索引中存在正在进行中的锻造任务，无法取消。");

        List<WorkingForgerItem> cancelItems = indexs.stream().map(i -> notCompletes.get(i)).collect(Collectors.toList());

        //移除任务
        for (WorkingForgerItem cancelItem : cancelItems) workingForger.getItems().remove(cancelItem);

        int minIndex = indexs.stream().mapToInt(i -> i).min().orElse(0);
        //重置后面任务的完成时间
        workingForger.resetEndTimeFrom(minIndex);

        if (workingForger.getNotCompleteItems().size() == 0) workingForger.setState(ForgeState.STOP);

        RoleBag bag = roleBagWrapper.getById(cmd.getRoleId());
        Map<String, Long> returnItems = new HashMap<>();
        for (WorkingForgerItem cancelItem : cancelItems) {
            //返还材料
            Forge forge = forger.getForgeByItemId(cancelItem.getItemId());
            Map<String, Long> reqItems = forge.getRequiredItems();
            for (Map.Entry<String, Long> e : reqItems.entrySet()) {
                returnItems.putIfAbsent(e.getKey(), 0L);
                returnItems.put(e.getKey(), returnItems.get(e.getKey()) + e.getValue());
                BagUtil.addItemWithCheck(bag, e.getKey(), e.getValue(), "锻造任务取消失败，您背包中的{{itemName}}持有数已达上限，无法返还材料。");
            }
        }
        returnItems.forEach((itemId, nums) -> {
            GameItem item = GameItemUtil.getById(itemId);
            res.append("[").append(item.getName()).append("]x").append(nums).append("\n");
        });
        //保存数据
        workingForgerWrapper.save(workingForger);
        roleBagWrapper.save(bag);

        return CommandResult.success(res.toString().trim());
    }

    @Override
    @Transactional(rollbackFor = Exception.class)
    public CommandResult cancelForges(RoleGameCommand cmd, String indexStr) throws CommonException {
        List<Integer> indexs = Arrays.stream(indexStr.trim().split("\\s+"))
                .map(i -> Integer.parseInt(i) - 1).collect(Collectors.toList());
        return cancelForges(cmd, new HashSet<>(indexs));
    }

    @Override
    @Transactional(rollbackFor = Exception.class)
    public CommandResult cancelForges(RoleGameCommand cmd, Integer from, Integer to) throws CommonException {
        Set<Integer> indexs = new HashSet<>();
        for (int i = from; i <= to; i++) {
            indexs.add(i);
        }
        return cancelForges(cmd, indexs);
    }

    @Override
    @Transactional(rollbackFor = Exception.class)
    public CommandResult cancelForges(RoleGameCommand cmd) throws CommonException {
        WorkingForger working = workingForgerWrapper.getById(cmd.getRoleId());
        int start = working.getState() == ForgeState.RUN ? 1 : 0;
        return cancelForges(cmd, start, working.getItems().size() - 1);
    }

    @Override
    @Transactional(rollbackFor = Exception.class)
    public CommandResult switchForge(RoleGameCommand cmd, int left, int right) throws CommonException {
        String prefix = RoleInfoUtil.getPrefix(cmd.getRoleInfo());
        StringBuilder res = new StringBuilder(prefix).append("成功将锻造任务换位，当前锻造任务如下:\n");

        //判断任务是否存在或是否进行中
        WorkingForger workingForger = workingForgerWrapper.getById(cmd.getRoleId());
        List<WorkingForgerItem> notCompletes = workingForger.getNotCompleteItems();
        CommonException.bySupplier(() -> notCompletes.size() == 0, prefix + "您当前暂无锻造任务，无法换位。");
        CommonException.bySupplier(() -> (left < 0 || left >= notCompletes.size()) || (right < 0 || right >= notCompletes.size()), prefix + "换位失败，存在没有指定索引的锻造任务。");
        int minIndex = Math.min(left, right);
        CommonException.bySupplier(() -> minIndex == 0 && workingForger.getState() == ForgeState.RUN, prefix + "指定索引中存在正在进行中的锻造任务，无法换位。");

        WorkingForgerItem leftItem = workingForger.getNotCompleteItems().get(left);
        WorkingForgerItem rightItem = workingForger.getNotCompleteItems().get(right);

        int actualLeft = workingForger.getItems().indexOf(leftItem);
        int actualRight = workingForger.getItems().indexOf(rightItem);
        workingForger.getItems().set(actualLeft, rightItem);
        workingForger.getItems().set(actualRight, leftItem);
        workingForger.resetEndTimeFrom(minIndex);

        workingForgerWrapper.save(workingForger);
        res.append(workingStr(workingForger));

        return CommandResult.success(res.toString().trim());
    }

    @Override
    public CommandResult moveForge(RoleGameCommand cmd, int from, int to) throws CommonException {
        String prefix = RoleInfoUtil.getPrefix(cmd.getRoleInfo());
        StringBuilder res = new StringBuilder(prefix).append("成功将锻造任务插队，当前锻造任务如下:\n");

        //判断任务是否存在或是否进行中
        WorkingForger workingForger = workingForgerWrapper.getById(cmd.getRoleId());
        List<WorkingForgerItem> notCompletes = workingForger.getNotCompleteItems();
        CommonException.bySupplier(() -> notCompletes.size() == 0, prefix + "您当前暂无锻造任务，无法插队。");
        CommonException.bySupplier(() -> (from < 0 || from >= notCompletes.size()) || (to < 0 || to >= notCompletes.size()), prefix + "插队失败，存在没有指定索引的锻造任务。");
        int minIndex = Math.min(from, to);
        CommonException.bySupplier(() -> minIndex == 0 && workingForger.getState() == ForgeState.RUN, prefix + "指定索引中存在正在进行中的锻造任务，无法插队。");
        CommonException.bySupplier(() -> from == to, prefix + "插队失败，插队的目标编号不可为当前编号。");

        if (from < to) {
            for (int i = from; i < to; i++) {
                WorkingForgerItem left = workingForger.getNotCompleteItems().get(i);
                WorkingForgerItem right = workingForger.getNotCompleteItems().get(i + 1);
                int actualLeft = workingForger.getItems().indexOf(left);
                int actualRight = workingForger.getItems().indexOf(right);
                workingForger.getItems().set(actualLeft, right);
                workingForger.getItems().set(actualRight, left);
            }
        } else {
            for (int i = from; i > to; i--) {
                WorkingForgerItem left = workingForger.getNotCompleteItems().get(i - 1);
                WorkingForgerItem right = workingForger.getNotCompleteItems().get(i);
                int actualLeft = workingForger.getItems().indexOf(left);
                int actualRight = workingForger.getItems().indexOf(right);
                workingForger.getItems().set(actualLeft, right);
                workingForger.getItems().set(actualRight, left);
            }
        }
        workingForger.resetEndTimeFrom(minIndex);

        workingForgerWrapper.save(workingForger);
        res.append(workingStr(workingForger));

        return CommandResult.success(res.toString().trim());
    }

    @Override
    @Transactional(rollbackFor = Exception.class)
    public CommandResult pauseForge(RoleGameCommand cmd) {
        String prefix = RoleInfoUtil.getPrefix(cmd.getRoleInfo());

        WorkingForger workingForger = workingForgerWrapper.getById(cmd.getRoleId());
        workingForger.setState(ForgeState.PAUSE);
        cmd.getRoleInfo().setState(RoleState.NORMAL);
        QuartzUtil.cancel(WORKING_KEY + cmd.getRoleId());

        workingForgerWrapper.save(workingForger);
        roleInfoWrapper.save(cmd.getRoleInfo());
        return CommandResult.success(prefix + "锻造任务暂停成功！");
    }

    @Override
    @Transactional(rollbackFor = Exception.class)
    public CommandResult continueForge(RoleGameCommand cmd) throws CommonException {
        String prefix = RoleInfoUtil.getPrefix(cmd.getRoleInfo());

        WorkingForger workingForger = workingForgerWrapper.getById(cmd.getRoleId());
        CommonException.bySupplier(() -> workingForger.getState() != ForgeState.PAUSE, prefix + "当前锻造状态不是暂停，无法继续。");
        workingForger.resetEndTimeFrom(0);

        workingForger.setState(ForgeState.RUN);
        cmd.getRoleInfo().setState(RoleState.FORGE);
        QuartzUtil.schedule(WORKING_KEY + cmd.getRoleId(), new ForgeJob(cmd.getRoleId()), workingForger.getNotCompleteItems().get(0).getEndTime());

        workingForgerWrapper.save(workingForger);
        roleInfoWrapper.save(cmd.getRoleInfo());
        return CommandResult.success(prefix + "锻造任务继续成功！锻造任务如下:\n" + workingStr(workingForger));
    }

    @Override
    @Transactional(rollbackFor = Exception.class)
    public CommandResult enhance(RoleGameCommand cmd, String equipName, Integer times, Integer maxLevel) throws CommonException {
        String prefix = RoleInfoUtil.getPrefix(cmd.getRoleInfo());
        StringBuilder res = new StringBuilder(prefix).append("强化情况如下:\n");

        if (maxLevel != null && maxLevel > enhanceConfig.getMaxUpLevel())
            throw new CommonException(prefix + "强化失败，最多可强化到+" + enhanceConfig.getMaxUpLevel() + "。");

        if (times > 30 && maxLevel == null)
            throw new CommonException(prefix + "强化失败，最多只可一次性强化30次。");
        if (maxLevel == null)
            maxLevel = enhanceConfig.getMaxUpLevel();

        //检查装备是否存在
        ActiveEquipItem actEqu = activeEquipWrapper.getByName(equipName);
        CommonException.byNull(actEqu, prefix + "强化失败，名称[" + equipName + "]的实例装备不存在。");

        //检查背包或身上是否有该装备
        RoleBag bag = roleBagWrapper.getById(cmd.getRoleId());
        if (!BagUtil.checkIsHad(bag, actEqu.getId(), 1)) {
            RoleEquip roleEqu = roleEquipWrapper.getById(cmd.getRoleId());
            String wear = (String) ReflectUtil.getFieldValue(roleEqu, actEqu.getEquipType());
            CommonException.bySupplier(() -> !actEqu.getId().equals(wear), prefix + "强化失败，您背包或穿戴中没有名称为[" + equipName + "]的实例装备。");
        }

        //装备类型不可强化
        CommonException.bySupplier(() -> !enhanceConfig.getEnhanceProp().containsKey(actEqu.getEquipType()), prefix + "强化失败，该装备类型不可进行强化。");

        //检查装备的强化等级
        CommonException.bySupplier(() -> enhanceConfig.getMaxUpLevel() <= actEqu.getEnhanceLevel(), prefix + "强化失败，该装备的强化等级已达上限。");

        if (actEqu.getEnhanceLevel() >= maxLevel)
            throw new CommonException(prefix + "强化失败，该装备强化等级已达到或超过目标等级。");
        //开始强化
        int count = 0;
        long money = 0;
        long copoun = 0;
        int before = actEqu.getEnhanceLevel();
        ObjProp oldProp = actEqu.getProp();
        Map<String, Long> consumeItems = new HashMap<>();
        List<Map<String, Long>> reqItemsList = actEqu.getEnhanceReqItemsList();
        List<Long> moneyList = actEqu.getEnhanceMoneyList();
        List<Long> copounList = actEqu.getEnhanceCopounList();
        times:
        for (int i = 0; i < times; i++) {
            //材料 珂尔 点券不够则跳出
            Map<String, Long> reqItems = reqItemsList.get(actEqu.getEnhanceLevel());
            for (Map.Entry<String, Long> e : reqItems.entrySet()) {
                String itemId = e.getKey();
                if (!BagUtil.checkIsHad(bag, itemId, e.getValue())) break times;
            }
            long cMoney = moneyList.get(actEqu.getEnhanceLevel());
            if (cMoney > cmd.getRoleInfo().getMoney()) break;

            long cCopoun = copounList.get(actEqu.getEnhanceLevel());
            if (cCopoun > cmd.getUserInfo().getCopoun()) break;

            //消耗材料 珂尔 点券
            reqItems.forEach((k, v) -> BagUtil.consumeItem(bag, k, v));
            cmd.getRoleInfo().setMoney(cmd.getRoleInfo().getMoney() - cMoney);
            cmd.getUserInfo().setCopoun(cmd.getUserInfo().getCopoun() - cCopoun);

            //记录材料 珂尔 点券
            reqItems.forEach((k, v) -> {
                consumeItems.putIfAbsent(k, 0L);
                consumeItems.put(k, consumeItems.get(k) + v);
            });
            money += cMoney;
            copoun += cCopoun;

            //计算强化结果
            double baseSuccessRate = enhanceConfig.getSuccessRate().get(actEqu.getEnhanceLevel());
            double successRate = new EnhanceSuccessRateGetter().get(new CalVal<>(baseSuccessRate, baseSuccessRate), cmd.getRoleId());
            if (RandomUtil.percentTest(successRate)) {
                actEqu.setEnhanceLevel(actEqu.getEnhanceLevel() + 1);
            } else {
                List<DropLevelItem> dropLevelItems = enhanceConfig.getDropLevel().get(actEqu.getEnhanceLevel());
                List<WeightRandom.WeightObj<Integer>> dropWeights = dropLevelItems.stream()
                        .map(d -> new WeightRandom.WeightObj<>(d.getDropLevel(), d.getWeight()))
                        .collect(Collectors.toList());
                Integer drop = cn.hutool.core.util.RandomUtil.weightRandom(dropWeights).next();
                actEqu.setEnhanceLevel(actEqu.getEnhanceLevel() - drop);
            }
            count++;
            if (actEqu.getEnhanceLevel() >= maxLevel) break;
        }

        int finalCount = count;
        CommonException.bySupplier(() -> finalCount == 0, prefix + "强化失败，您的" + dictConfig.get("money") + "、" + dictConfig.get("copoun") + "或材料不足。");

        //保存数据
        roleInfoWrapper.save(cmd.getRoleInfo());
        userInfoWrapper.save(cmd.getUserInfo());
        roleBagWrapper.save(bag);
        activeEquipWrapper.save(actEqu);

        res.append("实例名称: ").append(actEqu.getName()).append("\n");
        res.append("强化次数: ").append(count).append("\n");
        res.append("强化消耗: ");
        if (money > 0) res.append(money).append(dictConfig.get("money")).append("  ");
        if (copoun > 0) res.append(copoun).append(dictConfig.get("copoun")).append("  ");
        res.append("材料:").append(GameItemUtil.itemNumsStr(consumeItems, "{{itemName}}x{{nums}}", "、")).append("\n");
        res.append("强化结果: ").append(before);
        if (actEqu.getEnhanceLevel() != before) res.append("   ");
        if (actEqu.getEnhanceLevel() > before) res.append("+");
        if (actEqu.getEnhanceLevel() != before) res.append(actEqu.getEnhanceLevel() - before);
        res.append(" -> ").append(actEqu.getEnhanceLevel()).append("\n");
        String changeInfo = PropUtil.propChangeInfo(oldProp, actEqu.getProp());
        if (StrUtil.isNotBlank(changeInfo.trim())) {
            res.append("该装备的属性变更如下:\n").append(changeInfo);
        }
        return CommandResult.success(res.toString().trim());
    }

}
