package com.prodog.gamemodule.secondProfession.handler;

import com.prodog.command.annonations.*;
import com.prodog.command.entity.CommandResult;
import com.prodog.command.entity.RoleGameCommand;
import com.prodog.gamemodule.secondProfession.service.ForgerService;
import com.prodog.usermodule.role.interceptor.StateInterceptor;
import com.prodog.usermodule.user.interceptor.NotRegistInterceptor;
import com.prodog.utils.exception.CommonException;
import lombok.RequiredArgsConstructor;

@CommandBean(setName = "锻造师操作")
@RequiredArgsConstructor
@Iptors(vals = @Iptor(value = NotRegistInterceptor.class))
public class ForgerHandler {
    private final ForgerService forgerService;

    @Command(value = "(锻造列表|锻造大全)\\s*(\\d*)\\s*(\\d*)", props = "null,pno,size")
    @Iptors(vals = @Iptor(value = StateInterceptor.class, params = "{'states':[2,3],'required':false}", msg = "[{{state}}]状态下无法进行此操作。"))
    @Weight(2)
    public CommandResult forgeList(RoleGameCommand cmd, @Default("1") Integer pno, @Default("10") Integer size) {
        try {
            return forgerService.forgeList(cmd, pno, size);
        } catch (Exception e) {
            e.printStackTrace();
            return CommandResult.failure("服务器发生错误,锻造列表获取失败。");
        }
    }

    @Command(value = {
            "锻造\\s+(.+?)\\s+(\\d+)",
            "锻造(.+?)\\s+(\\d+)",
            "锻造(.+)"
    }, props = {"itemName,nums", "itemName,nums", "itemName"})
    @Iptors(vals = @Iptor(value = StateInterceptor.class, params = "{'states':[1,5]}", msg = "[{{state}}]状态下无法进行此操作。"))
    public CommandResult forge(RoleGameCommand cmd, String itemName, @Default("1") Long nums) {
        try {
            return forgerService.forge(cmd, itemName, nums);
        } catch (CommonException e) {
            return CommandResult.failure(e.getMessage());
        } catch (Exception e) {
            e.printStackTrace();
            return CommandResult.failure("服务器发生错误,锻造失败。");
        }
    }

    @Command(value = "(锻造取消|取消锻造)((\\s*\\d+)+)", props = "null,indexStr")
    @Iptors(vals = @Iptor(value = StateInterceptor.class, params = "{'states':[1,5]}", msg = "[{{state}}]状态下无法进行此操作。"))
    @Weight(2)
    public CommandResult cancelForges(RoleGameCommand cmd, String indexStr) {
        try {
            return forgerService.cancelForges(cmd, indexStr);
        } catch (CommonException e) {
            return CommandResult.failure(e.getMessage());
        } catch (Exception e) {
            e.printStackTrace();
            return CommandResult.failure("服务器发生错误,锻造任务取消失败。");
        }
    }

    @Command(value = "(锻造取消|取消锻造)从(\\d+)到(\\d+)", props = "null,from,to")
    @Iptors(vals = @Iptor(value = StateInterceptor.class, params = "{'states':[1,5]}", msg = "[{{state}}]状态下无法进行此操作。"))
    @Weight(2)
    public CommandResult cancelForges(RoleGameCommand cmd, Integer from, Integer to) {
        try {
            return forgerService.cancelForges(cmd, from - 1, to - 1);
        } catch (CommonException e) {
            return CommandResult.failure(e.getMessage());
        } catch (Exception e) {
            e.printStackTrace();
            return CommandResult.failure("服务器发生错误,锻造任务取消失败。");
        }
    }

    @Command(value = "(锻造取消所有|取消所有锻造)")
    @Iptors(vals = @Iptor(value = StateInterceptor.class, params = "{'states':[1,5]}", msg = "[{{state}}]状态下无法进行此操作。"))
    @Weight(2)
    public CommandResult cancelForges(RoleGameCommand cmd) {
        try {
            return forgerService.cancelForges(cmd);
        } catch (CommonException e) {
            return CommandResult.failure(e.getMessage());
        } catch (Exception e) {
            e.printStackTrace();
            return CommandResult.failure("服务器发生错误,锻造任务取消失败。");
        }
    }

    @Command(value = "(锻造换位|锻造换号)\\s*(\\d+)\\s+(\\d+)", props = "null,left,right")
    @Iptors(vals = @Iptor(value = StateInterceptor.class, params = "{'states':[1,5]}", msg = "[{{state}}]状态下无法进行此操作。"))
    @Weight(2)
    public CommandResult switchForge(RoleGameCommand cmd, Integer left, Integer right) {
        try {
            return forgerService.switchForge(cmd, left - 1, right - 1);
        } catch (CommonException e) {
            return CommandResult.failure(e.getMessage());
        } catch (Exception e) {
            e.printStackTrace();
            return CommandResult.failure("服务器发生错误,锻造任务换位失败。");
        }
    }

    @Command(value = "锻造(插队|位移)\\s*(\\d+)\\s+(\\d+)", props = "null,from,to")
    @Iptors(vals = @Iptor(value = StateInterceptor.class, params = "{'states':[1,5]}", msg = "[{{state}}]状态下无法进行此操作。"))
    @Weight(2)
    public CommandResult moveForge(RoleGameCommand cmd, Integer from, Integer to) {
        try {
            return forgerService.moveForge(cmd, from - 1, to - 1);
        } catch (CommonException e) {
            return CommandResult.failure(e.getMessage());
        } catch (Exception e) {
            e.printStackTrace();
            return CommandResult.failure("服务器发生错误,锻造任务插队失败。");
        }
    }

    @Command(value = "(锻造暂停|暂停锻造)")
    @Iptors(vals = @Iptor(value = StateInterceptor.class, params = "{'states':[5]}", msg = "[{{state}}]状态下无法进行此操作。"))
    @Weight(2)
    public CommandResult pauseForge(RoleGameCommand cmd) {
        try {
            return forgerService.pauseForge(cmd);
        } catch (Exception e) {
            e.printStackTrace();
            return CommandResult.failure("服务器发生错误,锻造任务暂停失败。");
        }
    }

    @Command(value = "(锻造继续|继续锻造)")
    @Iptors(vals = @Iptor(value = StateInterceptor.class, params = "{'states':[1]}", msg = "[{{state}}]状态下无法进行此操作。"))
    @Weight(2)
    public CommandResult continueForge(RoleGameCommand cmd) {
        try {
            return forgerService.continueForge(cmd);
        } catch (CommonException e) {
            return CommandResult.failure(e.getMessage());
        } catch (Exception e) {
            e.printStackTrace();
            return CommandResult.failure("服务器发生错误,锻造任务继续失败。");
        }
    }

    @Command(value = "锻造任务", props = "null,pno,size")
    @Iptors(vals = @Iptor(value = StateInterceptor.class, params = "{'states':[2,3],'required':false}", msg = "[{{state}}]状态下无法进行此操作。"))
    @Weight(2)
    public CommandResult currForgers(RoleGameCommand cmd) {
        try {
            return forgerService.currForgers(cmd);
        } catch (Exception e) {
            e.printStackTrace();
            return CommandResult.failure("服务器发生错误,锻造任务获取失败。");
        }
    }

    @Command(value = {"强化\\s*(.+?)\\s+(\\d+)", "强化\\s*(.+)"}, props = {"equipName,times", "equipName"})
    @Iptors(vals = @Iptor(value = StateInterceptor.class, params = "{'states':[1]}", msg = "[{{state}}]状态下无法进行此操作。"))
    public CommandResult enhance(RoleGameCommand cmd, String equipName, @Default("1") Integer times) {
        try {
            return forgerService.enhance(cmd, equipName, times, null);
        } catch (CommonException e) {
            return CommandResult.failure(e.getMessage());
        } catch (Exception e) {
            e.printStackTrace();
            return CommandResult.failure("服务器发生错误,强化失败。");
        }
    }

    @Command(value = {"强化\\s*(.+?)到(\\d+)"}, props = {"equipName,level"})
    @Iptors(vals = @Iptor(value = StateInterceptor.class, params = "{'states':[1]}", msg = "[{{state}}]状态下无法进行此操作。"))
    @Weight(2)
    public CommandResult enhanceTo(RoleGameCommand cmd, String equipName, @Default("1") Integer level) {
        try {
            return forgerService.enhance(cmd, equipName, 3000, level);
        } catch (CommonException e) {
            return CommandResult.failure(e.getMessage());
        } catch (Exception e) {
            e.printStackTrace();
            return CommandResult.failure("服务器发生错误,强化失败。");
        }
    }
}
