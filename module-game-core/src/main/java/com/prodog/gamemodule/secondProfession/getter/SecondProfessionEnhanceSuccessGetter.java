package com.prodog.gamemodule.secondProfession.getter;

import com.prodog.gamemodule.secondProfession.domain.dos.SecondProfessionItem;
import com.prodog.gamemodule.secondProfession.domain.po.Forger;
import com.prodog.gamemodule.secondProfession.domain.po.RoleSecondProfession;
import com.prodog.gamemodule.secondProfession.wrapper.RoleSecondProfessionWrapper;
import com.prodog.gamemodule.secondProfession.wrapper.SecondProfessionWrapper;
import com.prodog.usermodule.caculate.CalVal;
import com.prodog.utils.interfaces.AutowiredBean;
import org.springframework.beans.factory.annotation.Autowired;

public class SecondProfessionEnhanceSuccessGetter implements SuccessRateGetter, AutowiredBean {
    @Autowired
    private RoleSecondProfessionWrapper roleSecondProfessionWrapper;
    @Autowired
    private SecondProfessionWrapper professionWrapper;

    public SecondProfessionEnhanceSuccessGetter() {
        this.autowired();
    }

    @Override
    public double get(CalVal<Double> res, String roleId) {
        RoleSecondProfession roleSecondProfession = roleSecondProfessionWrapper.getById(roleId);
        SecondProfessionItem item = roleSecondProfession.getItemById("1");
        Forger forger = (Forger) professionWrapper.getById("1");

        Integer up = forger.getEnhanceSuccessRateUp().get(item.getLevel() - 1);
        double upRate = res.getBase() * ((double) up / (double) 100);
        res.setResult(res.getResult() + upRate);
        return res.getResult();
    }
}
