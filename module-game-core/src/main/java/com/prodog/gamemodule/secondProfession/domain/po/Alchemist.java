package com.prodog.gamemodule.secondProfession.domain.po;

import com.prodog.database.annotation.Config;
import com.prodog.gamemodule.secondProfession.domain.dos.SecondProfession;
import lombok.Data;

/***
 * 炼金术士
 */
@Config(path = "游戏数据/副职业/副职列表/炼金术士.json", module = "炼金术士配置")
@Data
public class Alchemist extends SecondProfession {
}
