package com.prodog.gamemodule.team.wrapper;

import com.prodog.database.annotation.Wrapper;
import com.prodog.database.wrapper.MongoDataWrapper;
import com.prodog.gamemodule.team.entity.TeamInvite;

@Wrapper(path = "游戏数据/队伍/队伍邀请", module = "队伍邀请数据")
public class TeamInviteWrapper extends MongoDataWrapper<TeamInvite, String> {
}
