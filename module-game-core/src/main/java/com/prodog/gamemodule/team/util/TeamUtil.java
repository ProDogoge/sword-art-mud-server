package com.prodog.gamemodule.team.util;

import com.prodog.usermodule.role.constant.RoleState;
import com.prodog.usermodule.role.entity.RoleInfo;
import com.prodog.usermodule.role.wrapper.RoleInfoWrapper;
import com.prodog.utils.bean.SpringBeanUtils;

import java.util.Collections;
import java.util.List;
import java.util.stream.Collectors;

public class TeamUtil {
    public static List<RoleInfo> teamCanFightRoles(RoleInfo roleInfo) {
        if (roleInfo.getTeamId() == null) return Collections.singletonList(roleInfo);
        else return SpringBeanUtils.getBean(RoleInfoWrapper.class).listByColumn("teamId", roleInfo.getTeamId())
                .stream().filter(r -> r.getState() == RoleState.NORMAL).collect(Collectors.toList());
    }
}
