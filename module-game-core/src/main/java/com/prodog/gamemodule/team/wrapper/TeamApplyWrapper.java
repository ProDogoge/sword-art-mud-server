package com.prodog.gamemodule.team.wrapper;

import com.prodog.database.annotation.Wrapper;
import com.prodog.database.wrapper.MongoDataWrapper;
import com.prodog.gamemodule.team.entity.TeamApply;

@Wrapper(path = "游戏数据/队伍/队伍申请", module = "队伍申请数据")
public class TeamApplyWrapper extends MongoDataWrapper<TeamApply, String> {
}
