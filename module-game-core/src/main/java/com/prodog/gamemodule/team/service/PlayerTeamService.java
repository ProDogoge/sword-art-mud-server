package com.prodog.gamemodule.team.service;

import com.prodog.command.entity.CommandResult;
import com.prodog.command.entity.RoleGameCommand;
import com.prodog.utils.exception.CommonException;

public interface PlayerTeamService {
    CommandResult createTeam(RoleGameCommand cmd, String teamName, String teamDesc) throws CommonException;

    CommandResult teamInfo(RoleGameCommand cmd) throws CommonException;

    CommandResult breakTeam(RoleGameCommand cmd) throws CommonException;

    CommandResult joinTeam(RoleGameCommand cmd, String teamId) throws CommonException;

    CommandResult applyReceive(RoleGameCommand cmd, String applyId) throws CommonException;

    CommandResult escTeam(RoleGameCommand cmd) throws CommonException;

    CommandResult teamInvite(RoleGameCommand cmd, String serialOrName) throws CommonException;

    CommandResult inviteReceive(RoleGameCommand cmd, String teamId) throws CommonException;

    CommandResult rejectInvite(RoleGameCommand cmd, String inviteId) throws CommonException;

    CommandResult rejectApply(RoleGameCommand cmd, String applyId) throws CommonException;

    CommandResult kictOut(RoleGameCommand cmd, String serialOrName) throws CommonException;

    CommandResult regionTeams(RoleGameCommand cmd, Integer pno, Integer size);

    CommandResult localTeams(RoleGameCommand cmd, Integer pno, Integer size);

    CommandResult teamTransfer(RoleGameCommand cmd, String serialOrName) throws CommonException;
}
