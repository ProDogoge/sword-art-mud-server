package com.prodog.gamemodule.team.service.impl;

import com.prodog.command.annonations.Service;
import com.prodog.command.entity.CommandResult;
import com.prodog.command.entity.RoleGameCommand;
import com.prodog.database.entity.Page;
import com.prodog.gamemodule.config.BaseConfig;
import com.prodog.gamemodule.gameworld.local.entiy.GameLocal;
import com.prodog.gamemodule.gameworld.local.wrapper.GameLocalWrapper;
import com.prodog.gamemodule.gameworld.map.entity.GameMap;
import com.prodog.gamemodule.gameworld.map.wrapper.GameMapWrapper;
import com.prodog.gamemodule.gameworld.region.entity.GameRegion;
import com.prodog.gamemodule.gameworld.region.wrapper.GameRegionWrapper;
import com.prodog.gamemodule.team.entity.PlayerTeam;
import com.prodog.gamemodule.team.entity.TeamApply;
import com.prodog.gamemodule.team.entity.TeamInvite;
import com.prodog.gamemodule.team.service.PlayerTeamService;
import com.prodog.gamemodule.team.wrapper.PlayerTeamWrapper;
import com.prodog.gamemodule.team.wrapper.TeamApplyWrapper;
import com.prodog.gamemodule.team.wrapper.TeamInviteWrapper;
import com.prodog.message.sender.MessageSender;
import com.prodog.usermodule.role.entity.RoleInfo;
import com.prodog.usermodule.role.util.RoleInfoUtil;
import com.prodog.usermodule.role.wrapper.RoleInfoWrapper;
import com.prodog.usermodule.user.entity.UserInfo;
import com.prodog.usermodule.user.wrapper.UserInfoWrapper;
import com.prodog.utils.exception.CommonException;
import com.prodog.utils.quartz.QuartzUtil;
import lombok.RequiredArgsConstructor;
import org.springframework.transaction.annotation.Transactional;

import java.util.ArrayList;
import java.util.List;
import java.util.Objects;
import java.util.stream.Collectors;

@Service
@RequiredArgsConstructor
public class PlayerTeamServiceImpl implements PlayerTeamService {
    private final PlayerTeamWrapper teamWrapper;
    private final RoleInfoWrapper roleInfoWrapper;
    private final BaseConfig baseConfig;
    private final TeamApplyWrapper applyWrapper;
    private final TeamInviteWrapper inviteWrapper;
    private final MessageSender messageSender;
    private final UserInfoWrapper userInfoWrapper;
    private final GameMapWrapper mapWrapper;
    private final GameRegionWrapper regionWrapper;
    private final GameLocalWrapper localWrapper;

    private static final String APPLY_KEY = "TEAM_APPLY_";
    private static final String INVITE_KEY = "TEAM_INVITE_";

    private String teamInfo(PlayerTeam team) {
        StringBuilder res = new StringBuilder();
        RoleInfo captaion = roleInfoWrapper.getById(team.getCaptainId());
        res.append("队伍名称: ").append(team.getName()).append("\n");
        res.append("标识码: ").append(team.getId()).append("\n");
        res.append("队长: ").append(RoleInfoUtil.getLevelProPrefix(captaion)).append("\n");
        res.append("描述: ").append(team.getDesc()).append("\n");
        List<RoleInfo> roles = roleInfoWrapper.listByColumn("teamId", team.getId());
        res.append("成员: \n");
        for (RoleInfo role : roles) {
            res.append(RoleInfoUtil.getLevelProPrefix(role)).append("\n");
        }
        return res.toString().trim();
    }

    @Override
    @Transactional(rollbackFor = Exception.class)
    public CommandResult createTeam(RoleGameCommand cmd, String teamName, String teamDesc) throws CommonException {
        String prefix = RoleInfoUtil.getPrefix(cmd.getRoleInfo());
        CommonException.bySupplier(() -> cmd.getRoleInfo().getTeamId() != null, prefix + "创建失败，您已创建或加入过一个队伍了。");
        CommonException.bySupplier(() -> teamName.length() > baseConfig.getMaxTeamNameLength(), prefix + "创建失败，队伍名称不能超过" + baseConfig.getMaxTeamNameLength() + "个字符。");
        CommonException.bySupplier(() -> teamDesc.length() > baseConfig.getMaxTeamDescLength(), prefix + "创建失败，队伍描述不能超过" + baseConfig.getMaxTeamNameLength() + "个字符。");
        PlayerTeam team = new PlayerTeam((teamWrapper.max("id") + 1) + "", cmd.getRoleId(), teamName, teamDesc);
        cmd.getRoleInfo().setTeamId(team.getId());
        teamWrapper.save(team);
        roleInfoWrapper.save(cmd.getRoleInfo());
        return CommandResult.success((prefix + "创建成功！队伍信息如下:\n" + teamInfo(team)).trim());
    }

    @Override
    public CommandResult teamInfo(RoleGameCommand cmd) throws CommonException {
        String prefix = RoleInfoUtil.getPrefix(cmd.getRoleInfo());
        CommonException.byNull(cmd.getRoleInfo().getTeamId(), prefix + "您当前没有加入过任何队伍。");
        PlayerTeam team = teamWrapper.getById(cmd.getRoleInfo().getTeamId());
        return CommandResult.success((prefix + "您的队伍信息如下:\n" + teamInfo(team)).trim());
    }

    @Override
    @Transactional(rollbackFor = Exception.class)
    public CommandResult breakTeam(RoleGameCommand cmd) throws CommonException {
        String prefix = RoleInfoUtil.getPrefix(cmd.getRoleInfo());
        CommonException.byNull(cmd.getRoleInfo().getTeamId(), prefix + "您当前没有加入过任何队伍。");
        PlayerTeam team = teamWrapper.getById(cmd.getRoleInfo().getTeamId());
        CommonException.bySupplier(() -> !team.getCaptainId().equals(cmd.getRoleId()), prefix + "解散失败，您不是队伍[" + team.getName() + "]的队长。");
        List<RoleInfo> roles = roleInfoWrapper.listByColumn("teamId", team.getId());
        roles.forEach(r -> r.setTeamId(null));
        roleInfoWrapper.save(roles);
        teamWrapper.removeById(team.getId());

        List<RoleInfo> persons = roleInfoWrapper.listByColumn("teamId", team.getId()).stream().filter(r -> !r.getId().equals(cmd.getRoleId())).collect(Collectors.toList());
        persons.forEach(p -> messageSender.sendToSelf(p.getUserId(), "队伍[" + team.getName() + "]已解散。"));

        return CommandResult.success((prefix + "成功解散队伍[" + team.getName() + "]。").trim());
    }

    @Override
    @Transactional(rollbackFor = Exception.class)
    public CommandResult joinTeam(RoleGameCommand cmd, String teamId) throws CommonException {
        String prefix = RoleInfoUtil.getPrefix(cmd.getRoleInfo());
        CommonException.bySupplier(() -> cmd.getRoleInfo().getTeamId() != null, prefix + "申请失败，您已创建或加入过一个队伍了。");

        PlayerTeam team = teamWrapper.getById(teamId);
        if (team == null && teamId.startsWith("@")) {
            UserInfo userInfo = userInfoWrapper.getById(teamId.substring(1));
            CommonException.byNull(userInfo, prefix + "加入失败，对方还没有注册账户。");
            team = teamWrapper.getById(roleInfoWrapper.getById(userInfo.getRoleId()).getTeamId());
            CommonException.byNull(team, prefix + "该玩家没有创建过队伍。");
        }
        CommonException.byNull(team, prefix + "标识码" + teamId + "的队伍不存在。");

        List<RoleInfo> roles = roleInfoWrapper.listByColumn("teamId", team.getId());
        CommonException.bySupplier(() -> roles.size() >= baseConfig.getMaxTeamRoleNums(), prefix + "申请失败，该队伍的成员已满。");

        TeamApply exists = applyWrapper.getByColumns("teamId", teamId, "roleId", cmd.getRoleId());
        CommonException.bySupplier(() -> exists != null, prefix + "您已申请过加入该队伍，请勿重复申请。");

        TeamApply apply = new TeamApply(applyWrapper.max("id") + 1 + "", teamId, cmd.getRoleId());
        applyWrapper.save(apply);

        //队伍申请过期
        QuartzUtil.schedule("TEAM_APPLY_" + apply.getId(), (context) -> {
            applyWrapper.removeById(apply.getId());
        }, baseConfig.getApplyTtl());

        messageSender.sendToSelf(roleInfoWrapper.getById(team.getCaptainId()).getUserId(), "玩家[" + cmd.getRoleInfo().getRoleName() + "]申请加入您的队伍，申请标识码:" + apply.getId());
        return CommandResult.success(prefix + "申请成功！申请标识码:" + apply.getId() + "，请耐心等待队长接受申请。");
    }

    @Override
    @Transactional(rollbackFor = Exception.class)
    public CommandResult applyReceive(RoleGameCommand cmd, String applyId) throws CommonException {
        //没队伍
        String prefix = RoleInfoUtil.getPrefix(cmd.getRoleInfo());
        CommonException.byNull(cmd.getRoleInfo().getTeamId(), prefix + "您当前没有创建过任何队伍。");

        //申请不存在
        TeamApply apply = applyWrapper.getById(applyId);
        CommonException.byNull(apply, prefix + "接受失败，标识码" + applyId + "的队伍申请不存在或已过期。");
        CommonException.bySupplier(() -> !cmd.getRoleInfo().getTeamId().equals(apply.getTeamId()), prefix + "该玩家没有申请过加入您的队伍。");

        //不是队长
        PlayerTeam team = teamWrapper.getById(cmd.getRoleInfo().getTeamId());
        CommonException.bySupplier(() -> !team.getCaptainId().equals(cmd.getRoleId()), prefix + "接受失败，您不是队伍[" + team.getName() + "]的队长。");

        //满员
        List<RoleInfo> roles = roleInfoWrapper.listByColumn("teamId", cmd.getRoleInfo().getTeamId());
        CommonException.bySupplier(() -> roles.size() >= baseConfig.getMaxTeamRoleNums(), prefix + "接受失败，当前队伍成员已满。");

        //玩家已加入别的队伍
        RoleInfo applyPlayer = roleInfoWrapper.getById(apply.getRoleId());
        CommonException.bySupplier(() -> applyPlayer.getTeamId() != null && applyPlayer.getTeamId().equals(team.getId()), prefix + "该玩家已在当前队伍。");
        CommonException.bySupplier(() -> applyPlayer.getTeamId() != null, prefix + "接受失败，该玩家已加入了其它队伍。");

        //加入队伍
        applyPlayer.setTeamId(team.getId());
        roleInfoWrapper.save(applyPlayer);
        applyWrapper.removeById(applyId);
        QuartzUtil.cancel(APPLY_KEY + applyId);
        messageSender.sendToSelf(applyPlayer.getUserId(), "队伍[" + team.getName() + "]接受了您的申请，队伍信息:\n" + teamInfo(team));
        return CommandResult.success(prefix + "接受成功！玩家[" + applyPlayer.getRoleName() + "]成为队伍成员，队伍信息:\n" + teamInfo(team));
    }

    @Override
    @Transactional(rollbackFor = Exception.class)
    public CommandResult escTeam(RoleGameCommand cmd) throws CommonException {
        String prefix = RoleInfoUtil.getPrefix(cmd.getRoleInfo());
        CommonException.byNull(cmd.getRoleInfo().getTeamId(), prefix + "您当前没有加入过任何队伍。");

        //自己是队长时解散队伍
        PlayerTeam team = teamWrapper.getById(cmd.getRoleInfo().getTeamId());
        if (team.getCaptainId().equals(cmd.getRoleId())) return breakTeam(cmd);

        cmd.getRoleInfo().setTeamId(null);
        roleInfoWrapper.save(cmd.getRoleInfo());
        messageSender.sendToSelf(roleInfoWrapper.getById(team.getCaptainId()).getUserId(), "成员[" + cmd.getRoleInfo().getRoleName() + "]已退出队伍，队伍信息如下:\n" + teamInfo(team));
        return CommandResult.success(prefix + "成功退出队伍[" + team.getName() + "]！");
    }

    @Override
    @Transactional(rollbackFor = Exception.class)
    public CommandResult teamInvite(RoleGameCommand cmd, String serialOrName) throws CommonException {
        String prefix = RoleInfoUtil.getPrefix(cmd.getRoleInfo());

        //没队伍
        CommonException.byNull(cmd.getRoleInfo().getTeamId(), prefix + "您当前没有创建过任何队伍。");

        //不是队长
        PlayerTeam team = teamWrapper.getById(cmd.getRoleInfo().getTeamId());
        CommonException.bySupplier(() -> !team.getCaptainId().equals(cmd.getRoleId()), prefix + "邀请失败，您不是队伍[" + team.getName() + "]的队长。");

        //满员
        List<RoleInfo> roles = roleInfoWrapper.listByColumn("teamId", team.getId());
        CommonException.bySupplier(() -> roles.size() >= baseConfig.getMaxTeamRoleNums(), prefix + "接受失败，当前队伍成员已满。");

        //玩家不存在 已有队伍
        RoleInfo role = roleInfoWrapper.getByColumnsOr("serialNum", serialOrName, "roleName", serialOrName);
        if (role == null && serialOrName.startsWith("@")) {
            UserInfo userInfo = userInfoWrapper.getById(serialOrName.substring(1));
            CommonException.byNull(userInfo, prefix + "邀请失败，对方还没有注册账户。");
            role = roleInfoWrapper.getById(userInfo.getRoleId());
        }
        CommonException.byNull(role, prefix + "没有找到该名称或识别码的玩家。");
        RoleInfo finalRole = role;
        CommonException.bySupplier(() -> finalRole.getTeamId() != null && finalRole.getTeamId().equals(team.getId()), prefix + "该玩家已在当前队伍。");
        RoleInfo finalRole1 = role;
        CommonException.bySupplier(() -> finalRole1.getTeamId() != null, prefix + "邀请失败，该玩家已加入了其它队伍。");

        TeamInvite exists = inviteWrapper.getByColumns("teamId", team.getId(), "roleId", role.getId());
        CommonException.bySupplier(() -> exists != null, prefix + "您已邀请过该玩家加入队伍，请勿重复邀请。");

        TeamInvite invite = new TeamInvite(inviteWrapper.max("id") + 1 + "", team.getId(), role.getId());
        inviteWrapper.save(invite);
        QuartzUtil.schedule(INVITE_KEY + invite.getId(), (context) -> {
            inviteWrapper.removeById(invite.getId());
        }, baseConfig.getInviteTtl());

        messageSender.sendToSelf(role.getUserId(), "玩家[" + cmd.getRoleInfo().getRoleName() + "]邀请您加入队伍，邀请识别码:" + invite.getId() + "，队伍信息:\n" + teamInfo(team));
        return CommandResult.success(prefix + "邀请成功！邀请识别码:" + invite.getId() + "，请耐心等待玩家接受邀请。");
    }

    @Override
    public CommandResult inviteReceive(RoleGameCommand cmd, String inviteId) throws CommonException {
        String prefix = RoleInfoUtil.getPrefix(cmd.getRoleInfo());

        //已有队伍
        CommonException.bySupplier(() -> cmd.getRoleInfo().getTeamId() != null, prefix + "接受失败，您已创建或加入过一个队伍了。");

        //邀请不存在
        TeamInvite invite = inviteWrapper.getById(inviteId);
        CommonException.byNull(invite, prefix + "接受失败，标识码" + inviteId + "的队伍邀请不存在或已过期。");
        CommonException.bySupplier(() -> !invite.getRoleId().equals(cmd.getRoleId()), prefix + "该队伍邀请过您加入。");

        //队伍不存在
        PlayerTeam team = teamWrapper.getById(invite.getTeamId());
        CommonException.byNull(team, prefix + "接受失败，该队伍不存在或已解散。");

        //满员
        List<RoleInfo> roles = roleInfoWrapper.listByColumn("teamId", invite.getTeamId());
        CommonException.bySupplier(() -> roles.size() >= baseConfig.getMaxTeamRoleNums(), prefix + "接受失败，该队伍成员已满。");

        cmd.getRoleInfo().setTeamId(invite.getTeamId());
        roleInfoWrapper.save(cmd.getRoleInfo());
        inviteWrapper.removeById(inviteId);
        QuartzUtil.cancel(INVITE_KEY + inviteId);
        RoleInfo captain = roleInfoWrapper.getById(team.getCaptainId());
        messageSender.sendToSelf(captain.getUserId(), "玩家[" + cmd.getRoleInfo().getRoleName() + "]接受了您的队伍邀请！队伍信息:\n" + teamInfo(team));
        return CommandResult.success(prefix + "接受成功，您已成为队伍[" + team.getName() + "]的成员，队伍信息:\n" + teamInfo(team));
    }

    @Override
    @Transactional(rollbackFor = Exception.class)
    public CommandResult rejectInvite(RoleGameCommand cmd, String inviteId) throws CommonException {
        String prefix = RoleInfoUtil.getPrefix(cmd.getRoleInfo());

        //邀请不存在
        TeamInvite invite = inviteWrapper.getById(inviteId);
        CommonException.byNull(invite, prefix + "拒绝失败，标识码" + inviteId + "的队伍邀请不存在或已过期。");
        CommonException.bySupplier(() -> !invite.getRoleId().equals(cmd.getRoleId()), prefix + "该队伍邀请过您加入。");

        //队伍不存在
        PlayerTeam team = teamWrapper.getById(invite.getTeamId());
        CommonException.byNull(team, prefix + "拒绝失败，该队伍不存在或已解散。");

        inviteWrapper.removeById(inviteId);
        QuartzUtil.cancel(INVITE_KEY + inviteId);

        RoleInfo captain = roleInfoWrapper.getById(team.getCaptainId());
        messageSender.sendToSelf(captain.getUserId(), "玩家[" + cmd.getRoleInfo().getRoleName() + "]拒绝了您的队伍邀请！");
        return CommandResult.success(prefix + "成功拒绝了队伍[" + team.getName() + "]的加入邀请！");
    }

    @Override
    @Transactional(rollbackFor = Exception.class)
    public CommandResult rejectApply(RoleGameCommand cmd, String applyId) throws CommonException {
        //没队伍
        String prefix = RoleInfoUtil.getPrefix(cmd.getRoleInfo());
        CommonException.byNull(cmd.getRoleInfo().getTeamId(), prefix + "您当前没有创建过任何队伍。");

        //申请不存在
        TeamApply apply = applyWrapper.getById(applyId);
        CommonException.byNull(apply, prefix + "拒绝失败，标识码" + applyId + "的队伍申请不存在或已过期。");
        CommonException.bySupplier(() -> !cmd.getRoleInfo().getTeamId().equals(apply.getTeamId()), prefix + "该玩家没有申请过加入您的队伍。");

        //不是队长
        PlayerTeam team = teamWrapper.getById(cmd.getRoleInfo().getTeamId());
        CommonException.bySupplier(() -> !team.getCaptainId().equals(cmd.getRoleId()), prefix + "拒绝失败，您不是队伍[" + team.getName() + "]的队长。");

        applyWrapper.removeById(applyId);
        QuartzUtil.cancel(APPLY_KEY + applyId);
        RoleInfo applyPlayer = roleInfoWrapper.getById(apply.getRoleId());
        messageSender.sendToSelf(applyPlayer.getUserId(), "队伍[" + team.getName() + "]拒绝了您的加入申请!");
        return CommandResult.success(prefix + "成功拒绝了玩家[" + applyPlayer.getRoleName() + "]的队伍申请！");
    }

    @Override
    @Transactional(rollbackFor = Exception.class)
    public CommandResult kictOut(RoleGameCommand cmd, String serialOrName) throws CommonException {
        String prefix = RoleInfoUtil.getPrefix(cmd.getRoleInfo());

        //没队伍
        CommonException.byNull(cmd.getRoleInfo().getTeamId(), prefix + "您当前没有创建过任何队伍。");

        //不是队长
        PlayerTeam team = teamWrapper.getById(cmd.getRoleInfo().getTeamId());
        CommonException.bySupplier(() -> !team.getCaptainId().equals(cmd.getRoleId()), prefix + "踢出失败，您不是队伍[" + team.getName() + "]的队长。");

        //玩家不存在
        RoleInfo role = roleInfoWrapper.getByColumnsOr("serialNum", serialOrName, "roleName", serialOrName);
        if (role == null && serialOrName.startsWith("@")) {
            UserInfo userInfo = userInfoWrapper.getById(serialOrName.substring(1));
            CommonException.byNull(userInfo, prefix + "踢出失败，对方还没有注册账户。");
            role = roleInfoWrapper.getById(userInfo.getRoleId());
        }
        CommonException.byNull(role, prefix + "没有找到该名称或识别码的玩家。");

        //不是你的队伍
        RoleInfo finalRole = role;
        CommonException.bySupplier(() -> !finalRole.getTeamId().equals(team.getId()), prefix + "踢出失败，该玩家不在你的队伍。");

        role.setTeamId(null);
        roleInfoWrapper.save(role);

        messageSender.sendToSelf(role.getUserId(), "队长[" + cmd.getRoleInfo().getRoleName() + "]将你踢出队伍[" + team.getName() + "]。");
        return CommandResult.success(prefix + "已将玩家[" + role.getRoleName() + "]踢出队伍，队伍信息:\n" + teamInfo(team));
    }

    private List<PlayerTeam> mapTeams(String mapId) {
        List<String> teamIds = roleInfoWrapper.listByColumn("mapId", mapId).stream()
                .map(RoleInfo::getTeamId).filter(Objects::nonNull)
                .collect(Collectors.toList());
        return teamWrapper.getByIds(teamIds).stream().filter(Objects::nonNull).collect(Collectors.toList());
    }

    private List<PlayerTeam> regionTeams(String regionId) {
        List<GameMap> maps = mapWrapper.listByColumn("region", regionId);
        List<PlayerTeam> teams = new ArrayList<>();
        maps.forEach(m -> teams.addAll(mapTeams(m.getId())));
        return teams;
    }

    private List<PlayerTeam> localTeams(String localId) {
        List<GameRegion> regions = regionWrapper.listByColumn("localId", localId);
        List<PlayerTeam> teams = new ArrayList<>();
        regions.forEach(r -> teams.addAll(regionTeams(r.getId())));
        return teams;
    }

    public String teamPageStr(Page<PlayerTeam> page, String label) {
        StringBuilder res = new StringBuilder();
        for (PlayerTeam team : page.getDatas()) {
            res.append("[").append(team.getName()).append("]");
            if (team.getDesc() != null && !team.getDesc().equals("无"))
                res.append(": ").append(team.getDesc());
            res.append("  队长:").append(roleInfoWrapper.getById(team.getCaptainId()).getRoleName());
            res.append("  识别码:").append(team.getId());
            res.append("\n\n");
        }
        if (page.getTotal() == 0) {
            res.append("当前" + label + "暂无任何队伍。\n\n");
        }
        res.deleteCharAt(res.length() - 1);
        res.append(page.getBottom());
        return res.toString();
    }

    @Override
    @Transactional(rollbackFor = Exception.class)
    public CommandResult regionTeams(RoleGameCommand cmd, Integer pno, Integer size) {
        GameMap map = mapWrapper.getById(cmd.getRoleInfo().getMapId());
        GameRegion region = regionWrapper.getById(map.getRegion());
        List<PlayerTeam> teams = regionTeams(region.getId());

        String prefix = RoleInfoUtil.getPrefix(cmd.getRoleInfo());
        StringBuilder res = new StringBuilder(prefix).append("区域[").append(region.getName()).append("]存在的队伍:\n");
        Page<PlayerTeam> page = Page.page(teams, PlayerTeam.class, pno, size);
        res.append(teamPageStr(page, "区域"));
        return CommandResult.success(res.toString().trim());
    }

    @Override
    public CommandResult localTeams(RoleGameCommand cmd, Integer pno, Integer size) {
        GameMap map = mapWrapper.getById(cmd.getRoleInfo().getMapId());
        GameRegion region = regionWrapper.getById(map.getRegion());
        GameLocal local = localWrapper.getById(region.getLocalId());
        List<PlayerTeam> teams = localTeams(local.getId());

        String prefix = RoleInfoUtil.getPrefix(cmd.getRoleInfo());
        StringBuilder res = new StringBuilder(prefix).append("地域[").append(local.getName()).append("]存在的队伍:\n");
        Page<PlayerTeam> page = Page.page(teams, PlayerTeam.class, pno, size);
        res.append(teamPageStr(page, "地域"));
        return CommandResult.success(res.toString().trim());
    }

    @Override
    public CommandResult teamTransfer(RoleGameCommand cmd, String serialOrName) throws CommonException {
        String prefix = RoleInfoUtil.getPrefix(cmd.getRoleInfo());

        //没队伍
        CommonException.byNull(cmd.getRoleInfo().getTeamId(), prefix + "您当前没有创建过任何队伍。");

        //不是队长
        PlayerTeam team = teamWrapper.getById(cmd.getRoleInfo().getTeamId());
        CommonException.bySupplier(() -> !team.getCaptainId().equals(cmd.getRoleId()), prefix + "转让失败，您不是队伍[" + team.getName() + "]的队长。");

        //玩家不存在
        RoleInfo role = roleInfoWrapper.getByColumnsOr("serialNum", serialOrName, "roleName", serialOrName);
        if (role == null && serialOrName.startsWith("@")) {
            UserInfo userInfo = userInfoWrapper.getById(serialOrName.substring(1));
            CommonException.byNull(userInfo, prefix + "转让失败，对方还没有注册账户。");
            role = roleInfoWrapper.getById(userInfo.getRoleId());
        }
        CommonException.byNull(role, prefix + "没有找到该名称或识别码的玩家。");

        //不是你的队伍
        RoleInfo finalRole = role;
        CommonException.bySupplier(() -> !team.getId().equals(finalRole.getTeamId()), prefix + "转让失败，该玩家不在你的队伍。");

        team.setCaptainId(finalRole.getId());
        teamWrapper.save(team);

        messageSender.sendToSelf(finalRole.getUserId(), "你被队长[" + cmd.getRoleInfo().getRoleName() + "]委任成功为[" + team.getName() + "]的队长!");
        return CommandResult.success(prefix + "转让成功！队员[" + role.getRoleName() + "]成为[" + team.getName() + "]的队长！");
    }

}
