package com.prodog.gamemodule.team.entity;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.data.mongodb.core.mapping.Document;

/***
 * 队伍申请
 */
@Document("g_team_apply")
@Data
@NoArgsConstructor
@AllArgsConstructor
public class TeamApply {
    private String id;
    private String teamId;  //队伍ID
    private String roleId;  //角色ID
}
