package com.prodog.gamemodule.team.wrapper;

import com.prodog.database.annotation.Wrapper;
import com.prodog.database.wrapper.MongoDataWrapper;
import com.prodog.gamemodule.team.entity.PlayerTeam;

@Wrapper(path = "游戏数据/队伍/队伍", module = "队伍数据")
public class PlayerTeamWrapper extends MongoDataWrapper<PlayerTeam, String> {
}
