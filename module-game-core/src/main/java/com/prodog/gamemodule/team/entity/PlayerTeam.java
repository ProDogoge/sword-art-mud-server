package com.prodog.gamemodule.team.entity;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.data.mongodb.core.mapping.Document;

/***
 * 队伍
 */
@Document("g_player_team")
@Data
@NoArgsConstructor
@AllArgsConstructor
public class PlayerTeam {
    private String id;
    private String captainId;   //队长ID
    private String name;    //队伍名称
    private String desc;    //队伍描述
}
