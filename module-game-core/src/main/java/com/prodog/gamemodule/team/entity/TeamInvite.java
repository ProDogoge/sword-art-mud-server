package com.prodog.gamemodule.team.entity;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.data.mongodb.core.mapping.Document;

/***
 * 队伍邀请
 */
@Document("g_team_invite")
@Data
@NoArgsConstructor
@AllArgsConstructor
public class TeamInvite {
    private String id;
    private String teamId;  //队伍ID
    private String roleId;  //角色ID
}
