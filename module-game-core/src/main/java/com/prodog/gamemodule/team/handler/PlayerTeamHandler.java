package com.prodog.gamemodule.team.handler;

import com.prodog.command.annonations.*;
import com.prodog.command.entity.CommandResult;
import com.prodog.command.entity.RoleGameCommand;
import com.prodog.gamemodule.team.service.PlayerTeamService;
import com.prodog.usermodule.role.interceptor.StateInterceptor;
import com.prodog.usermodule.user.interceptor.NotRegistInterceptor;
import com.prodog.utils.exception.CommonException;
import lombok.RequiredArgsConstructor;

@CommandBean(setName = "队伍操作")
@Iptors(vals = @Iptor(value = NotRegistInterceptor.class))
@RequiredArgsConstructor
public class PlayerTeamHandler {
    private final PlayerTeamService teamService;

    @Command(value = {
            "(创建队伍|队伍创建)\\s*(.+?)\\s+(.+)",
            "(创建队伍|队伍创建)\\s*(.+)",
    }, props = {"null,teamName,teamDesc", "null,teamName"})
    @Iptors(vals = @Iptor(value = StateInterceptor.class, params = "{'states':[1]}", msg = "[{{state}}]状态下无法进行此操作。"))
    public CommandResult createTeam(RoleGameCommand cmd, String teamName, @Default("无") String teamDesc) {
        try {
            return teamService.createTeam(cmd, teamName, teamDesc);
        } catch (CommonException e) {
            return CommandResult.failure(e.getMessage());
        } catch (Exception e) {
            e.printStackTrace();
            return CommandResult.failure("服务器发生错误,队伍创建失败。");
        }
    }

    @Command(value = "队伍信息")
    public CommandResult teamInfo(RoleGameCommand cmd) {
        try {
            return teamService.teamInfo(cmd);
        } catch (CommonException e) {
            return CommandResult.failure(e.getMessage());
        } catch (Exception e) {
            e.printStackTrace();
            return CommandResult.failure("服务器发生错误,队伍信息获取失败。");
        }
    }

    @Command(value = "(解散队伍|队伍解散|解散)")
    @Iptors(vals = @Iptor(value = StateInterceptor.class, params = "{'states':[1]}", msg = "[{{state}}]状态下无法进行此操作。"))
    public CommandResult breakTeam(RoleGameCommand cmd) {
        try {
            return teamService.breakTeam(cmd);
        } catch (CommonException e) {
            return CommandResult.failure(e.getMessage());
        } catch (Exception e) {
            e.printStackTrace();
            return CommandResult.failure("服务器发生错误,队伍解散失败。");
        }
    }

    @Command(value = "(队伍加入|加入队伍)\\s*(.+)", props = "null,teamId")
    @Iptors(vals = @Iptor(value = StateInterceptor.class, params = "{'states':[1]}", msg = "[{{state}}]状态下无法进行此操作。"))
    public CommandResult joinTeam(RoleGameCommand cmd, String teamId) {
        try {
            return teamService.joinTeam(cmd, teamId);
        } catch (CommonException e) {
            return CommandResult.failure(e.getMessage());
        } catch (Exception e) {
            e.printStackTrace();
            return CommandResult.failure("服务器发生错误,队伍加入失败。");
        }
    }

    @Command(value = "队伍接受申请\\s*(.+)", props = "applyId")
    @Iptors(vals = @Iptor(value = StateInterceptor.class, params = "{'states':[1]}", msg = "[{{state}}]状态下无法进行此操作。"))
    public CommandResult applyReceive(RoleGameCommand cmd, String applyId) {
        try {
            return teamService.applyReceive(cmd, applyId);
        } catch (CommonException e) {
            return CommandResult.failure(e.getMessage());
        } catch (Exception e) {
            e.printStackTrace();
            return CommandResult.failure("服务器发生错误,队伍接受失败。");
        }
    }

    @Command(value = "队伍退出")
    @Iptors(vals = @Iptor(value = StateInterceptor.class, params = "{'states':[1]}", msg = "[{{state}}]状态下无法进行此操作。"))
    public CommandResult escTeam(RoleGameCommand cmd) {
        try {
            return teamService.escTeam(cmd);
        } catch (CommonException e) {
            return CommandResult.failure(e.getMessage());
        } catch (Exception e) {
            e.printStackTrace();
            return CommandResult.failure("服务器发生错误,队伍退出失败。");
        }
    }

    @Command(value = "队伍邀请\\s*(.+)", props = "serialOrName")
    @Iptors(vals = @Iptor(value = StateInterceptor.class, params = "{'states':[1]}", msg = "[{{state}}]状态下无法进行此操作。"))
    public CommandResult teamInvite(RoleGameCommand cmd, String serialOrName) {
        try {
            return teamService.teamInvite(cmd, serialOrName);
        } catch (CommonException e) {
            return CommandResult.failure(e.getMessage());
        } catch (Exception e) {
            e.printStackTrace();
            return CommandResult.failure("服务器发生错误,队伍邀请失败。");
        }
    }

    @Command(value = "队伍接受邀请\\s*(.+)", props = "inviteId")
    @Iptors(vals = @Iptor(value = StateInterceptor.class, params = "{'states':[1]}", msg = "[{{state}}]状态下无法进行此操作。"))
    public CommandResult inviteReceive(RoleGameCommand cmd, String inviteId) {
        try {
            return teamService.inviteReceive(cmd, inviteId);
        } catch (CommonException e) {
            return CommandResult.failure(e.getMessage());
        } catch (Exception e) {
            e.printStackTrace();
            return CommandResult.failure("服务器发生错误,队伍邀请接受失败。");
        }
    }

    @Command(value = "队伍拒绝邀请\\s*(.+)", props = "inviteId")
    @Iptors(vals = @Iptor(value = StateInterceptor.class, params = "{'states':[1]}", msg = "[{{state}}]状态下无法进行此操作。"))
    public CommandResult rejectInvite(RoleGameCommand cmd, String inviteId) {
        try {
            return teamService.rejectInvite(cmd, inviteId);
        } catch (CommonException e) {
            return CommandResult.failure(e.getMessage());
        } catch (Exception e) {
            e.printStackTrace();
            return CommandResult.failure("服务器发生错误,队伍邀请拒绝失败。");
        }
    }

    @Command(value = "队伍拒绝申请\\s*(.+)", props = "applyId")
    @Iptors(vals = @Iptor(value = StateInterceptor.class, params = "{'states':[1]}", msg = "[{{state}}]状态下无法进行此操作。"))
    public CommandResult rejectApply(RoleGameCommand cmd, String applyId) {
        try {
            return teamService.rejectApply(cmd, applyId);
        } catch (CommonException e) {
            return CommandResult.failure(e.getMessage());
        } catch (Exception e) {
            e.printStackTrace();
            return CommandResult.failure("服务器发生错误,队伍申请拒绝失败。");
        }
    }

    @Command(value = "队伍踢出\\s*(.+)", props = "serialOrName")
    @Iptors(vals = @Iptor(value = StateInterceptor.class, params = "{'states':[1]}", msg = "[{{state}}]状态下无法进行此操作。"))
    public CommandResult kictOut(RoleGameCommand cmd, String serialOrName) {
        try {
            return teamService.kictOut(cmd, serialOrName);
        } catch (CommonException e) {
            return CommandResult.failure(e.getMessage());
        } catch (Exception e) {
            e.printStackTrace();
            return CommandResult.failure("服务器发生错误,队伍踢出失败。");
        }
    }

    @Command(value = {
            "区域队伍\\s+(\\d+)\\s+(\\d+)",
            "区域队伍(\\d+)\\s+(\\d+)",
            "区域队伍\\s*(\\d+)",
            "区域队伍"
    }, props = {"pno,size", "pno,size", "pno"})
    @Iptors(vals = @Iptor(value = StateInterceptor.class, params = "{'states':[2,3],'required':false}", msg = "[{{state}}]状态下无法进行此操作。"))
    public CommandResult regionTeams(RoleGameCommand cmd, @Default("1") Integer pno, @Default("10") Integer size) {
        try {
            return teamService.regionTeams(cmd, pno, size);
        } catch (Exception e) {
            e.printStackTrace();
            return CommandResult.failure("服务器发生错误,区域队伍获取失败。");
        }
    }

    @Command(value = {
            "地域队伍\\s+(\\d+)\\s+(\\d+)",
            "地域队伍(\\d+)\\s+(\\d+)",
            "地域队伍\\s*(\\d+)",
            "地域队伍"
    }, props = {"pno,size", "pno,size", "pno"})
    @Iptors(vals = @Iptor(value = StateInterceptor.class, params = "{'states':[2,3],'required':false}", msg = "[{{state}}]状态下无法进行此操作。"))
    public CommandResult localTeams(RoleGameCommand cmd, @Default("1") Integer pno, @Default("10") Integer size) {
        try {
            return teamService.localTeams(cmd, pno, size);
        } catch (Exception e) {
            e.printStackTrace();
            return CommandResult.failure("服务器发生错误,地域队伍获取失败。");
        }
    }

    @Command(value = "队伍转让\\s*(.+)", props = "serialOrName")
    @Iptors(vals = @Iptor(value = StateInterceptor.class, params = "{'states':[1]}", msg = "[{{state}}]状态下无法进行此操作。"))
    public CommandResult teamTransfer(RoleGameCommand cmd, String serialOrName) {
        try {
            return teamService.teamTransfer(cmd, serialOrName);
        } catch (CommonException e) {
            return CommandResult.failure(e.getMessage());
        } catch (Exception e) {
            e.printStackTrace();
            return CommandResult.failure("服务器发生错误,队伍转让失败。");
        }
        
    }
}
