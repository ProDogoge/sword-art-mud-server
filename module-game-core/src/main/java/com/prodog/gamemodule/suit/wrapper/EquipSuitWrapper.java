package com.prodog.gamemodule.suit.wrapper;

import com.prodog.database.annotation.Wrapper;
import com.prodog.database.wrapper.LocalDataWrapper;
import com.prodog.gamemodule.suit.domain.model.EquipSuit;

@Wrapper(path = "游戏数据/套装", module = "套装数据")
public class EquipSuitWrapper extends LocalDataWrapper<EquipSuit, String> {
}
