package com.prodog.gamemodule.suit.domain.model;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.ArrayList;
import java.util.List;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class EquipSuit {
    private String id;
    private String name;
    private List<PartSet> partSets = new ArrayList<>();
    private String desc;
}
