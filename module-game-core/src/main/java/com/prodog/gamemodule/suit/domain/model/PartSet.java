package com.prodog.gamemodule.suit.domain.model;

import com.prodog.commonmodule.prop.entity.ObjProp;
import com.prodog.gamemodule.fight.effect.entity.FightEffect;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.List;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class PartSet {
    private int partNums;
    private ObjProp prop;
    private List<FightEffect> effects;
}
