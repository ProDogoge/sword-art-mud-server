package com.prodog.gamemodule.suit.util;

import cn.hutool.core.util.StrUtil;
import com.prodog.gamemodule.item.entity.ActiveEquipItem;
import com.prodog.gamemodule.item.wrapper.ActiveEquipWrapper;
import com.prodog.gamemodule.suit.domain.model.EquipSuit;
import com.prodog.gamemodule.suit.domain.model.PartSet;
import com.prodog.gamemodule.suit.wrapper.EquipSuitWrapper;
import com.prodog.usermodule.role.entity.RoleEquip;
import com.prodog.usermodule.role.util.EquipUtil;
import com.prodog.usermodule.role.wrapper.RoleEquipWrapper;
import com.prodog.utils.bean.SpringBeanUtils;

import java.lang.reflect.Field;
import java.util.*;
import java.util.stream.Collectors;

public class EquipSuitUtil {

    public static List<PartSet> getPartSets(String roleId) {
        RoleEquip roleEquip = SpringBeanUtils.getBean(RoleEquipWrapper.class).getById(roleId);
        return getPartSets(roleEquip);
    }

    public static List<PartSet> getPartSets(RoleEquip roleEquip) {
        List<Field> equipFields = Arrays.stream(EquipUtil.roleEquipFields).filter(f -> !Objects.equals(f.getName(), "id"))
                .collect(Collectors.toList());
        ActiveEquipWrapper equipWrapper = SpringBeanUtils.getBean(ActiveEquipWrapper.class);
        Map<String, Integer> partSetMap = new HashMap<>();
        for (Field equipField : equipFields) {
            try {
                ActiveEquipItem actEqu = equipWrapper.getById((String) equipField.get(roleEquip));
                if (actEqu == null || StrUtil.isBlank(actEqu.getSuitId())) continue;
                partSetMap.putIfAbsent(actEqu.getSuitId(), 0);
                partSetMap.put(actEqu.getSuitId(), partSetMap.get(actEqu.getSuitId()) + 1);
            } catch (IllegalAccessException e) {
                e.printStackTrace();
            }
        }

        List<PartSet> res = new ArrayList<>();
        EquipSuitWrapper suitWrapper = SpringBeanUtils.getBean(EquipSuitWrapper.class);
        partSetMap.forEach((suitId, nums) -> {
            EquipSuit suit = suitWrapper.getById(suitId);
            List<PartSet> activedPartSets = suit.getPartSets().stream().filter(p -> nums > p.getPartNums()).collect(Collectors.toList());
            res.addAll(activedPartSets);
        });
        return res;
    }

    public static Map<EquipSuit, Integer> getSuits(String roleId) {
        RoleEquip roleEquip = SpringBeanUtils.getBean(RoleEquipWrapper.class).getById(roleId);
        return getSuits(roleEquip);
    }

    public static Map<EquipSuit, Integer> getSuits(RoleEquip roleEquip) {
        List<Field> equipFields = Arrays.stream(EquipUtil.roleEquipFields).filter(f -> !Objects.equals(f.getName(), "id"))
                .collect(Collectors.toList());
        ActiveEquipWrapper equipWrapper = SpringBeanUtils.getBean(ActiveEquipWrapper.class);
        EquipSuitWrapper suitWrapper = SpringBeanUtils.getBean(EquipSuitWrapper.class);
        Map<String, EquipSuit> suitMapById = new HashMap<>();
        Map<EquipSuit, Integer> suitNumsMap = new HashMap<>();

        for (Field equipField : equipFields) {
            try {
                ActiveEquipItem actEqu = equipWrapper.getById((String) equipField.get(roleEquip));
                if (actEqu == null || StrUtil.isBlank(actEqu.getSuitId())) continue;
                EquipSuit suit = suitMapById.computeIfAbsent(actEqu.getSuitId(), suitWrapper::getById);
                suitNumsMap.putIfAbsent(suit, 0);
                suitNumsMap.put(suit, suitNumsMap.get(suit) + 1);
            } catch (IllegalAccessException e) {
                e.printStackTrace();
            }
        }

        Map<EquipSuit, Integer> res = new HashMap<>();
        suitNumsMap.forEach((suit, nums) -> {
            List<PartSet> activedPartSets = suit.getPartSets().stream().filter(p -> nums > p.getPartNums()).collect(Collectors.toList());
            if (activedPartSets.size() > 0) {
                res.put(suit, activedPartSets.get(activedPartSets.size() - 1).getPartNums());
            }
        });
        return res;
    }
}
