package com.prodog.gamemodule.skill.wrapper;

import com.prodog.database.annotation.Wrapper;
import com.prodog.database.wrapper.LocalDataWrapper;
import com.prodog.gamemodule.profession.entity.Profession;
import com.prodog.gamemodule.profession.wrapper.ProfessionWrapper;
import com.prodog.gamemodule.skill.entity.Skill;
import com.prodog.usermodule.role.entity.RoleInfo;
import com.prodog.usermodule.role.wrapper.RoleInfoWrapper;
import lombok.RequiredArgsConstructor;

import java.util.List;

@Wrapper(path = "游戏数据/技能", module = "技能数据")
@RequiredArgsConstructor
public class SkillWrapper extends LocalDataWrapper<Skill, String> {
    private final ProfessionWrapper professionWrapper;
    private final RoleInfoWrapper roleInfoWrapper;

    public List<Skill> listByRoleId(String roleId) {
        return listByRole(roleInfoWrapper.getById(roleId));
    }

    public List<Skill> listByRole(RoleInfo info) {
        return listByProfessionId(info.getProfessionId());
    }

    public List<Skill> listByProfessionId(String professionId) {
        return listByProfession(professionWrapper.getById(professionId));
    }

    public List<Skill> listByProfession(Profession profession) {
        return getByIds(profession.getCanBeLearned());
    }
}
