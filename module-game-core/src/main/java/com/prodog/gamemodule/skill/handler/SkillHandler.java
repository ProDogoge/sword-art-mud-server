package com.prodog.gamemodule.skill.handler;

import com.prodog.command.annonations.*;
import com.prodog.command.entity.CommandResult;
import com.prodog.command.entity.RoleGameCommand;
import com.prodog.gamemodule.skill.service.SkillService;
import com.prodog.usermodule.role.interceptor.StateInterceptor;
import com.prodog.usermodule.user.interceptor.NotRegistInterceptor;
import com.prodog.utils.exception.CommonException;
import lombok.RequiredArgsConstructor;

@CommandBean(setName = "技能操作")
@Iptors(vals = @Iptor(value = NotRegistInterceptor.class))
@RequiredArgsConstructor
public class SkillHandler {
    private final SkillService skillService;

    @Command(value = {
            "技能列表\\s*(\\d+)\\s+(\\d+)",
            "技能列表\\s*(\\d+)",
            "技能列表",
            "技能列表(.+?)\\s*(\\d+)\\s+(\\d+)",
            "技能列表(.+?)\\s*(\\d+)",
            "技能列表(.+)"
    }, props = {"pno,size", "pno", "", "professionName,pno,size", "professionName,pno", "professionName"})
    @Iptors(vals = @Iptor(value = StateInterceptor.class, params = "{'states':[3],'required':false}", msg = "[{{state}}]状态下无法进行此操作。"))
    public CommandResult professionSkillList(RoleGameCommand cmd, String professionName, @Default("1") Integer pno, @Default("6") Integer size) {
        try {
            return skillService.professionSkillList(cmd, professionName, pno, size);
        } catch (CommonException e) {
            return CommandResult.failure(e.getMessage());
        } catch (Exception e) {
            e.printStackTrace();
            return CommandResult.failure("服务器发生错误,技能列表获取失败。");
        }
    }

    @Command(value = {"技能可学\\s*(\\d*)", "技能可学\\s*(\\d+)\\s+(\\d+)"}, props = {"pno", "pno,size"})
    @Iptors(vals = @Iptor(value = StateInterceptor.class, params = "{'states':[3],'required':false}", msg = "[{{state}}]状态下无法进行此操作。"))
    public CommandResult canLearnSkills(RoleGameCommand cmd, @Default("1") Integer pno, @Default("6") Integer size) {
        try {
            return skillService.canLearnSkills(cmd, pno, size);
        } catch (Exception e) {
            e.printStackTrace();
            return CommandResult.failure("服务器发生错误,可学技能获取失败。");
        }
    }

    @Command(value = {"技能已学\\s*(\\d*)", "技能已学\\s*(\\d+)\\s+(\\d+)"}, props = {"pno", "pno,size"})
    @Iptors(vals = @Iptor(value = StateInterceptor.class, params = "{'states':[3],'required':false}", msg = "[{{state}}]状态下无法进行此操作。"))
    public CommandResult learnedSkills(RoleGameCommand cmd, @Default("1") Integer pno, @Default("6") Integer size) {
        try {
            return skillService.learnedSkills(cmd, pno, size);
        } catch (Exception e) {
            e.printStackTrace();
            return CommandResult.failure("服务器发生错误,可学技能获取失败。");
        }
    }

    @Command(value = {"技能学习\\s*(.+?)\\s*(\\d+)", "技能学习\\s*(.+)"}, props = {"skillName,learnLevel", "skillName"})
    @Iptor(value = StateInterceptor.class, params = "{'states':[1]}", msg = "[{{state}}]状态下无法进行此操作。")
    public CommandResult learnSkill(RoleGameCommand cmd, String skillName, @Default("1") Integer learnLevel) {
        try {
            return skillService.learnSkill(cmd, skillName, learnLevel);
        } catch (CommonException e) {
            return CommandResult.failure(e.getMessage());
        } catch (Exception e) {
            e.printStackTrace();
            return CommandResult.failure("服务器发生错误,可学技能获取失败。");
        }
    }
}
