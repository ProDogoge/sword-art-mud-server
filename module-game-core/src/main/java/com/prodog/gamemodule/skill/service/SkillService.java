package com.prodog.gamemodule.skill.service;

import com.prodog.command.entity.CommandResult;
import com.prodog.command.entity.RoleGameCommand;
import com.prodog.utils.exception.CommonException;

public interface SkillService {
    CommandResult professionSkillList(RoleGameCommand cmd, String professionName, Integer pno, Integer size) throws CommonException;

    CommandResult canLearnSkills(RoleGameCommand cmd, Integer pno, Integer size);

    CommandResult learnedSkills(RoleGameCommand cmd, Integer pno, Integer size);

    CommandResult learnSkill(RoleGameCommand cmd, String skillName, Integer learnLevel) throws CommonException;
}
