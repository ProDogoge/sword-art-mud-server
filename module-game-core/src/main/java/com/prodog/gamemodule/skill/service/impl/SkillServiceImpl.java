package com.prodog.gamemodule.skill.service.impl;

import cn.hutool.core.util.StrUtil;
import com.prodog.command.annonations.Service;
import com.prodog.command.entity.CommandResult;
import com.prodog.command.entity.RoleGameCommand;
import com.prodog.database.entity.Page;
import com.prodog.gamemodule.profession.entity.Profession;
import com.prodog.gamemodule.profession.wrapper.ProfessionWrapper;
import com.prodog.gamemodule.skill.entity.Skill;
import com.prodog.gamemodule.skill.service.SkillService;
import com.prodog.gamemodule.skill.wrapper.SkillWrapper;
import com.prodog.usermodule.role.entity.RoleSkill;
import com.prodog.usermodule.role.util.RoleInfoUtil;
import com.prodog.usermodule.role.wrapper.RoleSkillWrapper;
import com.prodog.util.MsgTemplateUtil;
import com.prodog.utils.exception.CommonException;
import lombok.RequiredArgsConstructor;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

@Service
@RequiredArgsConstructor
public class SkillServiceImpl implements SkillService {
    private final SkillWrapper skillWrapper;
    private final RoleSkillWrapper roleSkillWrapper;
    private final ProfessionWrapper professionWrapper;

    @Override
    public CommandResult professionSkillList(RoleGameCommand cmd, String professionName, Integer pno, Integer size) throws CommonException {
        Profession profession;
        if (professionName != null) {
            profession = professionWrapper.getByColumn("professionName", professionName);
        } else {
            profession = professionWrapper.getById(cmd.getRoleInfo().getProfessionId());
        }
        CommonException.byNull(profession, RoleInfoUtil.getPrefix(cmd.getRoleInfo()) + "该职业不存在");
        List<Skill> skills = skillWrapper.listByProfession(profession);
        Page<Skill> page = Page.page(skills, Skill.class, pno, size);
        StringBuilder res = new StringBuilder(RoleInfoUtil.getPrefix(cmd.getRoleInfo()))
                .append("职业").append("[").append(profession.getProfessionName()).append("]的技能列表如下:\n");
        if (skills.size() == 0) res.append("暂无技能数据\n");
        for (Skill skill : page.getDatas()) {
            res.append("[").append(skill.getName()).append("]: 要求等级:").append(skill.getRequiredLevel()).append("  ");
            String desc = MsgTemplateUtil.formatStr(skill.getDescribe(), skill.getLevelProps().get(0));
            if (StrUtil.isNotBlank(desc)) res.append("描述: ").append(desc);
            res.append("\n\n");
        }
        res.deleteCharAt(res.length() - 1);
        res.append(page.getBottom());
        return CommandResult.success(res.toString().trim(), page);
    }

    @Override
    public CommandResult canLearnSkills(RoleGameCommand cmd, Integer pno, Integer size) {
        List<Skill> skills = skillWrapper.listByRole(cmd.getRoleInfo());
        RoleSkill roleSkill = roleSkillWrapper.getById(cmd.getRoleInfo().getId());
        StringBuilder res = new StringBuilder(RoleInfoUtil.getPrefix(cmd.getRoleInfo()))
                .append("当前等级可学习的技能如下(可用SP: ")
                .append(RoleInfoUtil.getSp(cmd.getRoleInfo())).append(")\n");
        StringBuilder canLearn = new StringBuilder();
        List<Skill> canLearnSkills = new ArrayList<>();
        for (Skill skill : skills) {
            int learnLevel = roleSkill.getLearnedSkill().get(skill.getId()) == null ? 0 : roleSkill.getLearnedSkill().get(skill.getId());
            int skipLevel = skill.getRequiredLevel() + learnLevel * skill.getSkipLevel();
            if (learnLevel >= skill.getMaxLearnLevel()) continue;
            if (skipLevel > cmd.getRoleInfo().getLevel()) continue;
            canLearnSkills.add(skill);
        }
        Page<Skill> page = Page.page(canLearnSkills, Skill.class, pno, size);
        for (Skill skill : page.getDatas()) {
            int learnLevel = roleSkill.getLearnedSkill().get(skill.getId()) == null ? 0 : roleSkill.getLearnedSkill().get(skill.getId());
            int skipLevel = skill.getRequiredLevel() + learnLevel * skill.getSkipLevel();
            String desc = MsgTemplateUtil.formatStr(skill.getDescribe(), skill.getLevelProps().get(learnLevel));
            canLearn.append("[").append(skill.getName()).append("]: 下一级:").append(learnLevel + 1).append("  ")
                    .append("要求等级:").append(skipLevel).append("  消耗SP:").append(skill.getConsumeSp()).append("  描述: ").append(desc).append("\n");
            canLearn.append("\n");
        }
        if (page.getTotal() == 0) canLearn.append("当前等级没有可学习的技能\n\n");
        canLearn.deleteCharAt(canLearn.length() - 1);
        return CommandResult.success(res.append(canLearn).append(page.getBottom()).toString().trim(), page);
    }

    @Override
    public CommandResult learnedSkills(RoleGameCommand cmd, Integer pno, Integer size) {
        RoleSkill roleSkill = roleSkillWrapper.getById(cmd.getRoleId());
        if (roleSkill == null) {
            roleSkill = new RoleSkill(cmd.getRoleId(), new HashMap<>());
            roleSkillWrapper.save(roleSkill);
        }
        StringBuilder res = new StringBuilder(RoleInfoUtil.getPrefix(cmd.getRoleInfo()))
                .append("当前已学技能如下(可用SP: ")
                .append(RoleInfoUtil.getSp(cmd.getRoleInfo())).append(")\n");
        List<String> learnedIds = new ArrayList<>(roleSkill.getLearnedSkill().keySet());
        Page<String> page = Page.page(learnedIds, String.class, pno, size);

        for (String skillId : page.getDatas()) {
            Skill skill = skillWrapper.getById(skillId);
            String desc = MsgTemplateUtil.formatStr(skill.getDescribe(), skill.getLevelProps().get(roleSkill.getLearnedSkill().get(skillId) - 1));
            res.append("[").append(skill.getName()).append("]: 等级:").append(roleSkill.getLearnedSkill().get(skillId)).append("  ")
                    .append("  描述: ").append(desc).append("\n");
            res.append("\n");
        }
        if (page.getTotal() == 0) res.append("您还没有学习过任何技能，请发送技能可学\n\n");
        res.deleteCharAt(res.length() - 1);
        return CommandResult.success(res.append(page.getBottom()).toString().trim());
    }

    @Override
    public CommandResult learnSkill(RoleGameCommand cmd, String skillName, Integer learnLevel) throws CommonException {
        //判断技能是否存在
        Skill skill = skillWrapper.getByColumn("name", skillName);
        CommonException.byNull(skill, RoleInfoUtil.getPrefix(cmd.getRoleInfo()) + "学习失败，该技能不存在。");
        //判断当前职业是否可学该技能
        Profession profession = professionWrapper.getById(cmd.getRoleInfo().getProfessionId());
        CommonException.bySupplier(() -> !profession.getCanBeLearned().contains(skill.getId()), RoleInfoUtil.getPrefix(cmd.getRoleInfo()) + "学习失败，当前职业不可学习该技能。");
        //判断学习等级是否已到顶点
        RoleSkill roleSkill = roleSkillWrapper.getById(cmd.getRoleId());
        Integer learnedLevel = roleSkill.getLearnedSkill().getOrDefault(skill.getId(), 0);
        Integer finalLearnedLevel = learnedLevel;
        CommonException.bySupplier(() -> skill.getMaxLearnLevel() < finalLearnedLevel + learnLevel, RoleInfoUtil.getPrefix(cmd.getRoleInfo()) + "学习失败，所学等级超过技能学习等级上限。");
        //判断要求等级是否达到
        int requiredLevel = skill.getRequiredLevel() + (learnedLevel + learnLevel - 1) * skill.getSkipLevel();
        CommonException.bySupplier(() -> cmd.getRoleInfo().getLevel() < requiredLevel, RoleInfoUtil.getPrefix(cmd.getRoleInfo()) + "学习失败，当前角色等级不足，当前等级:" + cmd.getRoleInfo().getLevel() + "  要求等级: " + requiredLevel + "。");
        //判断SP是否足够
        long currSp = RoleInfoUtil.getSp(cmd.getRoleInfo());
        long needSp = skill.getConsumeSp() * learnLevel;
        CommonException.bySupplier(() -> currSp < needSp, RoleInfoUtil.getPrefix(cmd.getRoleInfo()) + "学习失败，可用SP不足，可用SP:" + currSp + "  所需SP:" + needSp);
        //升级
        learnedLevel += learnLevel;
        roleSkill.getLearnedSkill().put(skill.getId(), learnedLevel);
        roleSkillWrapper.save(roleSkill);
        return CommandResult.success(RoleInfoUtil.getPrefix(cmd.getRoleInfo()) + "技能Lv." + learnedLevel + "[" + skillName + "]学习成功! 消耗SP:" + needSp + "  所剩SP:" + RoleInfoUtil.getSp(cmd.getRoleInfo()));
    }
}
