package com.prodog.gamemodule.skill.entity;

import com.prodog.gamemodule.fight.effect.entity.FightEffect;
import lombok.Data;

import java.util.*;

@Data
public class Skill {
    private String id;
    private String name;    //技能名称
    private String tag = "技能";
    private int requiredLevel;  //学习要求等级
    private int skipLevel;  //每级学习等级间隔
    private int maxLevel;   //最高技能等级
    private int maxLearnLevel;  //最高学习等级
    private long consumeSp;  //学习消耗SP
    private boolean normal;
    private Integer damageType;
    private int affectType = 1; //作用类型(1伤害 2恢复HP)
    private int consumeHp;  //消耗hp
    private int consumeMp;  //消耗mp
    private int coolTime;   //冷却时间
    private String expression;  //伤害公式
    private String describe;    //技能描述
    private boolean ignoreMyDead;   //无视自身死亡
    private boolean ignoreDead; //无视死亡单位
    private boolean affectTeam; //作用于己方
    private int skillType = 1;  //技能类型(1攻击 2非攻击 3纯被动技能)
    private Set<String> elemental = new LinkedHashSet<>();  //属性攻击
    private int targetNums = 1; //攻击目标数
    private boolean ignoreObjElemental; //无视释放者本身的属性攻击(即技能不吃属性攻击)
    private List<Map<String, Object>> levelProps;   //等级参数
    private List<FightEffect> effects = new ArrayList<>();   //技能效果
}
