package com.prodog.gamemodule.fight.action.handler;

import com.prodog.gamemodule.fight.action.entity.FightAction;
import com.prodog.gamemodule.fight.action.entity.AtkActionRes;
import com.prodog.gamemodule.fight.damage.handler.DamageHandlerStack;
import com.prodog.gamemodule.fight.damage.util.DamageUtil;
import com.prodog.gamemodule.fight.effect.entity.FightEffect;
import com.prodog.gamemodule.fight.effect.manager.EffectHandlerManager;
import com.prodog.gamemodule.fight.fightobj.entity.FightObj;
import com.prodog.gamemodule.fight.fightobj.entity.FightSkill;
import com.prodog.gamemodule.fight.session.FightSession;
import com.prodog.gamemodule.fight.util.FightUtil;
import com.prodog.utils.RandomUtil;
import com.prodog.utils.interfaces.AutowiredBean;
import lombok.Data;

import java.util.HashMap;
import java.util.List;

@Data
public class AttackHandler implements AutowiredBean {
    private FightSession session;
    private FightObj obj;

    public AttackHandler(FightSession session, FightObj obj) {
        autowired();
        this.session = session;
        this.obj = obj;
    }

    public void action() {
        //获取释放的技能 获取作用到的对象
        FightSkill skill = obj.getSkills().get(obj.getAction().getActionId());
        List<FightObj> beActedObjs = FightUtil.getBeActedObjs(session, obj, skill);

        //没有作用对象或死亡则等待
        if (beActedObjs.size() == 0 && skill.getSkillType() == 1) {
            obj.setAction(FightAction.waited());
            obj.setOver(true);
            return;
        }
        //消耗mp
        DamageUtil.consumeHp(obj, skill.getConsumeHp());
        DamageUtil.consumeMp(obj, skill.getConsumeMp());
        //进入冷却
        if (skill.getCoolTime() != 0) obj.getCoolTimeMap().put("SKILL:" + skill.getId(), skill.getCoolTime());

        StringBuilder useSkill = new StringBuilder();
        useSkill.append(obj.gName()).append("释放了" + skill.getTag() + "[").append(skill.getName()).append("]: ");
        session.getObjInfo().setActPrefix(obj.getUuid(), useSkill.toString());

        //攻击动作结果
        AtkActionRes res = new AtkActionRes(0, 0, new HashMap<>());
        //开始作用给单位
        for (FightObj beActedObj : beActedObjs) {
            //闪避
            if (skill.getAffectType() == 1 && RandomUtil.percentTest(beActedObj.getFightProp().getEvedge())) {
                session.getObjInfo().addBeActInfo(obj.getUuid(), "evade", beActedObj.gName() + "躲开了攻击");
                continue;
            }
            for (FightEffect effect : obj.getEffects()) {
                EffectHandlerManager.caculateFightProp(session, obj);
                EffectHandlerManager.beforeCaculateDamage(session, effect, obj, beActedObj, beActedObjs, res);
            }
            for (FightEffect effect : beActedObj.getEffects()) {
                EffectHandlerManager.caculateFightProp(session, obj);
                EffectHandlerManager.beforeBeCaculatedDamage(session, effect, obj, beActedObj, beActedObjs, res);
            }
            //计算各类伤害
            new DamageHandlerStack(obj, beActedObj, res).handle();
            for (FightEffect effect : obj.getEffects()) {
                EffectHandlerManager.caculateFightProp(session, obj);
                EffectHandlerManager.afterCaculateDamage(session, effect, obj, beActedObj, beActedObjs, res);
            }
            for (FightEffect effect : beActedObj.getEffects()) {
                EffectHandlerManager.caculateFightProp(session, obj);
                EffectHandlerManager.afterBeCaculatedDamage(session, effect, obj, beActedObj, beActedObjs, res);
            }
        }
        //伤害作用给单位
        AtkActionRes finalRes = res;
        res.getDamageMap().forEach((uuid, damageMap) -> {
            FightObj beActedObj = session.getObjsByUuid().get(uuid);
            beActedObj.setBeActedDamage(damageMap);
            for (FightEffect effect : obj.getEffects()) {
                EffectHandlerManager.caculateFightProp(session, obj);
                EffectHandlerManager.beforeExecuteDamage(session, effect, obj, beActedObj, beActedObjs, res);
            }
            for (FightEffect effect : beActedObj.getEffects()) {
                EffectHandlerManager.caculateFightProp(session, obj);
                EffectHandlerManager.BeforeBeExecutedDamage(session, effect, obj, beActedObj, beActedObjs, res);
            }
            //计算伤害造成的结果
            DamageUtil.executeDamage(session, obj, beActedObj, finalRes);
            for (FightEffect effect : obj.getEffects()) {
                EffectHandlerManager.caculateFightProp(session, obj);
                EffectHandlerManager.afterExecuteDamage(session, effect, obj, beActedObj, beActedObjs, res);
            }
            for (FightEffect effect : beActedObj.getEffects()) {
                EffectHandlerManager.caculateFightProp(session, obj);
                EffectHandlerManager.afterBeExecutedDamage(session, effect, obj, beActedObj, beActedObjs, res);
            }
            beActedObj.getBeActedDamage().clear();
        });
        for (FightEffect effect : obj.getEffects()) {
            EffectHandlerManager.caculateFightProp(session, obj);
            EffectHandlerManager.executeDamagesOver(session, effect, obj, beActedObjs, res);
        }
        if (res.getHpAbsore() != 0) {
            DamageUtil.replyHp(obj, res.getHpAbsore());
            session.getObjInfo().addHpAbsore(obj.getUuid(), res.getHpAbsore());
        }
        if (res.getMpAbsore() != 0) {
            DamageUtil.replyMp(obj, res.getMpAbsore());
            session.getObjInfo().addMpAbsore(obj.getUuid(), res.getMpAbsore());
        }
        //设置当前单位本回合行为结束
        obj.setOver(true);
    }

}
