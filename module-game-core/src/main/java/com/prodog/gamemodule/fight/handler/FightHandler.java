package com.prodog.gamemodule.fight.handler;

import com.prodog.command.annonations.*;
import com.prodog.command.entity.CommandResult;
import com.prodog.command.entity.RoleGameCommand;
import com.prodog.gamemodule.fight.service.FightService;
import com.prodog.gamemodule.gameworld.monster.interceptor.NoMonsterInterceptor;
import com.prodog.usermodule.role.interceptor.StateInterceptor;
import com.prodog.usermodule.user.interceptor.NotRegistInterceptor;
import com.prodog.utils.exception.CommonException;
import lombok.RequiredArgsConstructor;

@CommandBean(setName = "战斗操作")
@RequiredArgsConstructor
@Iptors(vals = @Iptor(value = NotRegistInterceptor.class))
public class FightHandler {
    private final FightService fightService;

    @Command("战斗")
    @Iptors(vals = {
            @Iptor(value = NoMonsterInterceptor.class, msg = "该地点未发现怪物。"),
            @Iptor(value = StateInterceptor.class, params = "{'states':[1]}", msg = "[{{state}}]状态下无法进行此操作。")

    })
    public CommandResult fight(RoleGameCommand command) {
        try {
            return fightService.fight(command);
        } catch (CommonException e) {
            return CommandResult.failure(e.getMessage());
        } catch (Exception e) {
            e.printStackTrace();
            return CommandResult.failure("服务器发生错误，进入战斗失败");
        }
    }

    @Command(value = "攻击\\s*(\\d*)", props = "index")
    @Iptors(vals = {
            @Iptor(value = NoMonsterInterceptor.class, msg = "该地点未发现怪物。"),
            @Iptor(value = StateInterceptor.class, params = "{'states':[1,2]}", msg = "[{{state}}]状态下无法进行此操作。")
    })
    public CommandResult fight(RoleGameCommand command, Integer index) {
        try {
            return fightService.attack(command, index);
        } catch (CommonException e) {
            return CommandResult.failure(e.getMessage());
        } catch (Exception e) {
            e.printStackTrace();
            return CommandResult.failure("服务器发生错误，攻击失败");
        }
    }

    @Command(value = {
            "释放(.+?)\\s*(\\d+)",
            "释放(.+)"
    }, props = {"skillName,index", "skillName"})
    @Iptors(vals = {
            @Iptor(value = NoMonsterInterceptor.class, msg = "该地点未发现怪物。"),
            @Iptor(value = StateInterceptor.class, params = "{'states':[1,2]}", msg = "[{{state}}]状态下无法进行此操作。")
    })
    public CommandResult release(RoleGameCommand command, String skillName, Integer index) {
        try {
            return fightService.release(command, skillName, index);
        } catch (CommonException e) {
            return CommandResult.failure(e.getMessage());
        } catch (Exception e) {
            e.printStackTrace();
            return CommandResult.failure("服务器发生错误，技能释放失败");
        }
    }

    @Command(value = "防御")
    @Iptors(vals = {
            @Iptor(value = NoMonsterInterceptor.class, msg = "该地点未发现怪物。"),
            @Iptor(value = StateInterceptor.class, params = "{'states':[1,2]}", msg = "[{{state}}]状态下无法进行此操作。")
    })
    public CommandResult defense(RoleGameCommand command) {
        try {
            return fightService.defense(command);
        } catch (CommonException e) {
            return CommandResult.failure(e.getMessage());
        } catch (Exception e) {
            e.printStackTrace();
            return CommandResult.failure("服务器发生错误，攻击失败");
        }
    }
}
