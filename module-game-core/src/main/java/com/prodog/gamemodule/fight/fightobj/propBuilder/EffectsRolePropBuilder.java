package com.prodog.gamemodule.fight.fightobj.propBuilder;

import com.prodog.gamemodule.fight.effect.getter.role.RoleEffectsGetter;
import com.prodog.gamemodule.fight.fightobj.entity.FightObj;
import com.prodog.usermodule.role.entity.RoleInfo;
import com.prodog.utils.interfaces.AutowiredBean;

import java.util.ArrayList;

public class EffectsRolePropBuilder implements PropBuilder<RoleInfo>, AutowiredBean {

    @Override
    public FightObj build(FightObj obj, RoleInfo info) {
        obj.setEffects(new RoleEffectsGetter().get(new ArrayList<>(), info));
        return obj;
    }
}
