package com.prodog.gamemodule.fight.action.handler;

import com.prodog.gamemodule.fight.action.entity.FightAction;
import com.prodog.gamemodule.fight.fightobj.entity.FightObj;
import com.prodog.gamemodule.fight.session.FightSession;
import com.prodog.gamemodule.fight.util.FightUtil;
import com.prodog.gamemodule.item.effect.handler.PropEffectManager;
import com.prodog.gamemodule.item.entity.PropItem;
import com.prodog.gamemodule.item.wrapper.PropWrapper;
import com.prodog.usermodule.role.util.BagUtil;
import com.prodog.utils.bean.SpringBeanUtils;
import com.prodog.utils.interfaces.AutowiredBean;
import lombok.Data;

import java.util.List;

@Data
public class UseItemHandler implements AutowiredBean {
    private FightSession session;
    private FightObj obj;

    public UseItemHandler(FightSession session, FightObj obj) {
        autowired();
        this.session = session;
        this.obj = obj;
    }

    public void action() {
        //获取释放的技能 获取作用到的对象
        PropItem propItem = SpringBeanUtils.getBean(PropWrapper.class).getById(obj.getAction().getActionId());
        List<FightObj> beActedObjs = FightUtil.getBeActedObjs(session, obj, propItem);

        //没有作用对象或死亡则等待
        if ((beActedObjs.size() == 0)) {
            obj.setAction(FightAction.waited());
            obj.setOver(true);
            return;
        }
        //进入冷却
        if (propItem.getCoolTime() != 0) obj.getCoolTimeMap().put("ITEM:" + propItem.getId(), propItem.getCoolTime());

        StringBuilder useItem = new StringBuilder();
        useItem.append(obj.gName()).append("使用了道具" + "[").append(propItem.getName()).append("]: ");
        session.getObjInfo().setActPrefix(obj.getUuid(), useItem.toString());

        for (FightObj beActedObj : beActedObjs) {
            PropEffectManager.fightHandle(session, propItem, obj, beActedObj, beActedObjs);
        }
        BagUtil.consumeItemAndSave(obj.getId(), propItem.getId(), 1);

        //设置当前单位本回合行为结束
        obj.setOver(true);
    }

}
