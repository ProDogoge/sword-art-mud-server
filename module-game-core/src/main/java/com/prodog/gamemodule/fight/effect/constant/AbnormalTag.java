package com.prodog.gamemodule.fight.effect.constant;

import java.util.Arrays;
import java.util.List;

public class AbnormalTag {
    public static final List<String> ROUND_CONSUME_HP = Arrays.asList("AB_NORMAL", "ROUND_CONSUME_HP");   //回合扣血
    public static final List<String> HOLDING = Arrays.asList("AB_NORMAL", "HOLDING");   //控制
}
