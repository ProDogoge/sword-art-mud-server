package com.prodog.gamemodule.fight.effect.getter;

import com.prodog.gamemodule.fight.effect.entity.FightEffect;

import java.util.List;

public interface EffectsGetter<T> {
    List<FightEffect> get(List<FightEffect> effects, T t);
}
