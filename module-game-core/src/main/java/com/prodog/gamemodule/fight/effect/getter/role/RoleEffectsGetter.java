package com.prodog.gamemodule.fight.effect.getter.role;

import com.prodog.gamemodule.fight.effect.entity.FightEffect;
import com.prodog.gamemodule.fight.effect.getter.EffectsGetter;
import com.prodog.usermodule.role.entity.RoleInfo;
import com.prodog.utils.interfaces.AutowiredBean;

import java.util.ArrayList;
import java.util.List;

public class RoleEffectsGetter implements EffectsGetter<RoleInfo>, AutowiredBean {
    private final List<EffectsGetter<RoleInfo>> getters = new ArrayList<>();

    public RoleEffectsGetter() {
        this.autowired();
        getters.add(new SkillEffectsGetter());
        getters.add(new EquipEffectsGetter());
    }

    @Override
    public List<FightEffect> get(List<FightEffect> effects, RoleInfo info) {
        getters.forEach(g -> g.get(effects, info));
        return effects;
    }
}
