package com.prodog.gamemodule.fight.fightobj.builder.impl;

import com.prodog.gamemodule.fight.fightobj.builder.FightObjBuilder;
import com.prodog.gamemodule.fight.fightobj.entity.FightObj;
import com.prodog.gamemodule.fight.fightobj.propBuilder.BaseRolePropBuilder;
import com.prodog.gamemodule.fight.fightobj.propBuilder.EffectsRolePropBuilder;
import com.prodog.gamemodule.fight.fightobj.propBuilder.PropBuilder;
import com.prodog.gamemodule.fight.fightobj.propBuilder.SkillRolePropBuilder;
import com.prodog.usermodule.role.entity.RoleInfo;
import com.prodog.utils.interfaces.AutowiredBean;

import java.util.ArrayList;
import java.util.List;

public class RoleInfoFightObjBuilder implements FightObjBuilder<RoleInfo>, AutowiredBean {
    private final List<PropBuilder<RoleInfo>> builders = new ArrayList<>();

    public RoleInfoFightObjBuilder() {
        this.autowired();
        builders.add(new BaseRolePropBuilder());
        builders.add(new SkillRolePropBuilder());
        builders.add(new EffectsRolePropBuilder());
    }

    @Override
    public FightObj build(RoleInfo roleInfo) {
        FightObj fightObj = null;
        for (PropBuilder<RoleInfo> builder : builders) {
            fightObj = builder.build(fightObj, roleInfo);
        }
        if (fightObj.getFightProp().getCurHp() <= 0) fightObj.setState(2);
        return fightObj;
    }
}
