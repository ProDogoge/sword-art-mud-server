package com.prodog.gamemodule.fight.effect.handler;

import com.prodog.gamemodule.fight.effect.entity.FightEffect;
import com.prodog.gamemodule.fight.fightobj.entity.FightObj;
import com.prodog.gamemodule.fight.session.FightSession;
import lombok.AllArgsConstructor;
import lombok.Data;

import java.util.Collection;
import java.util.Map;

@Data
@AllArgsConstructor
public abstract class AbstractEffectHandler implements EffectHandler {
    private FightSession session;
    private FightEffect effect;

    public Map<String, Object> getParams() {
        return effect.getParams();
    }

    public Object get(String prop) {
        return effect.getParams().get(prop);
    }

    public String getSkillId() {
        return (String) getParams().get("actSkillId");
    }

    public boolean checkIsCurrentSkill(FightObj actObj) {
        String skillId = (String) getParams().get("actSkillId");
        return actObj.getAction().getType() == 1 && actObj.getAction().getActionId().equals(skillId);
    }

    public boolean checkSkills(FightObj actObj, Collection<String> skillIds) {
        String skillId = actObj.getAction().getActionId();
        return actObj.getAction().getType() == 1 && skillIds.contains(skillId);
    }

    public boolean checkSkills(FightObj actObj, String prefix, Collection<String> skillIds) {
        String skillId = prefix + "_" + actObj.getAction().getActionId();
        return actObj.getAction().getType() == 1 && skillIds.contains(skillId);
    }
}
