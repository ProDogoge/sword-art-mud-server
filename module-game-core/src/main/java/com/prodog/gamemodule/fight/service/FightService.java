package com.prodog.gamemodule.fight.service;

import com.prodog.command.entity.CommandResult;
import com.prodog.command.entity.RoleGameCommand;
import com.prodog.utils.exception.CommonException;

public interface FightService {
    CommandResult fight(RoleGameCommand command) throws CommonException;

    CommandResult attack(RoleGameCommand command, Integer index) throws CommonException;

    CommandResult release(RoleGameCommand command, String skillName, Integer index) throws CommonException;

    CommandResult useItem(RoleGameCommand command, String itemName, Integer index) throws CommonException;

    CommandResult defense(RoleGameCommand command) throws CommonException;
}
