package com.prodog.gamemodule.fight.fightobj.overHandler;

import com.prodog.gamemodule.fight.session.FightSession;

public interface FightOverHandler {
    void handle(FightSession session,StringBuilder msg);
}
