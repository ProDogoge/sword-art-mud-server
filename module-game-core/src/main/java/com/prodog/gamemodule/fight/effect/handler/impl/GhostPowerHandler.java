package com.prodog.gamemodule.fight.effect.handler.impl;

import com.prodog.commonmodule.prop.entity.ObjCaculaterProp;
import com.prodog.commonmodule.prop.entity.ObjProp;
import com.prodog.gamemodule.fight.effect.entity.FightEffect;
import com.prodog.gamemodule.fight.fightobj.entity.FightObj;
import com.prodog.gamemodule.fight.fightobj.entity.FightSkill;
import com.prodog.gamemodule.fight.session.FightSession;

import java.util.Map;

public class GhostPowerHandler extends DefaultEffectHandler {
    public GhostPowerHandler(FightSession session, FightEffect effect) {
        super(session, effect);
    }

    @Override
    public void onFightStart(FightObj obj) {
        //技能形态变化
        Map<String, String> changeSkills = (Map<String, String>) get("changeSkills");
        changeSkills.forEach((skillId, name) -> {
            FightSkill skill = obj.getSkills().get(skillId);
            if (skill != null) skill.setName(name);
        });
        int initGhost = (int) get("initGhost");
        int maxGhost = (int) get("maxGhost");
        obj.getParams().put("ghostPower", initGhost);
        obj.getPanelExpansion().addExtendShow("SKILL:" + get("actSkillId"), "鬼力", initGhost + "", maxGhost + "");
    }

    @Override
    public void afterAction(FightObj actObj) {
        Map<String, Integer> ghostMap = (Map<String, Integer>) get("ghostMap");
        if (checkSkills(actObj, "skill", ghostMap.keySet())) {
            int ghostPower = ghostMap.get("skill_" + actObj.getAction().getActionId());
            ghostPower = (int) actObj.getParams().get("ghostPower") + ghostPower;
            ghostPower = Math.min((Integer) get("maxGhost"), ghostPower);
            actObj.getParams().put("ghostPower", ghostPower);
        }
        int currGhost = (int) actObj.getParams().get("ghostPower");
        int maxGhost = (int) get("maxGhost");
        actObj.getPanelExpansion().addExtendShow("SKILL:" + get("actSkillId"), "鬼力", String.valueOf(currGhost), maxGhost + "");

    }

    @Override
    public ObjCaculaterProp caculateProp(ObjCaculaterProp caculaterProp) {
        ObjProp abProp = caculaterProp.getAbsoluteProp();
        ObjProp prop = caculaterProp.getResultProp();
        int darkAttack = Integer.parseInt(String.valueOf(get("darkAttack")));
        prop.getElemental().add("dark");
        prop.setDarkAttack(prop.getDarkAttack() + darkAttack);

        int addDamageRate = (int) (abProp.getSpeed() / 20);
        prop.setAddDamageRate(prop.getAddDamageRate() + addDamageRate);
        return caculaterProp;
    }

    @Override
    public void caculateFightProp(FightObj obj) {
        ObjProp prop = obj.getFightProp();
        int currGhost = (int) obj.getParams().get("ghostPower");
        int physicalCrit = (int) get("physicalCrit");
        int critDamage = (int) get("criticalDamage");
        prop.setPhysicalCrit(prop.getPhysicalCrit() + currGhost * physicalCrit);
        prop.setCriticalDamage(prop.getCriticalDamage() + currGhost * critDamage);
    }
}
