package com.prodog.gamemodule.fight.effect.manager;

import cn.hutool.core.util.ReflectUtil;
import com.prodog.commonmodule.prop.entity.ObjCaculaterProp;
import com.prodog.commonmodule.prop.entity.ObjProp;
import com.prodog.gamemodule.fight.action.entity.AtkActionRes;
import com.prodog.gamemodule.fight.effect.entity.FightEffect;
import com.prodog.gamemodule.fight.effect.handler.EffectHandler;
import com.prodog.gamemodule.fight.effect.handler.impl.*;
import com.prodog.gamemodule.fight.fightobj.entity.FightObj;
import com.prodog.gamemodule.fight.session.FightSession;
import com.prodog.utils.bean.BeanUtil;

import java.lang.reflect.Constructor;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class EffectHandlerManager {
    public static Map<String, Constructor<? extends EffectHandler>> handlerMap;

    static {
        handlerMap = new HashMap<String, Constructor<? extends EffectHandler>>() {{
            put("test", ReflectUtil.getConstructor(ThunderEffectHandler.class, FightSession.class, FightEffect.class));
            put("paralysis", ReflectUtil.getConstructor(ParalysisEffectHandler.class, FightSession.class, FightEffect.class));
            put("reverseStab", ReflectUtil.getConstructor(ReverseStabHandler.class, FightSession.class, FightEffect.class));
            //回合扣血
            put(RoundConsumeHpHandler.NAME, ReflectUtil.getConstructor(RoundConsumeHpHandler.class, FightSession.class, FightEffect.class));
            //控制
            put(HoldingHandler.NAME, ReflectUtil.getConstructor(HoldingHandler.class, FightSession.class, FightEffect.class));
            //攻击概率回合扣血
            put(AttackToRoundConsumeHpHandler.NAME, ReflectUtil.getConstructor(AttackToRoundConsumeHpHandler.class, FightSession.class, FightEffect.class));
            //攻击概率控制
            put(AttackToHoldingHandler.NAME, ReflectUtil.getConstructor(AttackToHoldingHandler.class, FightSession.class, FightEffect.class));
            //自带暴击
            put("selfCrit", ReflectUtil.getConstructor(SelfCritHandler.class, FightSession.class, FightEffect.class));
            //幻鬼之力
            put("ghostPower", ReflectUtil.getConstructor(GhostPowerHandler.class, FightSession.class, FightEffect.class));
            //百分比属性
            put("percentProp", ReflectUtil.getConstructor(PercentPropHandler.class, FightSession.class, FightEffect.class));
            //血影披风
            put(BloodCloakHandler.NAME, ReflectUtil.getConstructor(BloodCloakHandler.class, FightSession.class, FightEffect.class));
            //灵魂猎者
            put(SoulHunterHandler.NAME, ReflectUtil.getConstructor(SoulHunterHandler.class, FightSession.class, FightEffect.class));
        }};
    }

    public static void onFightStart(FightSession session, FightEffect effect, FightObj obj) {
        try {
            EffectHandler handler = handlerMap.get(effect.getName()).newInstance(session, effect);
            handler.onFightStart(obj);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public static void onRoundStart(FightSession session, FightEffect effect, FightObj obj) {
        try {
            EffectHandler handler = handlerMap.get(effect.getName()).newInstance(session, effect);
            handler.onRoundStart(obj);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public static void beforeAction(FightSession session, FightEffect effect, FightObj obj) {
        try {
            EffectHandler handler = handlerMap.get(effect.getName()).newInstance(session, effect);
            handler.beforeAction(obj);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public static void afterExecuteDamage(FightSession session, FightEffect effect, FightObj obj, FightObj beActedObj, List<FightObj> beActedObjs, AtkActionRes res) {
        try {
            EffectHandler handler = handlerMap.get(effect.getName()).newInstance(session, effect);
            handler.afterExecuteDamage(obj, beActedObj, beActedObjs, res);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public static void roundDown(FightSession session, FightEffect effect, FightObj obj) {
        if (effect.getRound() != null) {
            effect.setRound(effect.getRound() - 1);
            if (effect.getRound() == 0) {
                try {
                    EffectHandler handler = handlerMap.get(effect.getName()).newInstance(session, effect);
                    handler.destroy(obj);
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        }
    }

    public static void afterBeExecutedDamage(FightSession session, FightEffect effect, FightObj actObj, FightObj beActedObj, List<FightObj> beActedObjs, AtkActionRes res) {
        try {
            EffectHandler handler = handlerMap.get(effect.getName()).newInstance(session, effect);
            handler.afterBeExecutedDamage(actObj, beActedObj, beActedObjs, res);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public static void beforeRoundOver(FightSession session, FightEffect effect, FightObj obj) {
        try {
            EffectHandler handler = handlerMap.get(effect.getName()).newInstance(session, effect);
            handler.beforeRoundOver(obj);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public static void afterAction(FightSession session, FightEffect effect, FightObj obj) {
        try {
            EffectHandler handler = handlerMap.get(effect.getName()).newInstance(session, effect);
            handler.afterAction(obj);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public static void afterRoundDown(FightSession session, FightEffect effect, FightObj obj) {
        try {
            EffectHandler handler = handlerMap.get(effect.getName()).newInstance(session, effect);
            handler.afterRoundDown(obj);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public static void executeDamagesOver(FightSession session, FightEffect effect, FightObj obj, List<FightObj> beActedObjs, AtkActionRes res) {
        try {
            EffectHandler handler = handlerMap.get(effect.getName()).newInstance(session, effect);
            handler.executeDamagesOver(obj, beActedObjs, res);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public static void beforeExecuteDamage(FightSession session, FightEffect effect, FightObj obj, FightObj beActedObj, List<FightObj> beActedObjs, AtkActionRes res) {
        try {
            EffectHandler handler = handlerMap.get(effect.getName()).newInstance(session, effect);
            handler.beforeExecuteDamage(obj, beActedObj, beActedObjs, res);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public static void BeforeBeExecutedDamage(FightSession session, FightEffect effect, FightObj actObj, FightObj beActedObj, List<FightObj> beActedObjs, AtkActionRes res) {
        try {
            EffectHandler handler = handlerMap.get(effect.getName()).newInstance(session, effect);
            handler.BeforeBeExecutedDamage(actObj, beActedObj, beActedObjs, res);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public static void beforeCaculateDamage(FightSession session, FightEffect effect, FightObj actObj, FightObj beActedObj, List<FightObj> beActedObjs, AtkActionRes res) {
        try {
            EffectHandler handler = handlerMap.get(effect.getName()).newInstance(session, effect);
            handler.beforeCaculateDamage(actObj, beActedObj, beActedObjs, res);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public static void beforeBeCaculatedDamage(FightSession session, FightEffect effect, FightObj actObj, FightObj beActedObj, List<FightObj> beActedObjs, AtkActionRes res) {
        try {
            EffectHandler handler = handlerMap.get(effect.getName()).newInstance(session, effect);
            handler.beforeBeCaculatedDamage(actObj, beActedObj, beActedObjs, res);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public static void afterCaculateDamage(FightSession session, FightEffect effect, FightObj actObj, FightObj beActedObj, List<FightObj> beActedObjs, AtkActionRes res) {
        try {
            EffectHandler handler = handlerMap.get(effect.getName()).newInstance(session, effect);
            handler.afterCaculateDamage(actObj, beActedObj, beActedObjs, res);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public static void afterBeCaculatedDamage(FightSession session, FightEffect effect, FightObj actObj, FightObj beActedObj, List<FightObj> beActedObjs, AtkActionRes res) {
        try {
            EffectHandler handler = handlerMap.get(effect.getName()).newInstance(session, effect);
            handler.afterBeCaculatedDamage(actObj, beActedObj, beActedObjs, res);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public static void caculateProp(FightEffect effect, ObjCaculaterProp caculaterProp) {
        try {
            EffectHandler handler = handlerMap.get(effect.getName()).newInstance(null, effect);
            handler.caculateProp(caculaterProp);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public static void caculateFightProp(FightSession session, FightObj obj) {
        long curHp = obj.getFightProp().getCurHp();
        long curMp = obj.getFightProp().getCurMp();
        obj.setFightProp(BeanUtil.bean2Bean(obj.getPanelProp(), ObjProp.class));
        obj.getFightProp().setCurHp(curHp);
        obj.getFightProp().setCurMp(curMp);
        for (FightEffect effect : obj.getEffects()) {
            try {
                EffectHandler handler = handlerMap.get(effect.getName()).newInstance(session, effect);
                handler.caculateFightProp(obj);
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }
}
