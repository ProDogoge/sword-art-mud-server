package com.prodog.gamemodule.fight.effect.handler.impl;

import com.prodog.gamemodule.fight.damage.util.DamageUtil;
import com.prodog.gamemodule.fight.effect.entity.FightEffect;
import com.prodog.gamemodule.fight.fightobj.entity.FightObj;
import com.prodog.gamemodule.fight.session.FightSession;

import java.util.List;
import java.util.Objects;
import java.util.stream.Collectors;

/***
 * 回合扣除HP
 */
public class RoundConsumeHpHandler extends DefaultEffectHandler {
    public static final String NAME = "roundConsumeHp";

    public RoundConsumeHpHandler(FightSession session, FightEffect effect) {
        super(session, effect);
    }

    @Override
    public void beforeRoundOver(FightObj obj) {
        if (obj.getState() != 2) {
            String abSTag = (String) get("abSTag");
            long damage = Long.parseLong(get("damage").toString());
            DamageUtil.consumeHp(obj, damage);
            obj.getPanelExpansion().addHpTag(abSTag, -damage);
            if (obj.getState() == 2) {
                getSession().getObjInfo().getSuffix().add(obj.gName() + "因" + abSTag + "死亡了");
            }
        }
        setAbSTag(obj);
    }

    private void setAbSTag(FightObj obj) {
        String abSTag = (String) get("abSTag");
        List<FightEffect> roundConsumes = obj.getEffects().stream().filter(e -> Objects.equals(e.getName(), NAME) && Objects.equals(e.getParams().get("abSTag"), abSTag))
                .collect(Collectors.toList());
        int maxRound = roundConsumes.stream().mapToInt(FightEffect::getRound).max().orElse(1);
        obj.getPanelExpansion().addAbSTag(abSTag, maxRound);
        if (maxRound <= 0) {
            obj.getPanelExpansion().removeAbSTag(abSTag);
        }
    }

}
