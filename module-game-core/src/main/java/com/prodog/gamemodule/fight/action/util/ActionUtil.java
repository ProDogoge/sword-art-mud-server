package com.prodog.gamemodule.fight.action.util;

import com.prodog.gamemodule.fight.action.entity.FightAction;
import com.prodog.gamemodule.fight.action.entity.FightActionSetRes;
import com.prodog.gamemodule.fight.fightobj.entity.FightObj;
import com.prodog.gamemodule.fight.fightobj.entity.FightSkill;
import com.prodog.gamemodule.fight.session.FightSession;
import com.prodog.gamemodule.fight.util.FightUtil;
import com.prodog.gamemodule.item.entity.PropItem;
import com.prodog.gamemodule.item.wrapper.PropWrapper;
import com.prodog.utils.bean.SpringBeanUtils;

import java.util.List;

/***
 * 行为工具类
 */
public class ActionUtil {

    public static FightActionSetRes setAction(FightSession session, String uuid, FightAction action) {
        FightObj fightObj = session.getObjsByUuid().get(uuid);
        if (action.getType().equals(FightAction.ACTION_TYPE_ATTACK)) {
            if (!FightUtil.hasSkill(fightObj, action.getActionId())) {
                return FightActionSetRes.failure("您未学习过该技能");
            }
            if (FightUtil.isSkillCooling(fightObj, action.getActionId())) {
                return FightActionSetRes.failure("该技能正在冷却中，剩余：" + FightUtil.getSkillCoolTime(fightObj, action.getActionId()) + "回合");
            }
            if (!FightUtil.isHpEnough(fightObj, action.getActionId())) {
                return FightActionSetRes.failure("您的HP不足，无法使用该技能。");
            }
            if (!FightUtil.isMpEnough(fightObj, action.getActionId())) {
                return FightActionSetRes.failure("您的TP不足，无法使用该技能。");
            }
            FightSkill skill = fightObj.getSkills().get(action.getActionId());
            action.setIgnoreDead(skill.isIgnoreMyDead());
            List<FightObj> beAffectedOjs = skill.isAffectTeam() ? FightUtil.getTeams(session, fightObj) : FightUtil.getEnemys(session, fightObj);
            if (action.getIndex() < 0 || beAffectedOjs.size() <= action.getIndex()) {
                return FightActionSetRes.failure("目标单位不存在");
            }
        }
        if (action.getType().equals(FightAction.ACTION_TYPE_USE_ITEM)) {
            if (FightUtil.isItemCooling(fightObj, action.getActionId())) {
                return FightActionSetRes.failure("该道具正在冷却中，剩余：" + FightUtil.getItemCoolTime(fightObj, action.getActionId()) + "回合");
            }
            PropWrapper propWrapper = SpringBeanUtils.getBean(PropWrapper.class);
            PropItem item = propWrapper.getById(action.getActionId());
            action.setIgnoreDead(item.isIgnoreMyDead());
            if (!item.getAllowState().contains(2)) return FightActionSetRes.failure("该道具无法在战斗状态下使用。");
            if (item.getRequiredLevel() > fightObj.getFightProp().getLevel())
                return FightActionSetRes.failure("您的当前等级无法使用[" + item.getName() + "]，要求等级:" + item.getRequiredLevel() + "。");
            List<FightObj> beAffectedOjs = item.isAffectTeam() ? FightUtil.getTeams(session, fightObj) : FightUtil.getEnemys(session, fightObj);
            if (action.getIndex() < 0 || beAffectedOjs.size() <= action.getIndex()) {
                return FightActionSetRes.failure("目标单位不存在");
            }
        }
        fightObj.setAction(action);
        return FightActionSetRes.success();
    }

    public static FightActionSetRes setSkillAction(FightSession session, String uuid, String skillId, Integer index) {
        return setAction(session, uuid, FightAction.attack(skillId, index));
    }

    public static FightActionSetRes setAttackAction(FightSession session, String uuid, Integer index) {
        FightObj obj = session.getObjsByUuid().get(uuid);
        FightSkill normalAttack = obj.getSkills().values().stream().filter(FightSkill::isNormal).findAny().get();
        return setSkillAction(session, uuid, normalAttack.getId(), index);
    }

    public static FightActionSetRes setUseItemAction(FightSession session, String uuid, String itemId, Integer index) {
        return setAction(session, uuid, FightAction.useItem(itemId, index));
    }

    public static FightActionSetRes setDefenseAction(FightSession session, String uuid) {
        return setAction(session, uuid, FightAction.defense());
    }
}
