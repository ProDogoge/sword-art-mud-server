package com.prodog.gamemodule.fight.effect.getter.role;

import cn.hutool.core.util.ReflectUtil;
import com.prodog.gamemodule.fight.effect.entity.FightEffect;
import com.prodog.gamemodule.fight.effect.getter.EffectsGetter;
import com.prodog.gamemodule.item.entity.ActiveEquipItem;
import com.prodog.gamemodule.item.entity.BadgeItem;
import com.prodog.gamemodule.item.wrapper.ActiveEquipWrapper;
import com.prodog.gamemodule.item.wrapper.BadgeWrapper;
import com.prodog.gamemodule.suit.domain.model.PartSet;
import com.prodog.gamemodule.suit.util.EquipSuitUtil;
import com.prodog.usermodule.role.entity.RoleEquip;
import com.prodog.usermodule.role.entity.RoleInfo;
import com.prodog.usermodule.role.util.EquipUtil;
import com.prodog.usermodule.role.wrapper.RoleEquipWrapper;
import com.prodog.utils.interfaces.AutowiredBean;
import org.springframework.beans.factory.annotation.Autowired;

import java.lang.reflect.Field;
import java.util.Arrays;
import java.util.List;
import java.util.Objects;
import java.util.stream.Collectors;

public class EquipEffectsGetter implements EffectsGetter<RoleInfo>, AutowiredBean {
    @Autowired
    private RoleEquipWrapper roleEquipWrapper;
    @Autowired
    private ActiveEquipWrapper activeEquipWrapper;
    @Autowired
    private BadgeWrapper badgeWrapper;


    public EquipEffectsGetter() {
        this.autowired();
    }

    @Override
    public List<FightEffect> get(List<FightEffect> effects, RoleInfo info) {
        RoleEquip roleEquip = roleEquipWrapper.getById(info.getId());
        List<String> ignoreField = Arrays.asList("id", "secondWeapon");
        List<Field> equIdFields = Arrays.stream(EquipUtil.roleEquipFields)
                .filter(f -> !ignoreField.contains(f.getName())).collect(Collectors.toList());
        List<String> equIds = equIdFields.stream().map(f -> (String) ReflectUtil.getFieldValue(roleEquip, f))
                .filter(Objects::nonNull)
                .collect(Collectors.toList());
        for (String equId : equIds) {
            //装备自身的效果
            ActiveEquipItem activeEquipItem = activeEquipWrapper.getById(equId);
            effects.addAll(activeEquipItem.getEffects());

            //徽章的效果
            List<BadgeItem> badges = activeEquipItem.getBadges().stream().map(b -> badgeWrapper.getById(b))
                    .collect(Collectors.toList());
            badges.forEach(b -> effects.addAll(b.getRoleEffects()));
        }

        //套装
        List<PartSet> partSets = EquipSuitUtil.getPartSets(roleEquip);
        partSets.forEach(partSet -> effects.addAll(partSet.getEffects()));
        return effects;
    }
}
