package com.prodog.gamemodule.fight.effect.handler.impl;

import com.prodog.gamemodule.fight.effect.entity.FightEffect;
import com.prodog.gamemodule.fight.fightobj.entity.FightObj;
import com.prodog.gamemodule.fight.session.FightSession;

import java.util.List;
import java.util.Objects;
import java.util.Optional;
import java.util.stream.Collectors;

/***
 * 控制效果
 */
public class HoldingHandler extends DefaultEffectHandler {
    public static final String NAME = "holding";

    public HoldingHandler(FightSession session, FightEffect effect) {
        super(session, effect);
    }

    @Override
    public void beforeAction(FightObj obj) {
        obj.getAction().setType(4);
        obj.setOver(true);

        Boolean printActContent = Optional.ofNullable(get("printActContent")).map(p -> (boolean) p).orElse(false);
        if (printActContent) {
            List<FightEffect> holdings = obj.getEffects().stream().filter(e -> e.getTags().contains("HOLDING")).collect(Collectors.toList());
            String holdingStr = holdings.stream().map(h -> (String) h.getParams().get("abSTag")).distinct().collect(Collectors.joining("、"));
            getSession().getObjInfo().setActContent(obj.getUuid(), obj.gName() + "因" + holdingStr + "无法行动");
        }
    }

    @Override
    public void beforeRoundOver(FightObj obj) {
        setAbSTag(obj);
    }

    private void setAbSTag(FightObj obj) {
        String abSTag = (String) get("abSTag");
        List<FightEffect> holdings = obj.getEffects().stream().filter(e -> Objects.equals(e.getName(), NAME) && Objects.equals(e.getParams().get("abSTag"), abSTag))
                .collect(Collectors.toList());
        int maxRound = holdings.stream().mapToInt(FightEffect::getRound).max().orElse(1);
        obj.getPanelExpansion().addAbSTag(abSTag, maxRound);
        if (maxRound <= 0) {
            obj.getPanelExpansion().removeAbSTag(abSTag);
        }
    }
}
