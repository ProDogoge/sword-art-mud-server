package com.prodog.gamemodule.fight.effect.handler.impl;

import com.prodog.commonmodule.prop.caculate.util.PropUtil;
import com.prodog.commonmodule.prop.entity.ObjProp;
import com.prodog.gamemodule.fight.action.entity.AtkActionRes;
import com.prodog.gamemodule.fight.damage.util.DamageUtil;
import com.prodog.gamemodule.fight.effect.constant.AbnormalTag;
import com.prodog.gamemodule.fight.effect.entity.FightEffect;
import com.prodog.gamemodule.fight.effect.manager.EffectHandlerManager;
import com.prodog.gamemodule.fight.fightobj.entity.FightObj;
import com.prodog.gamemodule.fight.session.FightSession;
import com.prodog.gamemodule.fight.util.FightUtil;
import com.prodog.utils.RandomUtil;
import com.prodog.utils.bean.BeanUtil;

import java.util.HashMap;
import java.util.List;
import java.util.Objects;

public class BloodCloakHandler extends DefaultEffectHandler {
    public final static String NAME = "bloodCloak";

    public BloodCloakHandler(FightSession session, FightEffect effect) {
        super(session, effect);
    }

    @Override
    public void onFightStart(FightObj obj) {
        //设置flag
        String flag = (String) get("roundToBleedKey");
        int roundToBleed = (int) get("roundToBleed");
        obj.getParams().put(flag, roundToBleed + 1);

        //进入出血状态
        toBleed(obj);
    }

    @Override
    public void afterBeExecutedDamage(FightObj actObj, FightObj beActedObj, List<FightObj> beActedObjs, AtkActionRes res) {
        int percentEnemyBleed = (int) get("percentEnemyBleed");
        if (RandomUtil.percentTest(FightUtil.getAbPercent(percentEnemyBleed, beActedObj, actObj))) {
            int enemyBleedRound = (int) get("enemyBleedRound");
            String enemyBleedExpression = (String) get("enemyBleedExpression");
            enemyBleedExpression = DamageUtil.preparedExpression(enemyBleedExpression, new HashMap<String, Object>() {{
                put("beActedObj", beActedObj);
            }});
            toBleed(actObj, enemyBleedExpression, enemyBleedRound);
        }
    }

    public void toBleed(FightObj obj, String expression, int round) {
        long bleedDamage = Long.parseLong(String.valueOf(DamageUtil.execLongExpression(expression)));
        FightEffect bleed = new FightEffect() {{
            setId(getId() + "_bleeding");
            setName(RoundConsumeHpHandler.NAME);
            setRound(round);
            setTags(AbnormalTag.ROUND_CONSUME_HP);
            setParams(new HashMap<String, Object>() {{
                put("abSTag", "出血");
                put("damage", bleedDamage);
            }});
        }};
        obj.getEffects().add(bleed);
    }

    public void toBleed(FightObj obj) {
        int bleedRound = (int) get("bleedRound");
        String bleedExpression = (String) get("bleedExpression");
        bleedExpression = DamageUtil.preparedExpression(bleedExpression, new HashMap<String, Object>() {{
            put("obj", obj);
        }});
        toBleed(obj, bleedExpression, bleedRound);
    }


    @Override
    public void beforeRoundOver(FightObj obj) {
        String flag = (String) get("roundToBleedKey");
        int currRoundToBleed = (int) obj.getParams().getOrDefault(flag, 0);
        currRoundToBleed--;
        obj.getParams().put(flag, currRoundToBleed);
        if (currRoundToBleed <= 0) {
            toBleed(obj);
            EffectHandlerManager.caculateFightProp(getSession(), obj);
            int roundToBleed = (int) get("roundToBleed");
            obj.getParams().put(flag, roundToBleed);
        }
    }

    @Override
    public void caculateFightProp(FightObj obj) {
        boolean isBleeding = obj.getEffects().stream().anyMatch(e -> Objects.equals(e.getName(), RoundConsumeHpHandler.NAME) && Objects.equals(e.getParams().get("abSTag"), "出血"));
        if (isBleeding) {
            ObjProp updProp = BeanUtil.bean2Bean(get("bleedUpdProp"), ObjProp.class);
            PropUtil.sumProp(obj.getFightProp(), updProp);
        }
    }
}
