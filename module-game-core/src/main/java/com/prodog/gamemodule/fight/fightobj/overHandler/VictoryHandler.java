package com.prodog.gamemodule.fight.fightobj.overHandler;

import com.prodog.gamemodule.fight.session.FightSession;

public class VictoryHandler implements FightOverHandler {
    @Override
    public void handle(FightSession session, StringBuilder msg) {
        msg.append("\n\n战斗结束,").append(session.getVictory() == 0 ? "我方胜利！\n" : "我方失败！\n");
    }
}
