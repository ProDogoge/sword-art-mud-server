package com.prodog.gamemodule.fight.effect.handler.impl;

import com.prodog.commonmodule.prop.caculate.util.PropUtil;
import com.prodog.commonmodule.prop.entity.ObjProp;
import com.prodog.gamemodule.fight.effect.entity.FightEffect;
import com.prodog.gamemodule.fight.effect.manager.EffectHandlerManager;
import com.prodog.gamemodule.fight.fightobj.entity.FightObj;
import com.prodog.gamemodule.fight.session.FightSession;
import com.prodog.usermodule.role.util.BagUtil;
import com.prodog.utils.bean.BeanUtil;

public class SoulHunterHandler extends DefaultEffectHandler {
    public final static String NAME = "soulHunter";

    public SoulHunterHandler(FightSession session, FightEffect effect) {
        super(session, effect);
    }

    @Override
    public void onFightStart(FightObj obj) {
        //设置flag
        String flag = (String) get("soulHunterRoundKey");
        int soulHunterRound = (int) get("soulHunterRound");
        obj.getParams().put(flag, soulHunterRound);

        //消耗材料并设置增强开关
        consumeAndEnableUpdProp(obj);
    }

    @Override
    public void beforeRoundOver(FightObj obj) {
        String flag = (String) get("soulHunterRoundKey");
        int currRoundToUpd = (int) obj.getParams().getOrDefault(flag, 0);
        currRoundToUpd--;
        obj.getParams().put(flag, currRoundToUpd);
        if (currRoundToUpd <= 0) {
            boolean enable = consumeAndEnableUpdProp(obj);
            EffectHandlerManager.caculateFightProp(getSession(), obj);

            if (enable) {
                int soulHunterRound = (int) get("soulHunterRound");
                obj.getParams().put(flag, soulHunterRound);
            }
        }
    }

    private boolean consumeAndEnableUpdProp(FightObj obj) {
        String consumeItem = (String) get("consumeItem");
        int consumeNums = (int) get("consumeNums");
        String enableUpdPropKey = (String) get("enableUpdPropKey");
        if (BagUtil.consumeItemAndSave(obj.getId(), consumeItem, consumeNums)) {
            obj.getParams().put(enableUpdPropKey, true);
        } else {
            obj.getParams().put(enableUpdPropKey, false);
        }
        return (boolean) obj.getParams().get(enableUpdPropKey);
    }

    @Override
    public void caculateFightProp(FightObj obj) {
        String enableUpdPropKey = (String) get("enableUpdPropKey");
        boolean enableUpd = (boolean) obj.getParams().getOrDefault(enableUpdPropKey, false);
        if (enableUpd) {
            ObjProp updProp = BeanUtil.bean2Bean(get("updProp"), ObjProp.class);
            PropUtil.upgradeProp(obj.getPanelProp(), obj.getFightProp(), updProp);
        }
    }
}
