package com.prodog.gamemodule.fight.fightobj.entity.panel;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.Objects;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class ExtendShow {
    private String id;
    private String name;
    private String curr;
    private String max;

    public String getShow() {
        return name + ": " + curr + "/" + max;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        ExtendShow that = (ExtendShow) o;
        return Objects.equals(id, that.id);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id);
    }
}
