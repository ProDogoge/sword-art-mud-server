package com.prodog.gamemodule.fight.fightobj.overHandler;

import cn.hutool.core.lang.WeightRandom;
import cn.hutool.core.util.StrUtil;
import com.prodog.gamemodule.config.dict.DictConfig;
import com.prodog.gamemodule.fight.fightobj.entity.FightObj;
import com.prodog.gamemodule.fight.session.FightSession;
import com.prodog.gamemodule.gameworld.monster.entity.Monster;
import com.prodog.gamemodule.gameworld.monster.entity.MonsterDrop;
import com.prodog.gamemodule.gameworld.monster.wrapper.MonsterWrapper;
import com.prodog.gamemodule.item.entity.GameItem;
import com.prodog.usermodule.role.entity.RoleBag;
import com.prodog.usermodule.role.entity.RoleInfo;
import com.prodog.usermodule.role.exp.ExpGetter;
import com.prodog.usermodule.role.util.BagUtil;
import com.prodog.usermodule.role.util.GameItemUtil;
import com.prodog.usermodule.role.wrapper.RoleBagWrapper;
import com.prodog.usermodule.role.wrapper.RoleInfoWrapper;
import com.prodog.utils.RandomUtil;
import com.prodog.utils.interfaces.AutowiredBean;
import org.springframework.beans.factory.annotation.Autowired;

import java.util.*;
import java.util.stream.Collectors;

public class ExpDropHandler implements FightOverHandler, AutowiredBean {
    @Autowired
    private DictConfig dictConfig;
    @Autowired
    private MonsterWrapper monsterWrapper;
    @Autowired
    private RoleInfoWrapper roleInfoWrapper;
    @Autowired
    private RoleBagWrapper roleBagWrapper;

    public ExpDropHandler() {
        this.autowired();
    }

    @Override
    public void handle(FightSession session, StringBuilder msg) {
        if (session.getVictory() != 0) return;
        List<Integer> monsterTypes = Arrays.asList(2, 4, 5);
        List<FightObj> enemys = session.getBlueObjs().values()
                .stream().filter(obj -> monsterTypes.contains(obj.getObjType()))
                .collect(Collectors.toList());
        List<FightObj> ours = session.getRedObjs().values()
                .stream().filter(obj -> obj.getObjType() == 1)
                .collect(Collectors.toList());
        List<Monster> monsters = monsterWrapper.getByIds(enemys.stream().map(FightObj::getId).collect(Collectors.toList()));
        for (FightObj our : ours) {
            RoleInfo role = roleInfoWrapper.getById(our.getId());
            msg.append(role.getRoleName()).append(": ").append("获得");
            long totalExp = 0;
            long totalMoney = 0;
            for (Monster monster : monsters) {
                totalMoney += monster.getMoney();
                totalExp += new ExpGetter().get(role, monster, monster.getExp());
            }
            totalExp = totalExp / ours.size();
            totalMoney = totalMoney / ours.size();
            if (totalExp > 0)
                msg.append(dictConfig.get("exp")).append(":").append(totalExp).append("  ");
            if (totalMoney > 0)
                msg.append(dictConfig.get("money")).append(":").append(totalMoney).append("  ");
            role.setExp(role.getExp() + totalExp);
            role.setMoney(role.getMoney() + totalMoney);
            roleInfoWrapper.save(role);

            //掉落
            StringBuilder dropStr = new StringBuilder();
            Map<String, Long> dropMap = new LinkedHashMap<>();
            RoleBag roleBag = roleBagWrapper.getById(role.getId());
            for (Monster monster : monsters) {
                for (MonsterDrop drop : monster.getDrops()) {
                    if (RandomUtil.percentTest(drop.getDropRate())) {
                        long dropNums = cn.hutool.core.util.RandomUtil.randomLong(drop.getMinDropNums(), drop.getMaxDropNums() + 1);
                        List<WeightRandom.WeightObj<String>> weightList = new ArrayList<>();
                        drop.getDropItems().forEach((itemId, weight) -> weightList.add(new WeightRandom.WeightObj<>(itemId, weight)));
                        for (long i = 0; i < dropNums; i++) {
                            String itemId = cn.hutool.core.util.RandomUtil.weightRandom(weightList).next();
                            Long nums = dropMap.getOrDefault(itemId, 0L);
                            dropMap.put(itemId, nums + 1);
                        }
                    }
                }
            }
            dropMap.forEach((itemId, nums) -> {
                GameItem item = GameItemUtil.getById(itemId);
                dropStr.append(item.getName()).append("x").append(nums).append(" ");
                BagUtil.addItem(roleBag, itemId, nums);
            });
            if (StrUtil.isNotBlank(dropStr.toString().trim())) {
                msg.append("掉落: ").append(dropStr.toString().trim());
            }
            msg.append("\n");
            roleBagWrapper.save(roleBag);
        }
    }
}
