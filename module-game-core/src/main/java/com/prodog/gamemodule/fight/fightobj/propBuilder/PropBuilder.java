package com.prodog.gamemodule.fight.fightobj.propBuilder;

import com.prodog.gamemodule.fight.fightobj.entity.FightObj;

public interface PropBuilder<T> {
    FightObj build(FightObj obj, T t);
}
