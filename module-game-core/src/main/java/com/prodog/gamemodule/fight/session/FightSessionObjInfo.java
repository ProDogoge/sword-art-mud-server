package com.prodog.gamemodule.fight.session;

import com.prodog.gamemodule.fight.fightobj.entity.FightObjActInfo;
import lombok.Data;

import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

@Data
public class FightSessionObjInfo {
    private String roundHead;
    private List<String> prefix = new ArrayList<>();
    private Map<String, FightObjActInfo> actInfos = new LinkedHashMap<>();
    private List<String> suffix = new ArrayList<>();

    public void addBeActEffectValue(String uuid, String beActUuid, String name, String msg, long value) {
        if (actInfos.get(uuid) == null) {
            actInfos.put(uuid, new FightObjActInfo());
        }
        actInfos.get(uuid).addBeActEffectValue(beActUuid, name, msg, value);
    }

    public void addbeActEffectMsg(String uuid, String beActUuid, String name, String msg) {
        if (actInfos.get(uuid) == null) {
            actInfos.put(uuid, new FightObjActInfo());
        }
        actInfos.get(uuid).addbeActEffectMsg(beActUuid, name, msg);
    }

    public void addActEffectValue(String uuid, String beActUuid, String name, String msg, long value) {
        if (actInfos.get(uuid) == null) {
            actInfos.put(uuid, new FightObjActInfo());
        }
        actInfos.get(uuid).addActEffectValue(beActUuid, name, msg, value);
    }

    public void addActEffectMsg(String uuid, String beActUuid, String name, String msg) {
        if (actInfos.get(uuid) == null) {
            actInfos.put(uuid, new FightObjActInfo());
        }
        actInfos.get(uuid).addActEffectMsg(beActUuid, name, msg);
    }

    public void addBeActInfo(String uuid, String beActUuid, String content) {
        if (actInfos.get(uuid) == null) {
            actInfos.put(uuid, new FightObjActInfo());
        }
        actInfos.get(uuid).addBeActInfo(beActUuid, content);
    }

    public void appendBeActInfo(String uuid, String beActUuid, String content) {
        FightObjActInfo actInfo = actInfos.computeIfAbsent(uuid, uid -> new FightObjActInfo());
        actInfo.apppendBeActInfo(beActUuid, content);
    }

    public void prependBeActInfo(String uuid, String beActUuid, String content) {
        FightObjActInfo actInfo = actInfos.computeIfAbsent(uuid, uid -> new FightObjActInfo());
        actInfo.prependBeActInfo(beActUuid, content);
    }

    public void addHpAbsore(String uuid, long value) {
        if (actInfos.get(uuid) == null) {
            actInfos.put(uuid, new FightObjActInfo());
        }
        actInfos.get(uuid).addHpAbsore(value);
    }

    public void addMpAbsore(String uuid, long value) {
        if (actInfos.get(uuid) == null) {
            actInfos.put(uuid, new FightObjActInfo());
        }
        actInfos.get(uuid).addMpAbsore(value);
    }

    public void setActPrefix(String uuid, String prefix) {
        if (actInfos.get(uuid) == null) {
            actInfos.put(uuid, new FightObjActInfo());
        }
        actInfos.get(uuid).setPrefix(prefix);
    }

    public void setActContent(String uuid, String content) {
        if (actInfos.get(uuid) == null) {
            actInfos.put(uuid, new FightObjActInfo());
        }
        actInfos.get(uuid).setContent(content);
    }

    public String getInfo() {
        StringBuilder sb = new StringBuilder();
        sb.append(roundHead).append("\n").append(prefix.stream().collect(Collectors.joining("\n")));
        actInfos.values().forEach(info -> sb.append(info.info()).append("\n"));
        sb.append(String.join("\n", suffix)).append("\n");
        if (suffix.size() > 0) sb.append("\n");
        return sb.toString();
    }
}
