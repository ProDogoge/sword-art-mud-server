package com.prodog.gamemodule.fight.damage.handler.impl;

import com.prodog.commonmodule.prop.entity.ObjProp;
import com.prodog.gamemodule.fight.damage.entity.Damage;
import com.prodog.gamemodule.fight.action.entity.AtkActionRes;
import com.prodog.gamemodule.fight.damage.handler.DamageHandler;
import com.prodog.gamemodule.fight.fightobj.entity.FightObj;
import com.prodog.gamemodule.fight.fightobj.entity.FightSkill;

/***
 * 增伤处理
 */
public class AdditionalDamageHandler implements DamageHandler {
    private FightObj obj;
    private FightObj beActedObj;
    private AtkActionRes atkActionRes;

    public AdditionalDamageHandler(FightObj obj, FightObj beActedObj, AtkActionRes atkActionRes) {
        this.obj = obj;
        this.beActedObj = beActedObj;
        this.atkActionRes = atkActionRes;
    }

    @Override
    public void handle() {
        FightSkill skill = obj.getSkills().get(obj.getAction().getActionId());
        ObjProp prop = obj.getFightProp();
        Damage normalDamage = atkActionRes.getDamage(beActedObj.getUuid(), Damage.NORMAL);
        long damage = normalDamage.getValue();
        //计算增伤
        if (prop.getAddDamage() != 0 || prop.getAddDamageRate() != 0) {
            long addDamage = prop.getAddDamage();
            addDamage += (damage * prop.getAddDamageRate()) / 100;
            long finalAddDamage = addDamage;
            atkActionRes.addDamage(beActedObj.getUuid(), new Damage() {{
                setByUuid(obj.getUuid());
                setDamageType(skill.getDamageType());
                setName(Damage.ADDITIONAL);
                setAffectType(skill.getAffectType());
                setValue(finalAddDamage);
            }});
        }
    }
}
