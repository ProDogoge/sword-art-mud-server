package com.prodog.gamemodule.fight.fightobj.entity;

import com.prodog.commonmodule.prop.entity.ObjProp;
import com.prodog.gamemodule.fight.action.entity.FightAction;
import com.prodog.gamemodule.fight.damage.entity.Damage;
import com.prodog.gamemodule.fight.effect.entity.FightEffect;
import com.prodog.gamemodule.fight.fightobj.entity.panel.PanelExpansion;
import com.prodog.gamemodule.fight.session.FightSessionManager;
import com.prodog.gamemodule.fight.util.FightUtil;
import lombok.Data;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;


/***
 * 战斗对象
 */
@Data
public class FightObj {
    private String id;
    private int objType;    //单位类型 1.玩家 2.怪物 3.召唤物 4.精英怪物 5.领主怪物
    private String sessionId;   //战斗sessionId
    private String uuid;    //识别码
    private String name;    //对象名称
    private Integer flag;   //标记(0为红方 1为蓝方)
    private ObjProp panelProp;  //面板属性(指不算任何被动加成时的属性) 用于动态计算战斗时属性
    private ObjProp fightProp;  //战斗时属性
    private int state = 1;  //1正常 2死亡
    private boolean over;   //当前回合动作完成
    private FightAction action; //当前回合的动作(攻击 防御 使用道具等)
    private String fightAi;    //战斗AI类名
    private Map<String, Object> params = new HashMap<>(); //用于被动使用的参数
    private Map<String, Integer> coolTimeMap = new HashMap<>(); //用于技能 道具等冷却时间
    private Map<String, Damage> beActedDamage = new HashMap<>();  //被作用的伤害
    private Map<String, FightSkill> skills = new HashMap<>();
    private List<FightEffect> effects = new ArrayList<>();  //被动效果

    /***************显示用***************/
    private PanelExpansion panelExpansion = new PanelExpansion();

    public String fightHpTpPanel() {
        StringBuilder sb = new StringBuilder();
        sb.append("[").append(fightProp.getLevel()).append("级]").append(name);
        if (state == 2) sb.append("(死亡)");
        sb.append(panelExpansion.getAbSTagsStr()).append("  ")
                .append(" HP: ").append(fightProp.getCurHp()).append("/").append(fightProp.getHp()).append(panelExpansion.getHpTagsStr());
        if (!(fightProp.getCurMp() == 0 && fightProp.getMp() == 0)) {
            sb.append(" | TP: ").append(fightProp.getCurMp()).append("/").append(fightProp.getMp()).append(panelExpansion.getMpTagsStr());
        }
        if (panelExpansion.getExtendShows().size() > 0) sb.append(" | ").append(panelExpansion.getExtendShowStr());
        /*if (state == 2) {
            sb.append(" (已死亡)");
        }*/
        return sb.toString();
    }

    public String gName() {
        List<FightObj> teams = FightUtil.getTeams(FightSessionManager.get(sessionId), this);
        int index = -1;
        for (int i = 0; i < teams.size(); i++) {
            if (teams.get(i).getUuid().equals(this.uuid)) {
                index = i;
            }
        }
        return "(" + (flag == 0 ? "R" : "B") + (index + 1) + ")" + getName();
    }
}
