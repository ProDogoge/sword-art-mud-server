package com.prodog.gamemodule.fight.fightobj.entity.panel;

import cn.hutool.core.util.StrUtil;
import lombok.Data;

import java.util.*;
import java.util.stream.Collectors;

@Data
/***
 * 显示面板拓展
 */
public class PanelExpansion {
    private Map<String, HpChangeTag> hpTags = new LinkedHashMap<>();
    private Map<String, MpChangeTag> mpTags = new LinkedHashMap<>();
    private Map<String, AbnormalStateTag> abnormalTags = new LinkedHashMap<>();
    private Set<ExtendShow> extendShows = new LinkedHashSet<>();

    public void addHpTag(String tagName, long value) {
        HpChangeTag tag = Optional.ofNullable(hpTags.get(tagName)).orElse(new HpChangeTag(tagName, 0));
        tag.setValue(tag.getValue() + value);
        hpTags.put(tagName, tag);
    }

    public void addMpTag(String tagName, long value) {
        MpChangeTag tag = Optional.ofNullable(mpTags.get(tagName)).orElse(new MpChangeTag(tagName, 0));
        tag.setValue(tag.getValue() + value);
        mpTags.put(tagName, tag);
    }

    public void addAbSTag(String tagName, int round) {
        AbnormalStateTag tag = Optional.ofNullable(abnormalTags.get(tagName)).orElse(new AbnormalStateTag(tagName, round));
        tag.setRound(round);
        abnormalTags.put(tagName, tag);
    }

    public void removeAbSTag(String tagName) {
        abnormalTags.remove(tagName);
    }

    public String getHpTagsStr() {
        String str = hpTags.values().stream().map(HpChangeTag::tagVal).collect(Collectors.joining(","));
        if (StrUtil.isNotBlank(str)) {
            return "(" + str + ")";
        }
        return "";
    }

    public String getMpTagsStr() {
        String str = mpTags.values().stream().map(MpChangeTag::tagVal).collect(Collectors.joining(","));
        if (StrUtil.isNotBlank(str)) {
            return "(" + str + ")";
        }
        return "";
    }

    public String getAbSTagsStr() {
        String str = abnormalTags.values().stream().map(AbnormalStateTag::tagVal).collect(Collectors.joining(","));
        if (StrUtil.isNotBlank(str)) {
            return "{" + str + "}";
        }
        return "";
    }

    public String getExtendShowStr() {
        return getExtendShows().stream().map(ExtendShow::getShow).collect(Collectors.joining(" | "));
    }

    public void addExtendShow(String id, String name, String curr, String max) {
        ExtendShow extend = extendShows.stream().filter(e -> e.getId().equals(id)).findAny().orElse(null);
        if (extend != null) extendShows.remove(extend);
        extendShows.add(new ExtendShow(id, name, curr, max));
    }

    public void clearAllTags() {
        hpTags.clear();
        mpTags.clear();
        abnormalTags.clear();
    }
}
