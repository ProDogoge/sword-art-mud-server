package com.prodog.gamemodule.fight.round.handler;

import com.prodog.gamemodule.config.dict.DictConfig;
import com.prodog.gamemodule.fight.damage.util.DamageUtil;
import com.prodog.gamemodule.fight.effect.entity.FightEffect;
import com.prodog.gamemodule.fight.effect.manager.EffectHandlerManager;
import com.prodog.gamemodule.fight.fightobj.entity.FightObj;
import com.prodog.gamemodule.fight.session.FightSession;
import com.prodog.utils.interfaces.AutowiredBean;
import org.springframework.beans.factory.annotation.Autowired;

import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

public class RoundHandler implements AutowiredBean {
    private FightSession session;

    @Autowired
    private DictConfig dictConfig;

    public RoundHandler(FightSession session) {
        this.autowired();
        this.session = session;
    }

    public void roundStart() {
        for (FightObj obj : session.getObjsByUuid().values()) {
            for (FightEffect effect : obj.getEffects()) {
                EffectHandlerManager.onRoundStart(session, effect, obj);
            }
            EffectHandlerManager.caculateFightProp(session, obj);
        }
    }


    public void roundOver() {
        for (FightObj obj : session.getObjsByUuid().values()) {
            //效果回合结束前 冷却-1 回合结束后处理
            for (int i = 0; i < obj.getEffects().size(); i++) {
                FightEffect effect = obj.getEffects().get(i);
                EffectHandlerManager.beforeRoundOver(session, effect, obj);
                EffectHandlerManager.roundDown(session, effect, obj);
                EffectHandlerManager.afterRoundDown(session, effect, obj);
            }
            //删除掉过期的效果
            List<FightEffect> destroyEffects = obj.getEffects().stream().filter(e -> e.getRound() != null && e.getRound() <= 0).collect(Collectors.toList());
            obj.getEffects().removeAll(destroyEffects);

            //技能道具等冷却时间-1
            for (Map.Entry<String, Integer> e : obj.getCoolTimeMap().entrySet()) {
                if (e.getValue() - 1 == 0) obj.getCoolTimeMap().remove(e.getKey());
                else obj.getCoolTimeMap().put(e.getKey(), e.getValue() - 1);
            }

            //触发HP TP恢复词条
            if (obj.getState() != 2) {
                long hpReply = obj.getFightProp().getHpReply();
                long mpReply = obj.getFightProp().getMpReply();
                if (hpReply > 0 && obj.getFightProp().getCurHp() < obj.getFightProp().getHp()) {
                    DamageUtil.replyHp(obj, hpReply);
                    obj.getPanelExpansion().addHpTag(dictConfig.get("hp") + "恢复", hpReply);
                }
                if (mpReply > 0 && obj.getFightProp().getCurMp() < obj.getFightProp().getMp()) {
                    DamageUtil.replyMp(obj, mpReply);
                    obj.getPanelExpansion().addMpTag(dictConfig.get("mp") + "恢复", mpReply);
                }
            }
            EffectHandlerManager.caculateFightProp(session, obj);
        }
    }
}
