package com.prodog.gamemodule.fight.action.entity;

import com.prodog.gamemodule.fight.damage.entity.Damage;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.LinkedHashMap;
import java.util.Map;

@Data
@NoArgsConstructor
@AllArgsConstructor
/***
 * 攻击动作结果
 */
public class AtkActionRes {
    private long hpAbsore;  //吸血量
    private long mpAbsore;  //吸蓝量
    private Map<String, Map<String, Damage>> damageMap;

    public void addDamage(String uuid, Damage damage) {
        if (!damageMap.containsKey(uuid)) {
            damageMap.put(uuid, new LinkedHashMap<>());
        }
        damageMap.get(uuid).put(damage.getName(), damage);
    }

    public Damage getDamage(String uuid, String name) {
        return damageMap.get(uuid).get(name);
    }

    public long getTotalDamage(String uuid) {
        Map<String, Damage> map = damageMap.get(uuid);
        long damage = 0;
        for (Damage value : map.values()) {
            damage += value.getValue();
        }
        return damage;
    }
}
