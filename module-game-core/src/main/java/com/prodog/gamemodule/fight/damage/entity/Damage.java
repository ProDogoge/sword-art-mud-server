package com.prodog.gamemodule.fight.damage.entity;

import lombok.Data;

;

/***
 * 战斗伤害
 */
@Data
public class Damage {
    private String name;    //伤害的名称(普通伤害normal 增伤additional 属性伤害elemental等)
    private Integer damageType;   //伤害类型
    private boolean abnormal;   //是否异常伤害
    private Integer affectType = 1;   //作用类型
    private String byUuid;  //造成本次伤害的单位
    private Long value; //值

    /**************伤害名称*****************/
    public static final String NORMAL = "NORMAL";    //普通伤害
    public static final String ADDITIONAL = "ADDITIONAL";    //增伤

    public static final String ELEMENTAL_FIRE = "ELEMENTAL_FIRE";  //火属性伤害
    public static final String ELEMENTAL_ICE = "ELEMENTAL_ICE";  //冰属性伤害
    public static final String ELEMENTAL_LIGHT = "ELEMENTAL_LIGHT";  //光属性伤害
    public static final String ELEMENTAL_DARK = "ELEMENTAL_DARK";  //暗属性伤害

    public static final String CRITICAL = "CRITICAL";   //暴伤

    /***************伤害类型*****************/
    public static final Integer DAMAGE_TYPE_PHYSICAL = 1;   //物理伤害
    public static final Integer DAMAGE_TYPE_MAGICAL = 2;    //魔法伤害
    public static final Integer DAMAGE_TYPE_REAL = 3;   //真实伤害

    /***************作用类型*****************/
    public static final Integer AFFECT_TYPE_INJURE = 1; //伤害
    public static final Integer AFFECT_TYPE_REPLY = 2;  //恢复
}
