package com.prodog.gamemodule.fight.effect.handler.impl;

import com.prodog.gamemodule.fight.effect.entity.FightEffect;
import com.prodog.gamemodule.fight.fightobj.entity.FightObj;
import com.prodog.gamemodule.fight.session.FightSession;

/***
 * 麻痹效果
 */
public class ParalysisEffectHandler extends DefaultEffectHandler {
    public ParalysisEffectHandler(FightSession session, FightEffect effect) {
        super(session, effect);
    }

    @Override
    public void beforeAction(FightObj obj) {
        obj.getAction().setType(4);
        obj.setOver(true);
        getSession().getObjInfo().setActContent(obj.getUuid(), obj.gName() + "因麻痹状态无法行动");
    }

    @Override
    public void destroy(FightObj obj) {
        obj.getPanelExpansion().removeAbSTag("麻痹");
    }

    @Override
    public void afterRoundDown(FightObj obj) {
        obj.getPanelExpansion().addAbSTag("麻痹", getEffect().getRound() + 1);
    }
}
