package com.prodog.gamemodule.fight.damage.handler.impl;

import com.prodog.commonmodule.prop.entity.ObjProp;
import com.prodog.gamemodule.fight.damage.entity.Damage;
import com.prodog.gamemodule.fight.action.entity.AtkActionRes;
import com.prodog.gamemodule.fight.damage.handler.DamageHandler;
import com.prodog.gamemodule.fight.fightobj.entity.FightObj;
import com.prodog.gamemodule.fight.fightobj.entity.FightSkill;
import com.prodog.utils.RandomUtil;

/***
 * 暴伤处理
 */
public class CriticalDamageHandler implements DamageHandler {
    private FightObj obj;
    private FightObj beActedObj;
    private AtkActionRes atkActionRes;

    public CriticalDamageHandler(FightObj obj, FightObj beActedObj, AtkActionRes atkActionRes) {
        this.obj = obj;
        this.beActedObj = beActedObj;
        this.atkActionRes = atkActionRes;
    }

    @Override
    public void handle() {
        FightSkill skill = obj.getSkills().get(obj.getAction().getActionId());
        ObjProp prop = obj.getFightProp();
        long damage = atkActionRes.getTotalDamage(beActedObj.getUuid());
        int criticalRate;
        if (skill.getDamageType() == 1) {
            criticalRate = prop.getPhysicalCrit();
        } else if (skill.getDamageType() == 2) {
            criticalRate = prop.getMagicalCrit();
        } else {
            criticalRate = Math.max(prop.getPhysicalCrit(), prop.getMagicalCrit());
        }
        if (RandomUtil.percentTest(criticalRate)) {
            long criticalDamage = (long) ((damage * (prop.getCriticalDamage() + 50)) * 0.01);
            atkActionRes.addDamage(beActedObj.getUuid(), new Damage() {{
                setByUuid(obj.getUuid());
                setDamageType(skill.getDamageType());
                setName(Damage.CRITICAL);
                setAffectType(skill.getAffectType());
                setValue(criticalDamage);
            }});
        }
    }
}
