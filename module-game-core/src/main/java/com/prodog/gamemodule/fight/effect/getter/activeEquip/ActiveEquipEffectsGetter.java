package com.prodog.gamemodule.fight.effect.getter.activeEquip;

import com.prodog.gamemodule.fight.effect.entity.FightEffect;
import com.prodog.gamemodule.fight.effect.getter.EffectsGetter;
import com.prodog.gamemodule.item.entity.ActiveEquipItem;
import com.prodog.gamemodule.item.entity.BadgeItem;
import com.prodog.gamemodule.item.wrapper.BadgeWrapper;
import com.prodog.utils.interfaces.AutowiredBean;
import org.springframework.beans.factory.annotation.Autowired;

import java.util.List;
import java.util.stream.Collectors;

public class ActiveEquipEffectsGetter implements EffectsGetter<ActiveEquipItem>, AutowiredBean {
    @Autowired
    private BadgeWrapper badgeWrapper;

    public ActiveEquipEffectsGetter() {
        this.autowired();
    }

    @Override
    public List<FightEffect> get(List<FightEffect> effects, ActiveEquipItem activeEquipItem) {
        List<BadgeItem> badges = activeEquipItem.getBadges().stream().map(b -> badgeWrapper.getById(b)).collect(Collectors.toList());
        for (BadgeItem badge : badges) {
            effects.addAll(badge.getEquipEffects());
        }
        return effects;
    }
}
