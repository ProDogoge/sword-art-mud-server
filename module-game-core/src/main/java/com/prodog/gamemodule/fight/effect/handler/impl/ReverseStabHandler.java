package com.prodog.gamemodule.fight.effect.handler.impl;

import com.prodog.gamemodule.fight.action.entity.AtkActionRes;
import com.prodog.gamemodule.fight.damage.util.DamageUtil;
import com.prodog.gamemodule.fight.effect.entity.FightEffect;
import com.prodog.gamemodule.fight.fightobj.entity.FightObj;
import com.prodog.gamemodule.fight.session.FightSession;
import com.prodog.gamemodule.fight.util.FightUtil;
import com.prodog.utils.RandomUtil;

import java.util.HashMap;
import java.util.List;

/***
 * 反刺效果
 */
public class ReverseStabHandler extends DefaultEffectHandler {
    public ReverseStabHandler(FightSession session, FightEffect effect) {
        super(session, effect);
    }

    @Override
    public void afterBeExecutedDamage(FightObj actObj, FightObj beActedObj, List<FightObj> beActedObjs, AtkActionRes res) {
        Integer reversePercent = (Integer) get("reversePercent");
        Integer reverseRate = (Integer) get("reverseRate");
        if (RandomUtil.percentTest(reversePercent)) {
            long damage = (long) (res.getTotalDamage(beActedObj.getUuid()) * reverseRate * 0.01);
            DamageUtil.consumeHp(actObj, damage);
            String msg = actObj.gName() + "因[反刺]受到了${value}点伤害";
            if (actObj.getState() == 2) {
                msg += ",死亡了";
            } else {
                Integer bleedingPercent = (Integer) get("bleedingPercent");
                if (RandomUtil.percentTest(FightUtil.getAbPercent(bleedingPercent, beActedObj, actObj))) {
                    int bleedRound = (int) get("bleedingRound");
                    String expression = (String) get("bleedingExpression");
                    expression = DamageUtil.preparedExpression(expression, new HashMap<String, Object>() {{
                        put("actObj", actObj);
                        put("beActedObj", beActedObj);
                    }});
                    long bleedDamage = Long.parseLong(String.valueOf(DamageUtil.execLongExpression(expression)));
                    msg += ",出血了";
                    actObj.getEffects().add(new FightEffect() {{
                        setName(RoundConsumeHpHandler.NAME);
                        setRound(bleedRound);
                        setParams(new HashMap<String, Object>() {{
                            put("abSTag", "出血");
                            put("damage", bleedDamage);
                        }});
                    }});
                }
            }
            getSession().getObjInfo().addBeActEffectValue(actObj.getUuid(), beActedObj.getUuid(), "reverseStab", msg, damage);
        }
    }
}
