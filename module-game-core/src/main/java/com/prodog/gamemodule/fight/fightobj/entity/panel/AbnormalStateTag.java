package com.prodog.gamemodule.fight.fightobj.entity.panel;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class AbnormalStateTag {
    private String tag;
    private int round;

    public String tagVal() {
        return tag + "(" + round + ")";
    }
}
