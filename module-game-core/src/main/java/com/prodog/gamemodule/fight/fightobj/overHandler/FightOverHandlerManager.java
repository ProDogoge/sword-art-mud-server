package com.prodog.gamemodule.fight.fightobj.overHandler;

import java.util.*;
import java.util.stream.Collectors;

public class FightOverHandlerManager {
    private static Map<String, FightOverHandler> handlerMap = new LinkedHashMap<>();

    static {
        handlerMap.put("victory", new VictoryHandler());
        handlerMap.put("expDrop", new ExpDropHandler());
        handlerMap.put("setCurHpMpState", new SetCurHpMpStateHandler());
        handlerMap.put("killRecord", new KillRecordHandler());
    }

    public static List<FightOverHandler> get(String... names) {
        return Arrays.stream(names).map(name -> handlerMap.get(name)).collect(Collectors.toList());
    }

    public static List<FightOverHandler> get(Collection<String> names) {
        return names.stream().map(name -> handlerMap.get(name)).collect(Collectors.toList());
    }
}
