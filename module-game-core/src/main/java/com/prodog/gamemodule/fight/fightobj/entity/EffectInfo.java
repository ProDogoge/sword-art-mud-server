package com.prodog.gamemodule.fight.fightobj.entity;

import com.prodog.util.MsgTemplateUtil;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.HashMap;
import java.util.Map;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class EffectInfo {
    private String name;
    private String msg;
    private Map<String, Object> params = new HashMap<>();

    public String msg() {
        return MsgTemplateUtil.formatStr(msg, params);
    }
}
