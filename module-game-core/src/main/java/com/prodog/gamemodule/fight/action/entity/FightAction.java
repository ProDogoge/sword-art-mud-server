package com.prodog.gamemodule.fight.action.entity;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

;

/***
 * 战斗行为
 */
@Data
@NoArgsConstructor
@AllArgsConstructor
public class FightAction {
    private Integer type; //行为类型
    private String actionId;    //行为所用ID(如技能ID 道具ID)
    private Integer index;  //索引
    private boolean ignoreDead; //忽略自身死亡

    /***************行为类型******************/
    public static final Integer ACTION_TYPE_ATTACK = 1; //攻击
    public static final Integer ACTION_TYPE_DEFENSE = 2;    //防御
    public static final Integer ACTION_TYPE_USE_ITEM = 3;   //使用道具
    public static final Integer ACTION_TYPE_WAIT = 4;   //等待
    public static final Integer ACTION_TYPE_ESCAPE = 5; //逃跑

    public FightAction(Integer type, String actionId, Integer index) {
        this.type = type;
        this.actionId = actionId;
        this.index = index;
    }

    public static FightAction attack(String id, Integer index) {
        return new FightAction(ACTION_TYPE_ATTACK, id, index);
    }

    public static FightAction defense() {
        return new FightAction(ACTION_TYPE_DEFENSE, null, null);
    }

    public static FightAction useItem(String id, Integer index) {
        return new FightAction(ACTION_TYPE_USE_ITEM, id, index);
    }

    public static FightAction waited() {
        return new FightAction(ACTION_TYPE_WAIT, null, null);
    }
}
