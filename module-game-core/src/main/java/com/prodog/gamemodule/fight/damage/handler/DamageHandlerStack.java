package com.prodog.gamemodule.fight.damage.handler;

import com.prodog.gamemodule.fight.action.entity.AtkActionRes;
import com.prodog.gamemodule.fight.damage.handler.impl.*;
import com.prodog.gamemodule.fight.fightobj.entity.FightObj;

import java.util.ArrayList;
import java.util.List;

public class DamageHandlerStack {
    private List<DamageHandler> handlers;
    private FightObj obj;
    private FightObj beActedObj;
    private AtkActionRes atkActionRes;

    public DamageHandlerStack(FightObj obj, FightObj beActedObj, AtkActionRes res) {
        this.handlers = new ArrayList<>();
        this.obj = obj;
        this.beActedObj = beActedObj;
        this.atkActionRes = res;
        this.init();
    }

    private void init() {
        handlers.add(new NormalDamageHandler(obj, beActedObj, atkActionRes));
        handlers.add(new AdditionalDamageHandler(obj, beActedObj, atkActionRes));
        handlers.add(new ElementalDamageHandler(obj, beActedObj, atkActionRes));
        handlers.add(new CriticalDamageHandler(obj, beActedObj, atkActionRes));
        handlers.add(new DamageAbsoreHandler(obj, beActedObj, atkActionRes));
    }

    public AtkActionRes handle() {
        for (DamageHandler handler : handlers) {
            handler.handle();
        }
        return atkActionRes;
    }
}
