package com.prodog.gamemodule.fight.ai.impl;

import cn.hutool.core.util.RandomUtil;
import com.prodog.gamemodule.fight.action.util.ActionUtil;
import com.prodog.gamemodule.fight.ai.FightAI;
import com.prodog.gamemodule.fight.fightobj.entity.FightObj;
import com.prodog.gamemodule.fight.fightobj.entity.FightSkill;
import com.prodog.gamemodule.fight.session.FightSession;
import com.prodog.gamemodule.fight.util.FightUtil;
import lombok.Data;

import java.util.Collections;
import java.util.List;

@Data
public class NormalFightAI implements FightAI {

    @Override
    public void autoFight(FightSession session, FightObj obj) {
        //获取当前所有可用技能 随机释放技能
        List<FightSkill> enableSkills = FightUtil.getEnableSkills(obj);
        Collections.shuffle(enableSkills);
        if (enableSkills.size() == 0) {
            ActionUtil.setDefenseAction(session, obj.getUuid());
            return;
        }
        //获取敌方未死亡单位 随机获取攻击敌方单位索引
        List<FightObj> enemys = FightUtil.getEnemys(session, obj);
        List<FightObj> livingEnemys = FightUtil.getAliveEnemys(session, obj);
        if (livingEnemys.size() != 0) {
            Integer randomIndex = RandomUtil.randomInt(0, livingEnemys.size());
            Integer attackIndex = enemys.indexOf(livingEnemys.get(randomIndex));
            //设置行为
            ActionUtil.setSkillAction(session, obj.getUuid(), enableSkills.get(0).getId(), attackIndex);
            return;
        }
        ActionUtil.setDefenseAction(session, obj.getUuid());
    }
}
