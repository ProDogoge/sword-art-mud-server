package com.prodog.gamemodule.fight.round.entity;

import lombok.Data;

@Data
public class RoundHpInfo {
    private String name;
    private long value;

    public String info() {
        return name + ": " + (value > 0 ? "+" : "-") + value;
    }
}
