package com.prodog.gamemodule.fight.effect.handler.impl;

import com.prodog.gamemodule.fight.action.entity.AtkActionRes;
import com.prodog.gamemodule.fight.effect.entity.FightEffect;
import com.prodog.gamemodule.fight.fightobj.entity.FightObj;
import com.prodog.gamemodule.fight.fightobj.entity.FightSkill;
import com.prodog.gamemodule.fight.session.FightSession;
import com.prodog.gamemodule.fight.util.FightUtil;
import com.prodog.utils.RandomUtil;

import java.util.Arrays;
import java.util.HashMap;
import java.util.List;

/***
 * 雷神之息被动效果
 */
public class ThunderEffectHandler extends DefaultEffectHandler {
    public ThunderEffectHandler(FightSession session, FightEffect effect) {
        super(session, effect);
    }


    @Override
    public void onFightStart(FightObj obj) {
        FightSkill normalAtk = FightUtil.getNormalAttack(obj);
        normalAtk.setTargetNums((Integer) getParams().get("targetNums"));
        normalAtk.setName("雷痕剑击");
        normalAtk.getElemental().add("light");
        Integer percent = (Integer) normalAtk.getProp().get("physicalAttackPercent");
        normalAtk.getProp().put("physicalAttackPercent", (int) (percent * (100 + (int) (getParams().get("upgradePercent"))) * 0.01));
    }

    @Override
    public void afterExecuteDamage(FightObj obj, FightObj beActedObj, List<FightObj> beActedObjs, AtkActionRes res) {
        if (obj.getAction().getActionId().equals("1")) {
            if (beActedObj.getState() != 2) {
                Integer paralysisRate = (Integer) getParams().get("paralysisRate");
                if (RandomUtil.percentTest(paralysisRate)) {
                    Integer paralysisRound = (Integer) getParams().get("paralysisRound");
                    FightEffect paralysis = beActedObj.getEffects().stream().filter(e -> e.getName().equals("paralysis")).findAny().orElse(null);
                    if (paralysis != null) {
                        if (paralysis.getRound() < paralysisRound) {
                            paralysis.setRound(paralysisRound);
                        }
                    } else {
                        paralysis = new FightEffect("paralysis", paralysisRound, new HashMap<>(), "skill_" + getSkillId() + "paralysis", Arrays.asList("control", "abnormal"));
                        beActedObj.getEffects().add(paralysis);
                    }
                    getSession().getObjInfo().addActEffectMsg(obj.getUuid(), beActedObj.getUuid(), "paralysis", "麻痹了");
                }
            }
        }
    }

}
