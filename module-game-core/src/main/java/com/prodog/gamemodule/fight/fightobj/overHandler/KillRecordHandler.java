package com.prodog.gamemodule.fight.fightobj.overHandler;

import com.prodog.gamemodule.fight.fightobj.entity.FightObj;
import com.prodog.gamemodule.fight.session.FightSession;
import com.prodog.gamemodule.fight.util.FightUtil;
import com.prodog.gamemodule.kill.entity.KillRecord;
import com.prodog.gamemodule.kill.wrapper.KillRecordWrapper;
import com.prodog.utils.interfaces.AutowiredBean;
import org.springframework.beans.factory.annotation.Autowired;

import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

/***
 * 击杀记录
 */
public class KillRecordHandler implements FightOverHandler, AutowiredBean {
    @Autowired
    private KillRecordWrapper recordWrapper;

    public KillRecordHandler() {
        this.autowired();
    }

    @Override
    public void handle(FightSession session, StringBuilder msg) {
        List<FightObj> enemys = FightUtil.getEnemys(session, session.getVictory());
        List<FightObj> ours = FightUtil.getTeams(session, session.getVictory()).stream()
                .filter(o -> o.getObjType() == 1).collect(Collectors.toList());
        for (FightObj our : ours) {
            KillRecord killRecord = Optional.ofNullable(recordWrapper.getById(our.getId())).orElse(new KillRecord(our.getId()));
            for (FightObj enemy : enemys) {
                killRecord.addRecord(enemy.getId(), enemy.getObjType());
            }
            recordWrapper.save(killRecord);
        }
    }
}
