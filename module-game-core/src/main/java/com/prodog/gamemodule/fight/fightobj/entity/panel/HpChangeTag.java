package com.prodog.gamemodule.fight.fightobj.entity.panel;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class HpChangeTag {
    private String tag;
    private long value;

    public String tagVal() {
        return tag + ": " + (value > 0 ? "+" : "") + value;
    }
}
