package com.prodog.gamemodule.fight.effect.handler.impl;

import com.prodog.commonmodule.prop.caculate.util.PropUtil;
import com.prodog.commonmodule.prop.entity.ObjCaculaterProp;
import com.prodog.commonmodule.prop.entity.ObjProp;
import com.prodog.gamemodule.fight.effect.entity.FightEffect;
import com.prodog.gamemodule.fight.session.FightSession;
import com.prodog.utils.bean.BeanUtil;

public class PercentPropHandler extends DefaultEffectHandler {
    public PercentPropHandler(FightSession session, FightEffect effect) {
        super(session, effect);
    }

    @Override
    public ObjCaculaterProp caculateProp(ObjCaculaterProp caculaterProp) {
        ObjProp upProp = BeanUtil.bean2Bean(get("prop"), ObjProp.class);
        PropUtil.upgradeProp(caculaterProp.getAbsoluteProp(), caculaterProp.getResultProp(), upProp);
        return caculaterProp;
    }
}
