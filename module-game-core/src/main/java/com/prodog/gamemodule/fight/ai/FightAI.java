package com.prodog.gamemodule.fight.ai;

import com.prodog.gamemodule.fight.fightobj.entity.FightObj;
import com.prodog.gamemodule.fight.session.FightSession;

;

public interface FightAI {
     void autoFight(FightSession session, FightObj obj);
}
