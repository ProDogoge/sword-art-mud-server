package com.prodog.gamemodule.fight.fightobj.entity;

import com.prodog.gamemodule.fight.effect.entity.FightEffect;
import lombok.Data;

import java.util.*;


@Data
public class FightSkill {
    private String id;
    private String name;
    private String tag = "技能";
    private boolean normal;
    private Integer damageType;
    private int affectType = 1; //作用类型(1伤害 2恢复HP)
    private Integer level;
    private int consumeHp;  //消耗hp
    private int consumeMp;  //消耗mp
    private int coolTime;   //冷却时间
    private String expression;
    private boolean ignoreMyDead;   //无视自身死亡
    private boolean ignoreDead; //无视死亡单位(除自身)
    private boolean affectTeam; //作用于己方
    private int skillType = 1;  //技能类型(1攻击 2非攻击 3纯被动技能)
    private Set<String> elemental = new LinkedHashSet<>();  //属性攻击
    private int targetNums = 1; //攻击目标数
    private boolean ignoreObjElemental; //无视释放者本身的属性攻击(即技能不吃属性攻击)
    private Map<String, Object> prop;
    private List<FightEffect> effects = new ArrayList<>();   //技能效果
}
