package com.prodog.gamemodule.fight.service.impl;

import cn.hutool.core.util.IdUtil;
import cn.hutool.core.util.RandomUtil;
import com.prodog.command.annonations.Service;
import com.prodog.command.entity.CommandResult;
import com.prodog.command.entity.RoleGameCommand;
import com.prodog.gamemodule.config.BaseConfig;
import com.prodog.gamemodule.config.dict.DictConfig;
import com.prodog.gamemodule.fight.action.entity.FightActionSetRes;
import com.prodog.gamemodule.fight.action.util.ActionUtil;
import com.prodog.gamemodule.fight.fightobj.builder.FightObjBuilder;
import com.prodog.gamemodule.fight.fightobj.entity.FightObj;
import com.prodog.gamemodule.fight.fightobj.entity.FightSkill;
import com.prodog.gamemodule.fight.fightobj.factory.FightObjBuilderFactory;
import com.prodog.gamemodule.fight.service.FightService;
import com.prodog.gamemodule.fight.session.FightSession;
import com.prodog.gamemodule.fight.session.FightSessionManager;
import com.prodog.gamemodule.fight.util.FightUtil;
import com.prodog.gamemodule.fight.wrapper.FightSessionWrapper;
import com.prodog.gamemodule.gameworld.map.wrapper.GameMapWrapper;
import com.prodog.gamemodule.gameworld.monster.entity.Monster;
import com.prodog.gamemodule.gameworld.monster.wrapper.MonsterWrapper;
import com.prodog.gamemodule.item.entity.PropItem;
import com.prodog.gamemodule.item.wrapper.PropWrapper;
import com.prodog.gamemodule.skill.entity.Skill;
import com.prodog.gamemodule.skill.wrapper.SkillWrapper;
import com.prodog.gamemodule.team.entity.PlayerTeam;
import com.prodog.gamemodule.team.util.TeamUtil;
import com.prodog.gamemodule.team.wrapper.PlayerTeamWrapper;
import com.prodog.usermodule.role.constant.RoleState;
import com.prodog.usermodule.role.entity.RoleInfo;
import com.prodog.usermodule.role.entity.RoleSkill;
import com.prodog.usermodule.role.util.BagUtil;
import com.prodog.usermodule.role.util.RoleInfoUtil;
import com.prodog.usermodule.role.wrapper.RoleInfoWrapper;
import com.prodog.usermodule.role.wrapper.RoleSkillWrapper;
import com.prodog.utils.exception.CommonException;
import com.sun.istack.internal.NotNull;
import lombok.RequiredArgsConstructor;
import org.springframework.transaction.annotation.Transactional;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;
import java.util.stream.Collectors;

@Service
@RequiredArgsConstructor
public class FightServiceImpl implements FightService {
    private final RoleInfoWrapper roleInfoWrapper;
    private final GameMapWrapper gameMapWrapper;
    private final MonsterWrapper monsterWrapper;
    private final FightSessionWrapper fightSessionWrapper;
    private final SkillWrapper skillWrapper;
    private final RoleSkillWrapper roleSkillWrapper;
    private final PropWrapper propWrapper;
    private final PlayerTeamWrapper teamWrapper;

    private final BaseConfig baseConfig;
    private final DictConfig dictConfig;

    public void consumeEnergy(RoleInfo roleInfo, List<RoleInfo> roles, long energy) throws CommonException {
        String prefix = RoleInfoUtil.getPrefix(roleInfo);
        List<RoleInfo> cannots = roles.stream().filter(r -> RoleInfoUtil.getEnergy(r) < energy).collect(Collectors.toList());
        if (cannots.size() > 0) {
            if (roles.size() == 1) {
                throw new CommonException(prefix + "进入战斗失败，您的" + dictConfig.get("energy") + "不足" + energy + "点。");
            } else {
                String names = cannots.stream().map(RoleInfo::getRoleName).collect(Collectors.joining("、"));
                throw new CommonException(prefix + "进入战斗失败，" + names + dictConfig.get("energy") + "不足" + energy + "点。");
            }
        }
        for (RoleInfo role : roles) {
            role.setEnergy(RoleInfoUtil.getEnergy(roleInfo) - energy);
            roleInfoWrapper.save(role);
        }
    }

    @Override
    @Transactional(rollbackFor = Exception.class)
    public CommandResult fight(RoleGameCommand cmd) throws CommonException {
        //获取队伍中的人和地图遇到的怪物(数量为(队伍人数-1)*2 ~ 队伍人数*2 最少1个)
        List<RoleInfo> roleInfos = TeamUtil.teamCanFightRoles(cmd.getRoleInfo());
        consumeEnergy(cmd.getRoleInfo(), roleInfos, baseConfig.getNormalFightEnergy());
        checkIsCaptain(cmd);
        int monsterNum = RandomUtil.randomInt((roleInfos.size() - 1) * 2, roleInfos.size() * 2 + 1);
        monsterNum = monsterNum == 0 ? 1 : monsterNum;
        List<Monster> monsters = monsterWrapper.getByIds(gameMapWrapper.getById(cmd.getRoleInfo().getMapId()).getMonsters());
        List<Monster> fightMonsters = new ArrayList<>();
        for (int i = 0; i < monsterNum; i++) {
            Collections.shuffle(monsters);
            fightMonsters.add(monsters.get(0));
        }

        FightObjBuilderFactory factory = new FightObjBuilderFactory();
        //封装为战斗对象 存入战斗session
        FightObjBuilder roleInfoBuilder = factory.getBuilder(RoleInfo.class);
        List<FightObj> fightObjs = new ArrayList<>();
        roleInfos.forEach(roleInfo -> {
            FightObj fightObj = roleInfoBuilder.build(roleInfo);
            roleInfo.setFightUuid(fightObj.getUuid());
            fightObjs.add(fightObj);
        });

        FightObjBuilder monsterBuilder = factory.getBuilder(Monster.class);
        fightMonsters.forEach(monster -> fightObjs.add(monsterBuilder.build(monster)));

        //战斗session存入战斗会话管理器
        FightSession session = new FightSession(fightObjs);
        session.setId(IdUtil.simpleUUID());
        fightObjs.forEach(obj -> obj.setSessionId(session.getId()));
        Arrays.asList("victory", "killRecord", "expDrop", "setCurHpMpState").forEach(h -> session.getOverHandlers().add(h));
        new Thread(() -> fightSessionWrapper.save(session)).start();
        FightSessionManager.put(session.getId(), session);

        //修改玩家状态和战斗会话Id 保存
        roleInfos.forEach(roleInfo -> {
            roleInfo.setFightSessionId(session.getId());
            roleInfo.setState(RoleState.FIGHT);
            roleInfoWrapper.save(roleInfo);
        });
        //返回战斗进入成功
        return CommandResult.success("成功进入战斗！战斗信息如下：\n" + FightUtil.fightInfo(session, roleInfos.get(0).getFightUuid()));
    }

    //检查是否队长
    private void checkIsCaptain(RoleGameCommand cmd) throws CommonException {
        String prefix = RoleInfoUtil.getPrefix(cmd.getRoleInfo());
        PlayerTeam team = teamWrapper.getById(cmd.getRoleInfo().getTeamId());
        if (team != null) {
            CommonException.bySupplier(() -> !team.getCaptainId().equals(cmd.getRoleId()), prefix + "您不是当前队伍的队长，无法自行进入战斗。");
        }
    }

    @Override
    public CommandResult attack(RoleGameCommand cmd, Integer index) throws CommonException {
        if (cmd.getRoleInfo().getState() != RoleState.FIGHT) {
            CommandResult res = fight(cmd);
            if (res.getCode() != 200) {
                return res;
            }
        }
        String prefix = RoleInfoUtil.getPrefix(cmd.getRoleInfo());
        cmd.setRoleInfo(roleInfoWrapper.getById(cmd.getRoleId()));
        //获取session
        FightSession session = FightSessionManager.get(cmd.getRoleInfo().getFightSessionId());
        FightObj obj = session.getObjsByUuid().get(cmd.getRoleInfo().getFightUuid());

        FightSkill normalAttack = obj.getSkills().values().stream().filter(FightSkill::isNormal).findAny().get();

        index = getOrDefaultIndex(cmd, session, index, normalAttack.isAffectTeam());

        FightActionSetRes fightActionSetRes = ActionUtil.setAttackAction(session, obj.getUuid(), index - 1);
        if (!fightActionSetRes.isSuccess()) {
            return CommandResult.failure(RoleInfoUtil.getPrefix(cmd.getRoleInfo()) + fightActionSetRes.getMsg());
        }
        if (session.checkReadyOver()) {
            return CommandResult.success(session.run(obj.getUuid()));
        } else {
            return CommandResult.success(prefix + "操作成功！请等待其它队友进行本回合操作。");
        }
    }

    @NotNull
    private Integer getOrDefaultIndex(RoleGameCommand cmd, FightSession session, Integer index, boolean affectTeam) {
        if (index == null) {
            if (affectTeam) {
                FightObj me = session.getObjsByUuid().get(cmd.getRoleInfo().getFightUuid());
                index = FightUtil.getTeams(session, me).indexOf(me) + 1;
            } else {
                index = FightUtil.getFirstEnemyIndex(session, cmd.getRoleInfo().getFightUuid());
            }
        }
        return index;
    }

    @Override
    public CommandResult release(RoleGameCommand command, String skillName, Integer index) throws CommonException {
        if (command.getRoleInfo().getState() != RoleState.FIGHT) {
            CommandResult res = fight(command);
            if (res.getCode() != 200) {
                return res;
            }
        }
        String prefix = RoleInfoUtil.getPrefix(command.getRoleInfo());
        command.setRoleInfo(roleInfoWrapper.getById(command.getRoleId()));
        //判断技能是否存在
        Skill skill = skillWrapper.getByColumn("name", skillName);
        CommonException.byNull(skill, RoleInfoUtil.getPrefix(command.getRoleInfo()) + "释放失败，该技能不存在。");
        //判断学习等级是否已到顶点
        RoleSkill roleSkill = roleSkillWrapper.getById(command.getRoleId());
        CommonException.byNull(roleSkill.getLearnedSkill().get(skill.getId()), RoleInfoUtil.getPrefix(command.getRoleInfo()) + "释放失败，您没有学习该技能。");

        //获取session
        FightSession session = FightSessionManager.get(command.getRoleInfo().getFightSessionId());
        FightObj obj = session.getObjsByUuid().get(command.getRoleInfo().getFightUuid());
        index = getOrDefaultIndex(command, session, index, skill.isAffectTeam());
        FightActionSetRes fightActionSetRes = ActionUtil.setSkillAction(session, obj.getUuid(), skill.getId(), index - 1);
        if (!fightActionSetRes.isSuccess()) {
            return CommandResult.failure(RoleInfoUtil.getPrefix(command.getRoleInfo()) + fightActionSetRes.getMsg());
        }
        if (session.checkReadyOver()) {
            return CommandResult.success(session.run(obj.getUuid()));
        } else {
            return CommandResult.success(prefix + "操作成功！请等待其它队友进行本回合操作。");
        }
    }

    @Override
    public CommandResult useItem(RoleGameCommand command, String itemName, Integer index) throws CommonException {
        if (command.getRoleInfo().getState() != RoleState.FIGHT) {
            CommandResult res = fight(command);
            if (res.getCode() != 200) {
                return res;
            }
        }
        command.setRoleInfo(roleInfoWrapper.getById(command.getRoleId()));

        PropItem prop = propWrapper.getByColumn("name", itemName);
        String prefix = RoleInfoUtil.getPrefix(command.getRoleInfo());
        CommonException.byNull(prop, prefix + "使用失败，该道具不存在。");
        CommonException.bySupplier(() -> !BagUtil.checkIsHad(command.getRoleId(), prop.getId(), 1), prefix + "使用失败，您背包中没有[" + itemName + "]。");

        //获取session
        FightSession session = FightSessionManager.get(command.getRoleInfo().getFightSessionId());
        FightObj obj = session.getObjsByUuid().get(command.getRoleInfo().getFightUuid());
        index = getOrDefaultIndex(command, session, index, prop.isAffectTeam());
        FightActionSetRes fightActionSetRes = ActionUtil.setUseItemAction(session, obj.getUuid(), prop.getId(), index - 1);
        if (!fightActionSetRes.isSuccess()) {
            return CommandResult.failure(RoleInfoUtil.getPrefix(command.getRoleInfo()) + fightActionSetRes.getMsg());
        }
        if (session.checkReadyOver()) {
            return CommandResult.success(session.run(obj.getUuid()));
        } else {
            return CommandResult.success(prefix + "操作成功！请等待其它队友进行本回合操作。");
        }
    }

    @Override
    public CommandResult defense(RoleGameCommand command) throws CommonException {
        if (command.getRoleInfo().getState() != RoleState.FIGHT) {
            CommandResult res = fight(command);
            if (res.getCode() != 200) {
                return res;
            }
        }
        String prefix = RoleInfoUtil.getPrefix(command.getRoleInfo());
        command.setRoleInfo(roleInfoWrapper.getById(command.getRoleId()));

        FightSession session = FightSessionManager.get(command.getRoleInfo().getFightSessionId());
        FightActionSetRes fightActionSetRes = ActionUtil.setDefenseAction(session, command.getRoleInfo().getFightUuid());
        if (!fightActionSetRes.isSuccess()) {
            return CommandResult.failure(RoleInfoUtil.getPrefix(command.getRoleInfo()) + fightActionSetRes.getMsg());
        }
        if (session.checkReadyOver()) {
            return CommandResult.success(session.run(command.getRoleInfo().getFightUuid()));
        } else {
            return CommandResult.success(prefix + "操作成功！请等待其它队友进行本回合操作。");
        }
    }
}
