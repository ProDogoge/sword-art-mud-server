package com.prodog.gamemodule.fight.damage.entity;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class ElementalProp {
    private String elemental;
    private Integer value;
}
