package com.prodog.gamemodule.fight.damage.handler.impl;

import com.prodog.commonmodule.prop.entity.ObjProp;
import com.prodog.gamemodule.fight.damage.entity.Damage;
import com.prodog.gamemodule.fight.action.entity.AtkActionRes;
import com.prodog.gamemodule.fight.damage.handler.DamageHandler;
import com.prodog.gamemodule.fight.damage.util.DamageUtil;
import com.prodog.gamemodule.fight.fightobj.entity.FightObj;
import com.prodog.gamemodule.fight.fightobj.entity.FightSkill;

/***
 * 基础伤害处理器
 */
public class NormalDamageHandler implements DamageHandler {
    private FightObj obj;
    private FightObj beActedObj;
    private AtkActionRes atkActionRes;

    public NormalDamageHandler(FightObj obj, FightObj beActedObj, AtkActionRes atkActionRes) {
        this.obj = obj;
        this.beActedObj = beActedObj;
        this.atkActionRes = atkActionRes;
    }

    @Override
    public void handle() {
        FightSkill skill = obj.getSkills().get(obj.getAction().getActionId());
        ObjProp prop = obj.getFightProp();
        ObjProp beActedProp = beActedObj.getFightProp();
        long damage = DamageUtil.getSkillDamage(obj, skill);
        //通过穿透和地方的减伤算出基础伤害
        if (skill.getAffectType() == 1) {
            long defense = skill.getDamageType() == 1 ? beActedProp.getPhysicalDefense() : (skill.getDamageType() == 2 ? beActedProp.getMagicalDefense() : 0);
            defense = beActedObj.getAction().getType() == 2 ? (long) (defense * 1.5) : defense;
            //穿透
            long through = skill.getDamageType() == 1 ? prop.getPhysicalThrough() : (skill.getDamageType() == 2 ? prop.getMagicalThrough() : 0);
            long throughRate = skill.getDamageType() == 1 ? prop.getPhysicalThroughRate() : (skill.getDamageType() == 2 ? prop.getMagicalThroughRate() : 0);
            defense -= through;
            defense *= (100 - throughRate) * 0.01;
            defense = defense < 0 ? 0 : defense;
            damage -= defense;
            damage -= beActedProp.getReduceDamage();
            damage *= ((double) (100 - beActedProp.getReduceDamageRate()) * 0.01);
            damage = damage < 0 ? 0 : damage;
        }
        long finalDamage = damage;
        atkActionRes.addDamage(beActedObj.getUuid(), new Damage() {{
            setByUuid(obj.getUuid());
            setDamageType(skill.getDamageType());
            setName(Damage.NORMAL);
            setAffectType(skill.getAffectType());
            setValue(finalDamage);
        }});
    }
}
