package com.prodog.gamemodule.fight.session;

import com.prodog.gamemodule.fight.wrapper.FightSessionWrapper;
import com.prodog.utils.bean.SpringBeanUtils;

import java.util.Map;
import java.util.Optional;
import java.util.concurrent.ConcurrentHashMap;

public class FightSessionManager {
    private static Map<String, FightSession> sessions = new ConcurrentHashMap<>();

    public static void put(String sessionId, FightSession session) {
        sessions.put(sessionId, session);
    }

    public static void add(FightSession session) {
        sessions.put(session.getId(), session);
    }

    public static FightSession get(String sessionId) {
        return Optional.ofNullable(sessions.get(sessionId))
                .orElseGet(() -> {
                    FightSession session = SpringBeanUtils.getBean(FightSessionWrapper.class).getById(sessionId);
                    put(session.getId(), session);
                    return session;
                });
    }

    public static void remove(String sessionId) {
        sessions.remove(sessionId);
        new Thread(() -> SpringBeanUtils.getBean(FightSessionWrapper.class).removeById(sessionId)).start();
    }
}
