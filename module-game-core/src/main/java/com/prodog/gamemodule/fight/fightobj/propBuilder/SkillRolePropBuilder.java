package com.prodog.gamemodule.fight.fightobj.propBuilder;

import com.prodog.gamemodule.fight.fightobj.entity.FightObj;
import com.prodog.gamemodule.fight.fightobj.entity.FightSkill;
import com.prodog.gamemodule.fight.util.FightUtil;
import com.prodog.gamemodule.skill.entity.Skill;
import com.prodog.gamemodule.skill.wrapper.SkillWrapper;
import com.prodog.usermodule.role.entity.RoleInfo;
import com.prodog.usermodule.role.entity.RoleSkill;
import com.prodog.usermodule.role.wrapper.RoleSkillWrapper;
import com.prodog.utils.interfaces.AutowiredBean;
import org.springframework.beans.factory.annotation.Autowired;

public class SkillRolePropBuilder implements PropBuilder<RoleInfo>, AutowiredBean {
    @Autowired
    private RoleSkillWrapper roleSkillWrapper;
    @Autowired
    private SkillWrapper skillWrapper;

    public SkillRolePropBuilder() {
        this.autowired();
    }

    @Override
    public FightObj build(FightObj obj, RoleInfo info) {
        RoleSkill roleSkill = roleSkillWrapper.getById(info.getId());
        roleSkill.getLearnedSkill().forEach((skillId, level) -> {
            Skill skill = skillWrapper.getById(skillId);
            FightSkill fightSkill = FightUtil.getFightSkill(skill, level);
            obj.getSkills().put(fightSkill.getId(), fightSkill);
        });
        return obj;
    }
}
