package com.prodog.gamemodule.fight.effect.entity;

import com.alibaba.fastjson2.annotation.JSONField;
import com.prodog.gamemodule.fight.effect.handler.EffectHandler;
import com.prodog.gamemodule.fight.effect.manager.EffectHandlerManager;
import com.prodog.gamemodule.fight.session.FightSession;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.jetbrains.annotations.NotNull;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class FightEffect implements Comparable<FightEffect> {
    private String name;    //效果名称
    private Integer round;  //效果持续回合
    private Map<String, Object> params;  //效果参数
    private String id;  //标识
    private List<String> tags = new ArrayList<>();  //特征(如控制 持续伤害等)
    private int weight = 500; //权重

    public FightEffect(String name, Integer round, Map<String, Object> params, String id, List<String> tags) {
        this.name = name;
        this.round = round;
        this.params = params;
        this.id = id;
        this.tags = tags;
    }

    @Override
    public int compareTo(@NotNull FightEffect o) {
        return Integer.compare(o.getWeight(), weight);
    }

    @JSONField(serialize = false)
    public EffectHandler getHandler(FightSession fightSession) {
        try {
            return EffectHandlerManager.handlerMap.get(name).newInstance(fightSession, this);
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }
    }
}
