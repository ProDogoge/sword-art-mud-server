package com.prodog.gamemodule.fight.fightobj.builder;

import com.prodog.gamemodule.fight.fightobj.entity.FightObj;

;

public interface FightObjBuilder<T> {
    FightObj build(T obj);
}
