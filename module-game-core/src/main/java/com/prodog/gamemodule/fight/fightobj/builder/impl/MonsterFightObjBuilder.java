package com.prodog.gamemodule.fight.fightobj.builder.impl;

import cn.hutool.core.util.IdUtil;
import com.prodog.commonmodule.prop.caculate.getter.MonsterPropGetter;
import com.prodog.commonmodule.prop.entity.ObjProp;
import com.prodog.gamemodule.fight.fightobj.builder.FightObjBuilder;
import com.prodog.gamemodule.fight.fightobj.entity.FightObj;
import com.prodog.gamemodule.fight.fightobj.entity.FightSkill;
import com.prodog.gamemodule.fight.util.FightUtil;
import com.prodog.gamemodule.gameworld.monster.entity.Monster;
import com.prodog.gamemodule.gameworld.monster.entity.MonsterProp;
import com.prodog.gamemodule.gameworld.monster.entity.MonsterSkill;
import com.prodog.gamemodule.skill.entity.Skill;
import com.prodog.gamemodule.skill.wrapper.SkillWrapper;
import com.prodog.utils.bean.BeanUtil;
import com.prodog.utils.interfaces.AutowiredBean;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;

import java.util.Optional;

@Slf4j
public class MonsterFightObjBuilder implements FightObjBuilder<Monster>, AutowiredBean {
    @Autowired
    private SkillWrapper skillWrapper;

    public MonsterFightObjBuilder() {
        this.autowired();
    }

    @Override
    public FightObj build(Monster monster) {
        MonsterProp prop = monster.getProp();
        FightObj fightObj = new FightObj() {{
            setName(monster.getName());
            setPanelProp(new MonsterPropGetter().get(prop));
            setFightProp(BeanUtil.beanToBean(getPanelProp(), ObjProp.class));
            setUuid(IdUtil.simpleUUID());
            setFlag(1);
            setObjType(monster.getObjType());
            setId(monster.getId());
            getEffects().addAll(monster.getEffects());
            setFightAi(Optional.ofNullable(monster.getFightAi()).orElse("com.prodog.gamemodule.fight.ai.impl.NormalFightAI"));
        }};
        for (MonsterSkill monsterSkill : monster.getSkills()) {
            Skill skill = skillWrapper.getById(monsterSkill.getSkillId());
            FightSkill fightSkill = FightUtil.getFightSkill(skill, monsterSkill.getLevel());
            fightObj.getSkills().put(fightSkill.getId(), fightSkill);
            fightObj.getEffects().addAll(fightSkill.getEffects());
        }
        fightObj.getEffects().addAll(monster.getEffects());
        return fightObj;
    }
}
