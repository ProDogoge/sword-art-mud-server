package com.prodog.gamemodule.fight.fightobj.factory;

import cn.hutool.core.util.ReflectUtil;
import com.prodog.gamemodule.fight.fightobj.builder.FightObjBuilder;
import com.prodog.gamemodule.fight.fightobj.builder.impl.MonsterFightObjBuilder;
import com.prodog.gamemodule.fight.fightobj.builder.impl.RoleInfoFightObjBuilder;
import com.prodog.gamemodule.gameworld.monster.entity.Monster;
import com.prodog.usermodule.role.entity.RoleInfo;

import java.util.HashMap;
import java.util.Map;

public class FightObjBuilderFactory {

    private Map<Class, Class<? extends FightObjBuilder>> builderMap;

    public FightObjBuilderFactory() {
        this.init();
    }

    private void init() {
        this.builderMap = new HashMap() {{
            put(RoleInfo.class, RoleInfoFightObjBuilder.class);
            put(Monster.class, MonsterFightObjBuilder.class);
        }};
    }

    public FightObjBuilder getBuilder(Class clz, Map<String, Object> handlerParam) {
        Class builderClz = builderMap.get(clz);
        if (builderClz != null) {
            Object handler = ReflectUtil.newInstance(builderClz, handlerParam);
            return (FightObjBuilder) handler;
        }
        return null;
    }

    public FightObjBuilder getBuilder(Class clz) {
        Class builderClz = builderMap.get(clz);
        if (builderClz != null) {
            Object handler = ReflectUtil.newInstance(builderClz);
            return (FightObjBuilder) handler;
        }
        return null;
    }
}
