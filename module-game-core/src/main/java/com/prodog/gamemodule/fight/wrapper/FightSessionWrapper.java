package com.prodog.gamemodule.fight.wrapper;

import com.prodog.database.annotation.Wrapper;
import com.prodog.database.wrapper.MongoDataWrapper;
import com.prodog.gamemodule.fight.session.FightSession;

@Wrapper(path = "游戏数据/战斗/战斗会话", module = "战斗会话")
public class FightSessionWrapper extends MongoDataWrapper<FightSession, String> {
}
