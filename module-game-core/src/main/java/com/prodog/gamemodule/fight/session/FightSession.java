package com.prodog.gamemodule.fight.session;

import cn.hutool.core.util.ReflectUtil;
import com.prodog.gamemodule.fight.action.entity.FightAction;
import com.prodog.gamemodule.fight.action.handler.AttackHandler;
import com.prodog.gamemodule.fight.action.handler.UseItemHandler;
import com.prodog.gamemodule.fight.ai.FightAI;
import com.prodog.gamemodule.fight.effect.entity.FightEffect;
import com.prodog.gamemodule.fight.effect.manager.EffectHandlerManager;
import com.prodog.gamemodule.fight.fightobj.entity.FightObj;
import com.prodog.gamemodule.fight.fightobj.overHandler.FightOverHandlerManager;
import com.prodog.gamemodule.fight.round.handler.RoundHandler;
import com.prodog.gamemodule.fight.util.FightUtil;
import com.prodog.gamemodule.fight.wrapper.FightSessionWrapper;
import com.prodog.utils.bean.SpringBeanUtils;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.data.mongodb.core.mapping.Document;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

@Data
@NoArgsConstructor
@Document("g_fight_session")
public class FightSession {
    private String id;
    private Map<Object, FightObj> objsByUuid;   //所有单位
    private Map<String, FightObj> redObjs;  //红方单位
    private Map<String, FightObj> blueObjs; //蓝方单位
    private FightSessionObjInfo objInfo;
    private int victory = -1;
    private int round;  //当前回合
    private List<String> overHandlers = new ArrayList<>();

    public static final Map<Integer, String> coolTimePrefixMap = new HashMap<>();

    static {
        coolTimePrefixMap.put(FightAction.ACTION_TYPE_ATTACK, "SKILL:");
        coolTimePrefixMap.put(FightAction.ACTION_TYPE_USE_ITEM, "ITEM:");
    }

    public FightSession(List<FightObj> fightObjs) {
        this.objsByUuid = fightObjs.stream().collect(Collectors.toMap(FightObj::getUuid, o -> o));
        this.redObjs = fightObjs.stream().filter(obj -> obj.getFlag() == 0).collect(Collectors.toMap(FightObj::getUuid, o -> o));
        this.blueObjs = fightObjs.stream().filter(obj -> obj.getFlag() == 1).collect(Collectors.toMap(FightObj::getUuid, o -> o));
        nextRound();
        //触发入场时被动
        for (FightObj obj : objsByUuid.values()) {
            for (int i = 0; i < obj.getEffects().size(); i++) {
                FightEffect effect = obj.getEffects().get(i);
                EffectHandlerManager.onFightStart(this, effect, obj);
            }
        }
    }

    //检查所有单位是否已准备好
    public boolean checkReadyOver() {
        for (FightObj obj : objsByUuid.values()) {
            if (obj.getAction() == null && obj.getFightAi() != null) {
                FightAI fightAI = null;
                try {
                    fightAI = (FightAI) ReflectUtil.newInstance(Class.forName(obj.getFightAi()));
                } catch (ClassNotFoundException e) {
                    e.printStackTrace();
                    try {
                        fightAI = (FightAI) ReflectUtil.newInstance(Class.forName("com.prodog.gamemodule.fight.ai.impl.NormalFightAI"));
                    } catch (ClassNotFoundException ex) {
                        ex.printStackTrace();
                    }
                }
                fightAI.autoFight(this, obj);
            }
        }
        return objsByUuid.values().stream().noneMatch(obj -> obj.getAction() == null);
    }

    public void fight(StringBuilder msg) {
        if (!checkReadyOver()) return;
        getObjInfo().setRoundHead("");

        //回合开始处理器
        new RoundHandler(this).roundStart();

        List<FightObj> objs;
        //按照敏捷度排序后按顺序进行行为处理
        do {
            objs = FightUtil.orderBySpped(objsByUuid);
            if (objs.size() > 0) {
                FightObj obj = objs.get(0);
                for (FightEffect effect : obj.getEffects()) {
                    EffectHandlerManager.caculateFightProp(this, obj);
                    EffectHandlerManager.beforeAction(this, effect, obj);
                }
                if (obj.getAction() == null) continue;
                switch (obj.getAction().getType()) {
                    case 1:
                        new AttackHandler(this, obj).action();
                        break;
                    case 2:
                        obj.setOver(true);
                        break;
                    case 3:
                        new UseItemHandler(this, obj).action();
                        break;
                }
                /*if (obj.getAction().getType() == 1) {
                    new AttackHandler(this, obj).action();
                }*/
                for (FightEffect effect : obj.getEffects()) {
                    EffectHandlerManager.afterAction(this, effect, obj);
                }
            }
        } while (objs.size() > 0);
        //回合结束处理器
        new RoundHandler(this).roundOver();
        msg.append(getObjInfo().getInfo());
        //检查并处理获胜
        boolean over = checkOver();
        if (over) return;
        //下一个回合
        nextRound();
        //如果所有单位设置好行为 直接运算下一回合作战
        if (checkReadyOver()) fight(msg);
    }

    private boolean checkOver() {
        victory = FightUtil.getVictory(this);
        if (victory != -1) {
            return true;
        }
        return false;
    }

    private void nextRound() {
        //清除本回合动作
        objsByUuid.values().forEach(obj -> obj.setAction(null));
        objsByUuid.values().forEach(obj -> obj.setOver(false));
        round++;
    }

    public String run(String uuid) {
        StringBuilder msg = new StringBuilder("战斗信息［" + round + "］:");
        objInfo = new FightSessionObjInfo();
        this.fight(msg);
        if (uuid != null) {
            msg.append(FightUtil.fightInfo(this, uuid));
        } else {
            msg.append(FightUtil.fightInfo(this));
        }
        objsByUuid.values().forEach(obj -> obj.getPanelExpansion().clearAllTags());
        FightSessionWrapper wrapper = SpringBeanUtils.getBean(FightSessionWrapper.class);
        if (checkOver()) {
            FightOverHandlerManager.get(overHandlers).forEach(h -> h.handle(this, msg));
            FightSessionManager.remove(getId());
        } else {
            new Thread(() -> wrapper.save(this)).start();
        }
        return msg.toString();
    }
}
