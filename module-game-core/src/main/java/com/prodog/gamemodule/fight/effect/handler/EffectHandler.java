package com.prodog.gamemodule.fight.effect.handler;

import com.prodog.commonmodule.prop.entity.ObjCaculaterProp;
import com.prodog.gamemodule.fight.action.entity.AtkActionRes;
import com.prodog.gamemodule.fight.fightobj.entity.FightObj;

import java.util.List;

/***
 * 效果处理器
 */
public interface EffectHandler {
    /***
     * 战斗开始时
     * @param obj
     */
    void onFightStart(FightObj obj);

    /***
     * 回合开始时
     * @param obj
     */
    void onRoundStart(FightObj obj);

    /***
     * 行为开始前
     * @param obj
     */
    void beforeAction(FightObj obj);

    /***
     * 计算伤害前
     * @param actObj
     * @param beActedObj
     * @param beActedObjs
     * @param res
     */
    void beforeCaculateDamage(FightObj actObj, FightObj beActedObj, List<FightObj> beActedObjs, AtkActionRes res);

    /***
     * 被计算伤害前
     * @param actObj
     * @param beActedObj
     * @param beActedObjs
     * @param res
     */
    void beforeBeCaculatedDamage(FightObj actObj, FightObj beActedObj, List<FightObj> beActedObjs, AtkActionRes res);

    /***
     * 计算伤害后
     * @param actObj
     * @param beActedObj
     * @param beActedObjs
     * @param res
     */
    void afterCaculateDamage(FightObj actObj, FightObj beActedObj, List<FightObj> beActedObjs, AtkActionRes res);

    /***
     * 被计算伤害后
     * @param actObj
     * @param beActedObj
     * @param beActedObjs
     * @param res
     */
    void afterBeCaculatedDamage(FightObj actObj, FightObj beActedObj, List<FightObj> beActedObjs, AtkActionRes res);

    /***
     * 伤害执行到敌方前
     * @param obj
     * @param beActedObj
     * @param beActedObjs
     * @param res
     */
    void beforeExecuteDamage(FightObj obj, FightObj beActedObj, List<FightObj> beActedObjs, AtkActionRes res);

    /***
     * 被敌方执行伤害前
     * @param actObj
     * @param beActedObj
     * @param beActedObjs
     * @param res
     */
    void BeforeBeExecutedDamage(FightObj actObj, FightObj beActedObj, List<FightObj> beActedObjs, AtkActionRes res);

    /***
     * 伤害执行到敌方后
     * @param actObj 执行方
     * @param beActedObj 被执行方
     */
    void afterExecuteDamage(FightObj actObj, FightObj beActedObj, List<FightObj> beActedObjs, AtkActionRes res);

    /***
     * 被敌方执行伤害后
     * @param actObj 执行方
     * @param beActedObj 被执行方
     */
    void afterBeExecutedDamage(FightObj actObj, FightObj beActedObj, List<FightObj> beActedObjs, AtkActionRes res);

    /***
     * 本回合结束前
     * @param obj
     */
    void beforeRoundOver(FightObj obj);

    /***
     * 行为结束后
     * @param obj
     */
    void afterAction(FightObj obj);

    /***
     * 效果销毁
     * @param obj
     */
    void destroy(FightObj obj);

    /***
     * 回合减一后
     * @param obj
     */
    void afterRoundDown(FightObj obj);

    /***
     * 伤害执行完毕
     * @param obj
     * @param beActedObjs
     * @param res
     */
    void executeDamagesOver(FightObj obj, List<FightObj> beActedObjs, AtkActionRes res);

    /***
     *
     * @param caculaterProp 计算属性
     */
    ObjCaculaterProp caculateProp(ObjCaculaterProp caculaterProp);

    /***
     * 计算战斗属性
     * @param obj
     */
    void caculateFightProp(FightObj obj);
}
