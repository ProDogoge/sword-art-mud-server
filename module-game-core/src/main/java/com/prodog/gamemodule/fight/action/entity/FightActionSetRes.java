package com.prodog.gamemodule.fight.action.entity;

import lombok.AllArgsConstructor;
import lombok.Data;

@Data
@AllArgsConstructor
public class FightActionSetRes {
    private boolean success;
    private String msg;

    public static FightActionSetRes success() {
        return new FightActionSetRes(true, null);
    }

    public static FightActionSetRes failure(String msg) {
        return new FightActionSetRes(false, msg);
    }
}
