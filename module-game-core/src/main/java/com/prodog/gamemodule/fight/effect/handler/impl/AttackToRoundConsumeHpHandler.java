package com.prodog.gamemodule.fight.effect.handler.impl;

import com.prodog.gamemodule.fight.action.entity.AtkActionRes;
import com.prodog.gamemodule.fight.damage.util.DamageUtil;
import com.prodog.gamemodule.fight.effect.constant.AbnormalTag;
import com.prodog.gamemodule.fight.effect.entity.FightEffect;
import com.prodog.gamemodule.fight.fightobj.entity.FightObj;
import com.prodog.gamemodule.fight.session.FightSession;
import com.prodog.gamemodule.fight.util.FightUtil;
import com.prodog.utils.RandomUtil;

import java.util.HashMap;
import java.util.List;

public class AttackToRoundConsumeHpHandler extends DefaultEffectHandler {
    public static final String NAME = "attackToRoundConsumeHp";

    public AttackToRoundConsumeHpHandler(FightSession session, FightEffect effect) {
        super(session, effect);
    }

    @Override
    public void afterExecuteDamage(FightObj actObj, FightObj beActedObj, List<FightObj> beActedObjs, AtkActionRes res) {
        if (checkIsCurrentSkill(actObj)) {
            int percent = (int) get("percent");
            if (RandomUtil.percentTest(FightUtil.getAbPercent(percent, actObj, beActedObj))) {
                int round = (int) get("round");
                String abSTag = (String) get("abSTag");
                String expression = (String) get("expression");
                expression = DamageUtil.preparedExpression(expression, new HashMap<String, Object>() {{
                    put("actObj", actObj);
                    put("beActedObj", beActedObj);
                }});
                long damage = Long.parseLong(String.valueOf(DamageUtil.execLongExpression(expression)));

                FightEffect effect = new FightEffect() {{
                    setId(getId() + "_roundConsumeHp");
                    setName(RoundConsumeHpHandler.NAME);
                    setTags(AbnormalTag.ROUND_CONSUME_HP);
                    setRound(round);
                    setParams(new HashMap<String, Object>() {{
                        put("abSTag", abSTag);
                        put("damage", damage);
                    }});
                }};
                beActedObj.getEffects().add(effect);
            }
        }
    }
}
