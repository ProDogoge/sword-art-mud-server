package com.prodog.gamemodule.fight.effect.handler.impl;

import com.prodog.gamemodule.fight.action.entity.AtkActionRes;
import com.prodog.gamemodule.fight.effect.constant.AbnormalTag;
import com.prodog.gamemodule.fight.effect.entity.FightEffect;
import com.prodog.gamemodule.fight.fightobj.entity.FightObj;
import com.prodog.gamemodule.fight.session.FightSession;
import com.prodog.gamemodule.fight.util.FightUtil;
import com.prodog.utils.RandomUtil;

import java.util.HashMap;
import java.util.List;

public class AttackToHoldingHandler extends DefaultEffectHandler {
    public static final String NAME = "attackToHolding";

    public AttackToHoldingHandler(FightSession session, FightEffect effect) {
        super(session, effect);
    }

    @Override
    public void afterExecuteDamage(FightObj actObj, FightObj beActedObj, List<FightObj> beActedObjs, AtkActionRes res) {
        if (checkIsCurrentSkill(actObj)) {
            int percent = (int) get("percent");
            boolean printActContent = (boolean) get("printActContent");
            if (RandomUtil.percentTest(FightUtil.getAbPercent(percent, actObj, beActedObj))) {
                int round = (int) get("round");
                String abSTag = (String) get("abSTag");
                FightEffect effect = new FightEffect() {{
                    setId(getId() + "_holding");
                    setName(HoldingHandler.NAME);
                    setTags(AbnormalTag.HOLDING);
                    setRound(round);
                    setParams(new HashMap<String, Object>() {{
                        put("abSTag", abSTag);
                        put("printActContent", printActContent);
                    }});
                }};
                beActedObj.getEffects().add(effect);
            }
        }
    }
}
