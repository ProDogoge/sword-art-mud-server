package com.prodog.gamemodule.fight.fightobj.propBuilder;

import cn.hutool.core.util.IdUtil;
import com.prodog.commonmodule.prop.caculate.getter.RolePropGetter;
import com.prodog.commonmodule.prop.entity.ObjProp;
import com.prodog.gamemodule.fight.fightobj.entity.FightObj;
import com.prodog.usermodule.role.entity.RoleInfo;
import com.prodog.utils.bean.BeanUtil;
import com.prodog.utils.bean.SpringBeanUtils;

public class BaseRolePropBuilder implements PropBuilder<RoleInfo> {
    @Override
    public FightObj build(FightObj obj, RoleInfo info) {
        return new FightObj() {{
            setName(info.getRoleName());
            setPanelProp(SpringBeanUtils.getBean(RolePropGetter.class).get(info));
            setFightProp(BeanUtil.beanToBean(getPanelProp(), ObjProp.class));
            setUuid(IdUtil.simpleUUID());
            setFlag(0);
            setObjType(1);
            setId(info.getId());
        }};
    }
}
