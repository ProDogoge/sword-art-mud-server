package com.prodog.gamemodule.fight.effect.handler.impl;

import com.prodog.commonmodule.prop.entity.ObjCaculaterProp;
import com.prodog.gamemodule.fight.action.entity.AtkActionRes;
import com.prodog.gamemodule.fight.effect.entity.FightEffect;
import com.prodog.gamemodule.fight.effect.handler.AbstractEffectHandler;
import com.prodog.gamemodule.fight.fightobj.entity.FightObj;
import com.prodog.gamemodule.fight.session.FightSession;

import java.util.List;

public class DefaultEffectHandler extends AbstractEffectHandler {
    public DefaultEffectHandler(FightSession session, FightEffect effect) {
        super(session, effect);
    }

    @Override
    public void onFightStart(FightObj obj) {

    }

    @Override
    public void onRoundStart(FightObj obj) {

    }

    @Override
    public void beforeAction(FightObj obj) {

    }

    @Override
    public void beforeCaculateDamage(FightObj actObj, FightObj beActedObj, List<FightObj> beActedObjs, AtkActionRes res) {

    }

    @Override
    public void beforeBeCaculatedDamage(FightObj actObj, FightObj beActedObj, List<FightObj> beActedObjs, AtkActionRes res) {

    }

    @Override
    public void afterCaculateDamage(FightObj actObj, FightObj beActedObj, List<FightObj> beActedObjs, AtkActionRes res) {

    }

    @Override
    public void afterBeCaculatedDamage(FightObj actObj, FightObj beActedObj, List<FightObj> beActedObjs, AtkActionRes res) {

    }

    @Override
    public void beforeExecuteDamage(FightObj obj, FightObj beActedObj, List<FightObj> beActedObjs, AtkActionRes res) {

    }

    @Override
    public void BeforeBeExecutedDamage(FightObj actObj, FightObj beActedObj, List<FightObj> beActedObjs, AtkActionRes res) {

    }

    @Override
    public void afterExecuteDamage(FightObj actObj, FightObj beActedObj, List<FightObj> beActedObjs, AtkActionRes res) {

    }

    @Override
    public void afterBeExecutedDamage(FightObj actObj, FightObj beActedObj, List<FightObj> beActedObjs, AtkActionRes res) {

    }

    @Override
    public void beforeRoundOver(FightObj obj) {

    }

    @Override
    public void afterAction(FightObj obj) {

    }

    @Override
    public void destroy(FightObj obj) {

    }

    @Override
    public void afterRoundDown(FightObj obj) {

    }

    @Override
    public void executeDamagesOver(FightObj obj, List<FightObj> beActedObjs, AtkActionRes res) {

    }

    @Override
    public ObjCaculaterProp caculateProp(ObjCaculaterProp caculaterProp) {
        return null;
    }

    @Override
    public void caculateFightProp(FightObj obj) {

    }
}
