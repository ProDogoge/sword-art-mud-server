package com.prodog.gamemodule.fight.fightobj.entity;

import cn.hutool.core.collection.CollUtil;
import lombok.Data;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.stream.Collectors;

/***
 * 战斗对象动作信息
 */
@Data
public class FightObjActInfo {
    private long hpAbsore;   //hp吸收
    private long mpAbsore;   //mp吸收
    private String prefix = "";  //头
    private String content = ""; //内容(用于技能释放时的描述)
    private LinkedHashMap<String, String> beActInfo = new LinkedHashMap<>();    //受作用的基本信息
    private LinkedHashMap<String, List<EffectInfo>> actEffectInfos = new LinkedHashMap<>(); //作用的效果信息
    private LinkedHashMap<String, List<EffectInfo>> beActEffectInfos = new LinkedHashMap<>();   //被作用的效果信息

    private void addEffectMsg(String uuid, String name, String msg, LinkedHashMap<String, List<EffectInfo>> efs) {
        if (!efs.containsKey(uuid)) {
            efs.put(uuid, new ArrayList<>());
        }
        List<EffectInfo> effectInfos = efs.get(uuid);
        if (effectInfos.stream().noneMatch(i -> i.getName().equals(name))) {
            effectInfos.add(new EffectInfo(name, msg, new HashMap() {{
            }}));
        }
    }

    private void addEffectValue(LinkedHashMap<String, List<EffectInfo>> efs, String uuid, String name, String msg, long value) {
        if (!efs.containsKey(uuid)) {
            efs.put(uuid, new ArrayList<>());
        }
        List<EffectInfo> effectInfos = efs.get(uuid);
        if (!effectInfos.stream().anyMatch(i -> i.getName().equals(name))) {
            effectInfos.add(new EffectInfo(name, msg, new HashMap() {{
                put("value", 0);
            }}));
        }
        EffectInfo info = effectInfos.stream().filter(i -> i.getName().equals(name)).findAny().get();
        long val = Long.parseLong(String.valueOf(info.getParams().get("value")));
        info.getParams().put("value", val + value);
    }

    public void addBeActEffectValue(String uuid, String name, String msg, long value) {
        addEffectValue(beActEffectInfos, uuid, name, msg, value);
    }

    public void addbeActEffectMsg(String uuid, String name, String msg) {
        addEffectMsg(uuid, name, msg, beActEffectInfos);
    }

    public void addActEffectValue(String uuid, String name, String msg, long value) {
        addEffectValue(actEffectInfos, uuid, name, msg, value);
    }

    public void addActEffectMsg(String uuid, String name, String msg) {
        addEffectMsg(uuid, name, msg, actEffectInfos);
    }

    public void addBeActInfo(String uuid, String content) {
        beActInfo.put(uuid, content);
    }

    public void apppendBeActInfo(String uuid, String content) {
        String old = beActInfo.getOrDefault(uuid, "");
        beActInfo.put(uuid, old + content);
    }

    public void prependBeActInfo(String uuid, String content) {
        String old = beActInfo.getOrDefault(uuid, "");
        beActInfo.put(uuid, content + old);
    }

    public String info() {
        StringBuilder sb = new StringBuilder();
        sb.append(prefix).append(content);
        beActInfo.forEach((uuid, beActStr) -> {
            sb.append(beActStr);
            List<EffectInfo> effectInfos = actEffectInfos.get(uuid);
            if (CollUtil.isNotEmpty(effectInfos)) {
                sb.append(",");
                sb.append(effectInfos.stream().map(EffectInfo::msg).collect(Collectors.joining(",")));
            }

            List<EffectInfo> beEffectInfos = beActEffectInfos.get(uuid);
            if (CollUtil.isNotEmpty(beEffectInfos)) {
                sb.append(",");
                sb.append(beEffectInfos.stream().map(EffectInfo::msg).collect(Collectors.joining(",")));
            }
            sb.append(" ");
        });
        if (hpAbsore != 0) {
            sb.append(" HP吸收: ").append(hpAbsore);
        }
        if (mpAbsore != 0) {
            sb.append(" TP吸收: ").append(mpAbsore);
        }
        return sb.toString();
    }

    public void addHpAbsore(long value) {
        hpAbsore += value;
    }

    public void addMpAbsore(long value) {
        mpAbsore += value;
    }
}
