package com.prodog.gamemodule.fight.effect.handler.impl;

import com.prodog.gamemodule.fight.action.entity.AtkActionRes;
import com.prodog.gamemodule.fight.effect.entity.FightEffect;
import com.prodog.gamemodule.fight.fightobj.entity.FightObj;
import com.prodog.gamemodule.fight.session.FightSession;

import java.util.List;

public class SelfCritHandler extends DefaultEffectHandler {
    public SelfCritHandler(FightSession session, FightEffect effect) {
        super(session, effect);
    }

    @Override
    public void beforeCaculateDamage(FightObj actObj, FightObj beActedObj, List<FightObj> beActedObjs, AtkActionRes res) {
        if (checkIsCurrentSkill(actObj)) {
            int physicalCrit = actObj.getFightProp().getPhysicalCrit();
            int magicalCrit = actObj.getFightProp().getMagicalCrit();
            actObj.getFightProp().setPhysicalCrit((int) getParams().get("crit") + physicalCrit);
            actObj.getFightProp().setMagicalCrit((int) getParams().get("crit") + magicalCrit);
        }
    }

    @Override
    public void afterCaculateDamage(FightObj actObj, FightObj beActedObj, List<FightObj> beActedObjs, AtkActionRes res) {
        if (checkIsCurrentSkill(actObj)) {
            int physicalCrit = actObj.getFightProp().getPhysicalCrit();
            int magicalCrit = actObj.getFightProp().getMagicalCrit();
            actObj.getFightProp().setPhysicalCrit((int) getParams().get("crit") - physicalCrit);
            actObj.getFightProp().setMagicalCrit((int) getParams().get("crit") - magicalCrit);
        }
    }
}
