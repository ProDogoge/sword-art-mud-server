package com.prodog.gamemodule.fight.effect.getter.role;

import com.prodog.gamemodule.fight.effect.entity.FightEffect;
import com.prodog.gamemodule.fight.effect.getter.EffectsGetter;
import com.prodog.gamemodule.fight.fightobj.entity.FightSkill;
import com.prodog.gamemodule.fight.util.FightUtil;
import com.prodog.gamemodule.skill.entity.Skill;
import com.prodog.gamemodule.skill.wrapper.SkillWrapper;
import com.prodog.usermodule.role.entity.RoleInfo;
import com.prodog.usermodule.role.entity.RoleSkill;
import com.prodog.usermodule.role.wrapper.RoleSkillWrapper;
import com.prodog.utils.interfaces.AutowiredBean;
import org.springframework.beans.factory.annotation.Autowired;

import java.util.List;

public class SkillEffectsGetter implements EffectsGetter<RoleInfo>, AutowiredBean {
    @Autowired
    private RoleSkillWrapper roleSkillWrapper;
    @Autowired
    private SkillWrapper skillWrapper;

    public SkillEffectsGetter() {
        this.autowired();
    }

    @Override
    public List<FightEffect> get(List<FightEffect> effects, RoleInfo info) {
        RoleSkill roleSkill = roleSkillWrapper.getById(info.getId());
        roleSkill.getLearnedSkill().forEach((skillId, level) -> {
            Skill skill = skillWrapper.getById(skillId);
            FightSkill fightSkill = FightUtil.getFightSkill(skill, level);
            effects.addAll(fightSkill.getEffects());
        });
        return effects;
    }
}
