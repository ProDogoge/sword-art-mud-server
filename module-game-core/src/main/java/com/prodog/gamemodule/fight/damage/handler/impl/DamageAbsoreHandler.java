package com.prodog.gamemodule.fight.damage.handler.impl;

import com.prodog.commonmodule.prop.entity.ObjProp;
import com.prodog.gamemodule.fight.action.entity.AtkActionRes;
import com.prodog.gamemodule.fight.damage.handler.DamageHandler;
import com.prodog.gamemodule.fight.fightobj.entity.FightObj;
import com.prodog.gamemodule.fight.fightobj.entity.FightSkill;

/***
 * 吸血吸蓝处理器
 */
public class DamageAbsoreHandler implements DamageHandler {
    private FightObj obj;
    private FightObj beActedObj;
    private AtkActionRes atkActionRes;

    public DamageAbsoreHandler(FightObj obj, FightObj beActedObj, AtkActionRes atkActionRes) {
        this.obj = obj;
        this.beActedObj = beActedObj;
        this.atkActionRes = atkActionRes;
    }

    @Override
    public void handle() {
        FightSkill skill = obj.getSkills().get(obj.getAction().getActionId());
        ObjProp prop = obj.getFightProp();
        int hpAbsore;
        int mpAbsore;
        if (skill.getDamageType() == 1) {
            hpAbsore = prop.getPhysicalHpAbsorb();
            mpAbsore = prop.getPhysicalMpAbsorb();
        } else if (skill.getDamageType() == 2) {
            hpAbsore = prop.getMagicalHpAbsorb();
            mpAbsore = prop.getMagicalMpAbsorb();
        } else {
            hpAbsore = Math.max(prop.getPhysicalHpAbsorb(), prop.getMagicalHpAbsorb());
            mpAbsore = Math.max(prop.getPhysicalMpAbsorb(), prop.getMagicalMpAbsorb());
        }
        if (skill.getAffectType() == 1) {
            atkActionRes.setHpAbsore(atkActionRes.getHpAbsore() + (long) (atkActionRes.getTotalDamage(beActedObj.getUuid()) * hpAbsore * 0.01));
            atkActionRes.setMpAbsore(atkActionRes.getMpAbsore() + (long) (atkActionRes.getTotalDamage(beActedObj.getUuid()) * mpAbsore * 0.01));
        }
    }
}
