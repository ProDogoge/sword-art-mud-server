package com.prodog.gamemodule.fight.fightobj.overHandler;

import com.prodog.commonmodule.prop.caculate.getter.RolePropGetter;
import com.prodog.commonmodule.prop.entity.ObjProp;
import com.prodog.gamemodule.fight.fightobj.entity.FightObj;
import com.prodog.gamemodule.fight.session.FightSession;
import com.prodog.usermodule.role.constant.RoleState;
import com.prodog.usermodule.role.entity.RoleInfo;
import com.prodog.usermodule.role.wrapper.RoleInfoWrapper;
import com.prodog.utils.bean.SpringBeanUtils;
import com.prodog.utils.interfaces.AutowiredBean;
import org.springframework.beans.factory.annotation.Autowired;

import java.util.List;
import java.util.stream.Collectors;

public class SetCurHpMpStateHandler implements FightOverHandler, AutowiredBean {
    @Autowired
    private RoleInfoWrapper roleInfoWrapper;

    public SetCurHpMpStateHandler() {
        this.autowired();
    }

    @Override
    public void handle(FightSession session, StringBuilder msg) {
        List<FightObj> redRoles = session.getRedObjs().values()
                .stream().filter(obj -> obj.getObjType() == 1)
                .collect(Collectors.toList());
        List<FightObj> blueRoles = session.getBlueObjs().values()
                .stream().filter(obj -> obj.getObjType() == 1)
                .collect(Collectors.toList());
        redRoles.addAll(blueRoles);
        for (FightObj roleObj : redRoles) {
            RoleInfo info = roleInfoWrapper.getById(roleObj.getId());
            ObjProp panelProp = SpringBeanUtils.getBean(RolePropGetter.class).get(info);
            info.setCurrHp(Math.min(panelProp.getHp(), roleObj.getFightProp().getCurHp()));
            info.setCurrMp(Math.min(panelProp.getMp(), roleObj.getFightProp().getCurMp()));
            info.setState(info.getCurrHp() <= 0 ? RoleState.DEAD : RoleState.NORMAL);
            roleInfoWrapper.save(info);
        }
    }
}
