package com.prodog.commonmodule.prop.caculate.getter;

import com.prodog.commonmodule.prop.entity.ObjCaculaterProp;
import com.prodog.commonmodule.prop.entity.ObjProp;
import com.prodog.commonmodule.prop.prosser.PropProcesser;
import com.prodog.commonmodule.prop.prosser.activeEquip.ActiveEquipEffectsProcesser;
import com.prodog.commonmodule.prop.prosser.activeEquip.BadgePropProcesser;
import com.prodog.commonmodule.prop.prosser.activeEquip.EnhanceProcesser;
import com.prodog.gamemodule.item.entity.ActiveEquipItem;
import com.prodog.gamemodule.item.wrapper.EquipWrapper;
import com.prodog.utils.bean.BeanUtil;
import com.prodog.utils.bean.SpringBeanUtils;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Component;

import javax.annotation.PostConstruct;
import java.util.ArrayList;
import java.util.List;

@Component
@RequiredArgsConstructor
public class ActiveEquipPropGetter implements ObjPropGetter<ActiveEquipItem> {
    private final EquipWrapper equipWrapper;

    private final List<PropProcesser<ActiveEquipItem>> processes = new ArrayList<>();

    @PostConstruct
    public void initProcesses() {
        processes.add(SpringBeanUtils.getBean(EnhanceProcesser.class));
        processes.add(SpringBeanUtils.getBean(BadgePropProcesser.class));
        processes.add(SpringBeanUtils.getBean(ActiveEquipEffectsProcesser.class));
    }

    @Override
    public ObjProp get(ActiveEquipItem equ) {
        ObjProp equProp = equipWrapper.getById(equ.getEquipId()).getProp();
        ObjCaculaterProp caculaterProp = new ObjCaculaterProp(BeanUtil.bean2Bean(equProp, ObjProp.class), null);
        for (PropProcesser<ActiveEquipItem> process : processes) {
            process.process(caculaterProp, equ);
        }
        return caculaterProp.getResultProp();
    }
}
