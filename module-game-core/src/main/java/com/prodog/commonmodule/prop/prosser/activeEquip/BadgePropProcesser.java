package com.prodog.commonmodule.prop.prosser.activeEquip;

import com.prodog.commonmodule.prop.caculate.util.PropUtil;
import com.prodog.commonmodule.prop.entity.ObjCaculaterProp;
import com.prodog.commonmodule.prop.entity.ObjProp;
import com.prodog.commonmodule.prop.prosser.PropProcesser;
import com.prodog.gamemodule.item.entity.ActiveEquipItem;
import com.prodog.gamemodule.item.entity.BadgeItem;
import com.prodog.gamemodule.item.wrapper.BadgeWrapper;
import com.prodog.utils.bean.BeanUtil;
import com.prodog.utils.interfaces.AutowiredBean;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Component;

import java.util.List;
import java.util.stream.Collectors;

@Component
@RequiredArgsConstructor
public class BadgePropProcesser implements PropProcesser<ActiveEquipItem>, AutowiredBean {
    private final BadgeWrapper badgeWrapper;

    @Override
    public ObjCaculaterProp process(ObjCaculaterProp caculaterProp, ActiveEquipItem equ) {
        List<BadgeItem> badges = equ.getBadges().stream().map(badgeWrapper::getById).collect(Collectors.toList());
        for (BadgeItem badge : badges) {
            PropUtil.sumProp(caculaterProp.getAbsoluteProp(), badge.getProp());
        }
        caculaterProp.setResultProp(BeanUtil.bean2Bean(caculaterProp.getAbsoluteProp(), ObjProp.class));
        return caculaterProp;
    }
}
