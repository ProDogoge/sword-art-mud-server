package com.prodog.commonmodule.prop.entity;

import com.prodog.gamemodule.config.dict.MapDictConfig;
import com.prodog.utils.bean.SpringBeanUtils;
import lombok.Data;

import java.util.HashSet;
import java.util.Set;

;

@Data
public class ObjProp {
    private int level;  //等级
    private String race; //种族
    private long hp;    //HP
    private long mp;    //TP
    private long curHp; //当前HP
    private long curMp; //当前TP
    private long hpReply; //HP回复
    private long mpReply; //TP回复
    private int hpReplyRate; //HP回复率
    private int mpReplyRate; //TP回复率
    private long physicalAttack;    //物攻
    private long magicalAttack; //法攻
    private long physicalDefense;   //物防
    private long magicalDefense;    //法防
    private long element;   //元素
    private long speed; //敏捷
    private int evedge;    //闪避率
    private int physicalCrit;   //物暴
    private int magicalCrit;    //法暴
    private int criticalDamage; //暴伤
    private long physicalThrough; //物穿
    private long magicalThrough; //法穿
    private int physicalThroughRate; //物穿率
    private int magicalThroughRate; //法穿率
    private int physicalHpAbsorb; //物理吸血
    private int magicalHpAbsorb; //魔法吸血
    private int physicalMpAbsorb; //物理吸蓝
    private int magicalMpAbsorb; //魔法吸蓝
    private long addDamage;  //增伤
    private int addDamageRate;  //增伤率
    private long reduceDamage;  //减伤
    private int reduceDamageRate;  //减伤率
    private long abnormalStrength;  //异常强化
    private long abnormalResistance;    //异常抗性
    private int lightAttack;  //光强
    private int darkAttack;  //暗强
    private int fireAttack; //火强
    private int iceAttack;  //冰强
    private int lightResistance;  //光抗
    private int darkResistance;  //暗抗
    private int fireResistance; //火抗
    private int iceResistance;  //冰抗
    private Set<String> elemental = new HashSet<>(); //属性攻击

    public String getElementalStr() {
        if (elemental.size() == 0) return "无";
        MapDictConfig dict = SpringBeanUtils.getBean(MapDictConfig.class);
        StringBuilder res = new StringBuilder();
        elemental.forEach(e -> res.append(dict.get("elementAttack", e)).append(" "));
        return res.toString().trim();
    }
}
