package com.prodog.commonmodule.prop.prosser;

import com.prodog.commonmodule.prop.entity.ObjCaculaterProp;

public interface PropProcesser<T> {
    ObjCaculaterProp process(ObjCaculaterProp caculaterProp, T obj);
}
