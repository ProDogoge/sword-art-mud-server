package com.prodog.commonmodule.prop.caculate.util;

import cn.hutool.core.util.ReflectUtil;
import com.prodog.commonmodule.prop.entity.ObjProp;
import com.prodog.gamemodule.config.dict.DictConfig;
import com.prodog.utils.bean.SpringBeanUtils;

import java.lang.reflect.Field;
import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;

public class PropUtil {
    public static final Field[] propFields = ReflectUtil.getFields(ObjProp.class);

    static {
        for (Field propField : propFields) {
            propField.setAccessible(true);
        }
    }

    public static void sumProp(ObjProp left, ObjProp right) {
        for (Field propField : propFields) {
            try {
                if (propField.get(left) instanceof Integer)
                    propField.set(left, (Integer) propField.get(left) + (Integer) propField.get(right));
                else if (propField.get(left) instanceof Long)
                    propField.set(left, (Long) propField.get(left) + (Long) propField.get(right));
                else if (propField.get(left) instanceof Double)
                    propField.set(left, (Double) propField.get(left) + (Double) propField.get(right));
                else if (propField.getName().equals("elemental"))
                    left.getElemental().addAll(right.getElemental());
            } catch (IllegalAccessException e) {
                e.printStackTrace();
            }
        }
    }

    public static void percentProp(ObjProp base, ObjProp percent) {
        for (Field propField : propFields) {
            try {
                if (propField.get(base) instanceof Integer)
                    propField.set(base, (Integer) propField.get(base) * (Integer) propField.get(percent) / 100);
                else if (propField.get(base) instanceof Long)
                    propField.set(base, (Long) propField.get(base) * (Long) propField.get(percent) / 100);
                else if (propField.get(base) instanceof Double)
                    propField.set(base, (Double) propField.get(base) * (Double) propField.get(percent) / 100);
            } catch (IllegalAccessException e) {
                e.printStackTrace();
            }
        }
    }

    public static void upgradeProp(ObjProp base, ObjProp result, ObjProp percent) {
        for (Field propField : propFields) {
            try {
                if (propField.get(result) instanceof Integer)
                    propField.set(result, (Integer) propField.get(result) + (Integer) propField.get(base) * (Integer) propField.get(percent) / 100);
                else if (propField.get(result) instanceof Long)
                    propField.set(result, (Long) propField.get(result) + (Long) propField.get(base) * (Long) propField.get(percent) / 100);
                else if (propField.get(result) instanceof Double)
                    propField.set(result, (Double) propField.get(result) + (Double) propField.get(base) * (Double) propField.get(percent) / 100);
                else if (propField.getName().equals("elemental"))
                    result.getElemental().addAll(percent.getElemental());
            } catch (IllegalAccessException e) {
                e.printStackTrace();
            }
        }
    }

    public static String propChangeInfo(ObjProp old, ObjProp newp) {
        DictConfig dictConfig = SpringBeanUtils.getBean(DictConfig.class);
        List<String> ignoreFields = Arrays.asList("id", "level", "race", "curHp", "curMp");
        List<String> percentFields = Arrays.asList("hpReplyRate", "mpReplyRate", "physicalCrit",
                "magicalCrit", "criticalDamage", "physicalThroughRate",
                "magicalThroughRate", "physicalHpAbsorb", "magicalHpAbsorb",
                "physicalMpAbsorb", "magicalMpAbsorb", "addDamageRate",
                "reduceDamageRate", "evedge");
        StringBuilder res = new StringBuilder();
        List<Field> fields = Arrays.stream(ReflectUtil.getFields(ObjProp.class))
                .filter(f -> !ignoreFields.contains(f.getName())).collect(Collectors.toList());
        for (Field field : fields) {
            try {
                field.setAccessible(true);
                Object oldVal = field.get(old);
                Object newVal = field.get(newp);
                if (oldVal.equals(newVal)) continue;
                if (oldVal instanceof Integer) {
                    int changeVal = (int) newVal - (int) oldVal;
                    res.append(dictConfig.get(field.getName())).append("：").append(oldVal);
                    if (percentFields.contains(field.getName())) res.append("%");
                    res.append("   ");
                    if (changeVal > 0) res.append("+");
                    res.append(changeVal);
                    if (percentFields.contains(field.getName())) res.append("%");
                    res.append(" -> ").append(newVal);
                    if (percentFields.contains(field.getName())) res.append("%");
                    res.append("\n");
                } else if (oldVal instanceof Long) {
                    long changeVal = (long) newVal - (long) oldVal;
                    res.append(dictConfig.get(field.getName())).append("：").append(oldVal);
                    if (percentFields.contains(field.getName())) res.append("%");
                    res.append("   ");
                    if (changeVal > 0) res.append("+");
                    res.append(changeVal);
                    if (percentFields.contains(field.getName())) res.append("%");
                    res.append(" -> ").append(newVal);
                    if (percentFields.contains(field.getName())) res.append("%");
                    res.append("\n");
                } else if (oldVal instanceof Double) {
                    double changeVal = (double) newVal - (double) oldVal;
                    res.append(dictConfig.get(field.getName())).append("：").append(oldVal);
                    if (percentFields.contains(field.getName())) res.append("%");
                    res.append("   ");
                    if (changeVal > 0) res.append("+");
                    res.append(changeVal);
                    if (percentFields.contains(field.getName())) res.append("%");
                    res.append(" -> ").append(newVal);
                    if (percentFields.contains(field.getName())) res.append("%");
                    res.append("\n");
                } else if (field.getName().equals("elemental") && !(newp.getElemental().containsAll(old.getElemental()) && newp.getElemental().size() == old.getElemental().size())) {
                    res.append(dictConfig.get(field.getName())).append("：").append(old.getElementalStr())
                            .append("   -> ").append(newp.getElementalStr());
                    res.append("\n");
                }
            } catch (IllegalAccessException e) {
                e.printStackTrace();
            }
        }
        return res.toString().trim();
    }
}
