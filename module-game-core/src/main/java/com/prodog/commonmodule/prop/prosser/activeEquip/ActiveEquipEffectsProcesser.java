package com.prodog.commonmodule.prop.prosser.activeEquip;

import com.prodog.commonmodule.prop.entity.ObjCaculaterProp;
import com.prodog.commonmodule.prop.prosser.PropProcesser;
import com.prodog.gamemodule.fight.effect.entity.FightEffect;
import com.prodog.gamemodule.fight.effect.getter.activeEquip.ActiveEquipEffectsGetter;
import com.prodog.gamemodule.item.entity.ActiveEquipItem;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.List;

@Component
@RequiredArgsConstructor
public class ActiveEquipEffectsProcesser implements PropProcesser<ActiveEquipItem> {

    @Override
    public ObjCaculaterProp process(ObjCaculaterProp caculaterProp, ActiveEquipItem equ) {
        List<FightEffect> effects = new ArrayList<>();
        new ActiveEquipEffectsGetter().get(effects, equ);
        for (FightEffect effect : effects) {
            effect.getHandler(null).caculateProp(caculaterProp);
        }
        return caculaterProp;
    }
}
