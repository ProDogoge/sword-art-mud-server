package com.prodog.commonmodule.prop.caculate.getter;

import com.prodog.commonmodule.prop.entity.ObjCaculaterProp;
import com.prodog.commonmodule.prop.entity.ObjProp;
import com.prodog.gamemodule.gameworld.monster.entity.MonsterProp;
import com.prodog.utils.bean.BeanUtil;
import com.prodog.utils.interfaces.AutowiredBean;

public class MonsterPropGetter implements AutowiredBean, ObjPropGetter<MonsterProp> {
    public MonsterPropGetter() {
        autowired();
    }


    @Override
    public ObjProp get(MonsterProp prop) {
        ObjCaculaterProp caculaterProp = new ObjCaculaterProp() {{
            setAbsoluteProp(prop);
            setResultProp(BeanUtil.beanToBean(prop, MonsterProp.class));
        }};
        //执行多个计算方式....
        return caculaterProp.getResultProp();
    }
}
