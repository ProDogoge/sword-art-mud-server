package com.prodog.commonmodule.prop.prosser.role;

import com.prodog.commonmodule.prop.entity.ObjCaculaterProp;
import com.prodog.commonmodule.prop.prosser.PropProcesser;
import com.prodog.gamemodule.fight.effect.entity.FightEffect;
import com.prodog.gamemodule.fight.effect.getter.role.RoleEffectsGetter;
import com.prodog.gamemodule.fight.effect.manager.EffectHandlerManager;
import com.prodog.usermodule.role.entity.RoleInfo;
import com.prodog.utils.interfaces.AutowiredBean;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.List;

@Component
@RequiredArgsConstructor
public class EffectPropProcesser implements PropProcesser<RoleInfo>, AutowiredBean {

    @Override
    public ObjCaculaterProp process(ObjCaculaterProp caculaterProp, RoleInfo info) {
        List<FightEffect> effects = new RoleEffectsGetter().get(new ArrayList<>(), info);
        for (FightEffect effect : effects) {
            EffectHandlerManager.caculateProp(effect, caculaterProp);
        }
        return caculaterProp;
    }
}
