package com.prodog.commonmodule.prop.entity;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class ObjCaculaterProp {
    private ObjProp absoluteProp;   //用于计算固定值的属性并提供给百分比计算的属性
    private ObjProp resultProp;   //最终得出的属性
}
