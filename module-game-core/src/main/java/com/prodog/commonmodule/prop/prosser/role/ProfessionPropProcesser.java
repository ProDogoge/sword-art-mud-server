package com.prodog.commonmodule.prop.prosser.role;

import com.prodog.commonmodule.prop.caculate.util.PropUtil;
import com.prodog.commonmodule.prop.entity.ObjCaculaterProp;
import com.prodog.commonmodule.prop.entity.ObjProp;
import com.prodog.commonmodule.prop.prosser.PropProcesser;
import com.prodog.gamemodule.profession.entity.Profession;
import com.prodog.gamemodule.profession.wrapper.ProfessionWrapper;
import com.prodog.usermodule.role.entity.RoleInfo;
import com.prodog.utils.interfaces.AutowiredBean;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Component;

@Component
@RequiredArgsConstructor
public class ProfessionPropProcesser implements PropProcesser<RoleInfo>, AutowiredBean {
    private final ProfessionWrapper professionWrapper;


    @Override
    public ObjCaculaterProp process(ObjCaculaterProp caculaterProp, RoleInfo info) {
        Profession profession = professionWrapper.getById(info.getProfessionId());
        ObjProp initialProp = profession.getInitialProp();
        PropUtil.sumProp(caculaterProp.getAbsoluteProp(), initialProp);
        for (int lev = 2; lev <= info.getLevel(); lev++) {
            PropUtil.sumProp(caculaterProp.getAbsoluteProp(), profession.getGrowth(lev).getProp());
        }
        return caculaterProp;
    }
}
