package com.prodog.commonmodule.prop.caculate.getter;

import com.prodog.commonmodule.prop.entity.ObjCaculaterProp;
import com.prodog.commonmodule.prop.entity.ObjProp;
import com.prodog.commonmodule.prop.prosser.PropProcesser;
import com.prodog.commonmodule.prop.prosser.role.EffectPropProcesser;
import com.prodog.commonmodule.prop.prosser.role.EquipPropProcesser;
import com.prodog.commonmodule.prop.prosser.role.ProfessionPropProcesser;
import com.prodog.usermodule.role.entity.RoleInfo;
import com.prodog.utils.bean.SpringBeanUtils;
import org.springframework.stereotype.Component;

import javax.annotation.PostConstruct;
import java.util.ArrayList;
import java.util.List;

@Component
public class RolePropGetter implements ObjPropGetter<RoleInfo> {
    private final List<PropProcesser<RoleInfo>> processes = new ArrayList<>();

    @PostConstruct
    public void initProcesses() {
        processes.add(SpringBeanUtils.getBean(ProfessionPropProcesser.class));
        processes.add(SpringBeanUtils.getBean(EquipPropProcesser.class));
        processes.add(SpringBeanUtils.getBean(EffectPropProcesser.class));
    }

    @Override
    public ObjProp get(RoleInfo info) {
        ObjCaculaterProp caculaterProp = new ObjCaculaterProp(new ObjProp(), new ObjProp());
        for (PropProcesser<RoleInfo> process : processes) {
            process.process(caculaterProp, info);
        }
        caculaterProp.getResultProp().setLevel(info.getLevel());
        caculaterProp.getResultProp().setCurHp(Math.min(caculaterProp.getResultProp().getHp(), info.getCurrHp()));
        caculaterProp.getResultProp().setCurMp(Math.min(caculaterProp.getResultProp().getMp(), info.getCurrMp()));
        caculaterProp.getResultProp().setRace("human");
        return caculaterProp.getResultProp();
    }
}
