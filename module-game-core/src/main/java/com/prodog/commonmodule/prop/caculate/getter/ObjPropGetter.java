package com.prodog.commonmodule.prop.caculate.getter;

import com.prodog.commonmodule.prop.entity.ObjProp;

public interface ObjPropGetter<T> {
    ObjProp get(T prop);
}
