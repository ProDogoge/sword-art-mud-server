package com.prodog.commonmodule.prop.caculate.getter.factory;

import cn.hutool.core.util.ReflectUtil;
import com.prodog.commonmodule.prop.caculate.getter.ActiveEquipPropGetter;
import com.prodog.commonmodule.prop.caculate.getter.MonsterPropGetter;
import com.prodog.commonmodule.prop.caculate.getter.ObjPropGetter;
import com.prodog.commonmodule.prop.caculate.getter.RolePropGetter;
import com.prodog.gamemodule.gameworld.monster.entity.MonsterProp;
import com.prodog.gamemodule.item.entity.ActiveEquipItem;
import com.prodog.usermodule.role.entity.RoleProp;

import java.util.HashMap;
import java.util.Map;

public class ObjPropGetterFactory {
    private Map<Class, Class<? extends ObjPropGetter>> handlerMap;

    public static final String KEY_ROLE_INFO = "ROLE_INFO";

    public ObjPropGetterFactory() {
        this.init();
    }

    private void init() {
        this.handlerMap = new HashMap() {{
            put(RoleProp.class, RolePropGetter.class);
            put(MonsterProp.class, MonsterPropGetter.class);
            put(ActiveEquipItem.class, ActiveEquipPropGetter.class);
        }};
    }

    public ObjPropGetter getGetter(Class clz, Map<String, Object> handlerParam) {
        Class handlerClz = handlerMap.get(clz);
        if (handlerClz != null) {
            Object handler = ReflectUtil.newInstance(handlerClz, handlerParam);
            return (ObjPropGetter) handler;
        }
        return null;
    }

    public ObjPropGetter getGetter(Class clz) {
        Class handlerClz = handlerMap.get(clz);
        if (handlerClz != null) {
            Object handler = ReflectUtil.newInstance(handlerClz);
            return (ObjPropGetter) handler;
        }
        return null;
    }
}
