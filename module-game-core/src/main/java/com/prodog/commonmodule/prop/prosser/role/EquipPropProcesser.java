package com.prodog.commonmodule.prop.prosser.role;

import com.prodog.commonmodule.prop.caculate.util.PropUtil;
import com.prodog.commonmodule.prop.entity.ObjCaculaterProp;
import com.prodog.commonmodule.prop.entity.ObjProp;
import com.prodog.commonmodule.prop.prosser.PropProcesser;
import com.prodog.gamemodule.item.entity.ActiveEquipItem;
import com.prodog.gamemodule.item.wrapper.ActiveEquipWrapper;
import com.prodog.gamemodule.suit.domain.model.PartSet;
import com.prodog.gamemodule.suit.util.EquipSuitUtil;
import com.prodog.usermodule.role.entity.RoleEquip;
import com.prodog.usermodule.role.entity.RoleInfo;
import com.prodog.usermodule.role.util.EquipUtil;
import com.prodog.usermodule.role.wrapper.RoleEquipWrapper;
import com.prodog.utils.bean.BeanUtil;
import com.prodog.utils.interfaces.AutowiredBean;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Component;

import java.lang.reflect.Field;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;
import java.util.stream.Collectors;

@Component
@RequiredArgsConstructor
public class EquipPropProcesser implements PropProcesser<RoleInfo>, AutowiredBean {
    private final RoleEquipWrapper roleEquipWrapper;
    private final ActiveEquipWrapper activeEquipWrapper;

    @Override
    public ObjCaculaterProp process(ObjCaculaterProp caculaterProp, RoleInfo info) {
        RoleEquip roleEquip = roleEquipWrapper.getById(info.getId());
        List<String> ignoreFields = Collections.singletonList("id");
        List<Field> fields = Arrays.stream(EquipUtil.roleEquipFields)
                .filter(f -> !ignoreFields.contains(f.getName())).collect(Collectors.toList());
        for (Field field : fields) {
            try {
                String equId = (String) field.get(roleEquip);
                if (equId != null) {
                    ActiveEquipItem equ = activeEquipWrapper.getById(equId);
                    PropUtil.sumProp(caculaterProp.getAbsoluteProp(), equ.getProp());
                }
            } catch (IllegalAccessException e) {
                e.printStackTrace();
            }
        }

        //套装
        List<PartSet> partSets = EquipSuitUtil.getPartSets(roleEquip);
        partSets.forEach(partSet -> PropUtil.sumProp(caculaterProp.getAbsoluteProp(), partSet.getProp()));

        caculaterProp.setResultProp(BeanUtil.bean2Bean(caculaterProp.getAbsoluteProp(), ObjProp.class));
        return caculaterProp;
    }
}
