package com.prodog.commonmodule.prop.prosser.activeEquip;

import cn.hutool.script.ScriptUtil;
import com.prodog.commonmodule.prop.caculate.util.PropUtil;
import com.prodog.commonmodule.prop.entity.ObjCaculaterProp;
import com.prodog.commonmodule.prop.entity.ObjProp;
import com.prodog.commonmodule.prop.prosser.PropProcesser;
import com.prodog.gamemodule.config.enhance.EnhanceConfig;
import com.prodog.gamemodule.item.entity.ActiveEquipItem;
import com.prodog.util.MsgTemplateUtil;
import com.prodog.utils.bean.BeanUtil;
import com.prodog.utils.interfaces.AutowiredBean;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Component;

import java.lang.reflect.Field;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;

@Component
@RequiredArgsConstructor
public class EnhanceProcesser implements PropProcesser<ActiveEquipItem>, AutowiredBean {
    private final EnhanceConfig enhanceConfig;

    @Override
    public ObjCaculaterProp process(ObjCaculaterProp caculaterProp, ActiveEquipItem equ) {
        //类型没有强化配置或没有强化过装备
        if (!enhanceConfig.getEnhanceProp().containsKey(equ.getEquipType()) || equ.getEnhanceLevel() <= 0)
            return caculaterProp;

        //获取强化到的词条
        List<String> fields = enhanceConfig.getEnhanceProp().get(equ.getEquipType());
        for (String field : fields) {
            try {
                Field f = Arrays.stream(PropUtil.propFields).filter(fd -> fd.getName().equals(field)).findAny().get();
                Object val = f.get(caculaterProp.getAbsoluteProp());
                double initVal = Double.parseDouble(String.valueOf(val));
                String expression = MsgTemplateUtil.formatStr(enhanceConfig.getUpPropExpression(), new HashMap<String, Object>() {{
                    put("prop", initVal);
                    put("upLevel", equ.getEnhanceLevel());
                }});
                double res = (double) ScriptUtil.eval(expression);
                if (val instanceof Integer) f.set(caculaterProp.getAbsoluteProp(), (int) res);
                if (val instanceof Long) f.set(caculaterProp.getAbsoluteProp(), (long) res);
                if (val instanceof Double) f.set(caculaterProp.getAbsoluteProp(), res);
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
        caculaterProp.setResultProp(BeanUtil.bean2Bean(caculaterProp.getAbsoluteProp(), ObjProp.class));
        return caculaterProp;
    }
}
