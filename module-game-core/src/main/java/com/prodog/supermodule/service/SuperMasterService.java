package com.prodog.supermodule.service;

import com.prodog.command.entity.CommandResult;
import com.prodog.command.entity.GameCommand;

public interface SuperMasterService {
    CommandResult reload(GameCommand command);
}
