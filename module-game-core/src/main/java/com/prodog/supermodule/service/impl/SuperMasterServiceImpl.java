package com.prodog.supermodule.service.impl;

import com.prodog.command.annonations.Service;
import com.prodog.command.entity.CommandResult;
import com.prodog.command.entity.GameCommand;
import com.prodog.supermodule.service.SuperMasterService;
import com.prodog.utils.bean.SpringBeanUtils;
import com.prodog.utils.loader.GameLoader;

import java.util.Map;

@Service
public class SuperMasterServiceImpl implements SuperMasterService {
    @Override
    public CommandResult reload(GameCommand command) {
        Map<String, GameLoader> loaders = SpringBeanUtils.getBeansOfType(GameLoader.class);
        loaders.values().forEach(GameLoader::load);
        return CommandResult.success("服务器数据重载成功!");
    }
}
