package com.prodog.supermodule.handler;

import com.prodog.command.annonations.Command;
import com.prodog.command.annonations.CommandBean;
import com.prodog.command.entity.CommandResult;
import com.prodog.command.entity.GameCommand;
import com.prodog.supermodule.service.SuperMasterService;
import lombok.RequiredArgsConstructor;

@CommandBean(setName = "超级管理员操作")
@RequiredArgsConstructor
/*@Interceptors(iptors = {
        @Iptor(value = NotRegistInterceptor.class),
        @Iptor(value = StateInterceptor.class, params = "{'states':[3],'required':false}", msg = "{{state}}状态下无法进行此操作")
})*/
public class SuperMasterHandler {
    private final SuperMasterService superMasterService;

    @Command(value = "重载")
    public CommandResult reload(GameCommand command) {
        try {
            return superMasterService.reload(command);
        } catch (Exception e) {
            e.printStackTrace();
            return CommandResult.failure("服务器错误，数据重载失败。");
        }
    }
}
