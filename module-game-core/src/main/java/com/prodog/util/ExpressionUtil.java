package com.prodog.util;

import cn.hutool.script.ScriptUtil;
import com.prodog.utils.dataType.ConvertUtil;

import java.util.Map;

public class ExpressionUtil {
    public static  <T> T parse(String expression, Map<String, Object> params, Class<T> clz) {
        String prepared = MsgTemplateUtil.formatStr(expression, params);
        String strVal = String.valueOf(ScriptUtil.eval(prepared));
        return (T) ConvertUtil.convert(strVal, clz);
    }
}
