package com.prodog.usermodule.caculate;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class CalVal<T> {
    private T base;
    private T result;
}
