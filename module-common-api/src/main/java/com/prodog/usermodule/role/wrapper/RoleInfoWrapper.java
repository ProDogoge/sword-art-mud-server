package com.prodog.usermodule.role.wrapper;

import com.prodog.database.annotation.Wrapper;
import com.prodog.database.wrapper.MongoDataWrapper;
import com.prodog.usermodule.role.entity.RoleInfo;

@Wrapper(path = "玩家数据/角色/角色信息", module = "角色信息")
public class RoleInfoWrapper extends MongoDataWrapper<RoleInfo, String> {
}
