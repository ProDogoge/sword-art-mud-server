package com.prodog.usermodule.role.entity;

import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.data.mongodb.core.mapping.Document;

@Document("g_role_etc")
@Data
@NoArgsConstructor
public class RoleEtc {
    private String id;  //角色id
    private int nextGift;   //下一个等级礼包

    public RoleEtc(String id) {
        this.id = id;
    }
}
