package com.prodog.usermodule.role.wrapper;

import com.prodog.database.annotation.Wrapper;
import com.prodog.database.wrapper.MongoDataWrapper;
import com.prodog.usermodule.role.entity.RoleEtc;

import java.util.Optional;

@Wrapper(path = "玩家数据/角色/角色杂项", module = "角色杂项信息")
public class RoleEtcWrapper extends MongoDataWrapper<RoleEtc, String> {
    @Override
    public RoleEtc getById(String id) {
        return Optional.ofNullable(super.getById(id)).orElseGet(() -> {
            RoleEtc roleEtc = new RoleEtc(id);
            save(roleEtc);
            return roleEtc;
        });
    }
}
