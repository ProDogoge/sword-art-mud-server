package com.prodog.usermodule.role.entity;

import lombok.Data;
import org.springframework.data.mongodb.core.mapping.Document;

@Data
@Document("g_role_info")
public class RoleInfo {
    private String id;
    private String userId;  //用户ID
    private long serialNum;   //识别码
    private String roleName;    //角色名
    private int level;  //等级
    private long exp;   //经验值
    private long currHp;    //当前HP
    private long currMp;    //当前TP
    private long energy;    //精力
    private String title;   //称号
    private String professionId;    //职业ID
    private String sex; //性别
    private long money; //货币
    private String teamId;  //队伍ID
    private String fightSessionId; //战斗会话ID
    private String fightUuid;   //战斗标识码
    private String groupId; //队伍ID
    private String mapId;   //地图ID
    private int state = 1;  //1正常 2战斗中 3死亡

    public void addMoney(long money) {
        this.money += money;
    }
}
