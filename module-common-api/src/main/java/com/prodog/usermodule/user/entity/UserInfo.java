package com.prodog.usermodule.user.entity;

import lombok.Data;
import org.springframework.data.mongodb.core.mapping.Document;

import java.util.Date;

@Data
@Document("g_user_info")
public class UserInfo {
    private String id;  //id
    private Long copoun;  //点券
    //@JsonFormat(pattern = "YYYY-MM-dd HH:mm:ss")
    private Date registTime;    //注册时间
    private String roleId; //当前角色ID
    private String delRoleId;   //当前要删除的角色

    public void addCopoun(long copoun) {
        copoun += copoun;
    }
}
