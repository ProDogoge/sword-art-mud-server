package com.prodog.usermodule.user.wrapper;

import com.prodog.database.annotation.Wrapper;
import com.prodog.database.wrapper.MongoDataWrapper;
import com.prodog.usermodule.user.entity.UserInfo;

@Wrapper(path = "玩家数据/账户/账户信息",module = "账户信息")
public class UserInfoWrapper extends MongoDataWrapper<UserInfo,String> {
}
