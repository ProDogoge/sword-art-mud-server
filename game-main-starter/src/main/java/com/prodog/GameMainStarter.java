package com.prodog;

import cn.hutool.cron.CronUtil;
import love.forte.simboot.spring.autoconfigure.EnableSimbot;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.scheduling.annotation.EnableScheduling;

@EnableSimbot
@EnableScheduling
@SpringBootApplication
public class GameMainStarter {

    public static void main(String[] args) {
        SpringApplication.run(GameMainStarter.class, args);
        CronUtil.setMatchSecond(true);
        CronUtil.start();
    }

}
