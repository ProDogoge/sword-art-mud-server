package com.prodog.message.sender;

import love.forte.simbot.bot.Bot;
import love.forte.simbot.bot.BotManager;
import love.forte.simbot.bot.OriginBotManager;
import love.forte.simbot.component.mirai.message.MiraiMessageParserUtil;
import love.forte.simbot.definition.Contact;
import love.forte.simbot.definition.Group;
import love.forte.simbot.message.Message;
import net.mamoe.mirai.message.data.MessageChain;
import net.mamoe.mirai.message.data.MessageChainBuilder;
import net.mamoe.mirai.message.data.SimpleServiceMessage;
import net.mamoe.mirai.message.data.XmlMessageBuilder;

import java.util.Collection;
import java.util.HashMap;
import java.util.Map;
import java.util.Optional;

//@Component
public class SimBotXmlMessageSender implements MessageSender {

    public Map<String, Group> getGroups() {
        Map<String, Group> groups = new HashMap<>();
        for (BotManager<?> manager : OriginBotManager.INSTANCE) {
            for (Bot bot : manager.all()) {
                bot.getGroups().asStream().forEach(group -> groups.put(group.getId().toString(), group));
            }
        }
        return groups;
    }

    public Map<String, Contact> getFriends() {
        Map<String, Contact> friends = new HashMap<>();
        for (BotManager<?> manager : OriginBotManager.INSTANCE) {
            for (Bot bot : manager.all()) {
                bot.getContacts().asStream().forEach(friend -> friends.put(friend.getId().toString(), friend));
            }
        }
        return friends;
    }

    private Message buildMsg(String content) {
        XmlMessageBuilder xmlMessageBuilder = new XmlMessageBuilder();
        xmlMessageBuilder.setAction("web");
        xmlMessageBuilder.setBrief("SWORD ART MUD");

        /*xmlMessageBuilder.item(-23296, 2, itemBuilder -> {
            itemBuilder.title("标题是：XXX", 10, "black");
            itemBuilder.summary("sth here", "#000000");
            return null;
        });*/

        xmlMessageBuilder.item(0, 6, itemBuilder -> {
            itemBuilder.summary(content.replace("<", "&lt;").replace(">", "&gt;"), "#000000");
            return null;
        });
        xmlMessageBuilder.setServiceId(1);
        MessageChain chain = new MessageChainBuilder()
                .append(new SimpleServiceMessage(1, xmlMessageBuilder.getText()))
                .build();
        return MiraiMessageParserUtil.asSimbotMessage(chain.get(0));
    }

    @Override
    public void sendToGroup(String groupId, String msg) {
        Optional.ofNullable(getGroups().get(groupId)).ifPresent(group -> group.sendBlocking(buildMsg(msg)));
    }

    @Override
    public void sendToSelf(String selfId, String msg) {
        Optional.ofNullable(getFriends().get(selfId)).ifPresent(friend -> friend.sendBlocking(buildMsg(msg)));
    }

    @Override
    public void sendToGroups(String msg, Collection<String> groupIds) {
        groupIds.forEach(groupId -> sendToGroup(groupId, msg));
    }

    @Override
    public void sendToSelfs(String msg, Collection<String> selfIds) {
        selfIds.forEach(selfId -> sendToGroup(selfId, msg));
    }
}
