package com.prodog.message.sender;

import cn.hutool.http.HttpRequest;
import cn.hutool.http.HttpResponse;
import cn.hutool.http.HttpUtil;
import com.alibaba.fastjson2.JSON;
import com.alibaba.fastjson2.JSONObject;
import love.forte.simbot.bot.Bot;
import love.forte.simbot.bot.BotManager;
import love.forte.simbot.bot.OriginBotManager;
import love.forte.simbot.component.mirai.message.MiraiMessageParserUtil;
import love.forte.simbot.definition.Contact;
import love.forte.simbot.definition.Group;
import love.forte.simbot.message.Message;
import net.mamoe.mirai.message.data.LightApp;
import org.springframework.web.client.RestTemplate;

import java.util.Collection;
import java.util.HashMap;
import java.util.Map;
import java.util.Optional;


//@Component
public class SimBotJsonMessageSender implements MessageSender {
    private static final String P_SKEY = "FAuimM*Ce2J3DGvVbtFeFj5PhT-AbXEHZNchX0l0u4c_";
    private static final String SKEY = "@wA7F0saIc";
    private final String url = "https://api.wya6.cn/api/qq_json_token?apiKey=d344514d4d37175a3a42d7ca5d2b066d";
    private final RestTemplate restTemplate = new RestTemplate();
    private final String TEMPLATE_1 = "{\n" +
            "    \"app\": \"com.tencent.bot.groupbot\",\n" +
            "    \"desc\": \"\",\n" +
            "    \"view\": \"index\",\n" +
            "    \"ver\": \"1.0.0.9\",\n" +
            "    \"prompt\": \"SWORD ART MUD\",\n" +
            "    \"appID\": \"\",\n" +
            "    \"sourceName\": \"\",\n" +
            "    \"actionData\": \"\",\n" +
            "    \"actionData_A\": \"\",\n" +
            "    \"sourceUrl\": \"\",\n" +
            "    \"meta\": {\n" +
            "        \"embed\": {\n" +
            "            \"fields\": [\n" +
            "                {\n" +
            "                    \"name\": \"{{res_content}}\"\n" +
            "                }\n" +
            "            ],\n" +
            "            \"thumbnail\": {\n" +
            "                \"url\": \"https://thirdqq.qlogo.cn/g?b=sdk&k=5DZAwHctPEvVP5S08ibHicxQ&kti=Y9tu9wAAAAI&s=100&t=1672453421\"\n" +
            "            }," +
            "            \"title\": \"SWORD ART MUD\"\n" +
            "        }\n" +
            "    },\n" +
            "    \"text\": \"\",\n" +
            "    \"sourceAd\": \"\",\n" +
            "    \"extra\": \"\"\n" +
            "}";
    private final String TEMPLATE_2 = "{\n" +
            "    \"app\": \"com.tencent.miniapp\",\n" +
            "    \"desc\": \"\",\n" +
            "    \"view\": \"all\",\n" +
            "    \"ver\": \"1.0.0.89\",\n" +
            "    \"prompt\": \"SWORD ART MUD\",\n" +
            "    \"appID\": \"\",\n" +
            "    \"sourceName\": \"\",\n" +
            "    \"actionData\": \"\",\n" +
            "    \"actionData_A\": \"\",\n" +
            "    \"sourceUrl\": \"\",\n" +
            "    \"meta\": {\n" +
            "        \"all\": {\n" +
            "            \"buttons\": [],\n" +
            "            \"jumpUrl\": \"\",\n" +
            "            \"preview\": \"\",\n" +
            "            \"summary\": \"{{res_content}}\",\n" +
            "            \"title\": \"SWORD ART MUD\"\n" +
            "        }\n" +
            "    },\n" +
            "    \"text\": \"\",\n" +
            "    \"sourceAd\": \"\",\n" +
            "    \"extra\": \"\"\n" +
            "}";

    private final String TEMPLATE_3 = "{\n" +
            "    \"app\": \"com.tencent.miniapp_01\",\n" +
            "    \"desc\": \"\",\n" +
            "    \"view\": \"notification\",\n" +
            "    \"ver\": \"1.0.0.11\",\n" +
            "    \"prompt\": \"SWORD ART MUD\",\n" +
            "    \"appID\": \"\",\n" +
            "    \"sourceName\": \"\",\n" +
            "    \"actionData\": \"\",\n" +
            "    \"actionData_A\": \"\",\n" +
            "    \"sourceUrl\": \"\",\n" +
            "    \"meta\": {\n" +
            "        \"notification\": {\n" +
            "            \"appInfo\": {\n" +
            "                \"appName\": \"SWORD ART MUD\",\n" +
            "                \"appType\": 4,\n" +
            "                \"appid\": 2174398127\n" +
            "            },\n" +
            "            \"button\": [\n" +
            "                {\n" +
            "                    \"action\": \"\",\n" +
            "                    \"name\": \"\"\n" +
            "                },\n" +
            "                {\n" +
            "                    \"action\": \"\",\n" +
            "                    \"name\": \"\"\n" +
            "                }\n" +
            "            ],\n" +
            "            \"data\": [\n" +
            "                {\n" +
            "                    \"value\": \"{{res_content}}\"\n" +
            "                }\n" +
            "            ],\n" +
            "            \"emphasis_keyword\": \"\",\n" +
            "            \"title\": \"SWORD ART MUD\"\n" +
            "        }\n" +
            "    },\n" +
            "    \"text\": \"\",\n" +
            "    \"sourceAd\": \"\",\n" +
            "    \"extra\": \"\"\n" +
            "}";

    private final String TEMPLATE_4 = "{\n" +
            "    \"app\": \"com.tencent.structmsg\",\n" +
            "    \"desc\": \"SWORD ART MUD\",\n" +
            "    \"view\": \"news\",\n" +
            "    \"ver\": \"0.0.0.1\",\n" +
            "    \"prompt\": \"SWORD ART MUD\",\n" +
            "    \"appID\": \"\",\n" +
            "    \"sourceName\": \"\",\n" +
            "    \"actionData\": \"\",\n" +
            "    \"actionData_A\": \"\",\n" +
            "    \"sourceUrl\": \"\",\n" +
            "    \"meta\": {\n" +
            "        \"news\": {\n" +
            "            \"action\": \"\",\n" +
            "            \"source_url\": \"\",\n" +
            "            \"android_pkg_name\": \"\",\n" +
            "            \"title\": \"SWORD ART MUD\",\n" +
            "            \"app_type\": 1,\n" +
            "            \"source_icon\": \"\",\n" +
            "            \"tag\": \"Version 1.0 -By ProDog\",\n" +
            "            \"appid\": 1103991616,\n" +
            "            \"desc\": \"{{res_content}}\"\n" +
            "        }\n" +
            "    },\n" +
            "    \"text\": \"\",\n" +
            "    \"extraApps\": [],\n" +
            "    \"sourceAd\": \"\"\n" +
            "}";

    private String arkSign(String toSignJson, String qq, String skey, String pSkey) {
        try {
            HttpRequest post = HttpUtil.createPost("https://docs.qq.com/v2/push/ark_sig?u=ee9f5e8f0d0d4a909cfeb4505d2f392f");
            String userAgent = "Mozilla/5.0 (Linux; Android 10.0; MHA-AL00 Build/HUAWEIMHA-AL00; wv) AppleWebKit/537.36 (KHTML, like Gecko) Version/4.0 Chrome/43.0.2357.134 Mobile Safari/537.36";
            StringBuilder cookie = new StringBuilder();
            cookie.append("skey=").append(skey).append(';');
            cookie.append("p_skey=").append(pSkey).append(';');

            StringBuilder uin = new StringBuilder("o");
            for (int i = 0; i < 10 - qq.length(); i++) uin.append("0");
            uin.append(qq);

            cookie.append("uin=").append(uin).append(';');
            cookie.append("p_uin=").append(uin).append(';');

            post.header("accept", "*/*");
            post.header("connection", "Keep-Alive");
            post.header("user-agent", userAgent);
            post.header("cookie", cookie.toString());
            post.header("content-type", "application/json");

            JSONObject params = new JSONObject();
            params.put("ark", toSignJson);
            params.put("type", 0);
            params.put("xsrf", "2950db67a53f7f0e");

            post.body(params.toString());
            HttpResponse resp = post.execute();
            JSONObject jsonRes = JSON.parseObject(resp.body());
            return jsonRes.getJSONObject("result").get("ark_with_sig").toString();
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }
    }

    public Message buildMsg(String msg) {
        String content = msg.replace("\n", "\\n").replace("\r", "\\r");
        String arkSignMsg = arkSign(TEMPLATE_1.replace("{{res_content}}", content), "99709407", SKEY, P_SKEY);
        LightApp lightApp = new LightApp(arkSignMsg);
        return MiraiMessageParserUtil.asSimbotMessage(lightApp);

        /*HttpRequest post = HttpUtil.createPost(url);
        post.header("contentType", "application/json;charset=UTF-8");
        String content = msg.replace("\n", "\\n")
                .replace("\r", "\\r")
                .replace("\t", "\\t");
        post.body(TEMPLATE_1.replace("{{res_content}}", content));
        HttpResponse execute = post.execute();
        String body = execute.body();
        LightApp lightApp = new LightApp(body);
        return MiraiMessageParserUtil.asSimbotMessage(lightApp);*/
    }

    public Map<String, Group> getGroups() {
        Map<String, Group> groups = new HashMap<>();
        for (BotManager<?> manager : OriginBotManager.INSTANCE) {
            for (Bot bot : manager.all()) {
                bot.getGroups().asStream().forEach(group -> groups.put(group.getId().toString(), group));
            }
        }
        return groups;
    }

    public Map<String, Contact> getFriends() {
        Map<String, Contact> friends = new HashMap<>();
        for (BotManager<?> manager : OriginBotManager.INSTANCE) {
            for (Bot bot : manager.all()) {
                bot.getContacts().asStream().forEach(friend -> friends.put(friend.getId().toString(), friend));
            }
        }
        return friends;
    }

    @Override
    public void sendToGroup(String groupId, String msg) {
        Optional.ofNullable(getGroups().get(groupId)).ifPresent(group -> group.sendBlocking(buildMsg(msg)));
    }

    @Override
    public void sendToSelf(String selfId, String msg) {
        Optional.ofNullable(getFriends().get(selfId)).ifPresent(friend -> friend.sendBlocking(buildMsg(msg)));
    }

    @Override
    public void sendToGroups(String msg, Collection<String> groupIds) {
        groupIds.forEach(groupId -> sendToGroup(groupId, msg));
    }

    @Override
    public void sendToSelfs(String msg, Collection<String> selfIds) {
        selfIds.forEach(selfId -> sendToGroup(selfId, msg));
    }
}
