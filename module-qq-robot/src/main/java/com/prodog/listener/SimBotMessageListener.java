package com.prodog.listener;

import com.prodog.command.core.GameCommandHandler;
import com.prodog.command.entity.CommandResult;
import com.prodog.command.entity.GameCommand;
import com.prodog.message.sender.MessageSender;
import lombok.RequiredArgsConstructor;
import love.forte.simboot.annotation.Listener;
import love.forte.simbot.event.*;
import love.forte.simbot.message.At;
import love.forte.simbot.message.Message;
import love.forte.simbot.message.Messages;
import love.forte.simbot.message.Text;
import org.apache.logging.log4j.util.Strings;
import org.springframework.stereotype.Component;

import java.util.List;


@Component
@RequiredArgsConstructor
public class SimBotMessageListener {
    private final GameCommandHandler handler;
    private final MessageSender sender;

    public String getDetailTextContent(MessageEvent event) {
        Messages messages = event.getMessageContent().getMessages();
        StringBuilder content = new StringBuilder();
        List<Text> texts = messages.get(Text.Key);
        List<At> ats = messages.get(At.Key);
        for (Message.Element<?> message : messages) {
            Text text = texts.stream().filter(t -> t.equals(message)).findAny().orElse(null);
            At at = ats.stream().filter(t -> t.equals(message)).findAny().orElse(null);
            if (text != null) content.append(text.getText());
            if (at != null) content.append(at.getOriginContent());
        }
        return content.toString();
    }

    @Listener
    public void onFriendMsgEvent(FriendMessageEvent event) {
        GameCommand command = new GameCommand() {{
            setFromQQ(String.valueOf(event.getUser().getId()));
            setType(1);
            setCommand(getDetailTextContent(event));
        }};
        CommandResult res = handler.handle(command);
        if (res != null && Strings.isNotEmpty(res.getMsg())) {
            sender.sendToSelf(event.getFriend().getId().toString(), res.getMsg().trim());
        }
    }

    @Listener
    public void onGroupMsgEvent(GroupMessageEvent event) {
        GameCommand command = new GameCommand() {{
            setFromQQ(String.valueOf(event.getAuthor().getId()));
            setType(2);
            setCommand(getDetailTextContent(event));
        }};
        CommandResult res = handler.handle(command);
        if (res != null && Strings.isNotEmpty(res.getMsg())) {
            sender.sendToGroup(event.getGroup().getId().toString(), res.getMsg().trim());
        }
    }

    @Listener
    public void onFriendAddEvent(FriendAddRequestEvent event) {
        event.acceptAsync();
    }

    @Listener
    public void onGroupAddEvent(GroupJoinRequestEvent event) {
        event.acceptAsync();
    }
}
