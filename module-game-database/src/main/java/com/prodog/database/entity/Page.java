package com.prodog.database.entity;

import lombok.Data;

import java.util.List;

@Data
public class Page<T> {
    private int pno;
    private int pages;
    private int size;
    private long total;
    private List<T> datas;

    public static <E> Page<E> page(List<E> datas, Class<E> clz, int pno, int size) {
        Page<E> page = new Page<>();
        int pages = datas.size() % size == 0 ? datas.size() / size : datas.size() / size + 1;
        pages = pages > 0 ? pages : 1;
        pno = Math.min(pno, pages);
        page.setTotal(datas.size());
        page.setPno(pno);
        page.setSize(size);
        page.setPages(pages);
        int start = (pno - 1) * size;
        int end = pno * size;
        end = Math.min(end, datas.size());
        List<E> pageDatas = datas.subList(start, end);
        page.setDatas(pageDatas);
        return page;
    }

    public static Page page(List datas, int pno, int size) {
        return page(datas, Object.class, pno, size);
    }

    public String getBottom() {
        return "页数: " + pno + "/" + pages + "    每页" + size + "条    总数: " + total + " 条";
    }
}
