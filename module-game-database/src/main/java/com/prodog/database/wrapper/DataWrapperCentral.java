package com.prodog.database.wrapper;

import lombok.Data;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Data
public class DataWrapperCentral {
    private static Map<Class, DataWrapper> wrapperMap = new HashMap<>();

    public static void pushWrapper(Class clz, DataWrapper wrapper) {
        wrapperMap.put(clz, wrapper);
    }

    public static void removeById(Object id) {
        wrapperMap.values().forEach(wrapper -> wrapper.removeById(id));
    }

    public static void removeByIds(List ids) {
        wrapperMap.values().forEach(wrapper -> wrapper.removeByIds(ids));
    }
}
