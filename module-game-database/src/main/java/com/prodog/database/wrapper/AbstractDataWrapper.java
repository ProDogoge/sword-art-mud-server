package com.prodog.database.wrapper;

import lombok.Data;

@Data
public abstract class AbstractDataWrapper<T, P> implements DataWrapper<T, P> {
    private Class<T> typeClass;
}
