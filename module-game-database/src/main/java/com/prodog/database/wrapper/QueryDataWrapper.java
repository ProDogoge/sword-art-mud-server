package com.prodog.database.wrapper;

import lombok.Data;

import java.lang.reflect.Field;
import java.lang.reflect.ParameterizedType;
import java.lang.reflect.Type;
import java.util.*;
import java.util.function.Predicate;
import java.util.stream.Collectors;

@Data
public class QueryDataWrapper<T, P> extends AbstractDataWrapper<T, P> {
    private Map<P, T> dataMap;
    private String idColumn;

    public QueryDataWrapper() {
        Type superClazz = this.getClass().getGenericSuperclass();
        ParameterizedType parameterizedType = (ParameterizedType) superClazz;
        if (parameterizedType.getActualTypeArguments().length > 0) {
            this.setTypeClass((Class) parameterizedType.getActualTypeArguments()[0]);
        }
    }

    public QueryDataWrapper(Collection<T> datas, String idColumn) {
        this.idColumn = idColumn;
        this.dataMap = new LinkedHashMap<>();
        datas.forEach(data -> dataMap.put((P) getFieldVal(data, idColumn), data));
    }

    public boolean insert(T obj) {
        try {
            P objId = (P) getFieldVal(obj, idColumn);
            insert(obj, String.valueOf(objId));
            return true;
        } catch (Exception e) {
            e.printStackTrace();
            return false;
        }
    }

    public boolean insert(T obj, String fileName) {
        try {
            P objId = (P) getFieldVal(obj, idColumn);

            if (dataMap.containsKey(objId)) {
                return false;
            } else {
                dataMap.put(objId, obj);
            }
            return true;
        } catch (Exception e) {
            e.printStackTrace();
            return false;
        }
    }

    public boolean update(T obj) {
        try {
            P objId = (P) getFieldVal(obj, idColumn);
            if (!dataMap.containsKey(objId)) {
                return false;
            } else {
                //更新到数据map
                dataMap.put(objId, obj);
                return true;
            }
        } catch (Exception e) {
            e.printStackTrace();
            return false;
        }
    }

    public boolean save(T obj) {
        try {
            P objId = (P) getFieldVal(obj, idColumn);

            if (!dataMap.containsKey(objId)) {
                return this.insert(obj);
            } else {
                return update(obj);
            }
        } catch (Exception e) {
            e.printStackTrace();
            return false;
        }
    }

    public T getById(P id) {
        return id == null ? null : dataMap.get(id);
    }

    public List<T> getByIds(Collection<P> ids) {
        List<T> datas = new ArrayList<>();
        for (P id : ids) {
            T data = dataMap.get(id);
            if (data != null) {
                datas.add(data);
            }
        }
        return datas;
    }

    public boolean removeById(P id) {
        if (getById(id) == null) {
            return false;
        }
        dataMap.remove(id);
        return true;
    }

    public boolean removeByIds(Collection<P> ids) {
        for (P id : ids) {
            removeById(id);
        }
        return true;
    }

    public List<T> list() {
        return new ArrayList<>(dataMap.values());
    }

    public List<T> list(Predicate<T> predicate) {
        return list().stream().filter(predicate).collect(Collectors.toList());
    }

    private Object getFieldVal(T data, String name) {
        try {
            Field field = getTypeClass().getDeclaredField(name);
            field.setAccessible(true);
            return field.get(data);
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }
    }

    public T getByColumn(String column, Object val) {
        for (T data : list()) {
            if (getFieldVal(data, column).equals(val)) {
                return data;
            }
        }
        return null;
    }

    public List<T> listByColumn(String column, Object val) {
        List<T> res = new ArrayList<>();
        for (T data : list()) {
            if (getFieldVal(data, column).equals(val)) {
                res.add(data);
            }
        }
        return res;
    }

    public long countByColumn(String column, Object val) {
        long count = 0;
        for (T data : list()) {
            if (getFieldVal(data, column).equals(val)) {
                count++;
            }
        }
        return count;
    }

    public long max(String column) {
        long max = 0;
        for (T data : list()) {
            long curr = (long) getFieldVal(data, column);
            if (curr > max) {
                max = curr;
            }
        }
        return max;
    }

    public T getByColumns(Object... items) {
        for (T data : list()) {
            boolean eq = true;
            for (int i = 0; i < items.length; i += 2) {
                if (!getFieldVal(data, (String) items[i]).equals(items[i + 1])) {
                    eq = false;
                    break;
                }
            }
            if (eq) {
                return data;
            }
        }
        return null;
    }

    public List<T> listByColumns(Object... items) {
        List<T> res = new ArrayList<>();
        for (T data : list()) {
            boolean eq = true;
            for (int i = 0; i < items.length; i += 2) {
                if (!getFieldVal(data, (String) items[i]).equals(items[i + 1])) {
                    eq = false;
                    break;
                }
            }
            if (eq) {
                res.add(data);
            }
        }
        return res;
    }

    @Override
    public long save(List<T> datas) {
        int res = 0;
        for (T data : datas) {
            if (save(data)) res++;
        }
        return res;
    }

    @Override
    public T getByColumnsOr(Object... items) {
        for (T data : list()) {
            boolean eq = false;
            for (int i = 0; i < items.length; i += 2) {
                if (getFieldVal(data, (String) items[i]).equals(items[i + 1])) {
                    eq = true;
                    break;
                }
            }
            if (eq) {
                return data;
            }
        }
        return null;
    }

    @Override
    public List listByColumnsOr(Object... items) {
        List<T> res = new ArrayList<>();
        for (T data : list()) {
            boolean eq = false;
            for (int i = 0; i < items.length; i += 2) {
                if (getFieldVal(data, (String) items[i]).equals(items[i + 1])) {
                    eq = true;
                    break;
                }
            }
            if (eq) {
                res.add(data);
            }
        }
        return res;
    }
}
