package com.prodog.database.wrapper;

import java.util.Collection;
import java.util.List;
import java.util.function.Predicate;

/***
 * 本来集成过MongoDB 不好用删了
 * @param <T> 对象类型
 * @param <P> 主键类型
 */
public interface DataWrapper<T, P> {
    boolean insert(T obj);

    boolean insert(T obj, String fileName);

    boolean update(T obj);

    boolean save(T obj);

    T getById(P id);

    List<T> getByIds(Collection<P> ids);

    boolean removeById(P id);

    boolean removeByIds(Collection<P> ids);

    List<T> list();

    List<T> list(Predicate<T> predicate);

    T getByColumn(String column, Object val);

    List<T> listByColumn(String column, Object val);

    long countByColumn(String column, Object val);

    long max(String column);

    T getByColumns(Object... items);

    List<T> listByColumns(Object... items);

    long save(List<T> datas);

    T getByColumnsOr(Object... items);

    List listByColumnsOr(Object... items);
}
