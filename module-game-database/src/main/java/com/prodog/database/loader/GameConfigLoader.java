package com.prodog.database.loader;

import cn.hutool.core.io.FileUtil;
import com.alibaba.fastjson2.JSONObject;
import com.prodog.database.annotation.Config;
import com.prodog.utils.bean.SpringBeanUtils;
import com.prodog.utils.loader.GameLoader;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.CommandLineRunner;
import org.springframework.core.annotation.Order;
import org.springframework.stereotype.Component;

import java.io.File;
import java.util.List;

@Component
@Slf4j(topic = "游戏配置加载器")
@Order(2)
public class GameConfigLoader implements CommandLineRunner, GameLoader {
    @Value("${game.rootPath}")
    private String gameRootPath;

    @Override
    public void run(String... args) {
        this.load();
    }

    @Override
    public void load() {
        this.scanConfigs();
    }

    private void scanConfigs() {
        log.info("正在加载游戏配置.........");
        //获取所有wrapper
        List<Object> configObjs = SpringBeanUtils.getBeansByAnnotation(Config.class);
        //设置wrapper的属性值
        for (Object config : configObjs) {
            loadConfig(config);
        }
        log.info("游戏配置加载完毕！");
    }

    private void loadConfig(Object config) {
        Config configAno = config.getClass().getAnnotation(Config.class);
        String path = configAno.path();
        String module = configAno.module();
        File configFile = new File(gameRootPath, path);

        String jsonStr = FileUtil.readUtf8String(configFile);
        Class clz = config.getClass();
        Object obj = JSONObject.parseObject(jsonStr, clz);
        BeanUtils.copyProperties(obj, config);
        log.info("  [" + module + "]加载完成！");
    }

}
