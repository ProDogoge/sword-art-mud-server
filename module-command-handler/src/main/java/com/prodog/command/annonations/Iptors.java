package com.prodog.command.annonations;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

@Target({ElementType.TYPE, ElementType.METHOD})
@Retention(RetentionPolicy.RUNTIME)
public @interface Iptors {
    /*Class<? extends Interceptor>[] iptorClzs();

    String[] msgs() default {};

    String[] params() default {};*/

    Iptor[] vals() default {};
}
