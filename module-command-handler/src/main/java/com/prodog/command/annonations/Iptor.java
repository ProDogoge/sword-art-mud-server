package com.prodog.command.annonations;

import com.prodog.command.interceptor.Interceptor;

import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;

@Retention(RetentionPolicy.RUNTIME)
public @interface Iptor {
    Class<? extends Interceptor> value();

    String msg() default "";

    String params() default "";
}
