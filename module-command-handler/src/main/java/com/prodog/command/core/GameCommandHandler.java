package com.prodog.command.core;

import cn.hutool.core.collection.CollUtil;
import cn.hutool.core.util.ReflectUtil;
import com.prodog.command.entity.*;
import com.prodog.command.invoke.builder.InvokeArgsBuilder;
import com.prodog.command.invoke.builder.InvokeArgsBuilderParam;
import com.prodog.utils.bean.ProxyUtil;
import com.prodog.utils.string.RegexUtil;
import lombok.RequiredArgsConstructor;
import org.springframework.aop.support.AopUtils;
import org.springframework.stereotype.Component;

import java.lang.annotation.Annotation;
import java.lang.reflect.Method;
import java.lang.reflect.Parameter;
import java.util.*;
import java.util.regex.Matcher;
import java.util.stream.Collectors;

@Component
@RequiredArgsConstructor
public class GameCommandHandler {
    private final GameCommandCentral central;

    /***
     * 处理命令
     * @param command
     * @return
     */
    public CommandResult handle(GameCommand command) {
        RegexInvokeParam invokeParam = central.getInvokeParam(command.getCommand());
        if (invokeParam != null) {
            Map<String, Object> methodArgs = buildMethodArgsMap(command, invokeParam);
            Object[] args = methodArgs.values().toArray(new Object[0]);
            try {
                return (CommandResult) invokeParam.getMethod().invoke(invokeParam.getBean(), args);
            } catch (Exception e) {
                e.printStackTrace();
                return CommandResult.failure("服务器发生错误。");
            }
        }
        return null;
    }

    /***
     * 获取命令方法的参数(方法调用参数和拦截器参数)
     * @param command
     * @param invokeParam
     * @return
     */
    private Map<String, Object> buildMethodArgsMap(GameCommand command, RegexInvokeParam invokeParam) {
        Parameter[] parameters = invokeParam.getMethod().getParameters();
        Class[] parameterTypes = invokeParam.getMethod().getParameterTypes();
        Annotation[][] annoss = invokeParam.getMethod().getParameterAnnotations();
        List<String> props = invokeParam.getProps();

        //兼容AOP
        if (AopUtils.isAopProxy(invokeParam.getBean())) {
            try {
                Object superBean = ProxyUtil.getTarget(invokeParam.getBean());
                Method superMethod = ReflectUtil.getMethod(superBean.getClass(), invokeParam.getMethod().getName(), parameterTypes);
                parameters = superMethod.getParameters();
                annoss = superMethod.getParameterAnnotations();
            } catch (Exception e) {
                e.printStackTrace();
            }
        }

        List<CommandParam> gParams = getCommandParams(invokeParam.getRegex(), command.getCommand(), props);
        List<CommandMethodParameter> methodParameters = new ArrayList<>();
        for (int i = 0; i < parameterTypes.length; i++) {
            methodParameters.add(new CommandMethodParameter(parameters[i].getName(), parameterTypes[i], Arrays.asList(annoss[i])));
        }
        Map<String, CommandParam> commandParamMap = CollUtil.toMap(gParams, new LinkedHashMap<>(), CommandParam::getProp);
        Map<String, CommandMethodParameter> methodParameterMap = CollUtil.toMap(methodParameters, new LinkedHashMap<>(), CommandMethodParameter::getName);
        Map<String, Object> argsMap = new InvokeArgsBuilder(new InvokeArgsBuilderParam(commandParamMap, methodParameterMap, command)).build();
        return argsMap;
    }

    /***
     * 获取正则和@Command注解中props对应的键值
     * @param regex
     * @param command
     * @param props
     * @return
     */
    private List<CommandParam> getCommandParams(String regex, String command, List<String> props) {
        Matcher matcher = RegexUtil.getMatcher(regex, command);
        List<CommandParam> params = new ArrayList<>();
        for (int i = 0; i < props.size(); i++) {
            String prop = props.get(i);
            CommandParam param = new CommandParam();
            param.setProp(prop);
            param.setValue(matcher.groupCount() > i ? matcher.group(i + 1) : null);
            params.add(param);
        }
        return params.stream().filter(param -> !param.getProp().equalsIgnoreCase("null")).collect(Collectors.toList());
    }
}
