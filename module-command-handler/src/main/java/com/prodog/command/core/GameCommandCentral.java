package com.prodog.command.core;

import com.prodog.command.annonations.Command;
import com.prodog.command.annonations.CommandBean;
import com.prodog.command.annonations.Weight;
import com.prodog.command.entity.RegexInvokeParam;
import com.prodog.utils.bean.SpringBeanUtils;
import com.prodog.utils.loader.GameLoader;
import lombok.Getter;
import lombok.extern.slf4j.Slf4j;
import org.springframework.boot.CommandLineRunner;
import org.springframework.core.annotation.AnnotationUtils;
import org.springframework.core.annotation.Order;
import org.springframework.stereotype.Component;

import java.lang.reflect.Method;
import java.util.*;
import java.util.stream.Collectors;

@Component
@Slf4j(topic = "游戏指令集加载器")
@Order(9999)
public class GameCommandCentral implements CommandLineRunner, GameLoader {
    @Getter
    private final Map<String, RegexInvokeParam> paramMap = new LinkedHashMap<>();

    @Override
    public void run(String... args) {
        this.load();
    }

    @Override
    public void load() {
        this.init();
    }

    public void init() {
        log.info("正在加载游戏指令.........");
        //获取游戏指令类 初始化map
        initMap();
        log.info("游戏指令加载完毕！");
    }

    private void initMap() {
        List<Object> handlerList = SpringBeanUtils.getBeansByAnnotation(CommandBean.class);
        handlerList.forEach(handler -> {
            CommandBean commandBean = AnnotationUtils.findAnnotation(handler.getClass(), CommandBean.class);
            //获取带注解的方法
            Method[] methods = handler.getClass().getMethods();
            List<Method> methodList = Arrays.stream(methods).filter(method -> AnnotationUtils.findAnnotation(method, Command.class) != null).collect(Collectors.toList());
            methodList.forEach(method -> {
                Command command = AnnotationUtils.findAnnotation(method, Command.class);
                for (int i = 0; i < command.value().length; i++) {
                    RegexInvokeParam invokeParam = new RegexInvokeParam();
                    String commandStr = command.value()[i];
                    invokeParam.setRegex(commandStr);
                    invokeParam.setBean(handler);
                    invokeParam.setMethod(method);
                    if (command.props().length >= i + 1) {
                        invokeParam.setProps(Arrays.asList(command.props()[i].split("\\,")));
                    } else {
                        invokeParam.setProps(new ArrayList<>());
                    }
                    paramMap.put(commandStr, invokeParam);
                }
            });
            log.info("  指令集[" + commandBean.setName() + "]加载完成！");
        });
    }

    public RegexInvokeParam getInvokeParam(String commandStr) {
        List<Map.Entry<String, RegexInvokeParam>> entries = new ArrayList<>(paramMap.entrySet());
        entries = entries.stream().sorted(Comparator.comparing(e -> {
            Weight weight = AnnotationUtils.findAnnotation(e.getValue().getMethod(), Weight.class);
            return weight == null ? 0 : -weight.value();
        })).collect(Collectors.toList());
        for (Map.Entry<String, RegexInvokeParam> entry : entries) {
            String regex = entry.getKey();
            if (commandStr.matches(regex)) {
                return entry.getValue();
            }
        }
        return null;
    }
}
