package com.prodog.command.aspect;

import com.prodog.command.entity.CommandResult;
import com.prodog.command.handler.InterceptorAnnontationHandler;
import com.prodog.command.interceptor.InterceptorRes;
import org.aspectj.lang.ProceedingJoinPoint;
import org.aspectj.lang.annotation.Around;
import org.aspectj.lang.annotation.Aspect;
import org.springframework.stereotype.Component;

@Component
@Aspect
public class InterceptorAspect {

    @Around("execution(* com.prodog..handler.*.*(..)) || execution(* com.prodog..service..*.*(..))")
    public Object invokeCommand(ProceedingJoinPoint point) throws Throwable {
        InterceptorRes interceptorRes = InterceptorAnnontationHandler.handleInterceptors(point);
        if (!interceptorRes.isPass()) {
            return CommandResult.failure(interceptorRes.getMsg());
        }
        return point.proceed();
    }
}
