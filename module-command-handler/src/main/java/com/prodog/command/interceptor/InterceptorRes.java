package com.prodog.command.interceptor;

import lombok.AllArgsConstructor;
import lombok.Data;

@Data
@AllArgsConstructor
public class InterceptorRes {
    private boolean pass;
    private String msg;

    public static InterceptorRes pass() {
        return new InterceptorRes(true, null);
    }

    public static InterceptorRes intercept(String msg) {
        return new InterceptorRes(false, msg);
    }
}
