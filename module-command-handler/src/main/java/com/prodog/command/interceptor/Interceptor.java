package com.prodog.command.interceptor;

import com.prodog.command.entity.GameCommand;
import com.prodog.utils.exception.CommonException;

import java.util.Map;

public interface Interceptor {
    InterceptorRes intercept(GameCommand command, Map<String, Object> params, String formatMsg) throws CommonException;
}
