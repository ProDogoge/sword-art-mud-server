package com.prodog.command.entity;

import com.prodog.usermodule.role.entity.RoleInfo;
import com.prodog.usermodule.user.entity.UserInfo;
import lombok.Data;


@Data
public class RoleGameCommand extends GameCommand {
    private String userId;
    private String roleId;
    private UserInfo userInfo;
    private RoleInfo roleInfo;
}
