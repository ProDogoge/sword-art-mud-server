package com.prodog.command.entity;

import lombok.Data;

@Data
public class CommandParam {
    String prop;
    String value;
}
