package com.prodog.command.entity;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

/***
 * 游戏指令结果
 */
@Data
@NoArgsConstructor
@AllArgsConstructor
public class CommandResult {
    private String msg;
    private Object result;  //返回的字符串结果
    private Integer code;   //状态值

    public static CommandResult res(Object result) {
        return new CommandResult() {{
            setResult(result);
        }};
    }

    /***
     * 成功
     * @param result
     * @return
     */
    public static CommandResult success(Object result) {
        return new CommandResult(null, result, 200);
    }

    /***
     * 失败
     * @param result
     * @return
     */
    public static CommandResult failure(Object result) {
        return new CommandResult(null, result, 500);
    }

    public static CommandResult success(String msg) {
        return new CommandResult(msg, null, 200);
    }

    public static CommandResult failure(String msg) {
        return new CommandResult(msg, null, 500);
    }

    public static CommandResult success(String msg, Object result) {
        return new CommandResult(msg, result, 200);
    }

    public static CommandResult failure(String msg, Object result) {
        return new CommandResult(msg, result, 500);
    }

    public static CommandResult next() {
        return new CommandResult(null, null, 600);
    }
}
