package com.prodog.command.entity;

import lombok.Data;

import java.lang.reflect.Method;
import java.util.List;

@Data
public class RegexInvokeParam {
    private String regex;
    private Method method;
    private Object bean;
    private List<String> props;
}
