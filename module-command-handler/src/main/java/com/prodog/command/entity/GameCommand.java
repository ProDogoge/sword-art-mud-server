package com.prodog.command.entity;

import lombok.Data;

/***
 * 游戏指令
 */
@Data
public class GameCommand {
    private String fromQQ;  //发送者QQ
    private String toQQ;    //发送到QQ
    private String qqGroup; //发送到qq群
    private Integer type;   //发送类型 1:发送到QQ 2:发送到群
    private String command; //发送的指令内容
}
