package com.prodog.command.entity;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.lang.reflect.Method;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class InterceptorsHandleParam {
    private Object obj;
    private Method method;
    private Object[] args;
}
