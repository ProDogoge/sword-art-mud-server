package com.prodog.command.entity;

import lombok.AllArgsConstructor;
import lombok.Data;

import java.lang.annotation.Annotation;
import java.util.List;

@Data
@AllArgsConstructor
public class CommandMethodParameter {
    private String name;
    private Class type;
    private List<Annotation> annons;
}
