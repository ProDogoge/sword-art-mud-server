package com.prodog.command.invoke.getter.entity;

import com.prodog.command.entity.CommandMethodParameter;
import com.prodog.command.entity.CommandParam;
import com.prodog.command.entity.GameCommand;
import lombok.AllArgsConstructor;
import lombok.Data;

import java.util.Map;

@Data
@AllArgsConstructor
public class MethodArgGetterParam {
    private Map<String,CommandParam> commandParams;
    private CommandMethodParameter methodParameter;
    private GameCommand gameCommand;
}
