package com.prodog.command.invoke.builder;

import com.prodog.command.entity.CommandMethodParameter;
import com.prodog.command.entity.CommandParam;
import com.prodog.command.entity.GameCommand;
import lombok.AllArgsConstructor;
import lombok.Data;

import java.util.Map;

@Data
@AllArgsConstructor
public class InvokeArgsBuilderParam {
    private Map<String, CommandParam> commandParamMap;
    private Map<String, CommandMethodParameter> methodParameterMap;
    private GameCommand gameCommand;
}
