package com.prodog.command.invoke.getter.impl;

import com.prodog.command.entity.GameCommand;
import com.prodog.command.entity.RoleGameCommand;
import com.prodog.command.invoke.getter.AbstractMethodArgGetter;
import com.prodog.command.invoke.getter.entity.GetterRes;
import com.prodog.command.invoke.getter.entity.MethodArgGetterParam;
import com.prodog.usermodule.role.entity.RoleInfo;
import com.prodog.usermodule.role.wrapper.RoleInfoWrapper;
import com.prodog.usermodule.user.entity.UserInfo;
import com.prodog.usermodule.user.wrapper.UserInfoWrapper;
import com.prodog.utils.bean.BeanUtil;
import com.prodog.utils.interfaces.AutowiredBean;
import org.springframework.beans.factory.annotation.Autowired;

public class CommandMethodArgGetter extends AbstractMethodArgGetter implements AutowiredBean {
    @Autowired
    private UserInfoWrapper userInfoWrapper;
    @Autowired
    private RoleInfoWrapper roleInfoWrapper;

    public CommandMethodArgGetter(MethodArgGetterParam getterParam) {
        super(getterParam);
        autowired();
    }

    @Override
    public GetterRes get() {
        if (getGetterParam().getMethodParameter().getType().equals(GameCommand.class)) {
            return new GetterRes(getGetterParam().getMethodParameter().getName(), getGetterParam().getGameCommand());
        } else if (getGetterParam().getMethodParameter().getType().equals(RoleGameCommand.class)) {
            RoleGameCommand roleGameCommand = BeanUtil.beanToBean(getGetterParam().getGameCommand(), RoleGameCommand.class);
            roleGameCommand.setUserId(getGetterParam().getGameCommand().getFromQQ());
            UserInfo userInfo = userInfoWrapper.getById(getGetterParam().getGameCommand().getFromQQ());
            if (userInfo != null) {
                RoleInfo roleInfo = roleInfoWrapper.getById(userInfo.getRoleId());
                roleGameCommand.setRoleId(roleInfo.getId());
                roleGameCommand.setUserInfo(userInfo);
                roleGameCommand.setRoleInfo(roleInfo);
            }
            return new GetterRes(getGetterParam().getMethodParameter().getName(), roleGameCommand);
        }
        return null;
    }
}
