package com.prodog.command.invoke.builder;

import com.prodog.command.entity.GameCommand;
import com.prodog.command.entity.RoleGameCommand;
import com.prodog.command.invoke.getter.entity.GetterRes;
import com.prodog.command.invoke.getter.entity.MethodArgGetterParam;
import com.prodog.command.invoke.getter.impl.BeanMethodArgGetter;
import com.prodog.command.invoke.getter.impl.CommandMethodArgGetter;
import com.prodog.command.invoke.getter.impl.SimpleTypeMethodArgGetter;
import com.prodog.utils.dataType.ConvertUtil;
import lombok.AllArgsConstructor;

import java.util.LinkedHashMap;
import java.util.Map;

@AllArgsConstructor
public class InvokeArgsBuilder {
    private InvokeArgsBuilderParam param;

    public Map<String, Object> build() {
        Map<String, Object> argsMap = new LinkedHashMap<>();
        param.getMethodParameterMap().values().forEach(methodParameter -> {
            GetterRes res;
            if (methodParameter.getType().equals(GameCommand.class) || methodParameter.getType().equals(RoleGameCommand.class)) {
                res = new CommandMethodArgGetter(new MethodArgGetterParam(param.getCommandParamMap(), methodParameter, param.getGameCommand())).get();
            } else if (ConvertUtil.isSimpleType(methodParameter.getType())) {
                res = new SimpleTypeMethodArgGetter(new MethodArgGetterParam(param.getCommandParamMap(), methodParameter, param.getGameCommand())).get();
            } else {
                res = new BeanMethodArgGetter(new MethodArgGetterParam(param.getCommandParamMap(), methodParameter, param.getGameCommand())).get();
            }
            if (res != null) {
                argsMap.put(res.getName(), res.getValue());
            } else {
                argsMap.put(methodParameter.getName(), null);
            }
        });
        return argsMap;
    }
}
