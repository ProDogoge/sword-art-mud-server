package com.prodog.command.invoke.getter;

import com.prodog.command.invoke.getter.entity.GetterRes;

public interface MethodArgGetter {
    GetterRes get();
}
