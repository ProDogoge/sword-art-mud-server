package com.prodog.command.invoke.getter.impl;

import cn.hutool.core.util.StrUtil;
import com.prodog.command.annonations.Default;
import com.prodog.command.entity.CommandMethodParameter;
import com.prodog.command.entity.CommandParam;
import com.prodog.command.invoke.getter.AbstractMethodArgGetter;
import com.prodog.command.invoke.getter.entity.GetterRes;
import com.prodog.command.invoke.getter.entity.MethodArgGetterParam;
import com.prodog.utils.dataType.ConvertUtil;

public class SimpleTypeMethodArgGetter extends AbstractMethodArgGetter {
    public SimpleTypeMethodArgGetter(MethodArgGetterParam getterParam) {
        super(getterParam);
    }

    @Override
    public GetterRes get() {
        CommandMethodParameter parameter = getGetterParam().getMethodParameter();
        CommandParam param = getGetterParam().getCommandParams().get(parameter.getName());
        if (param != null) {
            if (StrUtil.isNotBlank(param.getValue())) {
                return new GetterRes(parameter.getName(), ConvertUtil.convert(param.getValue(), parameter.getType()));
            } else {
                Default def = (Default) parameter.getAnnons().stream().filter(anno -> anno instanceof Default).findAny().orElse(null);
                if (def != null) {
                    return new GetterRes(parameter.getName(), ConvertUtil.convert(def.value(), parameter.getType()));
                }
            }
        } else {
            Default def = (Default) parameter.getAnnons().stream().filter(anno -> anno instanceof Default).findAny().orElse(null);
            if (def != null) {
                return new GetterRes(parameter.getName(), ConvertUtil.convert(def.value(), parameter.getType()));
            }
        }
        return new GetterRes(parameter.getName(), null);
    }
}
