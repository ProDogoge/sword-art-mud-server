package com.prodog.command.invoke.getter.entity;

import lombok.AllArgsConstructor;
import lombok.Data;

@Data
@AllArgsConstructor
public class GetterRes {
    private String name;
    private Object value;
}
