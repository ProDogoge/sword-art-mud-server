package com.prodog.command.invoke.getter;

import com.prodog.command.invoke.getter.entity.MethodArgGetterParam;
import lombok.AllArgsConstructor;
import lombok.Data;

@Data
@AllArgsConstructor
public abstract class AbstractMethodArgGetter implements MethodArgGetter{
    private MethodArgGetterParam getterParam;
}
