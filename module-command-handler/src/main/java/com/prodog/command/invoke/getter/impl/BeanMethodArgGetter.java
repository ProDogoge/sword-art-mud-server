package com.prodog.command.invoke.getter.impl;

import cn.hutool.core.exceptions.UtilException;
import cn.hutool.core.util.ReflectUtil;
import cn.hutool.core.util.StrUtil;
import com.prodog.command.annonations.Default;
import com.prodog.command.entity.CommandMethodParameter;
import com.prodog.command.entity.CommandParam;
import com.prodog.command.invoke.getter.AbstractMethodArgGetter;
import com.prodog.command.invoke.getter.entity.GetterRes;
import com.prodog.command.invoke.getter.entity.MethodArgGetterParam;
import com.prodog.utils.dataType.ConvertUtil;

import java.lang.reflect.Field;

public class BeanMethodArgGetter extends AbstractMethodArgGetter {
    public BeanMethodArgGetter(MethodArgGetterParam getterParam) {
        super(getterParam);
    }

    @Override
    public GetterRes get() {
        CommandMethodParameter parameter = getGetterParam().getMethodParameter();

        Object obj;
        try {
            obj = parameter.getType().newInstance();
        } catch (Exception e) {
            e.printStackTrace();
            return new GetterRes(parameter.getName(), null);
        }
        Field[] fields = parameter.getType().getDeclaredFields();
        for (Field field : fields) {
            CommandParam param = getGetterParam().getCommandParams().get(field.getName());
            if (param != null) {
                if (StrUtil.isNotBlank(param.getValue())) {
                    try {
                        ReflectUtil.setFieldValue(obj, field, ConvertUtil.convert(param.getValue(), field.getType()));
                    } catch (UtilException e) {
                        e.printStackTrace();
                    }
                } else {
                    Default def = field.getAnnotation(Default.class);
                    if (def != null) {
                        try {
                            ReflectUtil.setFieldValue(obj, field, ConvertUtil.convert(def.value(), field.getType()));
                        } catch (UtilException e) {
                            e.printStackTrace();
                        }
                    }
                }
            }
        }
        return new GetterRes(parameter.getName(), obj);
    }
}
